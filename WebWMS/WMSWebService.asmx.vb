﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports WMS_BL

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class WMSWebService
    Inherits System.Web.Services.WebService

    '<WebMethod()>
    'Public Function HelloWorld() As String
    '    Return "Hello World"
    'End Function

    <WebMethod()>
    Public Function CreateLogClickMenu(LoginHistoryID As Long, LogMsg As String) As String
        Dim ret As String = "false"
        LogFileData.LogTrans(LoginHistoryID, LogMsg)
        Return "true"
    End Function


    <WebMethod()>
    Public Function Logout(LoginHistoryID As Long) As String
        Dim ret As String = "false"
        Dim lnq As New TbLoginHistoryLinqDB
        lnq.GetDataByPK(LoginHistoryID, Nothing)

        If lnq.LOGIN_HISTORY_ID > 0 Then
            lnq.LOGOUT_TIME = DateTime.Now

            Dim trans As New TransactionDB
            Dim re As ExecuteDataInfo = lnq.UpdateData(lnq.USERNAME, trans.Trans)
            If re.IsSuccess = True Then
                trans.CommitTransaction()

                LogFileData.LogTrans(LoginHistoryID, "ออกจากระบบ")
                ret = "true"
            Else
                trans.RollbackTransaction()
                ret = "false|" & re.ErrorMessage
            End If
        End If

        Return ret
    End Function

End Class