﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="TB_Recevice.aspx.vb" Inherits="WebWMS.TB_Recevice" %>

<%@ Register Src="~/UserControl/ucSwitchButton.ascx" TagPrefix="uc1" TagName="ucSwitchButton" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="page-content container-fluid">
        <div class="page-header">
            <h4 class="page-title">เอกสารการรับ</h4>
        </div>
        <div class="page-content container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <asp:Panel ID="pnlList" runat="server" CssClass="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-2">
                                    <asp:LinkButton ID="btnAddDocumentRecieve" runat="server" CssClass="btn btn-warning">
                                    <i class="icon md-plus-circle-o"></i>
                                    <span>เอกสารการรับ</span>
                                    </asp:LinkButton>
                                </div>
                                <div class="col-sm-3">
                                    <asp:LinkButton ID="btnOpenSearch" runat="server" CssClass="btn btn-primary">
                                     <i class="icon fa-search"></i> <span>ค้นหา</span>
                                    </asp:LinkButton>
                                </div>
                            </div>
                            <asp:Panel ID="pnlSearch" runat="server">
                                <br />
                                <div class="row">
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtSearchDocID" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="100" placeholder="เลขที่เอกสาร"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlSearchReceiveType" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="ประเภทเอกสาร" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlSearchPO" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="ใบสั่งซื้อ" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtSearchRDate" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="100" placeholder="วันที่รับ"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server"
                                            Format="dd/MM/yyyy" TargetControlID="txtSearchRDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtSearchRTime" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="100" placeholder="เวลาที่รับ" TextMode="Time"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlSearchStaff" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="ผู้ตรวจ" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtSearchApprover" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="100" placeholder="ผู้อนุมัติ"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-1">
                                        <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn btn-warning">
                                        <i class="icon fa-search"></i>
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </asp:Panel>

                            <div class="clearfix visible-md-block"></div>
                            <br />
                            <table class="tablesaw table-striped table-bordered table-hover">
                                <thead>
                                    <tr class="bg-blue-grey-100">
                                        <th class="text-center">เลขที่</th>
                                        <th class="text-center">ประเภท</th>
                                        <th class="text-center">Reference Doc.</th>
                                        <th class="text-center">Receive Date</th>
                                        <th class="text-center">Receive Time</th>
                                        <th class="text-center">ผู้ตรวจ</th>
                                        <th class="text-center">ผู้อนุมัติ</th>
                                        <th class="text-center">แก้ไข</th>
                                        <th class="text-center">ลบ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptList" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td class="text-center">
                                                    <asp:Label ID="lblDocumentID" runat="server"></asp:Label>
                                                    <asp:Label ID="lblSequenceID" runat="server" Visible="false"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="lblDocumentType" runat="server"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="lblPOReference" runat="server"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="lblReceiveDate" runat="server"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="lblReceiveTime" runat="server"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="lblOfficer" runat="server"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="lblApprover" runat="server"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:LinkButton ID="btnEdit" runat="server" CommandName="EDIT">
                                                    <i class="icon md-edit" style="font-size: 20px;"></i>
                                                    </asp:LinkButton>
                                                </td>
                                                <td class="text-center">
                                                    <asp:LinkButton ID="btnDelete" runat="server" CommandName="DELETE" OnClientClick="return confirm('Do you want delete data?');">
                                                    <i class="icon md-delete" style="font-size: 20px;"></i>
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlEdit" runat="server" CssClass="panel" Visible="false">
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="form-group col-sm-2">
                                    <label>เลขที่ใบรับสินค้า  :<span style="color: red;">*</span></label>
                                </div>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtDocumentID" runat="server" CssClass="form-control" MaxLength="100" placeholder="เลขที่ใบรับสินค้า"></asp:TextBox>
                                    <asp:Label ID="lblSequenceID" runat="server" Visible="false" Text="0"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12" style ="display:none">
                                <div class="form-group col-sm-2">
                                    <label>วันที่รับสินค้า :<span style="color: red;">*</span></label>
                                </div>
                                <div class="col-sm-4 calender-top">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <asp:TextBox CssClass="form-control m-b" ID="txtReceieveDate" runat="server" placeholder=""></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                            Format="dd/MM/yyyy" TargetControlID="txtReceieveDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                    </div>
                                </div>
                                <div class="form-group col-sm-2">
                                    <label>เวลารับสินค้า :<span style="color: red;">*</span></label>
                                </div>

                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtReceieveTime" runat="server" CssClass="form-control" MaxLength="100" placeholder="รหัสพาเลท" TextMode="Time"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group col-sm-2">
                                    <label>ประเภทการรับ :<span style="color: red;">*</span></label>
                                </div>
                                <div class="col-sm-10">
                                    <asp:DropDownList ID="ddlReceiveType" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="ประเภทการรับ" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group col-sm-2">
                                    <label>
                                        <asp:Label ID="lblReferenceType" runat="server"></asp:Label>
                                        No.Refer :<span style="color: red;">*</span></label>
                                </div>

                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddlReferenceNo" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="เลข Reference no" AutoPostBack="true"></asp:DropDownList>
                                </div>

                                <div class="form-group col-sm-2">
                                    <label>Reference Date :<span style="color: red;">*</span></label>
                                </div>
                                <div class="col-sm-4 calender-top">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <asp:TextBox CssClass="form-control m-b" ID="txtDateReference" runat="server" placeholder="วันที่ใบสั่งซื้อสินค้า"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                            Format="dd/MM/yyyy" TargetControlID="txtDateReference" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <div class="form-group col-sm-2">
                                    <label>รหัสใบส่งสินค้า :</label>
                                </div>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtDoNo" runat="server" CssClass="form-control" placeholder="เลขใบส่งสินค้า" AutoPostBack="true"></asp:TextBox>
                                </div>
                                <div class="form-group col-sm-2">
                                    <label>DO Date :</label>
                                </div>
                                <div class="col-sm-4 calender-top">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <asp:TextBox CssClass="form-control m-b" ID="txtDateDONo" runat="server" placeholder="วันที่ใบส่งสินค้า" Enabled="false"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server"
                                            Format="dd/MM/yyyy" TargetControlID="txtDateDONo" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                    </div>
                                </div>
                            </div>

                            <h4 style="margin: 2px;">รายการสินค้า</h4>
                            <br />
                            <table class="tablesaw table-striped table-bordered table-hover">
                                <thead>
                                    <tr class="bg-blue-grey-100">
                                        <th class="text-center">รับ</th>
                                        <th class="text-center">SKU ID</th>
                                        <th class="text-center">ชื่อสินค้า</th>
                                        <th class="text-center">Lot/Batch</th>
                                        <th class="text-center">รับทั้งหมด</th>
                                        <th id="ReceiveAccess" class="text-center" visible="true" runat="server">รับแล้ว</th>
                                        <th class="text-center" width="80px">รับ</th>
                                        <th class="text-center">คงเหลือ</th>
                                        <th class="text-center">หน่วย</th>
                                        <th class="text-center">สถานนะสินค้า</th>
                                        <th class="text-center">วันหมดอายุ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptsku" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td class="text-center">
                                                    <uc1:ucSwitchButton runat="server" ID="chkReceive" />
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="lblSKUID" runat="server"></asp:Label>
                                                    <asp:Label ID="lblSequenceID" runat="server" Visible="false"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="lblProductName" runat="server"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="lblLotBatch" runat="server"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="lblAmountReceive" runat="server"></asp:Label>
                                                    <asp:Label ID="lblWeight" runat="server" Visible="false"></asp:Label>
                                                    <asp:Label ID="lblPrice" runat="server" Visible="false"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="lblAmountReceiveAccess" runat="server"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:TextBox ID="txtAmountSent" runat="server" CssClass="form-control text-center"></asp:TextBox>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="lblAmountBalance" runat="server"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="lblUnit" runat="server"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="lblProductStatus" runat="server"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="lblExpireDate" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                            <br />
                            <h4 style="margin: 2px;">ของแถม</h4>
                            <br />
                            <table class="tablesaw table-striped table-bordered table-hover">
                                <thead>
                                    <tr class="bg-blue-grey-100">
                                        <th class="text-center">SKU ID</th>
                                        <th class="text-center">ชื่อสินค้า</th>
                                        <th class="text-center">จำนวน Doc.</th>
                                        <th class="text-center">ลบ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptskufree" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td class="text-center">
                                                    <asp:UpdatePanel ID="udpList" runat="server">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlSkuIDFree" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" Style="width: 100%; border: none;" OnSelectedIndexChanged="ddlSkuIDFree_SelectedIndexChanged" title="เลือก SKU" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="ddlSkuIDFree" EventName="SelectedIndexChanged" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                    <asp:Label ID="lblSequenceID" runat="server" Visible="false"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="lblProductIFree" runat="server"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:TextBox ID="txtAmountFree" runat="server" CssClass="form-control text-center"></asp:TextBox>
                                                </td>
                                                <td class="text-center">
                                                    <asp:LinkButton ID="btnDelete" runat="server" CommandName="DELETE" OnClientClick="return confirm('Do you want delete data?');">
                                                    <i class="icon md-delete" style="font-size: 20px;"></i>
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                            <div class="row">
                                <center>
                                    <div class="col-sm-12"  style="margin-top:10px;">
                                         <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="+" CssClass="btn btn-primary" AutoPostBack="true" />
                                   </div>
                                </center>
                            </div>
                            <br />
                            <div class="col-sm-12">
                                <div class="form-group col-sm-2">
                                    <label>ผู้ตรวจ :<span style="color: red;">*</span></label>
                                </div>

                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddlStaff" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="ผู้ตรวจ" AutoPostBack="true"></asp:DropDownList>
                                </div>

                                <div class="form-group col-sm-2">
                                    <label>อนุมัติโดย :<span style="color: red;">*</span></label>
                                </div>

                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtApprover" runat="server" CssClass="form-control" MaxLength="100" placeholder="ผู้อนุมัติ"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group col-sm-2">
                                    <label>เอกสารอ้างอิง :</label>
                                </div>

                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtReferenceDoc" runat="server" CssClass="form-control" MaxLength="100" placeholder="เอกสารอ้างอิง"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group col-sm-2">
                                    <label>Note :</label>
                                </div>

                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtnote" runat="server" CssClass="form-control" MaxLength="100" placeholder="หมายเหตุ" TextMode="MultiLine"></asp:TextBox>
                                    <br />
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group col-sm-2">
                                    <label>ใช้งาน</label>
                                </div>

                                <div class="col-sm-9">
                                    <uc1:ucSwitchButton runat="server" ID="chkActive" />
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />
                            <div class="form-group form-material">
                                <div class="col-sm-12">
                                    <div class="col-sm-10 "></div>
                                    <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary">
                                    <span>Save</span>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-warning">
                                    <span>Cancel</span>
                                    </asp:LinkButton>
                                </div>
                            </div>

                        </div>
                    </asp:Panel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpFooter" runat="server">
</asp:Content>
