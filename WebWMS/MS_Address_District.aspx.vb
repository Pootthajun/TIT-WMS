﻿Imports LinqDB.TABLE
Imports WMS_BL
Public Class MS_Address_District
    Inherits System.Web.UI.Page

    Private ReadOnly Property UserSession As LoginSessionData
        Get
            Return Session(CommonBL.UserLoginSessionName)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect("frmLogin.aspx?rnd=" & DateTime.Now.Millisecond)
                Exit Sub

            Else
                BindList()
                ClearForm()
            End If
        End If
    End Sub

    Protected Sub chkActiveStatus_CheckedChanged(sender As ucSwitchButton, e As System.EventArgs)
        Dim chkActiveStatus As ucSwitchButton = sender
        Dim itm As RepeaterItem = chkActiveStatus.Parent
        Dim lblDistrictID As Label = itm.FindControl("lblDistrictID")
        Dim lblDistrictName As Label = itm.FindControl("lblDistrictName")

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.UpdateDistrictstatus(UserSession.UserName, lblDistrictID.Text, chkActiveStatus.Checked)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Update status fail');", True)
            LogFileData.LogError(UserSession, ret.ErrorMessage)
        Else
            LogFileData.LogTrans(UserSession, "Update District Status " & lblDistrictName.Text & " Status : " & chkActiveStatus.Checked.ToString)
        End If
    End Sub

    Private Sub BindList()
        Dim dt As DataTable = MasterDataBL.GetList_District(dllProvinceSearch.SelectedValue, txtSearch.Text.Trim, ddlstatus.SelectedValue)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Private Sub Bindddl()
        MasterDataBL.GetListDDL_Province(ddlProvince, "")
        MasterDataBL.GetListDDL_Province(dllProvinceSearch, "")
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblProvince As Label = e.Item.FindControl("lblProvince")
        Dim lblDistrictName As Label = e.Item.FindControl("lblDistrictName")
        Dim lblDistrictID As Label = e.Item.FindControl("lblDistrictID")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        lblProvince.Text = e.Item.DataItem("Province_Name")
        lblDistrictName.Text = e.Item.DataItem("District_Name")
        lblDistrictID.Text = e.Item.DataItem("District_ID")
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblDistrictName As Label = e.Item.FindControl("lblDistrictName")
        Dim lblDistrictID As Label = e.Item.FindControl("lblDistrictID")

        Select Case e.CommandName
            Case "EDIT"
                ClearForm()
                FillInData(lblDistrictID.Text)

                pnlList.Visible = False
                pnlEdit.Visible = True

                LogFileData.LogTrans(UserSession, "คลิกปุ่ม Edit District " & lblDistrictName.Text)
            Case "DELETE"
                Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.DeleteDistrict(lblDistrictID.Text)
                If ret.IsSuccess = True Then
                    LogFileData.LogTrans(UserSession, "Delete District " & lblDistrictName.Text)

                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Delete Data Fail');", True)
                    LogFileData.LogError(UserSession, ret.ErrorMessage)
                End If
        End Select
    End Sub

    Private Sub FillInData(DistrictID As Long)
        Dim ret As MsAddressDistrictLinqDB = MasterDataBL.GetDistrictData(DistrictID)
        If ret.DISTRICT_ID > 0 Then
            lblDistrictID.Text = ret.DISTRICT_ID
            ddlProvince.SelectedValue = ret.PROVINCE_ID
            txtDistrictName.Text = ret.DISTRICT_NAME
            chkActive.Checked = (ret.ACTIVE_STATUS = "Y")
        End If
        ret = Nothing
    End Sub

    Private Sub ClearForm()
        lblDistrictID.Text = "0"
        txtDistrictName.Text = ""
        chkActive.Checked = True
        Bindddl()
        ddlProvince.SelectedValue = 0
        dllProvinceSearch.SelectedValue = 0
    End Sub

    Private Sub btnAddDistrict_Click(sender As Object, e As EventArgs) Handles btnAddDistrict.Click
        ClearForm()
        pnlEdit.Visible = True
        pnlList.Visible = False
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Add District")
    End Sub

    Private Function ValidateData() As Boolean
        If lblDistrictID.Text = "0" Then
            'กรณีเป็นการเพิ่มใหม่ ให้ตรวจสอบข้อมูลการเข้าระบบด้วย
            If txtDistrictName.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุชื่ออำเภอ');", True)
                Return False
            End If
        End If

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.CheckDuplicateDistrict(ddlProvince.SelectedValue, txtDistrictName.Text)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');", True)
            Return False
        End If

        Return True
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save District")
        If ValidateData() = True Then

            Dim lnq As MsAddressDistrictLinqDB = MasterDataBL.SaveDistrict(UserSession.UserName, lblDistrictID.Text, ddlProvince.SelectedValue, txtDistrictName.Text, chkActive.Checked)
            If lnq.DISTRICT_ID > 0 Then
                lblDistrictID.Text = lnq.DISTRICT_ID
                LogFileData.LogTrans(UserSession, "Save District " & txtDistrictName.Text & " สำเร็จ")
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Data Complete');", True)
                pnlEdit.Visible = False
                pnlList.Visible = True
                BindList()
            Else
                LogFileData.LogError(UserSession, "Save District " & txtDistrictName.Text & " ไม่สำเร็จ Error :" & lnq.ErrorMessage)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Fail');", True)
            End If
            lnq = Nothing
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Cancel")
        ClearForm()
        pnlEdit.Visible = False
        pnlList.Visible = True
        BindList()
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click, txtSearch.TextChanged
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม ค้นหา")
        BindList()
    End Sub

    Private Sub ddlstatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlstatus.SelectedIndexChanged
        BindList()
    End Sub

    Private Sub dllProvinceSearch_SelectedIndexChanged(sender As Object, e As EventArgs) Handles dllProvinceSearch.SelectedIndexChanged
        BindList()
    End Sub

End Class