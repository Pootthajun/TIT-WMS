﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmTestForm.aspx.vb" Inherits="WebWMS.frmTestForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:TextBox ID="txtInput" runat="server"></asp:TextBox>
            <asp:Button ID="btnEncrypt" runat="server" Text="Encrypt" />
            <asp:Button ID="btnDecript" runat="server" Text="Decrypt" />

            <br />
            <asp:Label ID="lblOutput" runat="server"></asp:Label>
        </div>
    </form>
</body>
</html>
