﻿Imports LinqDB.TABLE
Imports WMS_BL

Public Class MS_Warehouse_Type
    Inherits System.Web.UI.Page

    Private ReadOnly Property UserSession As LoginSessionData
        Get
            Return Session(CommonBL.UserLoginSessionName)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect("frmLogin.aspx?rnd=" & DateTime.Now.Millisecond)
                Exit Sub
            Else
                BindList()
                ClearForm()
            End If
        End If
    End Sub

    Protected Sub chkActiveStatus_CheckedChanged(sender As ucSwitchButton, e As System.EventArgs)
        Dim chkActiveStatus As ucSwitchButton = sender
        Dim itm As RepeaterItem = chkActiveStatus.Parent
        Dim lblWarehouseTypeID As Label = itm.FindControl("lblWarehouseTypeID")
        Dim lblWarehouseTypeName As Label = itm.FindControl("lblWarehouseTypeName")

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.UpdateWarehouseTypestatus(UserSession.UserName, lblWarehouseTypeID.Text, chkActiveStatus.Checked)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Update status fail');", True)
            LogFileData.LogError(UserSession, ret.ErrorMessage)
        Else
            LogFileData.LogTrans(UserSession, "Update Warehouse Type Status " & lblWarehouseTypeName.Text & " Status : " & chkActiveStatus.Checked.ToString)
        End If
    End Sub

    Private Sub BindList()
        Dim dt As DataTable = MasterDataBL.GetList_WarehouseType(txtSearch.Text.Trim, ddlstatus.SelectedValue)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblWarehouseTypeName As Label = e.Item.FindControl("lblWarehouseTypeName")
        Dim lblWarehouseTypeID As Label = e.Item.FindControl("lblWarehouseTypeID")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        lblWarehouseTypeName.Text = e.Item.DataItem("Warehouse_Type")
        lblWarehouseTypeID.Text = e.Item.DataItem("Warehouse_Type_ID")
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblWarehouseTypeName As Label = e.Item.FindControl("lblWarehouseTypeName")
        Dim lblWarehouseTypeID As Label = e.Item.FindControl("lblWarehouseTypeID")

        Select Case e.CommandName
            Case "EDIT"
                ClearForm()
                FillInData(lblWarehouseTypeID.Text)

                pnlList.Visible = False
                pnlEdit.Visible = True

                LogFileData.LogTrans(UserSession, "Edit Warehouse Type " & lblWarehouseTypeName.Text)
            Case "DELETE"
                Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.DeleteWarehouseType(lblWarehouseTypeID.Text)
                If ret.IsSuccess = True Then
                    LogFileData.LogTrans(UserSession, "Delete Warehouse Type " & lblWarehouseTypeName.Text)

                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Delete Data Fail');", True)
                    LogFileData.LogError(UserSession, ret.ErrorMessage)
                End If
        End Select
    End Sub

    Private Sub FillInData(WarehouseTypeID As Long)
        Dim ret As MsWarehouseTypeLinqDB = MasterDataBL.GetWarehouseTypeData(WarehouseTypeID)
        If ret.WAREHOUSE_TYPE_ID > 0 Then
            lblWarehouseTypeID.Text = ret.WAREHOUSE_TYPE_ID
            txtWarehouseTypeName.Text = ret.WAREHOUSE_TYPE
            chkActive.Checked = (ret.ACTIVE_STATUS = "Y")
        End If
        ret = Nothing
    End Sub

    Private Sub ClearForm()
        lblWarehouseTypeID.Text = "0"
        txtWarehouseTypeName.Text = ""
        chkActive.Checked = True
    End Sub

    Private Sub btnAddWarehouseType_Click(sender As Object, e As EventArgs) Handles btnAddWarehouseType.Click
        ClearForm()
        pnlEdit.Visible = True
        pnlList.Visible = False
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Add WarehouseType")
    End Sub

    Private Function ValidateData() As Boolean
        If lblWarehouseTypeID.Text = "0" Then
            'กรณีเป็นการเพิ่มใหม่ ให้ตรวจสอบข้อมูลการเข้าระบบด้วย
            If txtWarehouseTypeName.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุชื่อประเภท');", True)
                Return False
            End If
        End If

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.CheckDuplicateWarehouseType(lblWarehouseTypeID.Text, txtWarehouseTypeName.Text)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');", True)
            Return False
        End If

        Return True
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save Warehouse Type")
        If ValidateData() = True Then

            Dim lnq As MsWarehouseTypeLinqDB = MasterDataBL.SaveWarehouseType(UserSession.UserName, lblWarehouseTypeID.Text, txtWarehouseTypeName.Text, chkActive.Checked)
            If lnq.WAREHOUSE_TYPE_ID > 0 Then
                lblWarehouseTypeID.Text = lnq.WAREHOUSE_TYPE_ID
                LogFileData.LogTrans(UserSession, "Save Warehouse Type " & txtWarehouseTypeName.Text & " สำเร็จ")
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Data Complete');", True)
                pnlEdit.Visible = False
                pnlList.Visible = True
                BindList()
            Else
                LogFileData.LogError(UserSession, "Save Warehouse Type " & txtWarehouseTypeName.Text & " ไม่สำเร็จ Error :" & lnq.ErrorMessage)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Fail');", True)
            End If
            lnq = Nothing
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Cancel")
        ClearForm()
        pnlEdit.Visible = False
        pnlList.Visible = True
        BindList()
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click, txtSearch.TextChanged
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม ค้นหา")
        BindList()
    End Sub

    Private Sub ddlstatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlstatus.SelectedIndexChanged
        BindList()
    End Sub

End Class