﻿Imports LinqDB.TABLE
Imports WMS_BL

Public Class MS_Product_Type
    Inherits System.Web.UI.Page

    Private ReadOnly Property UserSession As LoginSessionData
        Get
            Return Session(CommonBL.UserLoginSessionName)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect("frmLogin.aspx?rnd=" & DateTime.Now.Millisecond)
                Exit Sub
            Else
                BindList()
                ClearForm()
            End If
        End If
    End Sub

    Protected Sub chkActiveStatus_CheckedChanged(sender As ucSwitchButton, e As System.EventArgs)
        Dim chkActiveStatus As ucSwitchButton = sender
        Dim itm As RepeaterItem = chkActiveStatus.Parent
        Dim lblProductTypeID As Label = itm.FindControl("lblProductTypeID")
        Dim lblProductTypeName As Label = itm.FindControl("lblProductTypeName")

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.UpdateProductTypestatus(UserSession.UserName, lblProductTypeID.Text, chkActiveStatus.Checked)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Update status fail');", True)
            LogFileData.LogError(UserSession, ret.ErrorMessage)
        Else
            LogFileData.LogTrans(UserSession, "Update Product Type Status " & lblProductTypeName.Text & " Status : " & chkActiveStatus.Checked.ToString)
        End If
    End Sub

    Private Sub BindList()
        Dim dt As DataTable = MasterDataBL.GetList_ProductType(txtSearch.Text.Trim, ddlstatus.SelectedValue)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblProductTypeName As Label = e.Item.FindControl("lblProductTypeName")
        Dim lblProductTypeID As Label = e.Item.FindControl("lblProductTypeID")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        lblProductTypeName.Text = e.Item.DataItem("Product_Type")
        lblProductTypeID.Text = e.Item.DataItem("Product_Type_ID")
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblProductTypeName As Label = e.Item.FindControl("lblProductTypeName")
        Dim lblProductTypeID As Label = e.Item.FindControl("lblProductTypeID")

        Select Case e.CommandName
            Case "EDIT"
                ClearForm()
                FillInData(lblProductTypeID.Text)

                pnlList.Visible = False
                pnlEdit.Visible = True

                LogFileData.LogTrans(UserSession, "Edit Product Type " & lblProductTypeName.Text)
            Case "DELETE"
                Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.DeleteProductType(lblProductTypeID.Text)
                If ret.IsSuccess = True Then
                    LogFileData.LogTrans(UserSession, "Delete Product Type " & lblProductTypeName.Text)

                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Delete Data Fail');", True)
                    LogFileData.LogError(UserSession, ret.ErrorMessage)
                End If
        End Select
    End Sub

    Private Sub FillInData(ProductTypeID As Long)
        Dim ret As MsProductTypeLinqDB = MasterDataBL.GetProductTypeData(ProductTypeID)
        If ret.PRODUCT_TYPE_ID > 0 Then
            lblProductTypeID.Text = ret.PRODUCT_TYPE_ID
            txtProductTypeName.Text = ret.PRODUCT_TYPE
            chkActive.Checked = (ret.ACTIVE_STATUS = "Y")
        End If
        ret = Nothing
    End Sub

    Private Sub ClearForm()
        lblProductTypeID.Text = "0"
        txtProductTypeName.Text = ""
        chkActive.Checked = True
    End Sub

    Private Sub btnAddProductType_Click(sender As Object, e As EventArgs) Handles btnAddProductType.Click
        ClearForm()
        pnlEdit.Visible = True
        pnlList.Visible = False
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Add ProductType")
    End Sub

    Private Function ValidateData() As Boolean
        If lblProductTypeID.Text = "0" Then
            'กรณีเป็นการเพิ่มใหม่ ให้ตรวจสอบข้อมูลการเข้าระบบด้วย
            If txtProductTypeName.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุชื่อประเภท');", True)
                Return False
            End If
        End If

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.CheckDuplicateProductType(lblProductTypeID.Text, txtProductTypeName.Text)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');", True)
            Return False
        End If

        Return True
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save Product Type")
        If ValidateData() = True Then

            Dim lnq As MsProductTypeLinqDB = MasterDataBL.SaveProductType(UserSession.UserName, lblProductTypeID.Text, txtProductTypeName.Text, chkActive.Checked)
            If lnq.PRODUCT_TYPE_ID > 0 Then
                lblProductTypeID.Text = lnq.PRODUCT_TYPE_ID
                LogFileData.LogTrans(UserSession, "Save Product Type " & txtProductTypeName.Text & " สำเร็จ")
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Data Complete');", True)
                pnlEdit.Visible = False
                pnlList.Visible = True
                BindList()
            Else
                LogFileData.LogError(UserSession, "Save Product Type " & txtProductTypeName.Text & " ไม่สำเร็จ Error :" & lnq.ErrorMessage)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Fail');", True)
            End If
            lnq = Nothing
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Cancel")
        ClearForm()
        pnlEdit.Visible = False
        pnlList.Visible = True
        BindList()
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click, txtSearch.TextChanged
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม ค้นหา")
        BindList()
    End Sub

    Private Sub ddlstatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlstatus.SelectedIndexChanged
        BindList()
    End Sub
End Class