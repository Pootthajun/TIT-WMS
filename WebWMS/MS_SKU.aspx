﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="MS_SKU.aspx.vb" Inherits="WebWMS.MS_SKU" %>

<%@ Register Src="~/UserControl/ucSwitchButton.ascx" TagPrefix="uc1" TagName="ucSwitchButton" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="page-header">
        <h4 class="page-title">ข้อมูลชนิดสินค้า</h4>
    </div>
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <asp:Panel ID="pnlList" runat="server" CssClass="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-2">
                                <asp:LinkButton ID="btnAddSKU" runat="server" CssClass="btn btn-warning">
                                    <i class="icon md-plus-circle-o"></i>
                                    <span>เพื่มชนิดสินค้า</span>
                                </asp:LinkButton>
                            </div>
                            <div class="col-sm-3">
                                <asp:LinkButton ID="btnOpenSearch" runat="server" CssClass="btn btn-primary">
                                     <i class="icon fa-search"></i> <span>ค้นหา</span>
                                </asp:LinkButton>
                            </div>
                        </div>
                        <asp:Panel ID="pnlSearch" runat="server">
                            <br />
                            <div class="row">
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtSearchID" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="100" placeholder="รหัส SKU"></asp:TextBox>
                                </div>
                                <div class="col-sm-2">
                                    <asp:DropDownList ID="ddlSKUGroupSearch" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="กลุ่ม" AutoPostBack="true"></asp:DropDownList>
                                </div>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtSearchProductName" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="100" placeholder="ชื่อสินค้า"></asp:TextBox>
                                </div>
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddlstatus" CssClass="form-control" AutoPostBack="true" runat="server">
                                        <asp:ListItem Text="สถานะการใช้งาน" Value=""></asp:ListItem>
                                        <asp:ListItem Text="ใช้งาน" Value="Y"></asp:ListItem>
                                        <asp:ListItem Text="ไม่ใช้งาน" Value="N"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-sm-1">
                                    <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn btn-warning">
                                        <i class="icon fa-search"></i>
                                    </asp:LinkButton>
                                </div>
                            </div>
                            
                        </asp:Panel>

                        <div class="clearfix visible-md-block"></div>
                        <br />
                        <table class="tablesaw table-striped table-bordered table-hover">
                            <thead>
                                <tr class="bg-blue-grey-100">
                                    <th class="text-center">รหัส</th>
                                    <th class="text-center">กลุ่ม</th>
                                    <th class="text-center">สินค้า</th>
                                    <th class="text-center">ใช้งาน</th>
                                    <th class="text-center">แก้ไข</th>
                                    <th class="text-center">ลบ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptList" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td class="text-center">
                                                <asp:Label ID="lblSKUID" runat="server"></asp:Label>
                                                <asp:Label ID="lblSequenceID" runat="server" Visible="false"></asp:Label>
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="lblSKUGroupName" runat="server"></asp:Label>
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="lblProduct" runat="server"></asp:Label>
                                            </td>
                                            <td class="text-center">
                                                <uc1:ucSwitchButton runat="server" ID="chkActiveStatus" />
                                                <%--AutoPostBack="true" OnCheckedChanged="chkActiveStatus_CheckedChanged"--%> 
                                            </td>
                                            <td class="text-center">
                                                <asp:LinkButton ID="btnEdit" runat="server" CommandName="EDIT">
                                                    <i class="icon md-edit" style="font-size: 20px;"></i>
                                                </asp:LinkButton>
                                            </td>
                                            <td class="text-center">
                                                <asp:LinkButton ID="btnDelete" runat="server" CommandName="DELETE" OnClientClick="return confirm('Do you want delete data?');">
                                                    <i class="icon md-delete" style="font-size: 20px;"></i>
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlEdit" runat="server" CssClass="panel" Visible="false">
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>ID :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-10">
                                <asp:TextBox ID="txtSKUID" runat="server" CssClass="form-control" MaxLength="100" placeholder="รหัสชนิดสินค้า"></asp:TextBox>
                                <asp:Label ID="lblSequenceID" runat="server" Visible="false" Text="0"></asp:Label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Description :</label>
                            </div>

                            <div class="col-sm-10">
                                <asp:TextBox ID="txtSKUDetial" runat="server" CssClass="form-control" MaxLength="100" placeholder="รายละเอียด" TextMode="MultiLine"></asp:TextBox>
                                <br />
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>SKU Group :</label>
                            </div>
                            <div class="col-sm-10">
                                <asp:DropDownList ID="ddlSKUGroup" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="ประเภทสินค้า"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Size :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtsize" runat="server" CssClass="form-control" MaxLength="100" placeholder=""></asp:TextBox>
                            </div>
                            <div class="form-group col-sm-2">
                                <label>Color :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtcolor" runat="server" CssClass="form-control" MaxLength="100" placeholder=""></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Model :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtmodel" runat="server" CssClass="form-control" MaxLength="100" placeholder=""></asp:TextBox>
                            </div>
                            <div class="form-group col-sm-2">
                                <label>Brand :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control" MaxLength="100" placeholder=""></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Unit :</label>
                            </div>
                            <div class="col-sm-4">
                                <asp:DropDownList ID="ddlUnit" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="หน่วย" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="form-group col-sm-2">
                                <label>Weight/Unit :</label>
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtWeightPerUnit" runat="server" CssClass="form-control" MaxLength="100" placeholder=""></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Product Type :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-4">
                                <asp:DropDownList ID="ddlProductType" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="ประเภทสินค้า" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="form-group col-sm-2">
                                <label>Product :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-4">
                                <asp:DropDownList ID="ddlProduct" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="สินค้า"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Volume/Unit :</label>
                            </div>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtVolumePerUnit" runat="server" CssClass="form-control" MaxLength="100" placeholder=""></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Volume :</label>
                            </div>
                            <div class="col-sm-2">
                                <asp:TextBox ID="txtwidth" runat="server" CssClass="form-control" MaxLength="100" placeholder="กว้าง"></asp:TextBox>
                            </div>
                            <div class="form-group col-sm-1 text-center">
                                <label>X</label>
                            </div>
                            <div class="col-sm-2">
                                <asp:TextBox ID="txtlength" runat="server" CssClass="form-control" MaxLength="100" placeholder="ยาว"></asp:TextBox>
                            </div>
                            <div class="form-group col-sm-1 text-center">
                                <label>X</label>
                            </div>
                            <div class="col-sm-2">
                                <asp:TextBox ID="txtheight" runat="server" CssClass="form-control" MaxLength="100" placeholder="สูง"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Production Date :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox CssClass="form-control m-b" ID="txtProductionDate" runat="server" placeholder=""></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                        Format="dd/MM/yyyy" TargetControlID="txtProductionDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                </div>
                            </div>
                            <div class="form-group col-sm-2">
                                <label>Expire Date :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox CssClass="form-control m-b" ID="txtExpireDate" runat="server" placeholder="" AutoPostBack="true"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                        Format="dd/MM/yyyy" TargetControlID="txtExpireDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Age :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-2">
                                <asp:TextBox ID="txtAgeDay" runat="server" CssClass="form-control" MaxLength="100" placeholder="วัน" Enabled="false"></asp:TextBox>
                            </div>
                            <div class="form-group col-sm-1 text-center">
                                <label>-</label>
                            </div>
                            <div class="col-sm-2">
                                <asp:TextBox ID="txtAgeMonth" runat="server" CssClass="form-control" MaxLength="100" placeholder="เดือน" Enabled="false"></asp:TextBox>
                            </div>
                            <div class="form-group col-sm-1 text-center">
                                <label>-</label>
                            </div>
                            <div class="col-sm-2">
                                <asp:TextBox ID="txtAgeYear" runat="server" CssClass="form-control" MaxLength="100" placeholder="ปี" Enabled="false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Product Picture :</label>
                            </div>
                            <div class="col-sm-5">
                                <asp:TextBox ID="txtPicture" runat="server" CssClass="form-control" MaxLength="100" placeholder=""></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Buy Price :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtBuyPrice" runat="server" CssClass="form-control" MaxLength="100" placeholder=""></asp:TextBox>
                            </div>
                            <div class="form-group col-sm-2">
                                <label>Sell Price :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtSellPrice" runat="server" CssClass="form-control" MaxLength="100" placeholder=""></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Other Price :</label>
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtOtherPrice" runat="server" CssClass="form-control" MaxLength="100" placeholder=""></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Amount Max :</label>
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtAmountMax" runat="server" CssClass="form-control" MaxLength="100" placeholder=""></asp:TextBox>
                            </div>
                            <div class="form-group col-sm-2">
                                <label>Amount Min :</label>
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtAmountMin" runat="server" CssClass="form-control" MaxLength="100" placeholder=""></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Weight Max :</label>
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtWeightMax" runat="server" CssClass="form-control" MaxLength="100" placeholder=""></asp:TextBox>
                            </div>
                            <div class="form-group col-sm-2">
                                <label>Weight Min :</label>
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtWeightMin" runat="server" CssClass="form-control" MaxLength="100" placeholder=""></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Volume Max :</label>
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtVolumeMax" runat="server" CssClass="form-control" MaxLength="100" placeholder=""></asp:TextBox>
                            </div>
                            <div class="form-group col-sm-2">
                                <label>Volume Min :</label>
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtVolumeMin" runat="server" CssClass="form-control" MaxLength="100" placeholder=""></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Pallet Type :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-4">
                                <asp:DropDownList ID="ddlPallet" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="ประเภทสินค้า"></asp:DropDownList>
                            </div>
                            <div class="form-group col-sm-2">
                                <label>Max Pallet:<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtMaxPallet" runat="server" CssClass="form-control" MaxLength="100" placeholder=""></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Pick Order :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-4">
                                <asp:DropDownList ID="ddlPick" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="ประเภทสินค้า"></asp:DropDownList>
                            </div>
                            <div class="form-group col-sm-2">
                                <label>Amount/Unit :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtAmountPerUnit" runat="server" CssClass="form-control" MaxLength="100" placeholder=""></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>ใช้งาน</label>
                            </div>

                            <div class="col-sm-9">
                                <uc1:ucSwitchButton runat="server" ID="chkActive" />
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <hr />
                        <div class="form-group form-material">
                            <div class="col-sm-12 ">
                                <div class="col-sm-10 "></div>
                                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary">
                                    <span>Save</span>
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-warning">
                                    <span>Cancel</span>
                                </asp:LinkButton>
                            </div>
                        </div>

                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpFooter" runat="server">
</asp:Content>
