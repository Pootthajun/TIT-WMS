﻿Imports LinqDB.TABLE
Imports WMS_BL

Public Class MS_Receive_Type
    Inherits System.Web.UI.Page

    Private ReadOnly Property UserSession As LoginSessionData
        Get
            Return Session(CommonBL.UserLoginSessionName)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect("frmLogin.aspx?rnd=" & DateTime.Now.Millisecond)
                Exit Sub
            Else
                BindList()
                ClearForm()
            End If
        End If
    End Sub

    Protected Sub chkActiveStatus_CheckedChanged(sender As ucSwitchButton, e As System.EventArgs)
        Dim chkActiveStatus As ucSwitchButton = sender
        Dim itm As RepeaterItem = chkActiveStatus.Parent
        Dim lblReceiveTypeID As Label = itm.FindControl("lblReceiveTypeID")
        Dim lblReceiveTypeName As Label = itm.FindControl("lblReceiveTypeName")

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.UpdateReceiveTypestatus(UserSession.UserName, lblReceiveTypeID.Text, chkActiveStatus.Checked)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Update status fail');", True)
            LogFileData.LogError(UserSession, ret.ErrorMessage)
        Else
            LogFileData.LogTrans(UserSession, "Update Receive Type Status " & lblReceiveTypeName.Text & " Status : " & chkActiveStatus.Checked.ToString)
        End If
    End Sub

    Private Sub BindList()
        Dim dt As DataTable = MasterDataBL.GetList_ReceiveType(txtSearch.Text.Trim, ddlstatus.SelectedValue)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblReceiveTypeName As Label = e.Item.FindControl("lblReceiveTypeName")
        Dim lblReceiveTypeID As Label = e.Item.FindControl("lblReceiveTypeID")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        lblReceiveTypeName.Text = e.Item.DataItem("Recieve_Type")
        lblReceiveTypeID.Text = e.Item.DataItem("Receive_Type_ID")
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblReceiveTypeName As Label = e.Item.FindControl("lblReceiveTypeName")
        Dim lblReceiveTypeID As Label = e.Item.FindControl("lblReceiveTypeID")

        Select Case e.CommandName
            Case "EDIT"
                ClearForm()
                FillInData(lblReceiveTypeID.Text)

                pnlList.Visible = False
                pnlEdit.Visible = True

                LogFileData.LogTrans(UserSession, "Edit Receive Type " & lblReceiveTypeName.Text)
            Case "DELETE"
                Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.DeleteReceiveType(lblReceiveTypeID.Text)
                If ret.IsSuccess = True Then
                    LogFileData.LogTrans(UserSession, "Delete Receive Type " & lblReceiveTypeName.Text)

                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Delete Data Fail');", True)
                    LogFileData.LogError(UserSession, ret.ErrorMessage)
                End If
        End Select
    End Sub

    Private Sub FillInData(ReceiveTypeID As Long)
        Dim ret As MsReceiveTypeLinqDB = MasterDataBL.GetReceiveTypeData(ReceiveTypeID)
        If ret.RECEIVE_TYPE_ID > 0 Then
            lblReceiveTypeID.Text = ret.RECEIVE_TYPE_ID
            txtReceiveTypeName.Text = ret.RECIEVE_TYPE
            txtReceiveTypeDetial.Text = ret.RECIEVE_TYPE_DETAIL
            chkActive.Checked = (ret.ACTIVE_STATUS = "Y")
        End If
        ret = Nothing
    End Sub

    Private Sub ClearForm()
        lblReceiveTypeID.Text = "0"
        txtReceiveTypeName.Text = ""
        txtReceiveTypeDetial.Text = ""
        chkActive.Checked = True
    End Sub

    Private Sub btnAddReceiveType_Click(sender As Object, e As EventArgs) Handles btnAddReceiveType.Click
        ClearForm()
        pnlEdit.Visible = True
        pnlList.Visible = False
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Add ReceiveType")
    End Sub

    Private Function ValidateData() As Boolean
        If lblReceiveTypeID.Text = "0" Then
            'กรณีเป็นการเพิ่มใหม่ ให้ตรวจสอบข้อมูลการเข้าระบบด้วย
            If txtReceiveTypeName.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุชื่อประเภท');", True)
                Return False
            End If
        End If

        'Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.CheckDuplicateReceiveType(lblReceiveTypeID.Text, txtReceiveTypeName.Text)
        'If ret.IsSuccess = False Then
        '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');", True)
        '    Return False
        'End If

        Return True
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save Receive Type")
        If ValidateData() = True Then

            Dim lnq As MsReceiveTypeLinqDB = MasterDataBL.SaveReceiveType(UserSession.UserName, lblReceiveTypeID.Text, txtReceiveTypeName.Text, txtReceiveTypeDetial.Text, chkActive.Checked)
            If lnq.RECEIVE_TYPE_ID > 0 Then
                lblReceiveTypeID.Text = lnq.RECEIVE_TYPE_ID
                LogFileData.LogTrans(UserSession, "Save Receive Type " & txtReceiveTypeName.Text & " สำเร็จ")
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Data Complete');", True)
                pnlEdit.Visible = False
                pnlList.Visible = True
                BindList()
            Else
                LogFileData.LogError(UserSession, "Save Receive Type " & txtReceiveTypeName.Text & " ไม่สำเร็จ Error :" & lnq.ErrorMessage)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Fail');", True)
            End If
            lnq = Nothing
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Cancel")
        ClearForm()
        pnlEdit.Visible = False
        pnlList.Visible = True
        BindList()
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click, txtSearch.TextChanged
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม ค้นหา")
        BindList()
    End Sub

    Private Sub ddlstatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlstatus.SelectedIndexChanged
        BindList()
    End Sub

End Class