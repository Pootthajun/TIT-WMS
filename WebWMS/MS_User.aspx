﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" EnableEventValidation="false" MasterPageFile="~/frmMaster.Master" CodeBehind="MS_User.aspx.vb" Inherits="WebWMS.MS_User" %>

<%@ Register Src="~/UserControl/ucSwitchButton.ascx" TagPrefix="uc1" TagName="ucSwitchButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="page-header">
        <h4 class="page-title">ข้อมูลผู้ใช้งาน</h4>
    </div>
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <asp:Panel ID="pnlList" runat="server" CssClass="panel">
                    <div class="panel-body">
                        <asp:LinkButton ID="btnAddUser" runat="server" CssClass="btn btn-warning">
                            <i class="icon md-plus-circle-o"></i>
                            <span>เพื่มผู้ใช้</span>
                        </asp:LinkButton>

                        <asp:LinkButton ID="btnOpenSearch" runat="server" CssClass="btn btn-primary">
                            <i class="icon fa-search"></i>
                            <span>ค้นหา</span>
                        </asp:LinkButton>
                        
                        <asp:Panel ID="pnlSearch" runat="server">
                            <br />
                            <div class="row">
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="100" placeholder="ชื่อ-สกุล"></asp:TextBox>
                                </div>
                                <div class="col-sm-2">
                                    <asp:DropDownList ID="ddlsex" CssClass="form-control" AutoPostBack="true" runat="server">
                                        <asp:ListItem Text="เพศ" Value=""></asp:ListItem>
                                        <asp:ListItem Text="ชาย" Value="M"></asp:ListItem>
                                        <asp:ListItem Text="หญิง" Value="F"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddlPositionSearch" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="ตำแหน่ง" AutoPostBack="true"></asp:DropDownList>
                                </div>
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddlDepartmentSearch" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="แผนก" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddlstatus" CssClass="form-control" AutoPostBack="true" runat="server">
                                        <asp:ListItem Text="สถานะการใช้งาน" Value=""></asp:ListItem>
                                        <asp:ListItem Text="ใช้งาน" Value="Y"></asp:ListItem>
                                        <asp:ListItem Text="ไม่ใช้งาน" Value="N"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                                <div class="col-sm-1">
                                    <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn btn-warning">
                                        <i class="icon fa-search"></i>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </asp:Panel>

                        <div class="clearfix visible-md-block"></div>
                        <br />
                        <br />
                        <table class="tablesaw table-striped table-bordered table-hover">
                            <thead>
                                <tr class="bg-blue-grey-100">
                                    <th class="text-center">ชื่อ-สกุล</th>
                                    <th class="text-center">เพศ</th>
                                    <th class="text-center">ตำแหน่ง</th>
                                    <th class="text-center">แผนก</th>
                                    <th class="text-center">ใช้ระบบล่าสุด</th>
                                    <th class="text-center">ใช้งาน</th>
                                    <th class="text-center">แก้ไข</th>
                                    <th class="text-center">ลบ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptList" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblFullName" runat="server"></asp:Label>
                                                <asp:Label ID="lblUserID" runat="server" Visible="false"></asp:Label>
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="lblGender" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPositionName" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblDepartmentName" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblLastLoginTime" runat="server"></asp:Label>
                                            </td>
                                            <td class="text-center">
                                                <uc1:ucSwitchButton runat="server" ID="chkActiveStatus" AutoPostBack="true" OnCheckedChanged="chkActiveStatus_CheckedChanged" />
                                            </td>
                                            <td class="text-center">
                                                <asp:LinkButton ID="btnEdit" runat="server" CommandName="EDIT">
                                                    <i class="icon md-edit" style="font-size: 20px;"></i>
                                                </asp:LinkButton>
                                            </td>
                                            <td class="text-center">
                                                <asp:LinkButton ID="btnDelete" runat="server" CommandName="DELETE" OnClientClick="return confirm('Do you want delete data?');">
                                                    <i class="icon md-delete" style="font-size: 20px;"></i>
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlEdit" runat="server"  Visible="false">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="form-group col-sm-2">
                                    <label>ชื่อ-สกุล :<span style="color: red;">*</span></label>
                                </div>
                                <div class="col-sm-2">
                                    <asp:DropDownList ID="ddlPrefixName" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="คำนำหน้าชื่อ"></asp:DropDownList>
                                    <asp:Label ID="lblUserID" runat="server" Visible="false" Text="0"></asp:Label>
                                </div>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" MaxLength="255" placeholder="ชื่อ"></asp:TextBox>
                                </div>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" MaxLength="255" placeholder="นามสกุล"></asp:TextBox>
                                </div>
                                <div class="col-sm-1">
                                    <asp:LinkButton ID="rdiGenderMale" runat="server" CssClass="btn btn-success">
                                    <i class="icon fa-male" aria-hidden="true"></i>
                                    <span>ชาย</span>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="rdiGenderFemale" runat="server" CssClass="btn btn-info" Visible="false">
                                    <i class="icon fa-female" aria-hidden="true"></i>
                                    <span>หญิง</span>
                                    </asp:LinkButton>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group col-sm-2">
                                    <label>ตำแหน่ง :<span style="color: red;">*</span></label>
                                </div>

                                <div class="col-sm-9">
                                    <asp:DropDownList ID="ddlPosition" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title=""></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group col-sm-2">
                                    <label>แผนก :<span style="color: red;">*</span></label>
                                </div>

                                <div class="col-sm-9">
                                    <asp:DropDownList ID="ddlDepartment" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title=""></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group col-sm-2">
                                    <label>ใช้งาน</label>
                                </div>

                                <div class="col-sm-9">
                                    <uc1:ucSwitchButton runat="server" ID="chkActive" />
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <hr />
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group col-sm-2">
                                    <label>ชื่อเข้าระบบ<span style="color: red;">*</span></label>
                                </div>

                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtLoginUser" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group col-sm-2">
                                    <label>รหัสผ่าน<span style="color: red;">*</span></label>
                                </div>

                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" MaxLength="100" TextMode="Password" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group col-sm-2">
                                    <label>ยืนยันรหัสผ่าน<span style="color: red;">*</span></label>
                                </div>

                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtConfirmPassword" runat="server" CssClass="form-control" MaxLength="100" TextMode="Password" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">สิทธิ์การใช้งาน</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row row-lg">
                                <div class="col-lg-6">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="width-350 text-center">สิทธิ์การใช้งาน</th>
                                                <th class="text-center">กำหนดสิทธิ์</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="rptUserRole" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblRoleName" runat="server"></asp:Label>
                                                            <asp:Label ID="lblRoleID" runat="server" Visible="false" ></asp:Label>
                                                        </td>
                                                        <td class="text-center">
                                                            <uc1:ucSwitchButton runat="server" ID="chkGrant" Checked="false" />
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group form-material">
                        <div class="col-sm-12 col-sm-offset-9">
                            <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary">
                            <span>Save</span>
                            </asp:LinkButton>
                            <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-warning">
                            <span>Cancel</span>
                            </asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpFooter" runat="server">
</asp:Content>
