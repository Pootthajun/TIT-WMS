﻿Imports LinqDB.TABLE
Imports WMS_BL

Public Class MS_PickOrder
    Inherits System.Web.UI.Page

    Private ReadOnly Property UserSession As LoginSessionData
        Get
            Return Session(CommonBL.UserLoginSessionName)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect("frmLogin.aspx?rnd=" & DateTime.Now.Millisecond)
                Exit Sub
            Else
                BindList()
                ClearForm()
            End If
        End If
    End Sub

    Protected Sub chkActiveStatus_CheckedChanged(sender As ucSwitchButton, e As System.EventArgs)
        Dim chkActiveStatus As ucSwitchButton = sender
        Dim itm As RepeaterItem = chkActiveStatus.Parent
        Dim lblSequenceID As Label = itm.FindControl("lblSequenceID")
        Dim lblPickName As Label = itm.FindControl("lblPickName")

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.UpdatePickOrderstatus(UserSession.UserName, lblSequenceID.Text, chkActiveStatus.Checked)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Update status fail');", True)
            LogFileData.LogError(UserSession, ret.ErrorMessage)
        Else
            LogFileData.LogTrans(UserSession, "Update PickOrder Status " & lblPickName.Text & " Status : " & chkActiveStatus.Checked.ToString)
        End If
    End Sub

    Private Sub BindList()
        Dim dt As DataTable = MasterDataBL.GetList_PickOrder(txtSearch.Text.Trim, ddlstatus.SelectedValue)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblSequenceID As Label = e.Item.FindControl("lblSequenceID")
        Dim lblPickName As Label = e.Item.FindControl("lblPickName")
        Dim lblPickID As Label = e.Item.FindControl("lblPickID")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        lblSequenceID.Text = e.Item.DataItem("Sequence_ID")
        lblPickID.Text = e.Item.DataItem("Pick_ID")
        lblPickName.Text = e.Item.DataItem("Pick_Format")
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblSequenceID As Label = e.Item.FindControl("lblSequenceID")
        Dim lblPickName As Label = e.Item.FindControl("lblPickName")

        Select Case e.CommandName
            Case "EDIT"
                ClearForm()
                FillInData(lblSequenceID.Text)

                pnlList.Visible = False
                pnlEdit.Visible = True

                LogFileData.LogTrans(UserSession, "Edit PickOrder " & lblPickName.Text)
            Case "DELETE"
                Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.DeletePickOrder(lblSequenceID.Text)
                If ret.IsSuccess = True Then
                    LogFileData.LogTrans(UserSession, "Delete PickOrder " & lblPickName.Text)

                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Delete Data Fail');", True)
                    LogFileData.LogError(UserSession, ret.ErrorMessage)
                End If
        End Select
    End Sub

    Private Sub FillInData(PickOrderID As Long)
        Dim ret As MsPickOrderLinqDB = MasterDataBL.GetPickOrderData(PickOrderID)
        If ret.SEQUENCE_ID > 0 Then
            lblSequenceID.Text = ret.SEQUENCE_ID
            txtPickID.Text = ret.PICK_ID
            txtPickName.Text = ret.PICK_FORMAT
            chkActive.Checked = (ret.ACTIVE_STATUS = "Y")
        End If
        ret = Nothing
    End Sub

    Private Sub ClearForm()
        lblSequenceID.Text = "0"
        txtPickID.Text = ""
        txtPickName.Text = ""
        chkActive.Checked = True
        txtPickID.Enabled = False
    End Sub

    Private Sub btnAddPickOrder_Click(sender As Object, e As EventArgs) Handles btnAddPickOrder.Click
        ClearForm()
        pnlEdit.Visible = True
        pnlList.Visible = False
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Add PickOrder")
        txtPickID.Enabled = True
    End Sub

    Private Function ValidateData() As Boolean
        If lblSequenceID.Text = "0" Then
            'กรณีเป็นการเพิ่มใหม่ ให้ตรวจสอบข้อมูลการเข้าระบบด้วย
            If txtPickID.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุรหัสกิจกรรมการขาย');", True)
                Return False
            End If
            If txtPickName.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุชื่อกิจกรรมการขาย');", True)
                Return False
            End If
        End If

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.CheckDuplicatePickOrder(lblSequenceID.Text, txtPickName.Text)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');", True)
            Return False
        End If

        Return True
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save PickOrder")
        If ValidateData() = True Then

            Dim lnq As MsPickOrderLinqDB = MasterDataBL.SavePickOrder(UserSession.UserName, lblSequenceID.Text, txtPickID.Text, txtPickName.Text, chkActive.Checked)
            If lnq.SEQUENCE_ID > 0 Then
                lblSequenceID.Text = lnq.SEQUENCE_ID
                LogFileData.LogTrans(UserSession, "Save PickOrder " & txtPickName.Text & " สำเร็จ")
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Data Complete');", True)
                pnlEdit.Visible = False
                pnlList.Visible = True
                BindList()
            Else
                LogFileData.LogError(UserSession, "Save PickOrder " & txtPickName.Text & " ไม่สำเร็จ Error :" & lnq.ErrorMessage)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Fail');", True)
            End If
            lnq = Nothing
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Cancel")
        ClearForm()
        pnlEdit.Visible = False
        pnlList.Visible = True
        BindList()
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click, txtSearch.TextChanged
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม ค้นหา")
        BindList()
    End Sub

    Private Sub ddlstatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlstatus.SelectedIndexChanged
        BindList()
    End Sub
End Class