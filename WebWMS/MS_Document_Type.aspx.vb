﻿Imports LinqDB.TABLE
Imports WMS_BL

Public Class MS_Document_Type
    Inherits System.Web.UI.Page

    Private ReadOnly Property UserSession As LoginSessionData
        Get
            Return Session(CommonBL.UserLoginSessionName)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect("frmLogin.aspx?rnd=" & DateTime.Now.Millisecond)
                Exit Sub
            Else
                BindList()
                ClearForm()
            End If
        End If
    End Sub

    Protected Sub chkActiveStatus_CheckedChanged(sender As ucSwitchButton, e As System.EventArgs)
        Dim chkActiveStatus As ucSwitchButton = sender
        Dim itm As RepeaterItem = chkActiveStatus.Parent
        Dim lblDocumentTypeID As Label = itm.FindControl("lblDocumentTypeID")
        Dim lblDocumentTypeName As Label = itm.FindControl("lblDocumentTypeName")

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.UpdateDocumentTypestatus(UserSession.UserName, lblDocumentTypeID.Text, chkActiveStatus.Checked)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Update status fail');", True)
            LogFileData.LogError(UserSession, ret.ErrorMessage)
        Else
            LogFileData.LogTrans(UserSession, "Update DocumentType Status " & lblDocumentTypeName.Text & " Status : " & chkActiveStatus.Checked.ToString)
        End If
    End Sub

    Private Sub BindList()
        Dim dt As DataTable = MasterDataBL.GetList_DocumentType(txtSearch.Text.Trim, ddlstatus.SelectedValue, ddljobTypeForSearch.SelectedValue)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Private Sub Bindddl()
        MasterDataBL.GetListDDL_JobType(ddljobType)
        MasterDataBL.GetListDDL_JobType(ddljobTypeForSearch)
    End Sub


    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lbljobType As Label = e.Item.FindControl("lbljobType")
        Dim lblDocumentTypeName As Label = e.Item.FindControl("lblDocumentTypeName")
        Dim lblDocumentTypeID As Label = e.Item.FindControl("lblDocumentTypeID")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        lbljobType.Text = e.Item.DataItem("Job_Type")
        lblDocumentTypeName.Text = e.Item.DataItem("Document_Type")
        lblDocumentTypeID.Text = e.Item.DataItem("Document_Type_ID")
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblDocumentTypeID As Label = e.Item.FindControl("lblDocumentTypeID")
        Dim lblDocumentTypeName As Label = e.Item.FindControl("lblDocumentTypeName")

        Select Case e.CommandName
            Case "EDIT"
                ClearForm()
                FillInData(lblDocumentTypeID.Text)

                pnlList.Visible = False
                pnlEdit.Visible = True

                LogFileData.LogTrans(UserSession, "คลิกปุ่ม Edit DocumentType " & lblDocumentTypeName.Text)
            Case "DELETE"
                Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.DeleteDocumentType(lblDocumentTypeID.Text)
                If ret.IsSuccess = True Then
                    LogFileData.LogTrans(UserSession, "Delete DocumentType " & lblDocumentTypeName.Text)

                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Delete Data Fail');", True)
                    LogFileData.LogError(UserSession, ret.ErrorMessage)
                End If
        End Select
    End Sub

    Private Sub FillInData(DocumentTypeID As Long)
        Dim ret As MsDocumentTypeLinqDB = MasterDataBL.GetDocumentTypeData(DocumentTypeID)
        If ret.DOCUMENT_TYPE_ID > 0 Then
            lblDocumentTypeID.Text = ret.DOCUMENT_TYPE_ID
            txtDocumentTypeName.Text = ret.DOCUMENT_TYPE
            ddljobType.SelectedValue = ret.JOB_TYPE_ID
            chkActive.Checked = (ret.ACTIVE_STATUS = "Y")
        End If
        ret = Nothing
    End Sub

    Private Sub btnAddDocumentType_Click(sender As Object, e As EventArgs) Handles btnAddDocumentType.Click
        ClearForm()
        pnlEdit.Visible = True
        pnlList.Visible = False
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Add DocumentType")
    End Sub

    Private Sub ClearForm()
        Bindddl()
        lblDocumentTypeID.Text = "0"
        txtDocumentTypeName.Text = ""
        ddljobType.SelectedValue = 0
        chkActive.Checked = True
    End Sub

    Private Function ValidateData() As Boolean
        If lblDocumentTypeID.Text = "0" Then
            'กรณีเป็นการเพิ่มใหม่ ให้ตรวจสอบข้อมูลการเข้าระบบด้วย
            If txtDocumentTypeName.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุชื่อประเภทเอกสาร');", True)
                Return False
            End If

            If ddljobType.SelectedValue = "0" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุเลือกประเภทงาน');", True)
                Return False
            End If
        End If

        'Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.CheckDuplicateDocumentType(lblDocumentTypeID.Text, txtDocumentTypeName.Text)
        'If ret.IsSuccess = False Then
        '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');", True)
        '    Return False
        'End If

        Return True
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save DocumentType")
        If ValidateData() = True Then

            Dim lnq As MsDocumentTypeLinqDB = MasterDataBL.SaveDocumentType(UserSession.UserName, lblDocumentTypeID.Text, txtDocumentTypeName.Text, ddljobType.SelectedValue, chkActive.Checked)
            If lnq.DOCUMENT_TYPE_ID > 0 Then
                lblDocumentTypeID.Text = lnq.DOCUMENT_TYPE_ID
                LogFileData.LogTrans(UserSession, "Save DocumentType " & txtDocumentTypeName.Text & " สำเร็จ")
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Data Complete');", True)
                pnlEdit.Visible = False
                pnlList.Visible = True
                BindList()
            Else
                LogFileData.LogError(UserSession, "Save DocumentType " & txtDocumentTypeName.Text & " ไม่สำเร็จ Error :" & lnq.ErrorMessage)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Fail');", True)
            End If
            lnq = Nothing
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Cancel")
        ClearForm()
        pnlEdit.Visible = False
        pnlList.Visible = True
        BindList()
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click, txtSearch.TextChanged
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม ค้นหา")
        BindList()
    End Sub

    Private Sub ddlstatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlstatus.SelectedIndexChanged, ddljobTypeForSearch.SelectedIndexChanged
        BindList()
    End Sub
End Class