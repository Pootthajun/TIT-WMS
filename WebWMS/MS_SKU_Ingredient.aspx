﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="MS_SKU_Ingredient.aspx.vb" Inherits="WebWMS.MS_SKU_Ingredient" %>

<%@ Register Src="~/UserControl/ucSwitchButton.ascx" TagPrefix="uc1" TagName="ucSwitchButton" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="page-header">
        <h4 class="page-title">ข้อมูลส่วนผสม</h4>
    </div>
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <asp:Panel ID="pnlList" runat="server" CssClass="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-5">
                                <asp:LinkButton ID="btnAddIngredient" runat="server" CssClass="btn btn-warning">
                                    <i class="icon md-plus-circle-o"></i>
                                    <span>เพื่มส่วนผสม</span>
                                </asp:LinkButton>
                            </div>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="100" placeholder="ชื่อส่วนผสม"></asp:TextBox>
                            </div>
                            <div class="col-sm-3">
                                <asp:DropDownList ID="ddlstatus" CssClass="form-control" AutoPostBack="true" runat="server">
                                    <asp:ListItem Text="สถานะการใช้งาน" Value=""></asp:ListItem>
                                    <asp:ListItem Text="ใช้งาน" Value="Y"></asp:ListItem>
                                    <asp:ListItem Text="ไม่ใช้งาน" Value="N"></asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="col-sm-1">
                                <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn btn-warning">
                                        <i class="icon fa-search"></i>
                                </asp:LinkButton>
                            </div>
                        </div>

                        <div class="clearfix visible-md-block"></div>
                        <br />
                        <table class="tablesaw table-striped table-bordered table-hover">
                            <thead>
                                <tr class="bg-blue-grey-100">
                                    <th class="text-center">รหัสส่วนผสม</th>
                                    <th class="text-center">ชื่อส่วนผสม</th>
                                    <th class="text-center">ใช้งาน</th>
                                    <th class="text-center">แก้ไข</th>
                                    <th class="text-center">ลบ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptList" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td class="text-center">
                                                <asp:Label ID="lblIngredientID" runat="server"></asp:Label>
                                                <asp:Label ID="lblSequenceID" runat="server" Visible="false"></asp:Label>
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="lblIngredientName" runat="server"></asp:Label>

                                            </td>
                                            <td class="text-center">
                                                <uc1:ucSwitchButton runat="server" ID="chkActiveStatus" AutoPostBack="true" OnCheckedChanged="chkActiveStatus_CheckedChanged" />
                                            </td>
                                            <td class="text-center">
                                                <asp:LinkButton ID="btnEdit" runat="server" CommandName="EDIT">
                                                    <i class="icon md-edit" style="font-size: 20px;"></i>
                                                </asp:LinkButton>
                                            </td>
                                            <td class="text-center">
                                                <asp:LinkButton ID="btnDelete" runat="server" CommandName="DELETE" OnClientClick="return confirm('Do you want delete data?');">
                                                    <i class="icon md-delete" style="font-size: 20px;"></i>
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlEdit" runat="server" CssClass="panel" Visible="false">
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>รหัสส่วนผสม :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-9">
                                <asp:TextBox ID="txtIngredientID" runat="server" CssClass="form-control" MaxLength="100" placeholder="รหัสส่วนผสม"></asp:TextBox>
                                <asp:Label ID="lblSequenceID" runat="server" Visible="false" Text="0"></asp:Label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>ชื่อส่วนผสม :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-9">
                                <asp:TextBox ID="txtIngredientName" runat="server" CssClass="form-control" MaxLength="100" placeholder="ชื่อส่วนผสม"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <br />
                            <div class="form-group col-sm-11">
                                <label>ส่วนผสม :</label>
                            </div>
                            <div style="text-align: right">
                                <asp:LinkButton ID="btnAddSkuIngredient" runat="server" CssClass="btn btn-primary">
                                    <span>+</span>
                                </asp:LinkButton>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <table class="tablesaw table-striped table-bordered table-hover">
                                <thead>
                                    <tr class="bg-blue-grey-100">
                                        <th class="text-center" style="width: 20%">กลุ่มชนิดสินค้า</th>
                                        <th class="text-center" style="width: 20%">ชนิดสินค้า</th>
                                        <th class="text-center" style="width: 10%">Amount</th>
                                        <th class="text-center" style="width: 10%">Quantity</th>
                                        <th class="text-center" style="width: 10%">Volume</th>
                                        <th class="text-center" style="width: 10%">ใช้งาน</th>
                                        <th class="text-center" style="width: 10%">แก้ไข</th>
                                        <th class="text-center" style="width: 10%">ลบ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptListIngredient" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <asp:Label ID="lblRowIndex" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblSequenceID" runat="server" Visible ="false" ></asp:Label>
                                                <asp:Label ID="lblSKUIngredientID" runat="server" Visible="false"></asp:Label>
                                                <td class="text-center">
                                                    <asp:Label ID="lblSKUGroup" runat="server"></asp:Label>
                                                    <asp:Label ID="lblSKUGroupID" runat="server" Visible="false"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="lblSKUID" runat="server" ></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="lblSKUAmount" runat="server"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="lblSKUQuantity" runat="server"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="lblSKUVolume" runat="server"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <uc1:ucSwitchButton runat="server" ID="chkActiveStatus" Enabled="false" />
                                                    <asp:Label ID="lblSKUIngredientStatus" runat="server" Visible="false"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:LinkButton ID="btnEdit" runat="server" CommandName="EDIT">
                                                    <i class="icon md-edit" style="font-size: 20px;"></i>
                                                    </asp:LinkButton>
                                                </td>
                                                <td class="text-center">
                                                    <asp:LinkButton ID="btnDelete" runat="server" CommandName="DELETE" OnClientClick="return confirm('Do you want delete data?');">
                                                    <i class="icon md-delete" style="font-size: 20px;"></i>
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                            <br />
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>ใช้งาน</label>
                            </div>

                            <div class="col-sm-9">
                                <uc1:ucSwitchButton runat="server" ID="chkActive" />
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <hr />
                        <div class="form-group form-material">
                            <div class="col-sm-12 col-sm-offset-9">
                                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary">
                                    <span>Save</span>
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-warning">
                                    <span>Cancel</span>
                                </asp:LinkButton>
                            </div>
                        </div>

                    </div>
                </asp:Panel>
                <%--Ingredient--%>
                <asp:Panel ID="pnlEditIngredient" runat="server" CssClass="panel" Visible="false">
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Group SKU :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-9">
                                <asp:DropDownList ID="ddlSKUGroup" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="กลุ่มชนิดสินค้า" AutoPostBack="true" ></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>SKU :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-9">
                                <asp:DropDownList ID="ddlSKU" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="ชนิดสินค้า"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Amount :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-9">
                                <asp:TextBox ID="txtSKUAmount" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="Amount"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Quantity :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-4">
                                <asp:TextBox ID="txtSKUQuantity" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="Quantity"></asp:TextBox>
                            </div>
                            <div class="form-group col-sm-1">
                                <label>Volume :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-4">
                                <asp:TextBox ID="txtSKUVolume" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="Volume"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-sm-2">
                            <label>ใช้งาน</label>
                        </div>

                        <div class="col-sm-9">
                            <uc1:ucSwitchButton runat="server" ID="chkActiveSKUIngredient" />
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr />
                    <div class="form-group form-material">
                        <div class="col-sm-12 col-sm-offset-10">
                            <asp:LinkButton ID="btnSaveSkuIngredient" runat="server" CssClass="btn btn-primary">
                                    <span>Save</span>
                            </asp:LinkButton>
                            <asp:LinkButton ID="btnCanelSkuIngredient" runat="server" CssClass="btn btn-warning">
                                    <span>Cancel</span>
                            </asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpFooter" runat="server">
</asp:Content>
