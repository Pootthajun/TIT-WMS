﻿Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports WMS_BL

Public Class MS_Customer
    Inherits System.Web.UI.Page

    Private ReadOnly Property UserSession As LoginSessionData
        Get
            Return Session(CommonBL.UserLoginSessionName)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect("frmLogin.aspx?rnd=" & DateTime.Now.Millisecond)
                Exit Sub

            Else
                BindList("")
                ClearForm()
            End If
        End If
    End Sub


    Private Sub BindDDL()
        MasterDataBL.GetListDDL_CustomerType(ddlCustomerType)
        MasterDataBL.GetListDDL_PrefixName(ddlPrefixName)

        MasterDataBL.GetListDDL_CustomerType(ddlCustomerTypeSearch)
    End Sub

    Private Sub BindList(CustomerID)
        'Customer
        Dim dt As DataTable = MasterDataBL.GetList_Customer(txtSearchID.Text, ddlCustomerTypeSearch.SelectedValue, txtSearchName.Text, ddlstatus.SelectedValue)
        rptList.DataSource = dt
        rptList.DataBind()

        'customer_contact
        Dim dtContact As DataTable = MasterDataBL.GetList_Customer_Contact("", Nothing, CustomerID)
        rptListContact.DataSource = dtContact
        rptListContact.DataBind()

        'customer_address
        Dim dtAddress As DataTable = MasterDataBL.GetList_Customer_Address("", Nothing, CustomerID)
        rptListAddress.DataSource = dtAddress
        rptListAddress.DataBind()
    End Sub
#Region "customer"
    Protected Sub chkActiveStatus_CheckedChanged(sender As ucSwitchButton, e As System.EventArgs) 'Customer
        Dim chkActiveStatus As ucSwitchButton = sender
        Dim itm As RepeaterItem = chkActiveStatus.Parent
        Dim lblCustomerID As Label = itm.FindControl("lblCustomerID")
        Dim lblCustomerName As Label = itm.FindControl("lblCustomerName")

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.UpdateCustomerstatus(UserSession.UserName, lblCustomerID.Text, chkActiveStatus.Checked)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Update status fail');", True)
            LogFileData.LogError(UserSession, ret.ErrorMessage)
        Else
            LogFileData.LogTrans(UserSession, "Update Customer Status " & lblCustomerName.Text & " Status : " & chkActiveStatus.Checked.ToString)
        End If
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblCustomerID As Label = e.Item.FindControl("lblCustomerID")
        Dim lblCustomerCode As Label = e.Item.FindControl("lblCustomerCode")
        Dim lblCustomertype As Label = e.Item.FindControl("lblCustomertype")
        Dim lblCustomerName As Label = e.Item.FindControl("lblCustomerName")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")



        lblCustomerID.Text = e.Item.DataItem("Customer_ID")
        lblCustomerCode.Text = e.Item.DataItem("Customer_Code")
        lblCustomertype.Text = e.Item.DataItem("Customer_Type")
        lblCustomerName.Text = e.Item.DataItem("full_name")
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblCustomerID As Label = e.Item.FindControl("lblCustomerID")
        Dim lblCustomerName As Label = e.Item.FindControl("lblCustomerName")

        Select Case e.CommandName
            Case "EDIT"
                ClearForm()
                FillInData(lblCustomerID.Text)

                BindList(lblCustomerID.Text)

                pnlList.Visible = False
                pnlEdit.Visible = True
                LogFileData.LogTrans(UserSession, "คลิกปุ่ม Edit Customer " & lblCustomerName.Text)

            Case "DELETE"
                Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.DeleteCustomer(lblCustomerID.Text)
                If ret.IsSuccess = True Then
                    LogFileData.LogTrans(UserSession, "Delete Customer " & lblCustomerName.Text)

                    BindList("")
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Delete Data Fail');", True)
                    LogFileData.LogError(UserSession, ret.ErrorMessage)
                End If
        End Select
    End Sub

    Private Sub FillInData(CustomerID As Long)
        Dim ret As MsCustomerLinqDB = MasterDataBL.GetCustomerData(CustomerID)
        If ret.CUSTOMER_ID > 0 Then
            lblCustomerID.Text = ret.CUSTOMER_ID
            txtCustomerID.Text = ret.CUSTOMER_CODE
            ddlCustomerType.SelectedValue = ret.CUSTOMER_TYPE_ID
            txtCustomerNameTH.Text = ret.CUSTOMER_TNAME
            txtCustomerNameEN.Text = ret.CUSTOMER_ENAME
            txtAliasName.Text = ret.CUSTOMER_ALIAS
            txtTaxNo.Text = ret.CUSTOMER_TAX_NO
            txtMobile.Text = ret.CUSTOMER_MOBILE
            txtTel.Text = ret.CUSTOMER_PHONE
            txtFax.Text = ret.CUSTOMER_FAX
            txtEmail.Text = ret.CUSTOMER_EMAIL
            chkActive.Checked = (ret.ACTIVE_STATUS = "Y")

            'ข้อมูลการเข้าระบบ ไม่ต้องให้แก้ไข
            txtCustomerID.Enabled = False
        End If
        ret = Nothing
    End Sub

    Private Sub btnAddCustomer_Click(sender As Object, e As EventArgs) Handles btnAddCustomer.Click
        pnlEdit.Visible = True
        pnlList.Visible = False
        LogFileData.LogTrans(UserSession, "คลิกปุ่มเพิ่ม Customer")

        ClearForm()
    End Sub

    Private Sub ClearForm()
        lblCustomerID.Text = "0"
        txtCustomerID.Text = ""
        txtCustomerNameTH.Text = ""
        txtCustomerNameEN.Text = ""
        txtAliasName.Text = ""
        txtTaxNo.Text = ""
        txtMobile.Text = ""
        txtTel.Text = ""
        txtFax.Text = ""
        txtEmail.Text = ""
        chkActive.Checked = True
        BindList(0)
        BindDDL()

        pnlSearch.Visible = False

        txtCustomerID.Enabled = True
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save Customer")
        If ValidateData() = True Then


            Dim lnq As MsCustomerLinqDB = MasterDataBL.SaveCustomerData(UserSession.UserName, lblCustomerID.Text, txtCustomerID.Text, txtCustomerNameTH.Text, txtCustomerNameEN.Text, txtAliasName.Text, txtTaxNo.Text, ddlCustomerType.SelectedValue, txtTel.Text, txtMobile.Text, txtFax.Text, txtEmail.Text, chkActive.Checked)

            'ข้อมูลตาราง contact
            Dim DTcontact As New DataTable
            DTcontact = GetContactList()
            Dim lnqContact As MsCustomerContactLinqDB = MasterDataBL.SaveCustomer_ContactData(UserSession.UserName, lnq.CUSTOMER_ID, DTcontact)

            'ข้อมูลตาราง Address
            Dim DTAddress As New DataTable
            DTAddress = GetAddressList()
            Dim lnqAddress As MsCustomerAddressLinqDB = MasterDataBL.SaveCustomer_AddressData(UserSession.UserName, lnq.CUSTOMER_ID, DTAddress)


            If lnq.CUSTOMER_ID > 0 Then
                lblCustomerID.Text = lnq.CUSTOMER_ID
                LogFileData.LogTrans(UserSession, "Save Customer " & txtCustomerNameTH.Text & " สำเร็จ")
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Data Complete');", True)
                pnlEdit.Visible = False
                pnlList.Visible = True
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Fail');", True)
            End If
            lnq = Nothing
        End If
        BindList("")
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        pnlEdit.Visible = False
        pnlList.Visible = True
        ClearForm()
    End Sub

    Private Function ValidateData() As Boolean
        'customer
        If txtCustomerID.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุไอดีลูกค้า');", True)
            Return False
        End If
        If ddlCustomerType.SelectedValue = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกประเภทลูกค้า');", True)
            Return False
        End If
        If txtCustomerNameTH.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุชื่อลูกค้า');", True)
            Return False
        End If
        If txtTaxNo.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุเลขประจำตัวผู้เสียภาษี');", True)
            Return False
        End If
        Return True
    End Function

#End Region

#Region "Customer_Contact"

    Private Function GetContactList() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("Customer_Contact_ID", GetType(Long))
        dt.Columns.Add("Customer_ID", GetType(Long))
        dt.Columns.Add("Prefix_ID", GetType(Long))
        dt.Columns.Add("prefix_name")
        dt.Columns.Add("First_Name")
        dt.Columns.Add("Last_Name")
        dt.Columns.Add("MobileNo")
        dt.Columns.Add("TelephoneNo")
        dt.Columns.Add("FaxNo")
        dt.Columns.Add("Email")
        dt.Columns.Add("Active_Status")

        For Each itm As RepeaterItem In rptListContact.Items
            Dim lblContactCode As Label = itm.FindControl("lblContactCode")
            Dim lblContactID As Label = itm.FindControl("lblContactID")

            Dim lblPrefixID As Label = itm.FindControl("lblPrefixID")
            Dim lblPrefix As Label = itm.FindControl("lblPrefix")
            Dim lblContactFName As Label = itm.FindControl("lblContactFName")
            Dim lblContactLName As Label = itm.FindControl("lblContactLName")

            Dim lblContactTel As Label = itm.FindControl("lblContactTel")
            Dim lblContactMobile As Label = itm.FindControl("lblContactMobile")
            Dim lblContactFax As Label = itm.FindControl("lblContactFax")
            Dim lblContactEmail As Label = itm.FindControl("lblContactEmail")
            Dim lblContactStatus As Label = itm.FindControl("lblContactStatus")

            Dim dr As DataRow = dt.NewRow
            dr("Customer_Contact_ID") = lblContactID.Text
            dr("Customer_ID") = 0
            dr("Prefix_ID") = lblPrefixID.Text
            dr("prefix_name") = lblPrefix.Text
            dr("First_Name") = lblContactFName.Text
            dr("Last_Name") = lblContactLName.Text
            dr("MobileNo") = lblContactMobile.Text
            dr("TelephoneNo") = lblContactTel.Text
            dr("FaxNo") = lblContactFax.Text
            dr("Email") = lblContactEmail.Text
            dr("Active_Status") = lblContactStatus.Text

            dt.Rows.Add(dr)
        Next
        Return dt
    End Function

    Private Sub rptListContact_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptListContact.ItemDataBound
        Dim lblContactCode As Label = e.Item.FindControl("lblContactCode")
        Dim lblContactID As Label = e.Item.FindControl("lblContactID")

        Dim lblPrefix As Label = e.Item.FindControl("lblPrefix")
        Dim lblPrefixID As Label = e.Item.FindControl("lblPrefixID")
        Dim lblContactFName As Label = e.Item.FindControl("lblContactFName")
        Dim lblContactLName As Label = e.Item.FindControl("lblContactLName")

        Dim lblContactTel As Label = e.Item.FindControl("lblContactTel")
        Dim lblContactMobile As Label = e.Item.FindControl("lblContactMobile")
        Dim lblContactFax As Label = e.Item.FindControl("lblContactFax")
        Dim lblContactEmail As Label = e.Item.FindControl("lblContactEmail")
        Dim lblContactStatus As Label = e.Item.FindControl("lblContactStatus")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        Dim lblRowIndex As Label = e.Item.FindControl("lblRowIndex")

        lblRowIndex.Text = e.Item.ItemIndex
        lblContactCode.Text = e.Item.DataItem("Customer_ID") 'ID ของตาราง Customer เชื่อม Fk
        lblContactID.Text = e.Item.DataItem("Customer_Contact_ID") 'ID ของตาราง Contact 

        lblPrefixID.Text = e.Item.DataItem("Prefix_ID")
        lblPrefix.Text = e.Item.DataItem("prefix_name")
        lblContactFName.Text = e.Item.DataItem("First_Name")
        lblContactLName.Text = e.Item.DataItem("Last_Name")

        lblContactTel.Text = e.Item.DataItem("TelephoneNo")
        lblContactMobile.Text = e.Item.DataItem("MobileNo")
        lblContactFax.Text = e.Item.DataItem("FaxNo")
        lblContactEmail.Text = e.Item.DataItem("Email")
        lblContactStatus.Text = e.Item.DataItem("active_status")
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")

    End Sub

    Private Sub rptListContact_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptListContact.ItemCommand
        Dim lblRowIndex As Label = e.Item.FindControl("lblRowIndex")
        Dim lblContactID As Label = e.Item.FindControl("lblContactID")

        Dim lblPrefix As Label = e.Item.FindControl("lblPrefix")
        Dim lblPrefixID As Label = e.Item.FindControl("lblPrefixID")
        Dim lblContactFName As Label = e.Item.FindControl("lblContactFName")
        Dim lblContactLName As Label = e.Item.FindControl("lblContactLName")

        Dim lblContactTel As Label = e.Item.FindControl("lblContactTel")
        Dim lblContactMobile As Label = e.Item.FindControl("lblContactMobile")
        Dim lblContactFax As Label = e.Item.FindControl("lblContactFax")
        Dim lblContactEmail As Label = e.Item.FindControl("lblContactEmail")
        Dim lblContactStatus As Label = e.Item.FindControl("lblContactStatus")
        Select Case e.CommandName
            Case "EDIT"
                ClearFormContact()

                lblRowIndex.Text = e.Item.ItemIndex
                Session("row") = lblRowIndex.Text

                ddlPrefixName.SelectedValue = lblPrefixID.Text
                txtContactFName.Text = lblContactFName.Text
                txtContactLName.Text = lblContactLName.Text
                txtContactMobile.Text = lblContactMobile.Text
                txtContactTel.Text = lblContactTel.Text
                txtContactFax.Text = lblContactFax.Text
                txtContactEmail.Text = lblContactEmail.Text
                If lblContactStatus.Text = "Y" Then
                    chkActiveContact.Checked = True
                Else
                    chkActiveContact.Checked = False
                End If

                pnlList.Visible = False
                pnlEdit.Visible = False
                pnlEditContact.Visible = True

                LogFileData.LogTrans(UserSession, "คลิกปุ่ม Edit ContactName " & lblContactFName.Text)
            Case "DELETE"
                Dim dt As DataTable = GetContactList()
                dt.Rows.RemoveAt(e.Item.ItemIndex)

                rptListContact.DataSource = dt
                rptListContact.DataBind()
        End Select
    End Sub

    Private Sub btnAddContact_Click(sender As Object, e As EventArgs) Handles btnAddContact.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่มเพิ่ม Customer Contact ")
        pnlList.Visible = False
        pnlEdit.Visible = False
        pnlEditContact.Visible = True
        pnlEditAddress.Visible = False
        ClearFormContact()
    End Sub

    Private Sub ClearFormContact()
        Session("row") = ""
        txtContactFName.Text = ""
        txtContactLName.Text = ""
        txtContactMobile.Text = ""
        txtContactTel.Text = ""
        txtContactFax.Text = ""
        txtContactEmail.Text = ""
        chkActiveContact.Checked = True
        ddlPrefixName.SelectedValue = 0
    End Sub

    Private Sub btnSaveContact_Click(sender As Object, e As EventArgs) Handles btnSaveContact.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save ContactName " & txtContactFName.Text)
        If ValidateContactData() = True Then
            pnlList.Visible = False
            pnlEdit.Visible = True
            pnlEditContact.Visible = False
            pnlEditAddress.Visible = False

            Dim dt As DataTable = GetContactList()
            If Session("row").ToString() = "" Then
                'Add
                Dim dr As DataRow = dt.NewRow
                dr("Customer_Contact_ID") = dt.Rows.Count + 1
                dr("Customer_ID") = 0
                dr("Prefix_ID") = ddlPrefixName.SelectedValue
                dr("prefix_name") = ddlPrefixName.SelectedItem
                dr("First_Name") = txtContactFName.Text
                dr("Last_Name") = txtContactLName.Text
                dr("MobileNo") = txtContactMobile.Text
                dr("TelephoneNo") = txtContactTel.Text
                dr("FaxNo") = txtContactFax.Text
                dr("Email") = txtContactEmail.Text
                If chkActiveContact.Checked Then
                    dr("Active_Status") = "Y"
                Else
                    dr("Active_Status") = "N"
                End If

                dt.Rows.Add(dr)
            Else
                'Edit
                Dim itm As RepeaterItem = rptListContact.Items(Session("row").ToString())

                dt.Rows(Session("row").ToString())("Customer_Contact_ID") = dt.Rows.Count + 1
                dt.Rows(Session("row").ToString())("Customer_ID") = 0
                dt.Rows(Session("row").ToString())("Prefix_ID") = ddlPrefixName.SelectedValue
                dt.Rows(Session("row").ToString())("prefix_name") = ddlPrefixName.SelectedItem
                dt.Rows(Session("row").ToString())("First_Name") = txtContactFName.Text
                dt.Rows(Session("row").ToString())("Last_Name") = txtContactLName.Text
                dt.Rows(Session("row").ToString())("MobileNo") = txtContactMobile.Text
                dt.Rows(Session("row").ToString())("TelephoneNo") = txtContactTel.Text
                dt.Rows(Session("row").ToString())("FaxNo") = txtContactFax.Text
                dt.Rows(Session("row").ToString())("Email") = txtContactEmail.Text
                If chkActiveContact.Checked Then
                    dt.Rows(Session("row").ToString())("Active_Status") = "Y"
                Else
                    dt.Rows(Session("row").ToString())("Active_Status") = "N"
                End If
            End If

            rptListContact.DataSource = dt
            rptListContact.DataBind()
        End If


    End Sub

    Private Sub btnCanelContact_Click(sender As Object, e As EventArgs) Handles btnCanelContact.Click
        pnlList.Visible = False
        pnlEdit.Visible = True
        pnlEditContact.Visible = False
        pnlEditAddress.Visible = False
    End Sub
    Private Function ValidateContactData() As Boolean
        If ddlPrefixName.SelectedValue = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกคำนำหน้าชื่อ');", True)
            Return False
        End If
        If txtContactFName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุชื่อจริง');", True)
            Return False
        End If
        If txtContactLName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุนามสกุล');", True)
            Return False
        End If
        If txtContactTel.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุเบอร์โทรศัพท์');", True)
            Return False
        End If
        If txtContactMobile.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุเบอร์โทรศัพท์มือถือ');", True)
            Return False
        End If
        If txtContactEmail.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุอีเมมล์');", True)
            Return False
        End If
        Return True
    End Function

#End Region

#Region "Customer_Addresss"

    Private Function GetAddressList() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("Customer_Address_ID", GetType(Long))
        dt.Columns.Add("Customer_ID", GetType(Long))
        dt.Columns.Add("House_No")
        dt.Columns.Add("Building_Name")
        dt.Columns.Add("moo")
        dt.Columns.Add("soi_name")
        dt.Columns.Add("road_name")
        dt.Columns.Add("Tumbon_ID", GetType(Long))
        dt.Columns.Add("District_ID", GetType(Long))
        dt.Columns.Add("Province_ID", GetType(Long))
        dt.Columns.Add("Country_ID", GetType(Long))
        dt.Columns.Add("Address")
        dt.Columns.Add("Postcode")
        dt.Columns.Add("Active_Status")

        For Each itm As RepeaterItem In rptListAddress.Items
            Dim lblAddressID As Label = itm.FindControl("lblAddressID")
            Dim lblAddressCode As Label = itm.FindControl("lblAddressCode")
            Dim lblAddressHouseNo As Label = itm.FindControl("lblAddressHouseNo")
            Dim lblAddressBuildingName As Label = itm.FindControl("lblAddressBuildingName")
            Dim lblAddressMoo As Label = itm.FindControl("lblAddressMoo")
            Dim lblAddressSoi As Label = itm.FindControl("lblAddressSoi")
            Dim lblAddressRoad As Label = itm.FindControl("lblAddressRoad")
            Dim lblAddressTumbonID As Label = itm.FindControl("lblAddressTumbonID")
            Dim lblAddressDistrictID As Label = itm.FindControl("lblAddressDistrictID")
            Dim lblAddressProvinceID As Label = itm.FindControl("lblAddressProvinceID")
            Dim lblAddressCountryID As Label = itm.FindControl("lblAddressCountryID")
            Dim lblAddressPascode As Label = itm.FindControl("lblAddressPascode")
            Dim lblAddressStatus As Label = itm.FindControl("lblAddressStatus")
            Dim lblAddress As Label = itm.FindControl("lblAddress")

            Dim dr As DataRow = dt.NewRow
            dr("Customer_Address_ID") = lblAddressID.Text
            dr("Customer_ID") = 0
            dr("House_No") = lblAddressHouseNo.Text
            dr("Building_Name") = lblAddressBuildingName.Text
            dr("moo") = lblAddressMoo.Text
            dr("soi_name") = lblAddressSoi.Text
            dr("road_name") = lblAddressRoad.Text
            dr("Tumbon_ID") = lblAddressTumbonID.Text
            dr("District_ID") = lblAddressDistrictID.Text
            dr("Province_ID") = lblAddressProvinceID.Text
            dr("Country_ID") = lblAddressCountryID.Text
            dr("Address") = lblAddress.Text
            dr("Postcode") = lblAddressPascode.Text
            dr("Active_Status") = lblAddressStatus.Text
            dt.Rows.Add(dr)
        Next
        Return dt
    End Function

    Private Sub rptListAddress_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptListAddress.ItemDataBound
        Dim lblAddressID As Label = e.Item.FindControl("lblAddressID")
        Dim lblAddressCode As Label = e.Item.FindControl("lblAddressCode")
        Dim lblAddressHouseNo As Label = e.Item.FindControl("lblAddressHouseNo")
        Dim lblAddressBuildingName As Label = e.Item.FindControl("lblAddressBuildingName")
        Dim lblAddressMoo As Label = e.Item.FindControl("lblAddressMoo")
        Dim lblAddressSoi As Label = e.Item.FindControl("lblAddressSoi")
        Dim lblAddressRoad As Label = e.Item.FindControl("lblAddressRoad")
        Dim lblAddressTumbonID As Label = e.Item.FindControl("lblAddressTumbonID")
        Dim lblAddressDistrictID As Label = e.Item.FindControl("lblAddressDistrictID")
        Dim lblAddressProvinceID As Label = e.Item.FindControl("lblAddressProvinceID")
        Dim lblAddressCountryID As Label = e.Item.FindControl("lblAddressCountryID")
        Dim lblAddressPascode As Label = e.Item.FindControl("lblAddressPascode")
        Dim lblAddressStatus As Label = e.Item.FindControl("lblAddressStatus")

        Dim lblAddress As Label = e.Item.FindControl("lblAddress")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        Dim lblRowIndexAd As Label = e.Item.FindControl("lblRowIndexAd")

        lblRowIndexAd.Text = e.Item.ItemIndex

        lblAddressID.Text = e.Item.DataItem("Customer_Address_ID")
        lblAddressCode.Text = e.Item.DataItem("Customer_ID")
        lblAddressHouseNo.Text = e.Item.DataItem("House_No")
        lblAddressBuildingName.Text = e.Item.DataItem("Building_Name")
        lblAddressMoo.Text = e.Item.DataItem("moo")
        lblAddressSoi.Text = e.Item.DataItem("soi_name")
        lblAddressRoad.Text = e.Item.DataItem("road_name")
        lblAddressTumbonID.Text = e.Item.DataItem("Tumbon_ID")
        lblAddressDistrictID.Text = e.Item.DataItem("District_ID")
        lblAddressProvinceID.Text = e.Item.DataItem("Province_ID")
        lblAddressCountryID.Text = e.Item.DataItem("Country_ID")
        lblAddressPascode.Text = e.Item.DataItem("Postcode")
        lblAddressStatus.Text = e.Item.DataItem("Active_Status")

        lblAddress.Text = e.Item.DataItem("Address")
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")
    End Sub

    Private Sub rptListAddress_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptListAddress.ItemCommand
        Dim lblAddressID As Label = e.Item.FindControl("lblAddressID")
        Dim lblAddressCode As Label = e.Item.FindControl("lblAddressCode")
        Dim lblAddressHouseNo As Label = e.Item.FindControl("lblAddressHouseNo")
        Dim lblAddressBuildingName As Label = e.Item.FindControl("lblAddressBuildingName")
        Dim lblAddressMoo As Label = e.Item.FindControl("lblAddressMoo")
        Dim lblAddressSoi As Label = e.Item.FindControl("lblAddressSoi")
        Dim lblAddressRoad As Label = e.Item.FindControl("lblAddressRoad")
        Dim lblAddressTumbonID As Label = e.Item.FindControl("lblAddressTumbonID")
        Dim lblAddressDistrictID As Label = e.Item.FindControl("lblAddressDistrictID")
        Dim lblAddressProvinceID As Label = e.Item.FindControl("lblAddressProvinceID")
        Dim lblAddressCountryID As Label = e.Item.FindControl("lblAddressCountryID")
        Dim lblAddressPascode As Label = e.Item.FindControl("lblAddressPascode")
        Dim lblAddressStatus As Label = e.Item.FindControl("lblAddressStatus")
        Dim lblRowIndexAd As Label = e.Item.FindControl("lblRowIndexAd")
        Dim lblAddress As Label = e.Item.FindControl("lblAddress")
        Select Case e.CommandName
            Case "EDIT"


                lblRowIndexAd.Text = e.Item.ItemIndex
                Session("rowAddress") = lblRowIndexAd.Text

                txthouseNo.Text = lblAddressHouseNo.Text
                txtBuildingName.Text = lblAddressBuildingName.Text
                txtmoo.Text = lblAddressMoo.Text
                txtsoi.Text = lblAddressSoi.Text
                txtroad.Text = lblAddressRoad.Text

                MasterDataBL.GetListDDL_Country(ddlCountry)
                ddlCountry.SelectedValue = lblAddressCountryID.Text

                MasterDataBL.GetListDDL_Province(ddlProvince, ddlCountry.SelectedValue)
                ddlProvince.SelectedValue = lblAddressProvinceID.Text

                MasterDataBL.GetListDDL_District(ddlDistrict, ddlProvince.SelectedValue)
                ddlDistrict.SelectedValue = lblAddressDistrictID.Text

                MasterDataBL.GetListDDL_Tumbon(ddlTumbon, ddlDistrict.SelectedValue)
                ddlTumbon.SelectedValue = lblAddressTumbonID.Text

                txtPoscode.Text = lblAddressPascode.Text


                If lblAddressStatus.Text = "Y" Then
                    chkActiveAddress.Checked = True
                Else
                    chkActiveAddress.Checked = False
                End If

                pnlList.Visible = False
                pnlEdit.Visible = False
                pnlEditContact.Visible = False
                pnlEditAddress.Visible = True

                LogFileData.LogTrans(UserSession, "คลิกปุ่ม Edit Address " & lblAddress.Text)
            Case "DELETE"
                Dim dt As DataTable = GetAddressList()
                dt.Rows.RemoveAt(e.Item.ItemIndex)

                rptListAddress.DataSource = dt
                rptListAddress.DataBind()
        End Select
    End Sub

    Private Sub ClearFormAddress()
        Session("rowAddress") = ""

        txthouseNo.Text = ""
        txtBuildingName.Text = ""
        txtmoo.Text = ""
        txtsoi.Text = ""
        txtroad.Text = ""

        MasterDataBL.GetListDDL_Country(ddlCountry)
        ddlCountry.SelectedValue = "1"
        MasterDataBL.GetListDDL_Province(ddlProvince, ddlCountry.SelectedValue)
        MasterDataBL.GetListDDL_District(ddlDistrict, ddlProvince.SelectedValue)
        MasterDataBL.GetListDDL_Tumbon(ddlTumbon, ddlDistrict.SelectedValue)
        txtPoscode.Text = ""

    End Sub

    Private Sub btnAddAddress_Click(sender As Object, e As EventArgs) Handles btnAddAddress.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่มเพิ่ม Customer Address ")
        pnlList.Visible = False
        pnlEdit.Visible = False
        pnlEditContact.Visible = False
        pnlEditAddress.Visible = True

        ClearFormAddress()
    End Sub

    Private Sub btnCancelAddress_Click(sender As Object, e As EventArgs) Handles btnCancelAddress.Click
        pnlList.Visible = False
        pnlEdit.Visible = True
        pnlEditContact.Visible = False
        pnlEditAddress.Visible = False
    End Sub

    Private Sub btnSaveAddress_Click(sender As Object, e As EventArgs) Handles btnSaveAddress.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save Address " & txthouseNo.Text)
        If ValidateAddressData() = True Then
            pnlList.Visible = False
            pnlEdit.Visible = True
            pnlEditContact.Visible = False
            pnlEditAddress.Visible = False

            Dim dt As DataTable = GetAddressList()
            If Session("rowAddress").ToString() = "" Then
                'Add
                Dim dr As DataRow = dt.NewRow
                dr("Customer_Address_ID") = dt.Rows.Count + 1
                dr("Customer_ID") = 0
                dr("House_No") = txthouseNo.Text
                dr("Building_Name") = txtBuildingName.Text
                dr("moo") = txtmoo.Text
                dr("soi_name") = txtsoi.Text
                dr("road_name") = txtroad.Text
                dr("Tumbon_ID") = ddlTumbon.SelectedValue
                dr("District_ID") = ddlDistrict.SelectedValue
                dr("Province_ID") = ddlProvince.SelectedValue
                dr("Country_ID") = ddlCountry.SelectedValue
                dr("Postcode") = txtPoscode.Text

                Dim TumbonName As String = ddlTumbon.SelectedItem.ToString()
                Dim DistrictName As String = ddlDistrict.SelectedItem.ToString()
                Dim ProvinceName As String = ddlProvince.SelectedItem.ToString()
                Dim CountryName As String = ddlCountry.SelectedItem.ToString()

                dr("Address") = "บ้านเลขที่ " & txthouseNo.Text & " อาคารที่ตั้ง " & txtBuildingName.Text & " หมู่ " & txtmoo.Text & " ซอย " & txtsoi.Text & " ถนน " & txtroad.Text & " ตำบล/แขวง " & TumbonName & " อำเภอ/เขต " & DistrictName & " จังหวัด " & ProvinceName & " ประเทศ " & CountryName & " รหัสไปรษณีย์ " & txtPoscode.Text & ""

                If chkActiveAddress.Checked Then
                    dr("Active_Status") = "Y"
                Else
                    dr("Active_Status") = "N"
                End If

                dt.Rows.Add(dr)
            Else
                'Edit
                Dim itm As RepeaterItem = rptListAddress.Items(Session("rowAddress").ToString())

                dt.Rows(Session("rowAddress").ToString())("Customer_Address_ID") = dt.Rows.Count + 1
                dt.Rows(Session("rowAddress").ToString())("Customer_ID") = 0
                dt.Rows(Session("rowAddress").ToString())("House_No") = txthouseNo.Text
                dt.Rows(Session("rowAddress").ToString())("Building_Name") = txtBuildingName.Text
                dt.Rows(Session("rowAddress").ToString())("moo") = txtmoo.Text
                dt.Rows(Session("rowAddress").ToString())("soi_name") = txtsoi.Text
                dt.Rows(Session("rowAddress").ToString())("road_name") = txtroad.Text
                dt.Rows(Session("rowAddress").ToString())("Tumbon_ID") = ddlTumbon.SelectedValue
                dt.Rows(Session("rowAddress").ToString())("District_ID") = ddlDistrict.SelectedValue
                dt.Rows(Session("rowAddress").ToString())("Province_ID") = ddlProvince.SelectedValue
                dt.Rows(Session("rowAddress").ToString())("Country_ID") = ddlCountry.SelectedValue

                Dim TumbonName As String = ddlTumbon.SelectedItem.ToString()
                Dim DistrictName As String = ddlDistrict.SelectedItem.ToString()
                Dim ProvinceName As String = ddlProvince.SelectedItem.ToString()
                Dim CountryName As String = ddlCountry.SelectedItem.ToString()

                dt.Rows(Session("rowAddress").ToString())("Address") = "บ้านเลขที่ " & txthouseNo.Text & " อาคารที่ตั้ง " & txtBuildingName.Text & " หมู่ " & txtmoo.Text & " ซอย " & txtsoi.Text & " ถนน " & txtroad.Text & " ตำบล/แขวง " & TumbonName & " อำเภอ/เขต " & DistrictName & " จังหวัด " & ProvinceName & " ประเทศ " & CountryName & " รหัสไปรษณีย์ " & txtPoscode.Text & ""
                dt.Rows(Session("rowAddress").ToString())("Postcode") = txtPoscode.Text

                If chkActiveAddress.Checked Then
                    dt.Rows(Session("rowAddress").ToString())("Active_Status") = "Y"
                Else
                    dt.Rows(Session("rowAddress").ToString())("Active_Status") = "N"
                End If
            End If

            rptListAddress.DataSource = dt
            rptListAddress.DataBind()
        End If
    End Sub

    Private Function ValidateAddressData() As Boolean

        If txthouseNo.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุบ้านเลขที่');", True)
            Return False
        End If
        If txtBuildingName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุอาคารที่ตั้ง');", True)
            Return False
        End If
        If txtmoo.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุหมู่');", True)
            Return False
        End If
        If txtsoi.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุซอย');", True)
            Return False
        End If
        If txtroad.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุถนน');", True)
            Return False
        End If
        If ddlTumbon.SelectedValue = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกตำบล/แขวง');", True)
            Return False
        End If
        If ddlDistrict.SelectedValue = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกอำเภอ/เขต');", True)
            Return False
        End If
        If ddlProvince.SelectedValue = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกจังหวัด');", True)
            Return False
        End If
        If ddlCountry.SelectedValue = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกประเทศ');", True)
            Return False
        End If
        If txtPoscode.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุรหัสไปรษณีย์');", True)
            Return False
        End If
        Return True
    End Function


    Private Sub ddlDistrict_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDistrict.SelectedIndexChanged
        'Dim sql As String = "select Province_ID From MS_Address_District where District_ID = '" & ddlDistrict.SelectedValue & "'"
        'Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        'For i As Integer = 0 To dt.Rows.Count - 1
        '    Dim Province_ID As String = dt.Rows(i)("Province_ID").ToString()
        '    MasterDataBL.GetListDDL_Province(ddlProvince, Province_ID)
        'Next

        MasterDataBL.GetListDDL_Tumbon(ddlTumbon, ddlDistrict.SelectedValue)
    End Sub

    Private Sub ddlProvince_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProvince.SelectedIndexChanged
        'Dim sql As String = "select Country_ID From MS_Address_Province where Province_ID = '" & ddlProvince.SelectedValue & "'"
        'Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        'For i As Integer = 0 To dt.Rows.Count - 1
        '    Dim Country_ID As String = dt.Rows(i)("Country_ID").ToString()
        '    MasterDataBL.GetListDDL_Country(ddlCountry, Country_ID)
        'Next

        MasterDataBL.GetListDDL_District(ddlDistrict, ddlProvince.SelectedValue)
    End Sub

    Private Sub ddlCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCountry.SelectedIndexChanged
        MasterDataBL.GetListDDL_Province(ddlProvince, ddlCountry.SelectedValue)
        MasterDataBL.GetListDDL_District(ddlDistrict, ddlProvince.SelectedValue)
        MasterDataBL.GetListDDL_Tumbon(ddlTumbon, ddlDistrict.SelectedValue)
    End Sub

    Private Sub btnOpenSearch_Click(sender As Object, e As EventArgs) Handles btnOpenSearch.Click
        pnlSearch.Visible = True
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click, txtSearchID.TextChanged, ddlCustomerTypeSearch.SelectedIndexChanged, txtSearchName.TextChanged, ddlstatus.SelectedIndexChanged
        BindList(0)
    End Sub

#End Region
End Class