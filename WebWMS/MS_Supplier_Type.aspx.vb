﻿Imports LinqDB.TABLE
Imports WMS_BL

Public Class MS_Supplier_Type
    Inherits System.Web.UI.Page

    Private ReadOnly Property UserSession As LoginSessionData
        Get
            Return Session(CommonBL.UserLoginSessionName)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect("frmLogin.aspx?rnd=" & DateTime.Now.Millisecond)
                Exit Sub
            Else
                BindList()
                ClearForm()
            End If
        End If
    End Sub

    Protected Sub chkActiveStatus_CheckedChanged(sender As ucSwitchButton, e As System.EventArgs)
        Dim chkActiveStatus As ucSwitchButton = sender
        Dim itm As RepeaterItem = chkActiveStatus.Parent
        Dim lblSupplierTypetID As Label = itm.FindControl("lblSupplierTypetID")
        Dim lblSupplierTypeName As Label = itm.FindControl("lblSupplierTypeName")

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.UpdateSupplierTypestatus(UserSession.UserName, lblSupplierTypetID.Text, chkActiveStatus.Checked)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Update status fail');", True)
            LogFileData.LogError(UserSession, ret.ErrorMessage)
        Else
            LogFileData.LogTrans(UserSession, "Update Supplier Type Status " & lblSupplierTypeName.Text & " Status : " & chkActiveStatus.Checked.ToString)
        End If
    End Sub

    Private Sub BindList()
        Dim dt As DataTable = MasterDataBL.GetList_SupplierType(txtSearch.Text.Trim, ddlstatus.SelectedValue)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblSupplierTypeName As Label = e.Item.FindControl("lblSupplierTypeName")
        Dim lblSupplierTypetID As Label = e.Item.FindControl("lblSupplierTypetID")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        lblSupplierTypeName.Text = e.Item.DataItem("Supplier_Type_Name")
        lblSupplierTypetID.Text = e.Item.DataItem("Supplier_Type_ID")
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblSupplierTypeName As Label = e.Item.FindControl("lblSupplierTypeName")
        Dim lblSupplierTypetID As Label = e.Item.FindControl("lblSupplierTypetID")

        Select Case e.CommandName
            Case "EDIT"
                ClearForm()
                FillInData(lblSupplierTypetID.Text)

                pnlList.Visible = False
                pnlEdit.Visible = True

                LogFileData.LogTrans(UserSession, "Edit Supplier Type " & lblSupplierTypeName.Text)
            Case "DELETE"
                Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.DeleteSupplierType(lblSupplierTypetID.Text)
                If ret.IsSuccess = True Then
                    LogFileData.LogTrans(UserSession, "Delete Supplier Type " & lblSupplierTypeName.Text)

                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Delete Data Fail');", True)
                    LogFileData.LogError(UserSession, ret.ErrorMessage)
                End If
        End Select
    End Sub

    Private Sub FillInData(SupplierTypeID As Long)
        Dim ret As MsSupplierTypeLinqDB = MasterDataBL.GetSupplierTypeData(SupplierTypeID)
        If ret.SUPPLIER_TYPE_ID > 0 Then
            lblSupplierTypeID.Text = ret.SUPPLIER_TYPE_ID
            txtSupplierTypetName.Text = ret.SUPPLIER_TYPE_NAME
            chkActive.Checked = (ret.ACTIVE_STATUS = "Y")
        End If
        ret = Nothing
    End Sub

    Private Sub ClearForm()
        lblSupplierTypeID.Text = "0"
        txtSupplierTypetName.Text = ""
        chkActive.Checked = True
    End Sub

    Private Sub btnAddSupplierType_Click(sender As Object, e As EventArgs) Handles btnAddSupplierType.Click
        ClearForm()
        pnlEdit.Visible = True
        pnlList.Visible = False
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Add SupplierType")
    End Sub

    Private Function ValidateData() As Boolean
        If lblSupplierTypeID.Text = "0" Then
            'กรณีเป็นการเพิ่มใหม่ ให้ตรวจสอบข้อมูลการเข้าระบบด้วย
            If txtSupplierTypetName.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุชื่อประเภท');", True)
                Return False
            End If
        End If

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.CheckDuplicateSupplierType(lblSupplierTypeID.Text, txtSupplierTypetName.Text)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');", True)
            Return False
        End If

        Return True
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save Supplier Type")
        If ValidateData() = True Then

            Dim lnq As MsSupplierTypeLinqDB = MasterDataBL.SaveSupplierType(UserSession.UserName, lblSupplierTypeID.Text, txtSupplierTypetName.Text, chkActive.Checked)
            If lnq.SUPPLIER_TYPE_ID > 0 Then
                lblSupplierTypeID.Text = lnq.SUPPLIER_TYPE_ID
                LogFileData.LogTrans(UserSession, "Save Supplier Type " & txtSupplierTypetName.Text & " สำเร็จ")
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Data Complete');", True)
                pnlEdit.Visible = False
                pnlList.Visible = True
                BindList()
            Else
                LogFileData.LogError(UserSession, "Save Supplier Type " & txtSupplierTypetName.Text & " ไม่สำเร็จ Error :" & lnq.ErrorMessage)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Fail');", True)
            End If
            lnq = Nothing
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Cancel")
        ClearForm()
        pnlEdit.Visible = False
        pnlList.Visible = True
        BindList()
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click, txtSearch.TextChanged
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม ค้นหา")
        BindList()
    End Sub

    Private Sub ddlstatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlstatus.SelectedIndexChanged
        BindList()
    End Sub

End Class