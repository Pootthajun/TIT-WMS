﻿Imports LinqDB.TABLE
Imports WMS_BL

Public Class MS_Address_Tumbon
    Inherits System.Web.UI.Page

    Private ReadOnly Property UserSession As LoginSessionData
        Get
            Return Session(CommonBL.UserLoginSessionName)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect("frmLogin.aspx?rnd=" & DateTime.Now.Millisecond)
                Exit Sub
            Else
                BindList()
                ClearForm()
            End If
        End If
    End Sub

    Protected Sub chkActiveStatus_CheckedChanged(sender As ucSwitchButton, e As System.EventArgs)
        Dim chkActiveStatus As ucSwitchButton = sender
        Dim itm As RepeaterItem = chkActiveStatus.Parent
        Dim lblTumbonID As Label = itm.FindControl("lblTumbonID")
        Dim lblTumbonName As Label = itm.FindControl("lblTumbonName")

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.UpdateTumbonstatus(UserSession.UserName, lblTumbonID.Text, chkActiveStatus.Checked)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Update status fail');", True)
            LogFileData.LogError(UserSession, ret.ErrorMessage)
        Else
            LogFileData.LogTrans(UserSession, "Update Tumbon Status " & lblTumbonName.Text & " Status : " & chkActiveStatus.Checked.ToString)
        End If
    End Sub

    Private Sub BindList()
        Dim dt As DataTable = MasterDataBL.GetList_Tumbon(dllDistrictSearch.SelectedValue, txtSearch.Text.Trim, ddlstatus.SelectedValue)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Private Sub Bindddl()
        MasterDataBL.GetListDDL_District(ddlDistrict, "")
        MasterDataBL.GetListDDL_District(dllDistrictSearch, "")
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblDistrict As Label = e.Item.FindControl("lblDistrict")
        Dim lblTumbonName As Label = e.Item.FindControl("lblTumbonName")
        Dim lblTumbonID As Label = e.Item.FindControl("lblTumbonID")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        lblDistrict.Text = e.Item.DataItem("District_Name")
        lblTumbonName.Text = e.Item.DataItem("Tumbon_Name")
        lblTumbonID.Text = e.Item.DataItem("Tumbon_ID")
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblTumbonName As Label = e.Item.FindControl("lblTumbonName")
        Dim lblTumbonID As Label = e.Item.FindControl("lblTumbonID")

        Select Case e.CommandName
            Case "EDIT"
                ClearForm()
                FillInData(lblTumbonID.Text)

                pnlList.Visible = False
                pnlEdit.Visible = True

                LogFileData.LogTrans(UserSession, "คลิกปุ่ม Edit Tumbon " & lblTumbonName.Text)
            Case "DELETE"
                Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.DeleteTumbon(lblTumbonID.Text)
                If ret.IsSuccess = True Then
                    LogFileData.LogTrans(UserSession, "Delete Tumbon " & lblTumbonName.Text)

                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Delete Data Fail');", True)
                    LogFileData.LogError(UserSession, ret.ErrorMessage)
                End If
        End Select
    End Sub

    Private Sub FillInData(TumbonID As Long)
        Dim ret As MsAddressTumbonLinqDB = MasterDataBL.GetTumbonData(TumbonID)
        If ret.TUMBON_ID > 0 Then
            lblTumbonID.Text = ret.TUMBON_ID
            ddlDistrict.SelectedValue = ret.DISTRICT_ID
            txtTumbonName.Text = ret.TUMBON_NAME
            chkActive.Checked = (ret.ACTIVE_STATUS = "Y")
        End If
        ret = Nothing
    End Sub

    Private Sub ClearForm()
        lblTumbonID.Text = "0"
        txtTumbonName.Text = ""
        chkActive.Checked = True
        Bindddl()
        ddlDistrict.SelectedValue = 0
        dllDistrictSearch.SelectedValue = 0
    End Sub

    Private Sub btnAddTumbon_Click(sender As Object, e As EventArgs) Handles btnAddTumbon.Click
        ClearForm()
        pnlEdit.Visible = True
        pnlList.Visible = False
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Add Tumbon")
    End Sub

    Private Function ValidateData() As Boolean
        If lblTumbonID.Text = "0" Then
            'กรณีเป็นการเพิ่มใหม่ ให้ตรวจสอบข้อมูลการเข้าระบบด้วย
            If txtTumbonName.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุชื่ออำเภอ');", True)
                Return False
            End If
        End If

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.CheckDuplicateTumbon(ddlDistrict.SelectedValue, txtTumbonName.Text)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');", True)
            Return False
        End If

        Return True
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save Tumbon")
        If ValidateData() = True Then

            Dim lnq As MsAddressTumbonLinqDB = MasterDataBL.SaveTumbon(UserSession.UserName, lblTumbonID.Text, ddlDistrict.SelectedValue, txtTumbonName.Text, chkActive.Checked)
            If lnq.TUMBON_ID > 0 Then
                lblTumbonID.Text = lnq.TUMBON_ID
                LogFileData.LogTrans(UserSession, "Save Tumbon " & txtTumbonName.Text & " สำเร็จ")
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Data Complete');", True)
                pnlEdit.Visible = False
                pnlList.Visible = True
                BindList()
            Else
                LogFileData.LogError(UserSession, "Save Tumbon " & txtTumbonName.Text & " ไม่สำเร็จ Error :" & lnq.ErrorMessage)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Fail');", True)
            End If
            lnq = Nothing
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Cancel")
        ClearForm()
        pnlEdit.Visible = False
        pnlList.Visible = True
        BindList()
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click, txtSearch.TextChanged
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม ค้นหา")
        BindList()
    End Sub

    Private Sub ddlstatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlstatus.SelectedIndexChanged
        BindList()
    End Sub

    Private Sub dllDistrictSearch_SelectedIndexChanged(sender As Object, e As EventArgs) Handles dllDistrictSearch.SelectedIndexChanged
        BindList()
    End Sub
End Class