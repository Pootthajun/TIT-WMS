﻿Imports LinqDB.TABLE
Imports WMS_BL
Public Class MS_Staff
    Inherits System.Web.UI.Page

    Private ReadOnly Property UserSession As LoginSessionData
        Get
            Return Session(CommonBL.UserLoginSessionName)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect("frmLogin.aspx?rnd=" & DateTime.Now.Millisecond)
                Exit Sub

            Else
                BindList()
                ClearForm()

                pnlSearch.Visible = False
            End If
        End If
    End Sub

    Private Sub BindList()
        Dim dt As DataTable = MasterDataBL.GetList_Staff(txtSearch.Text.Trim, ddlDepartmentSearch.SelectedValue, ddlPositionSearch.SelectedValue, ddlstatus.SelectedValue)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblFullName As Label = e.Item.FindControl("lblFullName")
        Dim lblStaffID As Label = e.Item.FindControl("lblStaffID")
        Dim lblSequenceID As Label = e.Item.FindControl("lblSequenceID")
        Dim lblPositionName As Label = e.Item.FindControl("lblPositionName")
        Dim lblDepartmentName As Label = e.Item.FindControl("lblDepartmentName")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")


        lblFullName.Text = e.Item.DataItem("full_name")
        lblStaffID.Text = e.Item.DataItem("Staff_ID")
        lblSequenceID.Text = e.Item.DataItem("Sequence_ID")
        lblPositionName.Text = e.Item.DataItem("position_name")
        lblDepartmentName.Text = e.Item.DataItem("department_name")
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblListFullName As Label = e.Item.FindControl("lblFullName")
        Dim lblSequenceID As Label = e.Item.FindControl("lblSequenceID")

        Select Case e.CommandName
            Case "EDIT"
                ClearForm()
                FillInData(lblSequenceID.Text)

                pnlList.Visible = False
                pnlEdit.Visible = True

                LogFileData.LogTrans(UserSession, "คลิกปุ่ม Edit Staff " & lblListFullName.Text)
            Case "DELETE"
                Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.DeleteStaff(lblSequenceID.Text)
                If ret.IsSuccess = True Then
                    LogFileData.LogTrans(UserSession, "Delete Staff " & lblListFullName.Text)

                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Delete Data Fail');", True)
                    LogFileData.LogError(UserSession, ret.ErrorMessage)
                End If
        End Select
    End Sub

    Protected Sub chkActiveStatus_CheckedChanged(sender As ucSwitchButton, e As System.EventArgs)
        Dim chkActiveStatus As ucSwitchButton = sender
        Dim itm As RepeaterItem = chkActiveStatus.Parent
        Dim lblSequenceID As Label = itm.FindControl("lblSequenceID")
        Dim lblFullName As Label = itm.FindControl("lblFullName")

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.UpdateStaffStatus(UserSession.UserName, lblSequenceID.Text, chkActiveStatus.Checked)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Update status fail');", True)
            LogFileData.LogError(UserSession, ret.ErrorMessage)
        Else
            LogFileData.LogTrans(UserSession, "Update Staff Status " & lblFullName.Text & " Status : " & chkActiveStatus.Checked.ToString)
        End If
    End Sub

    Private Sub FillInData(StaffID As Long)
        Dim ret As MsStaffLinqDB = MasterDataBL.GetUserStaff(StaffID)
        If ret.SEQUENCE_ID > 0 Then
            lblSequenceID.Text = ret.SEQUENCE_ID
            txtStaffID.Text = ret.STAFF_ID
            ddlPrefixName.SelectedValue = ret.PREFIX_NAME_ID
            txtFirstName.Text = ret.FISRT_NAME
            txtLastName.Text = ret.LAST_NAME
            ddlPosition.SelectedValue = ret.POSITION_ID
            ddlDepartment.SelectedValue = ret.DEPARTMENT_ID
            chkActive.Checked = (ret.ACTIVE_STATUS = "Y")
        End If
        ret = Nothing
    End Sub


    Private Sub BindDDL()
        MasterDataBL.GetListDDL_Department(ddlDepartment)
        MasterDataBL.GetListDDL_Position(ddlPosition)
        MasterDataBL.GetListDDL_PrefixName(ddlPrefixName)
    End Sub

    Private Sub btnAddStaff_Click(sender As Object, e As EventArgs) Handles btnAddStaff.Click
        pnlEdit.Visible = True
        pnlList.Visible = False
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Add Staff")

        BindDDL()
    End Sub

    Private Sub ClearForm()
        lblSequenceID.Text = "0"
        txtFirstName.Text = ""
        txtLastName.Text = ""
        chkActive.Checked = True
        BindDDL()
        txtStaffID.Enabled = True

    End Sub

    Private Function ValidateData() As Boolean

        If txtStaffID.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุรหัสพนักงาน');", True)
            Return False
        End If
        If ddlPrefixName.SelectedValue = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกคำนำหน้าชื่อ');", True)
            Return False
        End If

        If txtFirstName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุชื่อ');", True)
            Return False
        End If
        If txtLastName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุนามสกุล');", True)
            Return False
        End If
        If ddlPosition.SelectedValue = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกตำแหน่ง');", True)
            Return False
        End If
        If ddlDepartment.SelectedValue = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกแผนก');", True)
            Return False
        End If


        'Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.CheckDuplicateUser(txtLoginUser.Text, lblUserID.Text)
        'If ret.IsSuccess = False Then
        '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');", True)
        '    Return False
        'End If

        Return True
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save User")
        If ValidateData() = True Then


            Dim lnq As MsStaffLinqDB = MasterDataBL.SaveUserStaff(UserSession.UserName, lblSequenceID.Text, txtStaffID.Text, ddlDepartment.SelectedValue, ddlPrefixName.SelectedValue, txtFirstName.Text, txtLastName.Text, ddlPosition.SelectedValue, chkActive.Checked)
            If lnq.SEQUENCE_ID > 0 Then
                lblSequenceID.Text = lnq.SEQUENCE_ID
                LogFileData.LogTrans(UserSession, "Save User " & txtStaffID.Text & " สำเร็จ")

                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Data Complete');", True)
                pnlEdit.Visible = False
                pnlList.Visible = True
                BindList()
            Else
                LogFileData.LogError(UserSession, "Save User " & txtStaffID.Text & " ไม่สำเร็จ Error :" & lnq.ErrorMessage)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Fail');", True)
            End If
            lnq = Nothing
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Cancel")
        ClearForm()
        pnlEdit.Visible = False
        pnlList.Visible = True
        BindList()
    End Sub

    Private Sub btnOpenSearch_Click(sender As Object, e As EventArgs) Handles btnOpenSearch.Click
        pnlSearch.Visible = True
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม ค้นหา")
        BindList()
        pnlSearch.Visible = False
    End Sub

    Private Sub ddlDepartmentSearch_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDepartmentSearch.SelectedIndexChanged, ddlPositionSearch.SelectedIndexChanged, ddlstatus.SelectedIndexChanged
        BindList()
    End Sub
End Class