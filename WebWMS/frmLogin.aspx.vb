﻿
Imports WMS_BL
Public Class frmLogin

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session.Remove(CommonBL.UserLoginSessionName)

        If IsPostBack = False Then
            Dim cf As LinqDB.TABLE.CfSystemConfigLinqDB = CommonBL.GetSystemConfig()
            lblMaxLoginFail.Text = cf.MAX_LOGIN_FAIL
            cf = Nothing
        End If
    End Sub

    Private Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        If Login() = True Then
            Response.Redirect("frmDashboard.aspx?rnd=" & DateTime.Now.Millisecond)
        End If
    End Sub

    Private Function Login() As Boolean
        Dim ret As Boolean = False
        Dim uLnq As LinqDB.TABLE.MsUserLinqDB = DataBL.GetLoginUserInfo(txtUsername.Text.Trim)
        If uLnq.USER_ID > 0 Then
            If uLnq.ACTIVE_STATUS = "Y" Then
                Dim psswd As String = CommonBL.EncryptText(txtPassword.Text.Trim)
                If uLnq.USER_PASSWORD = psswd Then
                    ret = CreateLoginSession(uLnq.USER_LOGIN, uLnq.USER_ID, uLnq.FIRST_NAME, uLnq.LAST_NAME, uLnq.FORCE_CHANGE_PWD, uLnq.LAST_LOGIN_TIME)
                Else
                    'รหัสผ่านไม่ถูกต้อง
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('รหัสผ่านไม่ถูกต้อง');", True)
                    ret = False

                    'Update จำนวนครั้งที่ Login ไม่สำเร็จ
                    lblLoginFailCount.Text = Convert.ToInt16(lblLoginFailCount.Text) + 1

                    If Convert.ToInt16(lblLoginFailCount.Text) >= Convert.ToInt16(lblMaxLoginFail.Text) Then
                        lblLoginFailCount.Text = "1"
                        ScriptManager.RegisterStartupScript(Me, Me.GetType, "CloseWindow", "CloseWindow();", True)
                    End If
                End If
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('ชื่อเข้าระบบได้ถูกระงับการใช้งาน');", True)
                ret = False
            End If
        Else
            'Username ไม่ถูกต้อง
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('ชื่อเข้าระบบไม่ถูกต้อง');", True)
            ret = False
        End If
        uLnq = Nothing
        Return ret
    End Function

    Private Function CreateLoginSession(Username As String, UserID As Long, FirstName As String, LastName As String, ForceChangePssWd As String, LastLoginTime As DateTime) As Boolean
        Dim ret As Boolean = False
        Dim UserSession As New LoginSessionData
        UserSession.UserID = UserID
        UserSession.UserName = Username

        Dim logHis As LinqDB.ConnectDB.ExecuteDataInfo = LogFileData.CreateLoginHistory(Request, Username, FirstName, LastName)
        If logHis.IsSuccess = True Then
            UserSession.LoginHistoryID = logHis.NewID
            UserSession.ForceChangePsswd = ForceChangePssWd
            UserSession.LastLoginTime = LastLoginTime
            ret = True
            Session(CommonBL.UserLoginSessionName) = UserSession
        Else
            ret = False
        End If

        Return ret
    End Function
End Class