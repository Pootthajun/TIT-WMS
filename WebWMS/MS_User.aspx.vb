﻿Imports LinqDB.TABLE
Imports WMS_BL
Public Class MS_User
    Inherits System.Web.UI.Page

    Private ReadOnly Property UserSession As LoginSessionData
        Get
            Return Session(CommonBL.UserLoginSessionName)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect("frmLogin.aspx?rnd=" & DateTime.Now.Millisecond)
                Exit Sub

            Else
                BindDDLForSearch()
                BindList()
                ClearForm()

                pnlSearch.Visible = False
            End If
        End If
    End Sub

    Private Sub BindList()
        Dim dt As DataTable = MasterDataBL.GetList_User(txtSearch.Text.Trim, ddlsex.SelectedValue, ddlDepartmentSearch.SelectedValue, ddlPositionSearch.SelectedValue, ddlstatus.SelectedValue)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblFullName As Label = e.Item.FindControl("lblFullName")
        Dim lblUserID As Label = e.Item.FindControl("lblUserID")
        Dim lblGender As Label = e.Item.FindControl("lblGender")
        Dim lblPositionName As Label = e.Item.FindControl("lblPositionName")
        Dim lblDepartmentName As Label = e.Item.FindControl("lblDepartmentName")
        Dim lblLastLoginTime As Label = e.Item.FindControl("lblLastLoginTime")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        lblFullName.Text = e.Item.DataItem("full_name")
        lblUserID.Text = e.Item.DataItem("user_id")
        lblGender.Text = e.Item.DataItem("gender")
        lblPositionName.Text = e.Item.DataItem("position_name")
        lblDepartmentName.Text = e.Item.DataItem("department_name")
        If Convert.IsDBNull(e.Item.DataItem("last_login_time")) = False Then
            lblLastLoginTime.Text = Convert.ToDateTime(e.Item.DataItem("last_login_time")).ToString("dd/MM/yyyy HH:mm:ss", New Globalization.CultureInfo("th-TH"))
        End If
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblListUserID As Label = e.Item.FindControl("lblUserID")
        Dim lblListFullName As Label = e.Item.FindControl("lblFullName")

        Select Case e.CommandName
            Case "EDIT"
                ClearForm()
                FillInData(lblListUserID.Text)

                pnlList.Visible = False
                pnlEdit.Visible = True

                LogFileData.LogTrans(UserSession, "คลิกปุ่ม Edit User " & lblListFullName.Text)
            Case "DELETE"
                Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.DeleteUser(lblListUserID.Text)
                If ret.IsSuccess = True Then
                    LogFileData.LogTrans(UserSession, "Delete User " & lblListFullName.Text)

                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Delete Data Fail');", True)
                    LogFileData.LogError(UserSession, ret.ErrorMessage)
                End If
        End Select
    End Sub

    Protected Sub chkActiveStatus_CheckedChanged(sender As ucSwitchButton, e As System.EventArgs)
        Dim chkActiveStatus As ucSwitchButton = sender
        Dim itm As RepeaterItem = chkActiveStatus.Parent
        Dim lblUserID As Label = itm.FindControl("lblUserID")
        Dim lblFullName As Label = itm.FindControl("lblFullName")

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.UpdateUserStatus(UserSession.UserName, lblUserID.Text, chkActiveStatus.Checked)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Update status fail');", True)
            LogFileData.LogError(UserSession, ret.ErrorMessage)
        Else
            LogFileData.LogTrans(UserSession, "Update User Status " & lblFullName.Text & " Status : " & chkActiveStatus.Checked.ToString)
        End If
    End Sub

    Private Sub FillInData(UserID As Long)
        Dim ret As MsUserLinqDB = MasterDataBL.GetUserData(UserID)
        If ret.USER_ID > 0 Then
            lblUserID.Text = ret.USER_ID
            ddlPrefixName.SelectedValue = ret.PREFIX_NAME_ID
            txtFirstName.Text = ret.FIRST_NAME
            txtLastName.Text = ret.LAST_NAME
            If ret.GENDER = "F" Then
                rdiGenderFemale.Visible = True
                rdiGenderMale.Visible = False
            End If
            ddlPosition.SelectedValue = ret.POSITION_ID
            ddlDepartment.SelectedValue = ret.DEPARTMENT_ID
            chkActive.Checked = (ret.ACTIVE_STATUS = "Y")

            'ข้อมูลการเข้าระบบ ไม่ต้องให้แก้ไข
            txtLoginUser.Text = ret.USER_LOGIN
            txtLoginUser.Enabled = False
            txtPassword.Enabled = False
            txtConfirmPassword.Enabled = False

            BindUserRole()
        End If
        ret = Nothing
    End Sub


    Private Sub BindDDL()
        MasterDataBL.GetListDDL_Department(ddlDepartment)
        MasterDataBL.GetListDDL_Position(ddlPosition)
        MasterDataBL.GetListDDL_PrefixName(ddlPrefixName)
    End Sub
    Private Sub BindDDLForSearch()
        MasterDataBL.GetListDDL_Department(ddlDepartmentSearch)
        MasterDataBL.GetListDDL_Position(ddlPositionSearch)
    End Sub

    Private Sub btnAddUser_Click(sender As Object, e As EventArgs) Handles btnAddUser.Click
        pnlEdit.Visible = True
        pnlList.Visible = False
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Add User")

        BindDDL()
        BindUserRole()
    End Sub

    Private Sub BindUserRole()
        Dim dt As DataTable = MasterDataBL.GetList_UserRole(lblUserID.Text)
        rptUserRole.DataSource = dt
        rptUserRole.DataBind()
    End Sub

    Private Sub rptUserRole_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptUserRole.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblRoleName As Label = e.Item.FindControl("lblRoleName")
        Dim lblRoleID As Label = e.Item.FindControl("lblRoleID")
        Dim chkGrant As ucSwitchButton = e.Item.FindControl("chkGrant")

        lblRoleID.Text = e.Item.DataItem("role_id")
        lblRoleName.Text = e.Item.DataItem("role_name")
        If Convert.IsDBNull(e.Item.DataItem("grant_role")) = False Then chkGrant.Checked = True
    End Sub

    Private Function GetListUserRole()
        Dim dt As New DataTable
        dt.Columns.Add("role_id", GetType(Long))
        dt.Columns.Add("role_name")
        dt.Columns.Add("grant_role", GetType(Boolean))

        For Each itm As RepeaterItem In rptUserRole.Items
            Dim lblRoleName As Label = itm.FindControl("lblRoleName")
            Dim lblRoleID As Label = itm.FindControl("lblRoleID")
            Dim chkGrant As ucSwitchButton = itm.FindControl("chkGrant")

            Dim dr As DataRow = dt.NewRow
            dr("role_id") = lblRoleID.Text
            dr("role_name") = lblRoleName.Text
            dr("grant_role") = chkGrant.Checked

            dt.Rows.Add(dr)
        Next

        Return dt
    End Function

    Private Sub ClearForm()
        lblUserID.Text = "0"
        txtFirstName.Text = ""
        txtLastName.Text = ""
        rdiGenderMale.Visible = True
        rdiGenderFemale.Visible = False
        chkActive.Checked = True
        txtLoginUser.Text = ""
        txtPassword.Text = ""
        txtConfirmPassword.Text = ""
        BindDDL()
        txtLoginUser.Enabled = True
        txtPassword.Enabled = True
        txtConfirmPassword.Enabled = True
    End Sub

    Private Function ValidateData() As Boolean
        If ddlPrefixName.SelectedValue = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกคำนำหน้าชื่อ');", True)
            Return False
        End If
        If txtFirstName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุชื่อ');", True)
            Return False
        End If
        If txtLastName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุนามสกุล');", True)
            Return False
        End If
        If ddlPosition.SelectedValue = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกตำแหน่ง');", True)
            Return False
        End If
        If ddlDepartment.SelectedValue = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกแผนก');", True)
            Return False
        End If

        If lblUserID.Text = "0" Then
            'กรณีเป็นการเพิ่มใหม่ ให้ตรวจสอบข้อมูลการเข้าระบบด้วย
            If txtLoginUser.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุชื่อเข้าระบบ');", True)
                Return False
            End If
            If txtPassword.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุรหัสผ่าน');", True)
                Return False
            End If
        End If

        If txtPassword.Text.Trim <> txtConfirmPassword.Text.Trim Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('การยืนยันรหัสผ่านไม่ตรงกัน');", True)
            Return False
        End If

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.CheckDuplicateUser(txtLoginUser.Text, lblUserID.Text)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');", True)
            Return False
        End If

        Return True
    End Function

    Private Sub rdiGenderFemale_Click(sender As Object, e As EventArgs) Handles rdiGenderFemale.Click
        rdiGenderFemale.Visible = False
        rdiGenderMale.Visible = True
    End Sub

    Private Sub rdiGenderMale_Click(sender As Object, e As EventArgs) Handles rdiGenderMale.Click
        rdiGenderMale.Visible = False
        rdiGenderFemale.Visible = True
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save User")
        If ValidateData() = True Then
            Dim Gender As String = "M"
            If rdiGenderFemale.Visible = True Then
                Gender = "F"
            End If

            Dim lnq As MsUserLinqDB = MasterDataBL.SaveUserData(UserSession.UserName, lblUserID.Text, txtLoginUser.Text, txtPassword.Text, ddlDepartment.SelectedValue, ddlPrefixName.SelectedValue, txtFirstName.Text, txtLastName.Text, Gender, ddlPosition.SelectedValue, chkActive.Checked, GetListUserRole())
            If lnq.USER_ID > 0 Then
                lblUserID.Text = lnq.USER_ID
                LogFileData.LogTrans(UserSession, "Save User " & txtLoginUser.Text & " สำเร็จ")

                txtLoginUser.Enabled = False
                txtPassword.Enabled = False
                txtConfirmPassword.Enabled = False
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Data Complete');", True)
                pnlEdit.Visible = False
                pnlList.Visible = True
                BindList()
            Else
                LogFileData.LogError(UserSession, "Save User " & txtLoginUser.Text & " ไม่สำเร็จ Error :" & lnq.ErrorMessage)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Fail');", True)
            End If
            lnq = Nothing
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Cancel")
        ClearForm()
        pnlEdit.Visible = False
        pnlList.Visible = True
        BindList()
    End Sub

    Private Sub btnOpenSearch_Click(sender As Object, e As EventArgs) Handles btnOpenSearch.Click
        pnlSearch.Visible = True
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม ค้นหา")
        BindList()
        pnlSearch.Visible = False
    End Sub

    Private Sub ddlDepartmentSearch_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDepartmentSearch.SelectedIndexChanged, ddlPositionSearch.SelectedIndexChanged, ddlstatus.SelectedIndexChanged, ddlsex.SelectedIndexChanged
        BindList()
    End Sub


End Class