﻿Imports LinqDB.TABLE
Imports WMS_BL

Public Class MS_Warehouse
    Inherits System.Web.UI.Page

    Private ReadOnly Property UserSession As LoginSessionData
        Get
            Return Session(CommonBL.UserLoginSessionName)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect("frmLogin.aspx?rnd=" & DateTime.Now.Millisecond)
                Exit Sub
            Else
                BindList("")
                ClearForm()
            End If
        End If
    End Sub

    Private Sub BindList(WarehouseID)
        Dim dt As DataTable = MasterDataBL.GetList_Warehouse("", "")
        rptList.DataSource = dt
        rptList.DataBind()

        'customer_address
        Dim dtAddress As DataTable = MasterDataBL.GetList_Warehouse_Address(WarehouseID)
        rptListAddress.DataSource = dtAddress
        rptListAddress.DataBind()
    End Sub

    Private Sub Bindddl()
        MasterDataBL.GetListDDL_WarehouseType(ddlWarehouseType)
    End Sub

#Region "Warehouse"

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblWarehouseID As Label = e.Item.FindControl("lblWarehouseID")
        Dim lblWarehouseCode As Label = e.Item.FindControl("lblWarehouseCode")
        Dim lblWarehouseTypeName As Label = e.Item.FindControl("lblWarehouseTypeName")
        Dim lblWarehouseName As Label = e.Item.FindControl("lblWarehouseName")
        Dim lblWarehouseDetial As Label = e.Item.FindControl("lblWarehouseDetial")

        Dim lblWarehouseSize As Label = e.Item.FindControl("lblWarehouseSize")
        Dim lblWarehouseBuilding As Label = e.Item.FindControl("lblWarehouseBuilding")
        Dim lblWarehouseFloor As Label = e.Item.FindControl("lblWarehouseFloor")
        Dim lblWarehouseRoom As Label = e.Item.FindControl("lblWarehouseRoom")
        Dim lblWarehouseZone As Label = e.Item.FindControl("lblWarehouseZone")
        Dim lblWarehouseLock As Label = e.Item.FindControl("lblWarehouseLock")
        Dim lblWarehouseRow As Label = e.Item.FindControl("lblWarehouseRow")
        Dim lblWarehouseShelf As Label = e.Item.FindControl("lblWarehouseShelf")
        Dim lblWarehouseDeep As Label = e.Item.FindControl("lblWarehouseDeep")
        Dim lblWarehouseType As Label = e.Item.FindControl("lblWarehouseType")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        lblWarehouseID.Text = e.Item.DataItem("Warehouse_ID")
        lblWarehouseCode.Text = e.Item.DataItem("Warehouse_Code")
        lblWarehouseTypeName.Text = e.Item.DataItem("Warehouse_Type")
        lblWarehouseName.Text = e.Item.DataItem("Warehouse_Name")

        lblWarehouseSize.Text = e.Item.DataItem("Warehouse_Size")
        lblWarehouseBuilding.Text = e.Item.DataItem("Warehouse_Building")
        lblWarehouseFloor.Text = e.Item.DataItem("Warehouse_Floor")
        lblWarehouseRoom.Text = e.Item.DataItem("Warehouse_Room")
        lblWarehouseZone.Text = e.Item.DataItem("Warehouse_Zone")
        lblWarehouseLock.Text = e.Item.DataItem("Warehouse_Lock")
        lblWarehouseRow.Text = e.Item.DataItem("Warehouse_Row")
        lblWarehouseShelf.Text = e.Item.DataItem("Warehouse_Shelf")
        lblWarehouseDeep.Text = e.Item.DataItem("Warehouse_Deep")
        lblWarehouseType.Text = e.Item.DataItem("Warehouse_Type_ID")
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")

        lblWarehouseDetial.Text = "ขนาด " + lblWarehouseSize.Text + " อาคาร " + lblWarehouseBuilding.Text + " ชั้น " + lblWarehouseFloor.Text + " ห้อง " + lblWarehouseRoom.Text + " โซน " + lblWarehouseZone.Text + " ล็อก " + lblWarehouseLock.Text + " แถว " + lblWarehouseRow.Text + " เชลส์ " + lblWarehouseShelf.Text + " ลึก  " + lblWarehouseDeep.Text + " "
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblWarehouseID As Label = e.Item.FindControl("lblWarehouseID")
        Dim lblWarehouseCode As Label = e.Item.FindControl("lblWarehouseCode")
        Dim lblWarehouseName As Label = e.Item.FindControl("lblWarehouseName")

        Select Case e.CommandName
            Case "EDIT"
                ClearForm()
                FillInData(lblWarehouseID.Text)

                pnlList.Visible = False
                pnlEdit.Visible = True

                BindList(lblWarehouseID.Text)

                LogFileData.LogTrans(UserSession, "Edit Warehouse " & lblWarehouseName.Text)
            Case "DELETE"
                Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.DeleteWarehouse(lblWarehouseID.Text)
                If ret.IsSuccess = True Then
                    LogFileData.LogTrans(UserSession, "Delete Warehouse " & lblWarehouseName.Text)

                    BindList("")
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Delete Data Fail');", True)
                    LogFileData.LogError(UserSession, ret.ErrorMessage)
                End If
        End Select
    End Sub


    Private Sub FillInData(WarehouseID As Long)
        Dim ret As MsWarehouseLinqDB = MasterDataBL.GetWarehouseData(WarehouseID)
        If ret.WAREHOUSE_ID > 0 Then
            lblWarehouseID.Text = ret.WAREHOUSE_ID
            txtWarehouseCode.Text = ret.WAREHOUSE_ID
            txtWarehouseName.Text = ret.WAREHOUSE_NAME
            ddlWarehouseType.SelectedValue = ret.WAREHOUSE_TYPE_ID
            txtWarehouseSize.Text = ret.WAREHOUSE_SIZE
            txtWarehouseBuild.Text = ret.WAREHOUSE_BUILDING
            txtWarehouseFloor.Text = ret.WAREHOUSE_FLOOR
            txtWarehouseRoom.Text = ret.WAREHOUSE_ROOM
            txtWarehouseZone.Text = ret.WAREHOUSE_ZONE
            txtWarehouseLock.Text = ret.WAREHOUSE_LOCK
            txtWarehouseRow.Text = ret.WAREHOUSE_ROW
            txtWarehouseShelf.Text = ret.WAREHOUSE_SHELF
            txtWarehouseDeep.Text = ret.WAREHOUSE_DEEP
            chkActive.Checked = (ret.ACTIVE_STATUS = "Y")
        End If

        txtWarehouseCode.Enabled = False
        ret = Nothing
    End Sub
    Private Sub ClearForm()
        lblWarehouseID.Text = "0"
        txtWarehouseCode.Text = ""
        txtWarehouseName.Text = ""
        Bindddl()
        ddlWarehouseType.SelectedValue = "0"
        txtWarehouseSize.Text = ""
        txtWarehouseBuild.Text = ""
        txtWarehouseFloor.Text = ""
        txtWarehouseRoom.Text = ""
        txtWarehouseZone.Text = ""
        txtWarehouseLock.Text = ""
        txtWarehouseRow.Text = ""
        txtWarehouseShelf.Text = ""
        txtWarehouseDeep.Text = ""
        chkActive.Checked = True
        txtWarehouseCode.Enabled = True
        BindList(0)
    End Sub

    Private Function ValidateData() As Boolean
        'กรณีเป็นการเพิ่มใหม่ ให้ตรวจสอบข้อมูลการเข้าระบบด้วย
        If txtWarehouseCode.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุรหัสคลังสินค้า');", True)
            Return False
        End If

        If ddlWarehouseType.SelectedValue = "0" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุประเภทคลังสินค้า');", True)
            Return False
        End If

        If txtWarehouseName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุชื่อคลังสินค้า');", True)
            Return False
        End If
        If lblWarehouseID.Text = "0" Then
            Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.CheckDuplicateWarehouse(ddlWarehouseType.SelectedValue, txtWarehouseName.Text)
            If ret.IsSuccess = False Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');", True)
                Return False
            End If
        End If

        Return True
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save Warehouse")
        If ValidateData() = True Then

            Dim lnq As MsWarehouseLinqDB = MasterDataBL.SaveWarehouse(UserSession.UserName, lblWarehouseID.Text, txtWarehouseCode.Text, txtWarehouseName.Text, ddlWarehouseType.SelectedValue, txtWarehouseSize.Text, txtWarehouseBuild.Text, txtWarehouseFloor.Text, txtWarehouseRoom.Text, txtWarehouseZone.Text, txtWarehouseLock.Text, txtWarehouseRow.Text, txtWarehouseShelf.Text, txtWarehouseDeep.Text, chkActive.Checked)

            'ข้อมูลตาราง Address
            Dim DTAddress As New DataTable
            DTAddress = GetAddressList()
            Dim lnqAddress As MsWarehouseAddressLinqDB = MasterDataBL.SaveWarehouse_AddressData(UserSession.UserName, lnq.WAREHOUSE_ID, DTAddress)

            If lnq.WAREHOUSE_ID > 0 Then
                lblWarehouseID.Text = lnq.WAREHOUSE_ID
                LogFileData.LogTrans(UserSession, "Save Warehouse " & txtWarehouseName.Text & " สำเร็จ")
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Data Complete');", True)
                pnlEdit.Visible = False
                pnlList.Visible = True
                BindList("")
            Else
                LogFileData.LogError(UserSession, "Save Warehouse " & txtWarehouseName.Text & " ไม่สำเร็จ Error :" & lnq.ErrorMessage)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Fail');", True)
            End If
            lnq = Nothing
        End If
    End Sub

    Private Sub btnAddWarehouse_Click(sender As Object, e As EventArgs) Handles btnAddWarehouse.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Add Warehouse ")
        pnlList.Visible = False
        pnlEdit.Visible = True
        ClearForm()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        pnlList.Visible = True
        pnlEdit.Visible = False
    End Sub
#End Region

#Region "Warehouse_Addresss"

    Private Function GetAddressList() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("Sequence_ID", GetType(Long))
        dt.Columns.Add("Warehouse_ID", GetType(Long))
        dt.Columns.Add("Warehouse_House_No")
        dt.Columns.Add("Warehouse_Building")
        dt.Columns.Add("Warehouse_moo")
        dt.Columns.Add("Warehouse_soi")
        dt.Columns.Add("Warehouse_road")
        dt.Columns.Add("Warehouse_Tumbon", GetType(Long))
        dt.Columns.Add("Warehouse_District", GetType(Long))
        dt.Columns.Add("Warehouse_Province", GetType(Long))
        dt.Columns.Add("Warehouse_Country", GetType(Long))
        dt.Columns.Add("Address")
        dt.Columns.Add("Warehouse_Postcode")
        dt.Columns.Add("Warehouse_Telephone")
        dt.Columns.Add("Warehouse_Mobile")
        dt.Columns.Add("Active_Status")

        For Each itm As RepeaterItem In rptListAddress.Items
            Dim lblSequence_ID As Label = itm.FindControl("lblSequence_ID")
            Dim lblAddressWID As Label = itm.FindControl("lblAddressWID")
            Dim lblAddressHouseNo As Label = itm.FindControl("lblAddressHouseNo")
            Dim lblAddressBuildingName As Label = itm.FindControl("lblAddressBuildingName")
            Dim lblAddressMoo As Label = itm.FindControl("lblAddressMoo")
            Dim lblAddressSoi As Label = itm.FindControl("lblAddressSoi")
            Dim lblAddressRoad As Label = itm.FindControl("lblAddressRoad")
            Dim lblAddressTumbonID As Label = itm.FindControl("lblAddressTumbonID")
            Dim lblAddressDistrictID As Label = itm.FindControl("lblAddressDistrictID")
            Dim lblAddressProvinceID As Label = itm.FindControl("lblAddressProvinceID")
            Dim lblAddressCountryID As Label = itm.FindControl("lblAddressCountryID")
            Dim lblAddressPascode As Label = itm.FindControl("lblAddressPascode")
            Dim lblAddressTel As Label = itm.FindControl("lblAddressTel")
            Dim lblAddressMobile As Label = itm.FindControl("lblAddressMobile")
            Dim lblAddressStatus As Label = itm.FindControl("lblAddressStatus")
            Dim lblAddressDetial As Label = itm.FindControl("lblAddressDetial")

            Dim dr As DataRow = dt.NewRow
            dr("Sequence_ID") = lblSequence_ID.Text
            dr("Warehouse_ID") = 0
            dr("Warehouse_House_No") = lblAddressHouseNo.Text
            dr("Warehouse_Building") = lblAddressBuildingName.Text
            dr("Warehouse_moo") = lblAddressMoo.Text
            dr("Warehouse_soi") = lblAddressSoi.Text
            dr("Warehouse_road") = lblAddressRoad.Text
            dr("Warehouse_Tumbon") = lblAddressTumbonID.Text
            dr("Warehouse_District") = lblAddressDistrictID.Text
            dr("Warehouse_Province") = lblAddressProvinceID.Text
            dr("Warehouse_Country") = lblAddressCountryID.Text
            dr("Address") = lblAddressDetial.Text
            dr("Warehouse_Postcode") = lblAddressPascode.Text
            dr("Warehouse_Telephone") = lblAddressTel.Text
            dr("Warehouse_Mobile") = lblAddressMobile.Text
            dr("Active_Status") = lblAddressStatus.Text
            dt.Rows.Add(dr)
        Next
        Return dt
    End Function

    Private Sub rptListAddress_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptListAddress.ItemDataBound
        Dim lblSequence_ID As Label = e.Item.FindControl("lblSequence_ID")
        Dim lblAddressWID As Label = e.Item.FindControl("lblAddressWID")
        Dim lblAddressHouseNo As Label = e.Item.FindControl("lblAddressHouseNo")
        Dim lblAddressBuildingName As Label = e.Item.FindControl("lblAddressBuildingName")
        Dim lblAddressMoo As Label = e.Item.FindControl("lblAddressMoo")
        Dim lblAddressSoi As Label = e.Item.FindControl("lblAddressSoi")
        Dim lblAddressRoad As Label = e.Item.FindControl("lblAddressRoad")
        Dim lblAddressTumbonID As Label = e.Item.FindControl("lblAddressTumbonID")
        Dim lblAddressDistrictID As Label = e.Item.FindControl("lblAddressDistrictID")
        Dim lblAddressProvinceID As Label = e.Item.FindControl("lblAddressProvinceID")
        Dim lblAddressCountryID As Label = e.Item.FindControl("lblAddressCountryID")
        Dim lblAddressPascode As Label = e.Item.FindControl("lblAddressPascode")
        Dim lblAddressTel As Label = e.Item.FindControl("lblAddressTel")
        Dim lblAddressMobile As Label = e.Item.FindControl("lblAddressMobile")
        Dim lblAddressStatus As Label = e.Item.FindControl("lblAddressStatus")

        Dim lblAddressDetial As Label = e.Item.FindControl("lblAddressDetial")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        Dim lblRowIndexAd As Label = e.Item.FindControl("lblRowIndexAd")

        lblRowIndexAd.Text = e.Item.ItemIndex

        lblSequence_ID.Text = e.Item.DataItem("Sequence_ID")
        lblAddressWID.Text = e.Item.DataItem("Warehouse_ID")
        lblAddressHouseNo.Text = e.Item.DataItem("Warehouse_House_No")
        lblAddressBuildingName.Text = e.Item.DataItem("Warehouse_Building")
        lblAddressMoo.Text = e.Item.DataItem("Warehouse_moo")
        lblAddressSoi.Text = e.Item.DataItem("Warehouse_soi")
        lblAddressRoad.Text = e.Item.DataItem("Warehouse_road")
        lblAddressTumbonID.Text = e.Item.DataItem("Warehouse_Tumbon")
        lblAddressDistrictID.Text = e.Item.DataItem("Warehouse_District")
        lblAddressProvinceID.Text = e.Item.DataItem("Warehouse_Province")
        lblAddressCountryID.Text = e.Item.DataItem("Warehouse_Country")
        lblAddressPascode.Text = e.Item.DataItem("Warehouse_Postcode")
        lblAddressTel.Text = e.Item.DataItem("Warehouse_Telephone")
        lblAddressMobile.Text = e.Item.DataItem("Warehouse_Mobile")
        lblAddressStatus.Text = e.Item.DataItem("Active_Status")

        lblAddressDetial.Text = e.Item.DataItem("Address")
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")
    End Sub

    Private Sub rptListAddress_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptListAddress.ItemCommand
        Dim lblSequence_ID As Label = e.Item.FindControl("lblSequence_ID")
        Dim lblAddressWID As Label = e.Item.FindControl("lblAddressWID")
        Dim lblAddressHouseNo As Label = e.Item.FindControl("lblAddressHouseNo")
        Dim lblAddressBuildingName As Label = e.Item.FindControl("lblAddressBuildingName")
        Dim lblAddressMoo As Label = e.Item.FindControl("lblAddressMoo")
        Dim lblAddressSoi As Label = e.Item.FindControl("lblAddressSoi")
        Dim lblAddressRoad As Label = e.Item.FindControl("lblAddressRoad")
        Dim lblAddressTumbonID As Label = e.Item.FindControl("lblAddressTumbonID")
        Dim lblAddressDistrictID As Label = e.Item.FindControl("lblAddressDistrictID")
        Dim lblAddressProvinceID As Label = e.Item.FindControl("lblAddressProvinceID")
        Dim lblAddressCountryID As Label = e.Item.FindControl("lblAddressCountryID")
        Dim lblAddressPascode As Label = e.Item.FindControl("lblAddressPascode")
        Dim lblAddressTel As Label = e.Item.FindControl("lblAddressTel")
        Dim lblAddressMobile As Label = e.Item.FindControl("lblAddressMobile")
        Dim lblAddressStatus As Label = e.Item.FindControl("lblAddressStatus")

        Dim lblAddressDetial As Label = e.Item.FindControl("lblAddressDetial")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        Dim lblRowIndexAd As Label = e.Item.FindControl("lblRowIndexAd")
        Select Case e.CommandName
            Case "EDIT"

                lblRowIndexAd.Text = e.Item.ItemIndex
                Session("rowAddress") = lblRowIndexAd.Text

                txthouseNo.Text = lblAddressHouseNo.Text
                txtBuildingName.Text = lblAddressBuildingName.Text
                txtmoo.Text = lblAddressMoo.Text
                txtsoi.Text = lblAddressSoi.Text
                txtroad.Text = lblAddressRoad.Text

                MasterDataBL.GetListDDL_Country(ddlCountry)
                ddlCountry.SelectedValue = lblAddressCountryID.Text

                MasterDataBL.GetListDDL_Province(ddlProvince, ddlCountry.SelectedValue)
                ddlProvince.SelectedValue = lblAddressProvinceID.Text

                MasterDataBL.GetListDDL_District(ddlDistrict, ddlProvince.SelectedValue)
                ddlDistrict.SelectedValue = lblAddressDistrictID.Text

                MasterDataBL.GetListDDL_Tumbon(ddlTumbon, ddlDistrict.SelectedValue)
                ddlTumbon.SelectedValue = lblAddressTumbonID.Text

                txtPoscode.Text = lblAddressPascode.Text
                txtTel.Text = lblAddressTel.Text
                txtMobile.Text = lblAddressMobile.Text

                If lblAddressStatus.Text = "Y" Then
                    chkActiveAddress.Checked = True
                Else
                    chkActiveAddress.Checked = False
                End If

                pnlList.Visible = False
                pnlEdit.Visible = False
                pnlEditAddress.Visible = True

                LogFileData.LogTrans(UserSession, "คลิกปุ่ม Edit Address " & lblAddressDetial.Text)
            Case "DELETE"
                Dim dt As DataTable = GetAddressList()
                dt.Rows.RemoveAt(e.Item.ItemIndex)

                rptListAddress.DataSource = dt
                rptListAddress.DataBind()
        End Select
    End Sub

    Private Sub ClearFormAddress()
        Session("rowAddress") = ""

        txthouseNo.Text = ""
        txtBuildingName.Text = ""
        txtmoo.Text = ""
        txtsoi.Text = ""
        txtroad.Text = ""

        MasterDataBL.GetListDDL_Country(ddlCountry)
        ddlCountry.SelectedValue = "1"
        MasterDataBL.GetListDDL_Province(ddlProvince, ddlCountry.SelectedValue)
        MasterDataBL.GetListDDL_District(ddlDistrict, ddlProvince.SelectedValue)
        MasterDataBL.GetListDDL_Tumbon(ddlTumbon, ddlDistrict.SelectedValue)
        txtPoscode.Text = ""
        txtTel.Text = ""
        txtMobile.Text = ""

    End Sub

    Private Sub btnAddAddress_Click(sender As Object, e As EventArgs) Handles btnAddAddress.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่มเพิ่ม Warehouse Address ")
        pnlList.Visible = False
        pnlEdit.Visible = False
        pnlEditAddress.Visible = True

        ClearFormAddress()
    End Sub

    Private Sub btnCancelAddress_Click(sender As Object, e As EventArgs) Handles btnCancelAddress.Click
        pnlList.Visible = False
        pnlEdit.Visible = True
        pnlEditAddress.Visible = False
    End Sub

    Private Sub btnSaveAddress_Click(sender As Object, e As EventArgs) Handles btnSaveAddress.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save Address " & txthouseNo.Text)
        If ValidateAddressData() = True Then
            pnlList.Visible = False
            pnlEdit.Visible = True
            pnlEditAddress.Visible = False

            Dim dt As DataTable = GetAddressList()
            If Session("rowAddress").ToString() = "" Then
                'Add
                Dim dr As DataRow = dt.NewRow

                dr("Sequence_ID") = dt.Rows.Count + 1
                dr("Warehouse_ID") = 0
                dr("Warehouse_House_No") = txthouseNo.Text
                dr("Warehouse_Building") = txtBuildingName.Text
                dr("Warehouse_moo") = txtmoo.Text
                dr("Warehouse_soi") = txtsoi.Text
                dr("Warehouse_road") = txtroad.Text
                dr("Warehouse_Tumbon") = ddlTumbon.SelectedValue
                dr("Warehouse_District") = ddlDistrict.SelectedValue
                dr("Warehouse_Province") = ddlProvince.SelectedValue
                dr("Warehouse_Country") = ddlCountry.SelectedValue
                dr("Warehouse_Postcode") = txtPoscode.Text
                dr("Warehouse_Telephone") = txtTel.Text
                dr("Warehouse_Mobile") = txtMobile.Text

                Dim TumbonName As String = ddlTumbon.SelectedItem.ToString()
                Dim DistrictName As String = ddlDistrict.SelectedItem.ToString()
                Dim ProvinceName As String = ddlProvince.SelectedItem.ToString()
                Dim CountryName As String = ddlCountry.SelectedItem.ToString()

                dr("Address") = "บ้านเลขที่ " & txthouseNo.Text & " อาคารที่ตั้ง " & txtBuildingName.Text & " หมู่ " & txtmoo.Text & " ซอย " & txtsoi.Text & " ถนน " & txtroad.Text & " ตำบล/แขวง " & TumbonName & " อำเภอ/เขต " & DistrictName & " จังหวัด " & ProvinceName & " ประเทศ " & CountryName & " รหัสไปรษณีย์ " & txtPoscode.Text & ""

                If chkActiveAddress.Checked Then
                    dr("Active_Status") = "Y"
                Else
                    dr("Active_Status") = "N"
                End If

                dt.Rows.Add(dr)
            Else
                'Edit
                Dim itm As RepeaterItem = rptListAddress.Items(Session("rowAddress").ToString())

                dt.Rows(Session("rowAddress").ToString())("Sequence_ID") = dt.Rows.Count + 1
                dt.Rows(Session("rowAddress").ToString())("Warehouse_ID") = 0
                dt.Rows(Session("rowAddress").ToString())("Warehouse_House_No") = txthouseNo.Text
                dt.Rows(Session("rowAddress").ToString())("Warehouse_Building") = txtBuildingName.Text
                dt.Rows(Session("rowAddress").ToString())("Warehouse_moo") = txtmoo.Text
                dt.Rows(Session("rowAddress").ToString())("Warehouse_soi") = txtsoi.Text
                dt.Rows(Session("rowAddress").ToString())("Warehouse_road") = txtroad.Text
                dt.Rows(Session("rowAddress").ToString())("Warehouse_Tumbon") = ddlTumbon.SelectedValue
                dt.Rows(Session("rowAddress").ToString())("Warehouse_District") = ddlDistrict.SelectedValue
                dt.Rows(Session("rowAddress").ToString())("Warehouse_Province") = ddlProvince.SelectedValue
                dt.Rows(Session("rowAddress").ToString())("Warehouse_Country") = ddlCountry.SelectedValue
                dt.Rows(Session("rowAddress").ToString())("Warehouse_Postcode") = txtPoscode.Text
                dt.Rows(Session("rowAddress").ToString())("Warehouse_Telephone") = txtTel.Text
                dt.Rows(Session("rowAddress").ToString())("Warehouse_Mobile") = txtMobile.Text

                Dim TumbonName As String = ddlTumbon.SelectedItem.ToString()
                Dim DistrictName As String = ddlDistrict.SelectedItem.ToString()
                Dim ProvinceName As String = ddlProvince.SelectedItem.ToString()
                Dim CountryName As String = ddlCountry.SelectedItem.ToString()

                dt.Rows(Session("rowAddress").ToString())("Address") = "บ้านเลขที่ " & txthouseNo.Text & " อาคารที่ตั้ง " & txtBuildingName.Text & " หมู่ " & txtmoo.Text & " ซอย " & txtsoi.Text & " ถนน " & txtroad.Text & " ตำบล/แขวง " & TumbonName & " อำเภอ/เขต " & DistrictName & " จังหวัด " & ProvinceName & " ประเทศ " & CountryName & " รหัสไปรษณีย์ " & txtPoscode.Text & ""

                If chkActiveAddress.Checked Then
                    dt.Rows(Session("rowAddress").ToString())("Active_Status") = "Y"
                Else
                    dt.Rows(Session("rowAddress").ToString())("Active_Status") = "N"
                End If
            End If

            rptListAddress.DataSource = dt
            rptListAddress.DataBind()
        End If
    End Sub

    Private Function ValidateAddressData() As Boolean

        If txthouseNo.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุบ้านเลขที่');", True)
        Return False
        End If
        If txtBuildingName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุอาคารที่ตั้ง');", True)
            Return False
        End If
        If txtmoo.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุหมู่');", True)
            Return False
        End If
        If txtsoi.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุซอย');", True)
            Return False
        End If
        If txtroad.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุถนน');", True)
            Return False
        End If
        If ddlTumbon.SelectedValue = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกตำบล/แขวง');", True)
            Return False
        End If
        If ddlDistrict.SelectedValue = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกอำเภอ/เขต');", True)
            Return False
        End If
        If ddlProvince.SelectedValue = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกจังหวัด');", True)
            Return False
        End If
        If ddlCountry.SelectedValue = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกประเทศ');", True)
            Return False
        End If
        If txtPoscode.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุรหัสไปรษณีย์');", True)
            Return False
        End If
        If txtTel.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุเบอร์โทรติดต่อ');", True)
            Return False
        End If
        If txtMobile.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุเบอร์โทรศัพท์มือถือ');", True)
            Return False
        End If
        Return True
    End Function


    Private Sub ddlDistrict_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDistrict.SelectedIndexChanged
        'Dim sql As String = "select Province_ID From MS_Address_District where District_ID = '" & ddlDistrict.SelectedValue & "'"
        'Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        'For i As Integer = 0 To dt.Rows.Count - 1
        '    Dim Province_ID As String = dt.Rows(i)("Province_ID").ToString()
        '    MasterDataBL.GetListDDL_Province(ddlProvince, Province_ID)
        'Next

        MasterDataBL.GetListDDL_Tumbon(ddlTumbon, ddlDistrict.SelectedValue)
    End Sub

    Private Sub ddlProvince_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProvince.SelectedIndexChanged
        'Dim sql As String = "select Country_ID From MS_Address_Province where Province_ID = '" & ddlProvince.SelectedValue & "'"
        'Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        'For i As Integer = 0 To dt.Rows.Count - 1
        '    Dim Country_ID As String = dt.Rows(i)("Country_ID").ToString()
        '    MasterDataBL.GetListDDL_Country(ddlCountry, Country_ID)
        'Next

        MasterDataBL.GetListDDL_District(ddlDistrict, ddlProvince.SelectedValue)
    End Sub

    Private Sub ddlCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCountry.SelectedIndexChanged
        MasterDataBL.GetListDDL_Province(ddlProvince, ddlCountry.SelectedValue)
        MasterDataBL.GetListDDL_District(ddlDistrict, ddlProvince.SelectedValue)
        MasterDataBL.GetListDDL_Tumbon(ddlTumbon, ddlDistrict.SelectedValue)
    End Sub

#End Region
End Class