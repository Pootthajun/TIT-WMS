﻿Imports LinqDB.TABLE
Imports WMS_BL

Public Class MS_SKU_Group
    Inherits System.Web.UI.Page

    Private ReadOnly Property UserSession As LoginSessionData
        Get
            Return Session(CommonBL.UserLoginSessionName)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect("frmLogin.aspx?rnd=" & DateTime.Now.Millisecond)
                Exit Sub
            Else
                BindList()
                ClearForm()
            End If
        End If
    End Sub

    Protected Sub chkActiveStatus_CheckedChanged(sender As ucSwitchButton, e As System.EventArgs)
        Dim chkActiveStatus As ucSwitchButton = sender
        Dim itm As RepeaterItem = chkActiveStatus.Parent
        Dim lblSKUGroupID As Label = itm.FindControl("lblSKUGroupID")
        Dim lblSKUGroupName As Label = itm.FindControl("lblSKUGroupName")

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.UpdateSKUGroupstatus(UserSession.UserName, lblSKUGroupID.Text, chkActiveStatus.Checked)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Update status fail');", True)
            LogFileData.LogError(UserSession, ret.ErrorMessage)
        Else
            LogFileData.LogTrans(UserSession, "Update SKU Group Status " & lblSKUGroupName.Text & " Status : " & chkActiveStatus.Checked.ToString)
        End If
    End Sub

    Private Sub BindList()
        Dim dt As DataTable = MasterDataBL.GetList_SKUGroup(txtSearch.Text.Trim, ddlstatus.SelectedValue)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblSKUGroupName As Label = e.Item.FindControl("lblSKUGroupName")
        Dim lblSKUGroupID As Label = e.Item.FindControl("lblSKUGroupID")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        lblSKUGroupName.Text = e.Item.DataItem("SKU_Group_Name")
        lblSKUGroupID.Text = e.Item.DataItem("Sequence_ID")
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblSKUGroupName As Label = e.Item.FindControl("lblSKUGroupName")
        Dim lblSKUGroupID As Label = e.Item.FindControl("lblSKUGroupID")

        Select Case e.CommandName
            Case "EDIT"
                ClearForm()
                FillInData(lblSKUGroupID.Text)

                pnlList.Visible = False
                pnlEdit.Visible = True

                LogFileData.LogTrans(UserSession, "คลิกปุ่ม Edit SKU Group " & lblSKUGroupName.Text)
            Case "DELETE"
                Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.DeleteSKUGroup(lblSKUGroupID.Text)
                If ret.IsSuccess = True Then
                    LogFileData.LogTrans(UserSession, "Delete SKU Group " & lblSKUGroupName.Text)

                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Delete Data Fail');", True)
                    LogFileData.LogError(UserSession, ret.ErrorMessage)
                End If
        End Select
    End Sub

    Private Sub FillInData(SKUGroupID As Long)
        Dim ret As MsSKUGroupLinqDB = MasterDataBL.GetSKUGroupData(SKUGroupID)
        If ret.SEQUENCE_ID > 0 Then
            lblSKUGroupID.Text = ret.SEQUENCE_ID
            txtSKUGroupID.Text = ret.SKU_GROUP_ID
            txtSKUGroupName.Text = ret.SKU_GROUP_NAME
            chkActive.Checked = (ret.ACTIVE_STATUS = "Y")
        End If
        ret = Nothing
    End Sub

    Private Sub ClearForm()
        lblSKUGroupID.Text = "0"
        txtSKUGroupID.Text = ""
        txtSKUGroupName.Text = ""
        chkActive.Checked = True
        txtSKUGroupID.Enabled = False
    End Sub

    Private Sub btnAddSKUGroup_Click(sender As Object, e As EventArgs) Handles btnAddSKUGroup.Click
        ClearForm()
        pnlEdit.Visible = True
        pnlList.Visible = False
        txtSKUGroupID.Enabled = True
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Add SKUGroup")
    End Sub

    Private Function ValidateData() As Boolean
        If lblSKUGroupID.Text = "0" Then
            'กรณีเป็นการเพิ่มใหม่ ให้ตรวจสอบข้อมูลการเข้าระบบด้วย
            If txtSKUGroupName.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุชื่อกลุ่มชนิดสินค้า');", True)
                Return False
            End If

            If txtSKUGroupID.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุรหัสกลุ่มชนิดสินค้า');", True)
                Return False
            End If
        End If

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.CheckDuplicateSKUGroup(lblSKUGroupID.Text, txtSKUGroupName.Text)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');", True)
            Return False
        End If

        Return True
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save SKU Group")
        If ValidateData() = True Then

            Dim lnq As MsSkuGroupLinqDB = MasterDataBL.SaveSKUGroup(UserSession.UserName, lblSKUGroupID.Text, txtSKUGroupID.Text, txtSKUGroupName.Text, chkActive.Checked)
            If lnq.SEQUENCE_ID > 0 Then
                lblSKUGroupID.Text = lnq.SEQUENCE_ID
                LogFileData.LogTrans(UserSession, "Save SKU Group " & txtSKUGroupName.Text & " สำเร็จ")
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Data Complete');", True)
                pnlEdit.Visible = False
                pnlList.Visible = True
                BindList()
            Else
                LogFileData.LogError(UserSession, "Save SKU Group " & txtSKUGroupName.Text & " ไม่สำเร็จ Error :" & lnq.ErrorMessage)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Fail');", True)
            End If
            lnq = Nothing
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Cancel")
        ClearForm()
        pnlEdit.Visible = False
        pnlList.Visible = True
        BindList()
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click, txtSearch.TextChanged
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม ค้นหา")
        BindList()
    End Sub

    Private Sub ddlstatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlstatus.SelectedIndexChanged
        BindList()
    End Sub
End Class