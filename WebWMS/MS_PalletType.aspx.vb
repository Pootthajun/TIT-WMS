﻿Imports LinqDB.TABLE
Imports WMS_BL

Public Class MS_PalletType
    Inherits System.Web.UI.Page

    Private ReadOnly Property UserSession As LoginSessionData
        Get
            Return Session(CommonBL.UserLoginSessionName)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect("frmLogin.aspx?rnd=" & DateTime.Now.Millisecond)
                Exit Sub
            Else
                BindList()
                ClearForm()
            End If
        End If
    End Sub


    Protected Sub chkActiveStatus_CheckedChanged(sender As ucSwitchButton, e As System.EventArgs)
        Dim chkActiveStatus As ucSwitchButton = sender
        Dim itm As RepeaterItem = chkActiveStatus.Parent
        Dim lblSequenceID As Label = itm.FindControl("lblSequenceID")
        Dim lblPalletTypeName As Label = itm.FindControl("lblPalletTypeName")

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.UpdatePalletTypestatus(UserSession.UserName, lblSequenceID.Text, chkActiveStatus.Checked)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Update status fail');", True)
            LogFileData.LogError(UserSession, ret.ErrorMessage)
        Else
            LogFileData.LogTrans(UserSession, "Update Pallet Type Status " & lblPalletTypeName.Text & " Status : " & chkActiveStatus.Checked.ToString)
        End If
    End Sub

    Private Sub BindList()
        Dim dt As DataTable = MasterDataBL.GetList_PalletType(txtSearch.Text.Trim, ddlstatus.SelectedValue)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblPalletTypeName As Label = e.Item.FindControl("lblPalletTypeName")
        Dim lblPalletTypeID As Label = e.Item.FindControl("lblPalletTypeID")
        Dim lblSequenceID As Label = e.Item.FindControl("lblSequenceID")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        lblPalletTypeName.Text = e.Item.DataItem("Pallet_Type")
        lblPalletTypeID.Text = e.Item.DataItem("Pallet_Type_ID")
        lblSequenceID.Text = e.Item.DataItem("Sequence_ID")
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblPalletTypeName As Label = e.Item.FindControl("lblPalletTypeName")
        Dim lblSequenceID As Label = e.Item.FindControl("lblSequenceID")

        Select Case e.CommandName
            Case "EDIT"
                ClearForm()
                FillInData(lblSequenceID.Text)

                pnlList.Visible = False
                pnlEdit.Visible = True

                LogFileData.LogTrans(UserSession, "คลิกปุ่ม Edit Pallet Type " & lblPalletTypeName.Text)
            Case "DELETE"
                Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.DeletePalletType(lblSequenceID.Text)
                If ret.IsSuccess = True Then
                    LogFileData.LogTrans(UserSession, "Delete Pallet Type " & lblPalletTypeName.Text)

                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Delete Data Fail');", True)
                    LogFileData.LogError(UserSession, ret.ErrorMessage)
                End If
        End Select
    End Sub

    Private Sub FillInData(PalletTypeID As Long)
        Dim ret As MsPalletTypeLinqDB = MasterDataBL.GetPalletTypeData(PalletTypeID)
        If ret.SEQUENCE_ID > 0 Then
            lblSequenceID.Text = ret.SEQUENCE_ID
            txtPalletTypeID.Text = ret.PALLET_TYPE_ID
            txtPalletTypeName.Text = ret.PALLET_TYPE
            chkActive.Checked = (ret.ACTIVE_STATUS = "Y")
        End If
        ret = Nothing
    End Sub

    Private Sub ClearForm()
        lblSequenceID.Text = "0"
        txtPalletTypeID.Text = ""
        txtPalletTypeName.Text = ""
        chkActive.Checked = True
        txtPalletTypeID.Enabled = False
    End Sub

    Private Sub btnAddPalletType_Click(sender As Object, e As EventArgs) Handles btnAddPalletType.Click
        ClearForm()
        pnlEdit.Visible = True
        pnlList.Visible = False
        txtPalletTypeID.Enabled = True
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Add PalletType")
    End Sub

    Private Function ValidateData() As Boolean
        If lblSequenceID.Text = "0" Then
            'กรณีเป็นการเพิ่มใหม่ ให้ตรวจสอบข้อมูลการเข้าระบบด้วย
            If txtPalletTypeName.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุชื่อประเภท');", True)
                Return False
            End If

            If txtPalletTypeID.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุรหัสประเภท');", True)
                Return False
            End If
        End If

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.CheckDuplicatePalletType(lblSequenceID.Text, txtPalletTypeName.Text)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');", True)
            Return False
        End If

        Return True
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save Pallet Type")
        If ValidateData() = True Then

            Dim lnq As MsPalletTypeLinqDB = MasterDataBL.SavePalletType(UserSession.UserName, lblSequenceID.Text, txtPalletTypeID.Text, txtPalletTypeName.Text, chkActive.Checked)
            If lnq.SEQUENCE_ID > 0 Then
                lblSequenceID.Text = lnq.SEQUENCE_ID
                LogFileData.LogTrans(UserSession, "Save Pallet Type " & txtPalletTypeName.Text & " สำเร็จ")
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Data Complete');", True)
                pnlEdit.Visible = False
                pnlList.Visible = True
                BindList()
            Else
                LogFileData.LogError(UserSession, "Save Pallet Type " & txtPalletTypeName.Text & " ไม่สำเร็จ Error :" & lnq.ErrorMessage)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Fail');", True)
            End If
            lnq = Nothing
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Cancel")
        ClearForm()
        pnlEdit.Visible = False
        pnlList.Visible = True
        BindList()
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click, txtSearch.TextChanged
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม ค้นหา")
        BindList()
    End Sub

    Private Sub ddlstatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlstatus.SelectedIndexChanged
        BindList()
    End Sub
End Class