﻿Imports System.Data.SqlClient
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports WMS_BL

Public Class TB_PO
    Inherits System.Web.UI.Page

    Private ReadOnly Property UserSession As LoginSessionData
        Get
            Return Session(CommonBL.UserLoginSessionName)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect("frmLogin.aspx?rnd=" & DateTime.Now.Millisecond)
                Exit Sub
            Else
                BindList()
                ClearForm()
            End If
        End If
    End Sub

    Protected Sub chkActiveStatus_CheckedChanged(sender As ucSwitchButton, e As System.EventArgs)
        Dim chkActiveStatus As ucSwitchButton = sender
        Dim itm As RepeaterItem = chkActiveStatus.Parent
        Dim lblSequenceID As Label = itm.FindControl("lblSequenceID")
        Dim lblPOID As Label = itm.FindControl("lblPOID")

        Dim ret As ExecuteDataInfo = MasterDataBL.UpdatePOstatus(UserSession.UserName, lblPOID.Text, chkActiveStatus.Checked)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Update status fail');", True)
            LogFileData.LogError(UserSession, ret.ErrorMessage)
        Else
            LogFileData.LogTrans(UserSession, "Update PO Status " & lblPOID.Text & " Status : " & chkActiveStatus.Checked.ToString)
        End If
    End Sub

    Private Sub BindList()
        Dim dt As DataTable = MasterDataBL.GetList_PO(ddlSearchReceiveType.SelectedValue, txtSearchPOID.Text, txtSearchPoDate.Text)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblRecieveType As Label = e.Item.FindControl("lblRecieveType")
        Dim lblSequenceID As Label = e.Item.FindControl("lblSequenceID")
        Dim lblPOID As Label = e.Item.FindControl("lblPOID")
        Dim lblPODate As Label = e.Item.FindControl("lblPODate")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        lblRecieveType.Text = e.Item.DataItem("Recieve_Type")
        lblPOID.Text = e.Item.DataItem("Reference_No")
        lblPODate.Text = e.Item.DataItem("Reference_date")
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblPOID As Label = e.Item.FindControl("lblPOID")

        Select Case e.CommandName
            Case "EDIT"
                ClearForm()
                FillInData(lblPOID.Text)

                pnlList.Visible = False
                pnlEdit.Visible = True

                LogFileData.LogTrans(UserSession, "Edit PO " & lblPOID.Text)
            Case "DELETE"

                Dim trans As New TransactionDB
                Dim p_ab(1) As SqlParameter
                p_ab(0) = SqlDB.SetText("@POID", lblPOID.Text)
                SqlDB.ExecuteNonQuery("DELETE FROM TB_PO_Test WHERE Reference_No = @POID ", trans.Trans, p_ab)
                LogFileData.LogTrans(UserSession, "Delete PO " & lblPOID.Text)
                BindList()
        End Select
    End Sub

    Private Sub FillInData(POID As String)
        Dim dt As DataTable = MasterDataBL.GetList_PO_Detial(POID)
        rptsku.DataSource = dt
        rptsku.DataBind()

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim PONO As String = dt.Rows(i)("Reference_No").ToString()
            Dim PODate As String = dt.Rows(i)("Reference_date").ToString()
            Dim Receive_Type_ID As String = dt.Rows(i)("Receive_Type_ID").ToString()

            txtPOID.Text = PONO
            txtPODate.Text = Convert.ToDateTime(PODate).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
            MasterDataBL.GetListDDL_ReceiveType(ddlReceiveType)
            ddlReceiveType.SelectedValue = Receive_Type_ID
        Next

    End Sub

    Private Function Getrptsku() As DataTable
        Dim dt As New DataTable

        dt.Columns.Add("SKU_ID")
        dt.Columns.Add("Lot_Batch")
        dt.Columns.Add("Product_Status_ID")
        dt.Columns.Add("AmountReceive")

        For Each itm As RepeaterItem In rptsku.Items

            Dim ddlSku As DropDownList = itm.FindControl("ddlSku")
            Dim txtLotBatch As TextBox = itm.FindControl("txtLotBatch")
            Dim ddlProductStatus As DropDownList = itm.FindControl("ddlProductStatus")
            Dim txtAmount As TextBox = itm.FindControl("txtAmount")


            Dim dr As DataRow = dt.NewRow
            dr("SKU_ID") = ddlSku.SelectedValue
            dr("Lot_Batch") = txtLotBatch.Text
            dr("Product_Status_ID") = ddlProductStatus.SelectedValue
            dr("AmountReceive") = txtAmount.Text

            dt.Rows.Add(dr)
        Next
        Return dt
    End Function

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)

        Dim dt As DataTable = Getrptsku()
        Dim dr As DataRow
        dr = dt.NewRow
        dr("SKU_ID") = 0
        dr("Lot_Batch") = ""
        dr("Product_Status_ID") = 0
        dr("AmountReceive") = 0
        dt.Rows.Add(dr)

        rptsku.DataSource = dt
        rptsku.DataBind()

    End Sub

    Private Sub rptsku_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptsku.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim ddlSku As DropDownList = e.Item.FindControl("ddlSku")
        Dim txtLotBatch As TextBox = e.Item.FindControl("txtLotBatch")
        Dim ddlProductStatus As DropDownList = e.Item.FindControl("ddlProductStatus")
        Dim txtAmount As TextBox = e.Item.FindControl("txtAmount")

        MasterDataBL.GetListDDL_SKU(ddlSku, "")
        ddlSku.SelectedValue = e.Item.DataItem("SKU_ID")
        txtLotBatch.Text = e.Item.DataItem("Lot_Batch")
        MasterDataBL.GetListDDL_ProductStatus(ddlProductStatus)
        ddlProductStatus.SelectedValue = e.Item.DataItem("Product_Status_ID")
        txtAmount.Text = e.Item.DataItem("AmountReceive")
    End Sub

    Private Sub rptsku_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptsku.ItemCommand
        Select Case e.CommandName
            Case "DELETE"
                Dim dt As DataTable = Getrptsku()
                dt.Rows.RemoveAt(e.Item.ItemIndex)
                rptsku.DataSource = dt
                rptsku.DataBind()
        End Select
    End Sub

    Private Sub ClearForm()
        MasterDataBL.GetListDDL_ReceiveType(ddlReceiveType)
        MasterDataBL.GetListDDL_ReceiveType(ddlSearchReceiveType)
        txtPOID.Text = ""
        ddlReceiveType.SelectedValue = 0
        txtPODate.Text = ""
        chkActive.Checked = True
        FillInData(0)
        txtPOID.Enabled = False
    End Sub

    Private Sub btnAddPO_Click(sender As Object, e As EventArgs) Handles btnAddPO.Click
        ClearForm()
        pnlEdit.Visible = True
        pnlList.Visible = False
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Add PO")
        txtPOID.Enabled = True
    End Sub

    Private Function ValidateData() As Boolean
        If txtPOID.Text <> "" Then
            'กรณีเป็นการเพิ่มใหม่ ให้ตรวจสอบข้อมูลการเข้าระบบด้วย
            If txtPOID.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุรหัสเอกสาร');", True)
                Return False
            End If

            If txtPODate.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุวันที่ออก');", True)
                Return False
            End If

            If ddlReceiveType.SelectedValue = 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุเลือกประเภทการรับ');", True)
                Return False
            End If
        End If
        Return True
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save PO")
        If ValidateData() = True Then

            Dim DTPO_detial As New DataTable
            DTPO_detial = Getrptsku()

            Dim lnq As TbPoTestLinqDB = MasterDataBL.SavePO(UserSession.UserName, "0", ddlReceiveType.SelectedValue, txtPOID.Text, txtPODate.Text, DTPO_detial, chkActive.Checked)
            If lnq.SEQUENCE_ID > 0 Then

                LogFileData.LogTrans(UserSession, "Save PO " & txtPOID.Text & " สำเร็จ")
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Data Complete');", True)
                pnlEdit.Visible = False
                pnlList.Visible = True
                BindList()
            Else
                LogFileData.LogError(UserSession, "Save PO " & txtPOID.Text & " ไม่สำเร็จ Error :" & lnq.ErrorMessage)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Fail');", True)
            End If
            lnq = Nothing
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Cancel")
        ClearForm()
        pnlEdit.Visible = False
        pnlList.Visible = True
        BindList()
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindList()
    End Sub
End Class