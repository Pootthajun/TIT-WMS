﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="MS_Document_Type.aspx.vb" Inherits="WebWMS.MS_Document_Type" %>

<%@ Register Src="~/UserControl/ucSwitchButton.ascx" TagPrefix="uc1" TagName="ucSwitchButton" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="page-header">
        <h4 class="page-title">ข้อมูลประเภทเอกสาร</h4>
    </div>
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <asp:Panel ID="pnlList" runat="server" CssClass="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-2">
                                <asp:LinkButton ID="btnAddDocumentType" runat="server" CssClass="btn btn-warning">
                            <i class="icon md-plus-circle-o"></i>
                            <span >เพิ่มประเภทเอกสาร</span>
                                </asp:LinkButton>
                            </div>
                            <div class="col-sm-3">
                                <asp:DropDownList ID="ddljobTypeForSearch" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="ประเภทงานเอกสาร" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" MaxLength="100" placeholder="ชื่อประเภทเอกสาร" AutoPostBack="true"></asp:TextBox>
                            </div>
                            <div class="col-sm-3">
                                <asp:DropDownList ID="ddlstatus" CssClass="form-control" AutoPostBack="true" runat="server">
                                    <asp:ListItem Text="สถานะการใช้งาน" Value=""></asp:ListItem>
                                    <asp:ListItem Text="ใช้งาน" Value="Y"></asp:ListItem>
                                    <asp:ListItem Text="ไม่ใช้งาน" Value="N"></asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="col-sm-1">
                                <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn btn-warning">
                                        <i class="icon fa-search"></i>
                                </asp:LinkButton>
                            </div>
                        </div>
                        <div class="clearfix visible-md-block"></div>
                        <br />
                        <table class="tablesaw table-striped table-bordered table-hover">
                            <thead>
                                <tr class="bg-blue-grey-100">
                                    <th class="text-center">ประเภทงาน</th>
                                    <th class="text-center">เอกสาร</th>
                                    <th class="text-center">ใช้งาน</th>
                                    <th class="text-center">แก้ไข</th>
                                    <th class="text-center">ลบ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptList" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td class="text-center">
                                                <asp:Label ID="lbljobType" runat="server"></asp:Label>

                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="lblDocumentTypeName" runat="server"></asp:Label>
                                                <asp:Label ID="lblDocumentTypeID" runat="server" Visible="false"></asp:Label>
                                            </td>
                                            <td class="text-center">
                                                <uc1:ucSwitchButton runat="server" ID="chkActiveStatus" AutoPostBack="true" OnCheckedChanged="chkActiveStatus_CheckedChanged" />
                                            </td>
                                            <td class="text-center">
                                                <asp:LinkButton ID="btnEdit" runat="server" CommandName="EDIT">
                                                    <i class="icon md-edit" style="font-size: 20px;"></i>
                                                </asp:LinkButton>
                                            </td>
                                            <td class="text-center">
                                                <asp:LinkButton ID="btnDelete" runat="server" CommandName="DELETE" OnClientClick="return confirm('Do you want delete data?');">
                                                    <i class="icon md-delete" style="font-size: 20px;"></i>
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlEdit" runat="server" CssClass="panel" Visible="false">
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>ประเภทเอกสาร :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtDocumentTypeName" runat="server" CssClass="form-control" MaxLength="100" placeholder="ชื่อประเภทเอกสาร"></asp:TextBox>
                                <asp:Label ID="lblDocumentTypeID" runat="server" Visible="false" Text="0"></asp:Label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>ประเภทงานเอกสาร :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="ddljobType" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="ประเภทงานเอกสาร" AutoPostBack="true"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>ใช้งาน</label>
                            </div>

                            <div class="col-sm-9">
                                <uc1:ucSwitchButton runat="server" ID="chkActive" />
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <hr />
                        <div class="form-group form-material">
                            <div class="col-sm-12 col-sm-offset-9">
                                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary">
                                    <span>Save</span>
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-warning">
                                    <span>Cancel</span>
                                </asp:LinkButton>
                            </div>
                        </div>

                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpFooter" runat="server">
</asp:Content>
