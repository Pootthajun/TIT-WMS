﻿Imports LinqDB.TABLE
Imports WMS_BL

Public Class MS_SKU_Ingredient
    Inherits System.Web.UI.Page

    Private ReadOnly Property UserSession As LoginSessionData
        Get
            Return Session(CommonBL.UserLoginSessionName)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect("frmLogin.aspx?rnd=" & DateTime.Now.Millisecond)
                Exit Sub
            Else
                BindList("")
                ClearForm()
            End If
        End If
    End Sub


    Protected Sub chkActiveStatus_CheckedChanged(sender As ucSwitchButton, e As System.EventArgs)
        Dim chkActiveStatus As ucSwitchButton = sender
        Dim itm As RepeaterItem = chkActiveStatus.Parent
        Dim lblSequenceID As Label = itm.FindControl("lblSequenceID")
        Dim lblIngredientName As Label = itm.FindControl("lblIngredientName")

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.UpdateIngredientstatus(UserSession.UserName, lblSequenceID.Text, chkActiveStatus.Checked)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Update status fail');", True)
            LogFileData.LogError(UserSession, ret.ErrorMessage)
        Else
            LogFileData.LogTrans(UserSession, "Update Ingredient Status " & lblIngredientName.Text & " Status : " & chkActiveStatus.Checked.ToString)
        End If
    End Sub

    Private Sub BindList(IngredientID)
        Dim dt As DataTable = MasterDataBL.GetList_Ingredient(txtSearch.Text.Trim, ddlstatus.SelectedValue)
        rptList.DataSource = dt
        rptList.DataBind()

        Dim dt1 As DataTable = MasterDataBL.GetList_IngredientBy(IngredientID)
        rptListIngredient.DataSource = dt1
        rptListIngredient.DataBind()
    End Sub

#Region "SKU_Ingredient"

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim lblIngredientID As Label = e.Item.FindControl("lblIngredientID")
        Dim lblIngredientName As Label = e.Item.FindControl("lblIngredientName")
        Dim lblSequenceID As Label = e.Item.FindControl("lblSequenceID")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        lblIngredientName.Text = e.Item.DataItem("SKU_Ingredient_Name")
        lblIngredientID.Text = e.Item.DataItem("SKU_Ingredient_ID")
        lblSequenceID.Text = e.Item.DataItem("Sequence_ID")
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblIngredientName As Label = e.Item.FindControl("lblIngredientName")
        Dim lblSequenceID As Label = e.Item.FindControl("lblSequenceID")
        Dim lblIngredientID As Label = e.Item.FindControl("lblIngredientID")

        Select Case e.CommandName
            Case "EDIT"
                ClearForm()
                FillInData(lblSequenceID.Text)

                BindList(lblIngredientID.Text)
                pnlList.Visible = False
                pnlEdit.Visible = True

                LogFileData.LogTrans(UserSession, "Edit Ingredient " & lblIngredientName.Text)
            Case "DELETE"
                Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.DeleteIngredient(lblSequenceID.Text)
                If ret.IsSuccess = True Then
                    LogFileData.LogTrans(UserSession, "Delete Ingredient " & lblIngredientName.Text)

                    BindList("")
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Delete Data Fail');", True)
                    LogFileData.LogError(UserSession, ret.ErrorMessage)
                End If
        End Select
    End Sub

    Private Sub FillInData(IngredientID As Long)
        Dim ret As MsSkuIngredientLinqDB = MasterDataBL.GetIngredientData(IngredientID)
        If ret.SEQUENCE_ID > 0 Then
            lblSequenceID.Text = ret.SEQUENCE_ID
            txtIngredientID.Text = ret.SKU_INGREDIENT_ID
            txtIngredientName.Text = ret.SKU_INGREDIENT_NAME
            chkActive.Checked = (ret.ACTIVE_STATUS = "Y")
        End If
        ret = Nothing
    End Sub

    Private Sub ClearForm()
        lblSequenceID.Text = "0"
        txtIngredientID.Text = ""
        txtIngredientName.Text = ""
        chkActive.Checked = True
        txtIngredientID.Enabled = False
        Bindddl()
    End Sub

    Private Sub btnAddIngredient_Click(sender As Object, e As EventArgs) Handles btnAddIngredient.Click
        ClearForm()
        pnlEdit.Visible = True
        pnlList.Visible = False
        txtIngredientID.Enabled = True
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Add Ingredient")
    End Sub

    Private Function ValidateData() As Boolean
        If lblSequenceID.Text = "0" Then
            'กรณีเป็นการเพิ่มใหม่ ให้ตรวจสอบข้อมูลการเข้าระบบด้วย
            If txtIngredientID.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุรหัสส่วนผสม');", True)
                Return False
            End If

            If txtIngredientName.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุชื่อส่วนผสม');", True)
                Return False
            End If
        End If

        Return True
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save Ingredient")
        If ValidateData() = True Then

            Dim lnq As MsSkuIngredientLinqDB = MasterDataBL.SaveIngredient(UserSession.UserName, lblSequenceID.Text, txtIngredientID.Text, txtIngredientName.Text, chkActive.Checked)

            'ข้อมูลตาราง Ingredient_By
            Dim DTIngredient As New DataTable
            DTIngredient = GetIngredientList()
            Dim lnqIngredient As MsSkuIngredientByLinqDB = MasterDataBL.SaveIngredientBy(UserSession.UserName, lnq.SKU_INGREDIENT_ID, DTIngredient)

            If lnq.SEQUENCE_ID > 0 Then
                lblSequenceID.Text = lnq.SEQUENCE_ID
                LogFileData.LogTrans(UserSession, "Save Ingredient " & txtIngredientName.Text & " สำเร็จ")
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Data Complete');", True)
                pnlEdit.Visible = False
                pnlList.Visible = True
                BindList("")
            Else
                LogFileData.LogError(UserSession, "Save Ingredient " & txtIngredientName.Text & " ไม่สำเร็จ Error :" & lnq.ErrorMessage)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Fail');", True)
            End If
            lnq = Nothing
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Cancel")
        ClearForm()
        pnlEdit.Visible = False
        pnlList.Visible = True
        BindList("")
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click, txtSearch.TextChanged
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม ค้นหา")
        BindList("")
    End Sub

    Private Sub ddlstatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlstatus.SelectedIndexChanged
        BindList("")
    End Sub

#End Region

#Region "Ingredient By"
    Private Function GetIngredientList() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("Sequence_ID", GetType(Long))
        dt.Columns.Add("SKU_Ingredient_ID", GetType(Long))
        dt.Columns.Add("SKU_Group_ID")
        dt.Columns.Add("SKU_Group_Name")
        dt.Columns.Add("SKU_ID")
        dt.Columns.Add("Amount")
        dt.Columns.Add("Quantity")
        dt.Columns.Add("Volume")
        dt.Columns.Add("Active_Status")

        For Each itm As RepeaterItem In rptListIngredient.Items
            Dim lblSKUIngredientID As Label = itm.FindControl("lblSKUIngredientID")
            Dim lblSequenceID As Label = itm.FindControl("lblSequenceID")

            Dim lblSKUGroupID As Label = itm.FindControl("lblSKUGroupID")
            Dim lblSKUGroup As Label = itm.FindControl("lblSKUGroup")

            Dim lblSKUID As Label = itm.FindControl("lblSKUID")

            Dim lblSKUAmount As Label = itm.FindControl("lblSKUAmount")
            Dim lblSKUQuantity As Label = itm.FindControl("lblSKUQuantity")
            Dim lblSKUVolume As Label = itm.FindControl("lblSKUVolume")

            Dim lblSKUIngredientStatus As Label = itm.FindControl("lblSKUIngredientStatus")


            Dim dr As DataRow = dt.NewRow
            dr("Sequence_ID") = lblSequenceID.Text
            dr("SKU_Ingredient_ID") = 0
            dr("SKU_Group_ID") = lblSKUGroupID.Text
            dr("SKU_Group_Name") = lblSKUGroup.Text
            dr("SKU_ID") = lblSKUID.Text
            dr("Amount") = lblSKUAmount.Text
            dr("Quantity") = lblSKUQuantity.Text
            dr("Volume") = lblSKUVolume.Text
            dr("Active_Status") = lblSKUIngredientStatus.Text

            dt.Rows.Add(dr)
        Next
        Return dt
    End Function

    Private Sub Bindddl()
        MasterDataBL.GetListDDL_SKUGroup(ddlSKUGroup)
        MasterDataBL.GetListDDL_SKU(ddlSKU, ddlSKUGroup.SelectedValue)
    End Sub

    Private Sub rptListIngredient_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptListIngredient.ItemDataBound
        Dim lblSKUIngredientID As Label = e.Item.FindControl("lblSKUIngredientID")
        Dim lblSequenceID As Label = e.Item.FindControl("lblSequenceID")

        Dim lblSKUGroupID As Label = e.Item.FindControl("lblSKUGroupID")
        Dim lblSKUGroup As Label = e.Item.FindControl("lblSKUGroup")

        Dim lblSKUID As Label = e.Item.FindControl("lblSKUID")

        Dim lblSKUAmount As Label = e.Item.FindControl("lblSKUAmount")
        Dim lblSKUQuantity As Label = e.Item.FindControl("lblSKUQuantity")
        Dim lblSKUVolume As Label = e.Item.FindControl("lblSKUVolume")

        Dim lblSKUIngredientStatus As Label = e.Item.FindControl("lblSKUIngredientStatus")

        Dim lblRowIndex As Label = e.Item.FindControl("lblRowIndex")

        lblRowIndex.Text = e.Item.ItemIndex

        lblSequenceID.Text = e.Item.DataItem("Sequence_ID")
        lblSKUIngredientID.Text = e.Item.DataItem("SKU_Ingredient_ID")

        lblSKUGroupID.Text = e.Item.DataItem("SKU_Group_ID")
        lblSKUGroup.Text = e.Item.DataItem("SKU_Group_Name")
        lblSKUID.Text = e.Item.DataItem("SKU_ID")

        lblSKUAmount.Text = e.Item.DataItem("Amount")
        lblSKUQuantity.Text = e.Item.DataItem("Quantity")
        lblSKUVolume.Text = e.Item.DataItem("Volume")

        lblSKUIngredientStatus.Text = e.Item.DataItem("active_status")
        chkActiveSKUIngredient.Checked = (e.Item.DataItem("active_status") = "Y")

    End Sub

    Private Sub rptListIngredient_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptListIngredient.ItemCommand
        Dim lblSKUIngredientID As Label = e.Item.FindControl("lblSKUIngredientID")
        Dim lblSequenceID As Label = e.Item.FindControl("lblSequenceID")

        Dim lblSKUGroupID As Label = e.Item.FindControl("lblSKUGroupID")
        Dim lblSKUGroup As Label = e.Item.FindControl("lblSKUGroup")

        Dim lblSKUID As Label = e.Item.FindControl("lblSKUID")

        Dim lblSKUAmount As Label = e.Item.FindControl("lblSKUAmount")
        Dim lblSKUQuantity As Label = e.Item.FindControl("lblSKUQuantity")
        Dim lblSKUVolume As Label = e.Item.FindControl("lblSKUVolume")

        Dim lblSKUIngredientStatus As Label = e.Item.FindControl("lblSKUIngredientStatus")
        Dim lblRowIndex As Label = e.Item.FindControl("lblRowIndex")

        Select Case e.CommandName
            Case "EDIT"
                ClearFormIngredient()

                lblRowIndex.Text = e.Item.ItemIndex
                Session("row") = lblRowIndex.Text
                MasterDataBL.GetListDDL_SKUGroup(ddlSKUGroup)
                ddlSKUGroup.SelectedValue = lblSKUGroupID.Text
                MasterDataBL.GetListDDL_SKU(ddlSKU, ddlSKUGroup.SelectedValue)
                ddlSKU.SelectedValue = lblSKUID.Text
                txtSKUAmount.Text = lblSKUAmount.Text
                txtSKUQuantity.Text = lblSKUQuantity.Text
                txtSKUVolume.Text = lblSKUVolume.Text

                If lblSKUIngredientStatus.Text = "Y" Then
                    chkActiveSKUIngredient.Checked = True
                Else
                    chkActiveSKUIngredient.Checked = False
                End If

                pnlList.Visible = False
                pnlEdit.Visible = False
                pnlEditIngredient.Visible = True

                LogFileData.LogTrans(UserSession, "คลิกปุ่ม Edit Ingredient " & lblSKUID.Text)
            Case "DELETE"
                Dim dt As DataTable = GetIngredientList()
                dt.Rows.RemoveAt(e.Item.ItemIndex)

                rptListIngredient.DataSource = dt
                rptListIngredient.DataBind()
        End Select
    End Sub

    Private Sub btnAddSkuIngredientt_Click(sender As Object, e As EventArgs) Handles btnAddSkuIngredient.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่มเพิ่ม  Ingredient ")
        pnlList.Visible = False
        pnlEdit.Visible = False
        pnlEditIngredient.Visible = True
        ClearFormIngredient()
    End Sub

    Private Sub ClearFormIngredient()
        Session("row") = ""
        ddlSKUGroup.SelectedValue = 0
        ddlSKU.SelectedValue = 0
        txtSKUAmount.Text = ""
        txtSKUQuantity.Text = ""
        txtSKUVolume.Text = ""
        chkActiveSKUIngredient.Checked = True
    End Sub

    Private Sub btnSaveSkuIngredient_Click(sender As Object, e As EventArgs) Handles btnSaveSkuIngredient.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save Ingredient " & ddlSKU.SelectedItem.ToString())
        If ValidateIngredientData() = True Then
            pnlList.Visible = False
            pnlEdit.Visible = True
            pnlEditIngredient.Visible = False

            Dim dt As DataTable = GetIngredientList()
            If Session("row").ToString() = "" Then
                'Add
                Dim dr As DataRow = dt.NewRow
                dr("Sequence_ID") = dt.Rows.Count + 1
                dr("SKU_Ingredient_ID") = 0
                dr("SKU_Group_ID") = ddlSKUGroup.SelectedValue
                dr("SKU_Group_Name") = ddlSKUGroup.SelectedItem.ToString()
                dr("SKU_ID") = ddlSKU.SelectedValue
                dr("Amount") = txtSKUAmount.Text
                dr("Quantity") = txtSKUQuantity.Text
                dr("Volume") = txtSKUVolume.Text
                If chkActiveSKUIngredient.Checked Then
                    dr("Active_Status") = "Y"
                Else
                    dr("Active_Status") = "N"
                End If

                dt.Rows.Add(dr)
            Else
                'Edit
                Dim itm As RepeaterItem = rptListIngredient.Items(Session("row").ToString())

                dt.Rows(Session("row").ToString())("Sequence_ID") = dt.Rows.Count + 1
                dt.Rows(Session("row").ToString())("SKU_Ingredient_ID") = 0
                dt.Rows(Session("row").ToString())("SKU_Group_ID") = ddlSKUGroup.SelectedValue
                dt.Rows(Session("row").ToString())("SKU_Group_Name") = ddlSKUGroup.SelectedItem.ToString()
                dt.Rows(Session("row").ToString())("SKU_ID") = ddlSKU.SelectedValue
                dt.Rows(Session("row").ToString())("Amount") = txtSKUAmount.Text
                dt.Rows(Session("row").ToString())("Quantity") = txtSKUQuantity.Text
                dt.Rows(Session("row").ToString())("Volume") = txtSKUVolume.Text
                If chkActiveSKUIngredient.Checked Then
                    dt.Rows(Session("row").ToString())("Active_Status") = "Y"
                Else
                    dt.Rows(Session("row").ToString())("Active_Status") = "N"
                End If
            End If

            rptListIngredient.DataSource = dt
            rptListIngredient.DataBind()
        End If


    End Sub

    Private Sub btnCanelSkuIngredient_Click(sender As Object, e As EventArgs) Handles btnCanelSkuIngredient.Click
        pnlList.Visible = False
        pnlEdit.Visible = True
        pnlEditIngredient.Visible = False
    End Sub
    Private Function ValidateIngredientData() As Boolean
        If ddlSKUGroup.SelectedValue = "0" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกกลุ่มชนิดสินค้า');", True)
            Return False
        End If

        If ddlSKU.SelectedValue = "0" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกชนิดสินค้า');", True)
            Return False
        End If

        If txtSKUAmount.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุ Amount');", True)
            Return False
        End If
        If txtSKUQuantity.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุ Quantity');", True)
            Return False
        End If
        If txtSKUVolume.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุ Volume');", True)
            Return False
        End If
        Return True
    End Function

    Private Sub ddlSKUGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSKUGroup.SelectedIndexChanged
        MasterDataBL.GetListDDL_SKU(ddlSKU, ddlSKUGroup.SelectedValue)
        ddlSKU.SelectedValue = 0
    End Sub
#End Region
End Class