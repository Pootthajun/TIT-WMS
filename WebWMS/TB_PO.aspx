﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="TB_PO.aspx.vb" Inherits="WebWMS.TB_PO" %>

<%@ Register Src="~/UserControl/ucSwitchButton.ascx" TagPrefix="uc1" TagName="ucSwitchButton" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="page-header">
        <h4 class="page-title">เอกสารใบสั่งซื้อ(PO)</h4>
    </div>
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <asp:Panel ID="pnlList" runat="server" CssClass="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <asp:LinkButton ID="btnAddPO" runat="server" CssClass="btn btn-warning">
                                    <i class="icon md-plus-circle-o"></i>
                                    <span>เพื่มเอกสารใบสั่งซื้อ</span>
                                </asp:LinkButton>
                            </div>
                            <div class="col-sm-3">
                                <asp:DropDownList ID="ddlSearchReceiveType" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="ประเภทเอกสาร" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-sm-2">
                                <asp:TextBox ID="txtSearchPOID" runat="server" CssClass="form-control" MaxLength="100" placeholder="เลขที่เอกสาร"></asp:TextBox>
                            </div>                        
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtSearchPoDate" runat="server" CssClass="form-control" MaxLength="100" placeholder="วันที่ออกเอกสาร"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                    Format="dd/MM/yyyy" TargetControlID="txtSearchPoDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                            </div>
                             <div class="col-sm-1">
                                        <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn btn-warning">
                                        <i class="icon fa-search"></i>
                                        </asp:LinkButton>
                                    </div>
                        </div>

                        <div class="clearfix visible-md-block"></div>
                        <br />
                        <table class="tablesaw table-striped table-bordered table-hover">
                            <thead>
                                <tr class="bg-blue-grey-100">
                                    <th class="text-center">ประเภทการรับ</th>
                                    <th class="text-center">รหัสเอกสารใบสั่งซื้อ</th>
                                    <th class="text-center">วันที่ออกใบ</th>
                                    <th class="text-center">ใช้งาน</th>
                                    <th class="text-center">แก้ไข</th>
                                    <th class="text-center" style="display: none">ลบ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptList" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td class="text-center">
                                                <asp:Label ID="lblRecieveType" runat="server"></asp:Label>
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="lblPOID" runat="server"></asp:Label>
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="lblPODate" runat="server"></asp:Label>
                                            </td>
                                            <td class="text-center">
                                                <uc1:ucSwitchButton runat="server" ID="chkActiveStatus" AutoPostBack="true" OnCheckedChanged="chkActiveStatus_CheckedChanged" />
                                            </td>
                                            <td class="text-center">
                                                <asp:LinkButton ID="btnEdit" runat="server" CommandName="EDIT">
                                                    <i class="icon md-edit" style="font-size: 20px;"></i>
                                                </asp:LinkButton>
                                            </td>
                                            <td class="text-center" style="display: none">
                                                <asp:LinkButton ID="btnDelete" runat="server" CommandName="DELETE" OnClientClick="return confirm('Do you want delete data?');">
                                                    <i class="icon md-delete" style="font-size: 20px;"></i>
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlEdit" runat="server" CssClass="panel" Visible="false">
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>รหัสเอกสาร (PO) :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-9">
                                <asp:TextBox ID="txtPOID" runat="server" CssClass="form-control" MaxLength="100" placeholder="รหัสเอกสารใบสั่งซื้อ"></asp:TextBox>
                                <asp:Label ID="lblSequenceID" runat="server" Visible="false" Text="0"></asp:Label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>วันที่ออกเอกสาร :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-9">
                                <asp:TextBox ID="txtPODate" runat="server" CssClass="form-control" MaxLength="100" placeholder="วันที่ออกเอกสาร"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server"
                                    Format="dd/MM/yyyy" TargetControlID="txtPODate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>ประเภทการรับ :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-9">
                                <asp:DropDownList ID="ddlReceiveType" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="ประเภทการรับ" AutoPostBack="true"></asp:DropDownList>
                            </div>
                        </div>
                        <h4 style="margin: 2px;">รายการสินค้า</h4>
                        <br />
                        <table class="tablesaw table-striped table-bordered table-hover">
                            <thead>
                                <tr class="bg-blue-grey-100">
                                    <th class="text-center">รหัส SKU</th>
                                    <th class="text-center">Lot_Batch</th>
                                    <th class="text-center">สถานะสินค้า</th>
                                    <th class="text-center">จำนวน</th>
                                    <th class="text-center">ลบ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptsku" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td class="text-center">
                                                <asp:DropDownList ID="ddlSku" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="Sku"></asp:DropDownList>
                                            </td>
                                            <td class="text-center">
                                                <asp:TextBox ID="txtLotBatch" runat="server" CssClass="form-control" MaxLength="100" placeholder="รหัสเอกสารใบสั่งซื้อ"></asp:TextBox>
                                            </td>
                                            <td class="text-center">
                                                <asp:DropDownList ID="ddlProductStatus" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="สถานะสินค้า"></asp:DropDownList>
                                            </td>
                                            <td class="text-center">
                                                <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control" MaxLength="100" placeholder="จำนวน"></asp:TextBox>
                                            </td>
                                            <td class="text-center">
                                                <asp:LinkButton ID="btnDelete" runat="server" CommandName="DELETE" OnClientClick="return confirm('Do you want delete data?');">
                                                    <i class="icon md-delete" style="font-size: 20px;"></i>
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-sm-12" style="margin-top: 10px; text-align: right">
                                <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="+" CssClass="btn btn-primary" AutoPostBack="true" />
                            </div>
                        </div>
                        <br />
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>ใช้งาน</label>
                            </div>

                            <div class="col-sm-9">
                                <uc1:ucSwitchButton runat="server" ID="chkActive" />
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <hr />
                        <div class="form-group form-material">
                            <div class="col-sm-12 col-sm-offset-9">
                                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary">
                                    <span>Save</span>
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-warning">
                                    <span>Cancel</span>
                                </asp:LinkButton>
                            </div>
                        </div>

                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpFooter" runat="server">
</asp:Content>
