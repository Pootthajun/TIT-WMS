﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="MS_Role.aspx.vb" Inherits="WebWMS.MS_Role" %>

<%@ Register Src="~/UserControl/ucSwitchButton.ascx" TagPrefix="uc1" TagName="ucSwitchButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="page-header">
        <h4 class="page-title">สิทธิ์การใช้งาน</h4>
    </div>
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <asp:Panel ID="pnlList" runat="server" CssClass="panel">
                    <div class="panel-body">
                        <asp:LinkButton ID="btnAddRole" runat="server" CssClass="btn btn-warning">
                            <i class="icon md-plus-circle-o"></i>
                            <span>เพื่มสิทธิ์การใช้งาน</span>
                        </asp:LinkButton>

                        <asp:LinkButton ID="btnOpenSearch" runat="server" CssClass="btn btn-primary">
                            <i class="icon fa-search"></i>
                            <span>ค้นหา</span>
                        </asp:LinkButton>
                        
                        <asp:Panel ID="pnlSearch" runat="server" Visible="false" >
                            <br />
                            <div class="row">
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="100" placeholder="สิทธิ์การใช้งาน"></asp:TextBox>
                                </div>
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddlstatus" CssClass="form-control" AutoPostBack="true" runat="server">
                                        <asp:ListItem Text="สถานะการใช้งาน" Value=""></asp:ListItem>
                                        <asp:ListItem Text="ใช้งาน" Value="Y"></asp:ListItem>
                                        <asp:ListItem Text="ไม่ใช้งาน" Value="N"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-sm-1">
                                    <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn btn-warning">
                                        <i class="icon fa-search"></i>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </asp:Panel>

                        <div class="clearfix visible-md-block"></div>
                        <br />
                        <br />

                        <table class="tablesaw table-striped table-bordered table-hover">
                            <thead>
                                <tr class="bg-blue-grey-100">
                                    <th class="text-center">สิทธิ์การใช้งาน</th>
                                    <th class="text-center">จำนวนเมนู</th>
                                    <th class="text-center">จำนวนผู้ใช้</th>
                                    <th class="text-center">ใช้งาน</th>
                                    <th class="text-center">แก้ไข</th>
                                    <th class="text-center">ลบ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptList" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblRoleName" runat="server"></asp:Label>
                                                <asp:Label ID="lblRoleID" runat="server" Visible="false"></asp:Label>
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="lblMenuQty" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblUserQty" runat="server"></asp:Label>
                                            </td>
                                            <td class="text-center">
                                                <uc1:ucSwitchButton runat="server" ID="chkActiveStatus" AutoPostBack="true" OnCheckedChanged="chkActiveStatus_CheckedChanged" />
                                            </td>
                                            <td class="text-center">
                                                <asp:LinkButton ID="btnEdit" runat="server" CommandName="EDIT">
                                                    <i class="icon md-edit" style="font-size: 20px;"></i>
                                                </asp:LinkButton>
                                            </td>
                                            <td class="text-center">
                                                <asp:LinkButton ID="btnDelete" runat="server" CommandName="DELETE" OnClientClick="return confirm('Do you want delete data?');">
                                                    <i class="icon md-delete" style="font-size: 20px;"></i>
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlEdit" runat="server"  Visible="false">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="form-group col-sm-2">
                                    <label>สิทธิ์การใช้งาน :<span style="color: red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtRoleName" runat="server" CssClass="form-control" MaxLength="255" placeholder="สิทธิ์การใช้งาน"></asp:TextBox>
                                    <asp:Label ID="lblRoleID" runat="server" Visible="false" Text="0"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group col-sm-2">
                                    <label>ใช้งาน</label>
                                </div>

                                <div class="col-sm-9">
                                    <uc1:ucSwitchButton runat="server" ID="chkActive" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-body">
                            <div class="nav-tabs-horizontal nav-tabs-inverse">
                                <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                                    <li role="presentation" id="liTabMenu" runat="server">
                                        <asp:LinkButton ID="btnTabMenu" runat="server">
                                            เมนู
                                        </asp:LinkButton>
                                    </li>
                                    <li role="presentation" id="liTabUser" runat="server">
                                        <asp:LinkButton ID="btnTabUser" runat="server">
                                            ผู้ใช้งาน
                                        </asp:LinkButton>
                                    </li>
                                </ul>

                                <div class="row row-lg">
                                    <div class="tab-content padding-20">
                                        <asp:Panel ID="pnlTabMenu" runat="server"  >
                                            <table class="tablesaw table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr class="bg-blue-grey-100">
                                                        <th class="text-center">ชื่อเมนู</th>
                                                        <th class="text-center">กำหนดสิทธิ์</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <asp:Repeater ID="rptMenuList" runat="server">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblMenuName" runat="server"></asp:Label>
                                                                    <asp:Label ID="lblMenuID" runat="server" Visible="false" ></asp:Label>
                                                                </td>
                                                                <td class="text-center">
                                                                    <uc1:ucSwitchButton runat="server" ID="chkMenuGrant" Checked="false" />
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </tbody>
                                            </table>
                                        </asp:Panel>

                                        <asp:Panel ID="pnlTabUser" runat="server">
                                            <div class="col-md-12">
                                                <!-- Example Panel With Footer -->
                                                <div class="panel panel-bordered">
                                                    <div class="panel-body">
                                                        <asp:Repeater ID="rptUserList" runat="server">
                                                            <ItemTemplate>
                                                                <span class="btn btn-info text-white h5 user-item">
                                                                    <i class="fa fa-user"></i>
                                                                    <asp:Label ID="lblUserName" runat="server"></asp:Label>
                                                                    <asp:Label ID="lblUserID" runat="server" Visible="false"></asp:Label>
                                                                    <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete">x</asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                                <!-- End Example Panel With Footer -->
                                            </div>

                                            <div class="form-group form-material">
                                                <div class="col-sm-5">
                                                    <asp:LinkButton CssClass="btn btn-primary btn-icon loading-demo mr5 btn-shadow" ID="btnOpenDialogAddUser" runat="server">
                                                        <i class="fa fa-plus-circle"></i>
                                                        <span>Add User</span>
                                                    </asp:LinkButton>
                                                </div>
                                            </div>


                                            <!-- Modal -->
                                            <asp:Panel ID="pnlDialogAddUser" runat="server" CssClass="modal fade in" aria-hidden="true"
                                                aria-labelledby="examplePositionCenter" role="dialog" TabIndex="-1" Style="display: block;" Visible="false">
                                                <div class="modal-dialog modal-center">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <asp:LinkButton ID="btnCloseDialogAddUser" runat="server" CssClass="close" aria-label="Close" >
                                                            <span aria-hidden="true">x</span>
                                                        </asp:LinkButton>
                                                        <h4 class="modal-title">Add User 
                                                            <asp:Label ID="lblDialogRoleName" runat="server"></asp:Label>
                                                        </h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <h4>ผู้ใช้ที่มีสิทธิ์</h4>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <h4>ผู้ใช้ที่ไม่มีสิทธิ์</h4>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <table class="tablesaw table-striped table-bordered table-hover">
                                                                    <thead>
                                                                        <tr class="bg-blue-grey-100">
                                                                            <th class="text-center">ผู้ใช้ระบบ</th>
                                                                            <th class="text-center width-50">#</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <asp:Repeater ID="rptDialogGrantUser" runat="server">
                                                                            <ItemTemplate>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="lblUserID" runat="server" Visible="false"></asp:Label>
                                                                                        <asp:Label ID="lblUserFullName" runat="server"></asp:Label>
                                                                                        <asp:Label ID="lblFirstName" runat="server" Visible="false"></asp:Label>
                                                                                        <asp:Label ID="lblLastName" runat="server" Visible="false"></asp:Label>
                                                                                        <asp:Label ID="lblUserLogin" runat="server" visible="false"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:LinkButton ID="lnkUserDeny" runat="server" CssClass="btn btn-danger" CommandName="DENY" >
                                                                                             <i class="fa fa-times-circle"></i>
                                                                                        </asp:LinkButton>
                                                                                    </td>
                                                                                </tr>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <table class="tablesaw table-striped table-bordered table-hover">
                                                                    <thead>
                                                                        <tr class="bg-blue-grey-100">
                                                                            <th class="text-center width-50">#</th>
                                                                            <th class="text-center">ผู้ใช้ระบบ</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <asp:Repeater ID="rptDialogUnselectUser" runat="server">
                                                                            <ItemTemplate>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:LinkButton ID="lnkUserSelected" runat="server" CssClass="btn btn-primary" CommandName="GRANT" >
                                                                                            <i class="fa fa-plus-circle"></i>
                                                                                        </asp:LinkButton>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="lblUserID" runat="server" Visible="false"></asp:Label>
                                                                                        <asp:Label ID="lblUserFullName" runat="server"></asp:Label>
                                                                                        <asp:Label ID="lblFirstName" runat="server" Visible="false"></asp:Label>
                                                                                        <asp:Label ID="lblLastName" runat="server" Visible="false"></asp:Label>
                                                                                        <asp:Label ID="lblUserLogin" runat="server" visible="false"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                     </tbody>
                                                                </table>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <asp:LinkButton ID="btnDialogAddUser" runat="server" CssClass="btn btn-primary">
                                                            <i class="fa fa-plus-circle"></i>
                                                            <span>Add</span>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                                </div>
                                            </asp:Panel>
                                            <!-- End Modal -->
                                            
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group form-material">
                        <div class="col-sm-12 col-sm-offset-9">
                            <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary">
                            <span>Save</span>
                            </asp:LinkButton>
                            <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-warning">
                            <span>Cancel</span>
                            </asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>


    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpFooter" runat="server">
</asp:Content>
