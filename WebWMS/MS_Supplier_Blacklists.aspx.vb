﻿Imports LinqDB.TABLE
Imports WMS_BL

Public Class MS_Supplier_Blacklists
    Inherits System.Web.UI.Page

    Private ReadOnly Property UserSession As LoginSessionData
        Get
            Return Session(CommonBL.UserLoginSessionName)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect("frmLogin.aspx?rnd=" & DateTime.Now.Millisecond)
                Exit Sub
            Else
                BindList()
                ClearForm()
            End If
        End If
    End Sub

    Protected Sub chkActiveStatus_CheckedChanged(sender As ucSwitchButton, e As System.EventArgs)
        Dim chkActiveStatus As ucSwitchButton = sender
        Dim itm As RepeaterItem = chkActiveStatus.Parent
        Dim lblBacklistID As Label = itm.FindControl("lblBacklistID")
        Dim lblBacklistName As Label = itm.FindControl("lblBacklistName")

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.UpdateSupplierBackliststatus(UserSession.UserName, lblBacklistID.Text, chkActiveStatus.Checked)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Update status fail');", True)
            LogFileData.LogError(UserSession, ret.ErrorMessage)
        Else
            LogFileData.LogTrans(UserSession, "Update Backlist Status " & lblBacklistName.Text & " Status : " & chkActiveStatus.Checked.ToString)
        End If
    End Sub

    Private Sub BindList()
        Dim dt As DataTable = MasterDataBL.GetList_Supplier_Backlist("", Nothing)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Private Sub BindDDL()
        MasterDataBL.GetListDDL_Supplier(ddlSupplier)
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblBacklistName As Label = e.Item.FindControl("lblBacklistName")
        Dim lblBacklistID As Label = e.Item.FindControl("lblBacklistID")
        Dim lblDate As Label = e.Item.FindControl("lblDate")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        Dim startDate As String = e.Item.DataItem("Blacklist_StartDate")
        Dim EndDate As String = e.Item.DataItem("Blacklist_EndDate")

        lblBacklistID.Text = e.Item.DataItem("Supplier_Blacklist_ID")
        lblBacklistName.Text = e.Item.DataItem("SupName")
        lblDate.Text = startDate + "-" + EndDate
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblBacklistName As Label = e.Item.FindControl("lblBacklistName")
        Dim lblBacklistID As Label = e.Item.FindControl("lblBacklistID")

        Select Case e.CommandName
            Case "EDIT"
                ClearForm()
                FillInData(lblBacklistID.Text)

                pnlList.Visible = False
                pnlEdit.Visible = True

                LogFileData.LogTrans(UserSession, "Edit Backlist  " & lblBacklistName.Text)
            Case "DELETE"
                Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.DeleteSupplier_Backlist(lblBacklistID.Text)
                If ret.IsSuccess = True Then
                    LogFileData.LogTrans(UserSession, "Delete Backlist  " & lblBacklistName.Text)

                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Delete Data Fail');", True)
                    LogFileData.LogError(UserSession, ret.ErrorMessage)
                End If
        End Select
    End Sub

    Private Sub FillInData(Supplier_BacklistID As Long)
        Dim ret As MsSupplierBlacklistLinqDB = MasterDataBL.GetSupplier_BacklistData(Supplier_BacklistID)
        If ret.SUPPLIER_BLACKLIST_ID > 0 Then
            lblBacklistID.Text = ret.SUPPLIER_BLACKLIST_ID
            ddlSupplier.SelectedValue = ret.SUPPLIER_ID

            Dim _start_date As String = ""
            Dim _end_date As String = ""

            _start_date = Convert.ToDateTime(ret.BLACKLIST_STARTDATE).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
            _end_date = Convert.ToDateTime(ret.BLACKLIST_ENDDATE).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))

            txtStartDate.Text = _start_date
            txtEndDate.Text = _end_date

            txtReason.Text = ret.BLACKLIST_REASON
            txtNotation.Text = ret.NOTATION
            chkActive.Checked = (ret.ACTIVE_STATUS = "Y")
        End If
        ret = Nothing
    End Sub

    Private Sub ClearForm()
        lblBacklistID.Text = "0"
        txtStartDate.Text = ""
        txtEndDate.Text = ""
        txtReason.Text = ""
        txtNotation.Text = ""
        chkActive.Checked = True

        BindDDL()
        ddlSupplier.SelectedValue = 0
    End Sub

    Private Sub btnAddBacklistSup_Click(sender As Object, e As EventArgs) Handles btnAddBacklistSup.Click
        pnlEdit.Visible = True
        pnlList.Visible = False
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Add Backlist")
    End Sub

    Private Function ValidateData() As Boolean
        'If lblPositionID.Text = "0" Then
        '    'กรณีเป็นการเพิ่มใหม่ ให้ตรวจสอบข้อมูลการเข้าระบบด้วย
        '    If txtPositiontName.Text = "" Then
        '        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุชื่อตำแหน่ง');", True)
        '        Return False
        '    End If
        'End If

        'Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.CheckDuplicatePosition(lblPositionID.Text, txtPositiontName.Text)
        'If ret.IsSuccess = False Then
        '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');", True)
        '    Return False
        'End If

        Return True
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save Backlist")
        If ValidateData() = True Then

            Dim lnq As MsSupplierBlacklistLinqDB = MasterDataBL.SaveSupplier_BacklistData(UserSession.UserName, lblBacklistID.Text, ddlSupplier.SelectedValue, txtStartDate.Text.Trim, txtEndDate.Text.Trim, txtReason.Text, chkActive.Checked)
            If lnq.SUPPLIER_BLACKLIST_ID > 0 Then
                lblBacklistID.Text = lnq.SUPPLIER_BLACKLIST_ID
                LogFileData.LogTrans(UserSession, "Save Backlist " & ddlSupplier.SelectedItem.ToString() & " สำเร็จ")
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Data Complete');", True)
                pnlEdit.Visible = False
                pnlList.Visible = True
                BindList()
            Else
                LogFileData.LogError(UserSession, "Save Backlist " & ddlSupplier.SelectedItem.ToString() & " ไม่สำเร็จ Error :" & lnq.ErrorMessage)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Fail');", True)
            End If
            lnq = Nothing
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Cancel")
        ClearForm()
        pnlEdit.Visible = False
        pnlList.Visible = True
        BindList()
    End Sub
End Class