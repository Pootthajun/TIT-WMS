﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="MS_Supplier_Blacklists.aspx.vb" Inherits="WebWMS.MS_Supplier_Blacklists" %>

<%@ Register Src="~/UserControl/ucSwitchButton.ascx" TagPrefix="uc1" TagName="ucSwitchButton" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="page-header">
        <h4 class="page-title">ข้อมูล BLACKLIST ผู้ขาย</h4>
    </div>
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <asp:Panel ID="pnlList" runat="server" CssClass="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-5">
                                <asp:LinkButton ID="btnAddBacklistSup" runat="server" CssClass="btn btn-warning">
                                    <i class="icon md-plus-circle-o"></i>
                                    <span>เพื่มBLACKLIST ผู้ขาย</span>
                                </asp:LinkButton>
                            </div>
                            <%--   <div class="col-sm-3">
                                <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="100" placeholder="ชื่อตำแหน่ง"></asp:TextBox>
                            </div>
                            <div class="col-sm-3">
                                <asp:DropDownList ID="ddlstatus" CssClass="form-control" AutoPostBack="true" runat="server">
                                    <asp:ListItem Text="สถานะการใช้งาน" Value=""></asp:ListItem>
                                    <asp:ListItem Text="ใช้งาน" Value="Y"></asp:ListItem>
                                    <asp:ListItem Text="ไม่ใช้งาน" Value="N"></asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="col-sm-1">
                                <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn btn-warning">
                                        <i class="icon fa-search"></i>
                                </asp:LinkButton>
                            </div>--%>
                        </div>

                        <div class="clearfix visible-md-block"></div>
                        <br />
                        <table class="tablesaw table-striped table-bordered table-hover">
                            <thead>
                                <tr class="bg-blue-grey-100">
                                    <th class="text-center">รายชื่อ</th>
                                    <th class="text-center">วันเริ่มต้น-วันสิ้นสุด</th>
                                    <th class="text-center">ใช้งาน</th>
                                    <th class="text-center">แก้ไข</th>
                                    <th class="text-center">ลบ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptList" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td class="text-center">
                                                <asp:Label ID="lblBacklistName" runat="server"></asp:Label>
                                                <asp:Label ID="lblBacklistID" runat="server" Visible="false"></asp:Label>
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="lblDate" runat="server"></asp:Label>
                                            </td>
                                            <td class="text-center">
                                                <uc1:ucSwitchButton runat="server" ID="chkActiveStatus" AutoPostBack="true" OnCheckedChanged="chkActiveStatus_CheckedChanged" />
                                            </td>
                                            <td class="text-center">
                                                <asp:LinkButton ID="btnEdit" runat="server" CommandName="EDIT">
                                                    <i class="icon md-edit" style="font-size: 20px;"></i>
                                                </asp:LinkButton>
                                            </td>
                                            <td class="text-center">
                                                <asp:LinkButton ID="btnDelete" runat="server" CommandName="DELETE" OnClientClick="return confirm('Do you want delete data?');">
                                                    <i class="icon md-delete" style="font-size: 20px;"></i>
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlEdit" runat="server" CssClass="panel" Visible="false">
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>ผู้ขาย :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-10">
                                <asp:DropDownList ID="ddlSupplier" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="ผู้ขาย" AutoPostBack="true"></asp:DropDownList>
                                <asp:Label ID="lblBacklistID" runat="server" Visible="false" Text="0"></asp:Label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>วันที่ :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-5 calender-top">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox CssClass="form-control m-b" ID="txtStartDate" runat="server" placeholder="เริ่มต้น"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                        Format="dd/MM/yyyy" TargetControlID="txtStartDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                </div>
                            </div>
                            <div class="col-sm-5 calender-top">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox CssClass="form-control m-b" ID="txtEndDate" runat="server" placeholder="สิ้นสุด"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                        Format="dd/MM/yyyy" TargetControlID="txtEndDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>เหตุผล :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-10">
                                <asp:TextBox ID="txtReason" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="100" placeholder="เหตุผล" TextMode="MultiLine"></asp:TextBox>
                                  <br/>
                            </div>
                        </div>
                      
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>เอกสาร :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-10">
                                <asp:TextBox ID="txtNotation" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="100" placeholder="เอกสาร"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>ใช้งาน</label>
                            </div>

                            <div class="col-sm-9">
                                <uc1:ucSwitchButton runat="server" ID="chkActive" />
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <hr />
                        <div class="form-group form-material">
                            <div class="col-sm-12 col-sm-offset-9">
                                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary">
                                    <span>Save</span>
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-warning">
                                    <span>Cancel</span>
                                </asp:LinkButton>
                            </div>
                        </div>

                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpFooter" runat="server">
</asp:Content>
