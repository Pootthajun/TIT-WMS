﻿Public Class ucSwitchButton
    Inherits System.Web.UI.UserControl

    Public Event CheckedChanged(sender As ucSwitchButton, e As System.EventArgs)
    Public Overridable Property Enabled As Boolean
        Get
            Return chk.Enabled
        End Get
        Set(value As Boolean)
            chk.Enabled = value
        End Set
    End Property
    Public Overridable Property Checked As Boolean
        Get
            Return chk.Checked
        End Get
        Set(value As Boolean)
            chk.Checked = value
        End Set
    End Property
    Public Overridable Property AutoPostBack As Boolean
        Get
            Return chk.AutoPostBack
        End Get
        Set(value As Boolean)
            chk.AutoPostBack = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        'ถ้าในหน้านี้มีการใช้ Checkbox ก็ให้มีคำสั่งข้างล่างนี้ด้วยนะ
        '################## Checkbox Style ##############################
        'ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "iniSwitchery", "iniSwitchery();", True)
        SetInitCheckBoxActive(chk)
    End Sub

    Private Sub SetInitCheckBoxActive(chk As CheckBox)
        If chk.AutoPostBack = True Then
            chk.InputAttributes.Add("onchange", "CheckboxChange('" & btn.ClientID & "');")
        End If

        chk.InputAttributes.Remove("data-plugin")
        chk.InputAttributes.Remove("data-color")

        chk.InputAttributes.Add("data-plugin", "switchery")
        chk.InputAttributes.Add("data-color", "#009933")

    End Sub

    Private Sub btn_Click(sender As Object, e As EventArgs) Handles btn.Click
        RaiseEvent CheckedChanged(Me, e)
    End Sub
End Class