﻿Imports LinqDB.TABLE
Imports WMS_BL
Public Class MS_Role
    Inherits System.Web.UI.Page

    Private ReadOnly Property UserSession As LoginSessionData
        Get
            Return Session(CommonBL.UserLoginSessionName)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect("frmLogin.aspx?rnd=" & DateTime.Now.Millisecond)
                Exit Sub
            Else
                BindList()
                'ClearForm()
            End If
        End If
    End Sub

    Private Sub BindList()
        Dim dt As DataTable = MasterDataBL.GetList_Role(txtSearch.Text, ddlstatus.SelectedValue)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblRoleID As Label = e.Item.FindControl("lblRoleID")
        Dim lblRoleName As Label = e.Item.FindControl("lblRoleName")
        Dim lblMenuQty As Label = e.Item.FindControl("lblMenuQty")
        Dim lblUserQty As Label = e.Item.FindControl("lblUserQty")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        lblRoleID.Text = e.Item.DataItem("role_id")
        lblRoleName.Text = e.Item.DataItem("role_name")
        If Convert.IsDBNull(e.Item.DataItem("role_menu_qty")) = False Then lblMenuQty.Text = e.Item.DataItem("role_menu_qty")
        If Convert.IsDBNull(e.Item.DataItem("role_user_qty")) = False Then lblUserQty.Text = e.Item.DataItem("role_user_qty")
        If e.Item.DataItem("active_status") = "Y" Then
            chkActiveStatus.Checked = True
        Else
            chkActiveStatus.Checked = False
        End If
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblRoleName As Label = e.Item.FindControl("lblRoleName")
        Dim lblRoleID As Label = e.Item.FindControl("lblRoleID")

        Select Case e.CommandName
            Case "EDIT"
                ClearForm()
                FillInData(lblRoleID.Text)

                pnlList.Visible = False
                pnlEdit.Visible = True
                CurrentTab = Tab.TabMenu

                LogFileData.LogTrans(UserSession, "คลิกปุ่ม Edit Role " & lblRoleName.Text)
            Case "DELETE"
                Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.DeleteRole(lblRoleID.Text)
                If ret.IsSuccess = True Then
                    LogFileData.LogTrans(UserSession, "Delete Role " & lblRoleName.Text)

                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Delete Data Fail');", True)
                    LogFileData.LogError(UserSession, ret.ErrorMessage)
                End If
        End Select
    End Sub

    Private Sub FillInData(RoleID As Long)
        Dim ret As MsRoleLinqDB = MasterDataBL.GetRoleData(RoleID)
        If ret.ROLE_ID > 0 Then
            lblRoleID.Text = ret.ROLE_ID
            txtRoleName.Text = ret.ROLE_NAME
            chkActive.Checked = (ret.ACTIVE_STATUS = "Y")

            BindRoleMenuList()
            BindRoleUserList()
        End If
        ret = Nothing
    End Sub

    Protected Sub chkActiveStatus_CheckedChanged(sender As ucSwitchButton, e As System.EventArgs)
        Dim chkActiveStatus As ucSwitchButton = sender
        Dim itm As RepeaterItem = chkActiveStatus.Parent
        Dim lblRoleID As Label = itm.FindControl("lblRoleID")
        Dim lblRoleName As Label = itm.FindControl("lblRoleName")

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.UpdateRoleStatus(UserSession.UserName, lblRoleID.Text, chkActiveStatus.Checked)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Update status fail');", True)
            LogFileData.LogError(UserSession, ret.ErrorMessage)
        Else
            LogFileData.LogTrans(UserSession, "Update Role Status " & lblRoleName.Text & " Status : " & chkActiveStatus.Checked.ToString)
        End If
    End Sub

    Private Sub btnAddRole_Click(sender As Object, e As EventArgs) Handles btnAddRole.Click
        ClearForm()
        pnlEdit.Visible = True
        pnlList.Visible = False
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Add Role")
        CurrentTab = Tab.TabMenu
        BindRoleMenuList()
    End Sub

    Private Sub btnOpenSearch_Click(sender As Object, e As EventArgs) Handles btnOpenSearch.Click
        pnlSearch.Visible = True
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindList()
        pnlSearch.Visible = False
    End Sub

#Region "Manage Tab"
    Protected Enum Tab
        TabMenu = 1
        TabUser = 2
    End Enum

    Protected Property CurrentTab As Tab
        Get
            Select Case True
                Case pnlTabMenu.Visible
                    Return Tab.TabMenu
                Case pnlTabUser.Visible
                    Return Tab.TabUser
                Case Else
            End Select
        End Get
        Set(value As Tab)
            pnlTabMenu.Visible = False
            pnlTabUser.Visible = False

            liTabMenu.Attributes("class") = ""
            liTabUser.Attributes("class") = ""

            Select Case value
                Case Tab.TabMenu
                    SetActiveTab(pnlTabMenu, liTabMenu)
                Case Tab.TabUser
                    SetActiveTab(pnlTabUser, liTabUser)
                Case Else
            End Select
        End Set
    End Property

    Private Sub SetActiveTab(pnl As Panel, liTab As HtmlGenericControl)
        pnl.Visible = True
        liTab.Attributes("class") = "active"
    End Sub

    Private Sub ChangeTab(sender As Object, e As System.EventArgs) Handles btnTabMenu.Click, btnTabUser.Click
        Select Case True
            Case Equals(sender, btnTabMenu)
                CurrentTab = Tab.TabMenu
            Case Equals(sender, btnTabUser)
                CurrentTab = Tab.TabUser
            Case Else
        End Select
    End Sub
#End Region

#Region "Role Menu Data"
    Private Sub BindRoleMenuList()
        Dim dt As DataTable = MasterDataBL.GetList_RoleMenu(lblRoleID.Text)
        rptMenuList.DataSource = dt
        rptMenuList.DataBind()
    End Sub

    Private Sub rptMenuList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptMenuList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblMenuName As Label = e.Item.FindControl("lblMenuName")
        Dim lblMenuID As Label = e.Item.FindControl("lblMenuID")
        Dim chkMenuGrant As ucSwitchButton = e.Item.FindControl("chkMenuGrant")

        lblMenuName.Text = e.Item.DataItem("menu_full_name")
        lblMenuID.Text = e.Item.DataItem("menu_id")
        If Convert.ToInt16(e.Item.DataItem("grant_role")) > 0 Then
            chkMenuGrant.Checked = True
        Else
            chkMenuGrant.Checked = False
        End If
    End Sub

    Private Sub rptMenuList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptMenuList.ItemCommand

    End Sub

    Private Function GetMenuList() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("menu_id", GetType(Long))
        dt.Columns.Add("menu_full_name")
        dt.Columns.Add("grant_role")

        For Each itm As RepeaterItem In rptMenuList.Items
            Dim lblMenuID As Label = itm.FindControl("lblMenuID")
            Dim lblMenuName As Label = itm.FindControl("lblMenuName")
            Dim chkMenuGrant As ucSwitchButton = itm.FindControl("chkMenuGrant")

            Dim dr As DataRow = dt.NewRow
            dr("menu_id") = lblMenuID.Text
            dr("menu_full_name") = lblMenuName.Text
            dr("grant_role") = chkMenuGrant.Checked

            dt.Rows.Add(dr)
        Next

        Return dt
    End Function
#End Region

#Region "Role User Data"
    Private Sub BindRoleUserList()
        Dim dt As DataTable = MasterDataBL.GetList_RoleUser(lblRoleID.Text)
        rptUserList.DataSource = dt
        rptUserList.DataBind()

        Dim dtAllUser As DataTable = MasterDataBL.GetList_RoleUnselectUser(lblRoleID.Text)
        rptDialogUnselectUser.DataSource = dtAllUser
        rptDialogUnselectUser.DataBind()

        Dim dtRoleUser As DataTable = MasterDataBL.GetList_RoleUser(lblRoleID.Text)
        rptDialogGrantUser.DataSource = dtRoleUser
        rptDialogGrantUser.DataBind()
    End Sub

    Private Sub rptUserList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptUserList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim lblUserName As Label = e.Item.FindControl("lblUserName")
        Dim lblUserID As Label = e.Item.FindControl("lblUserID")

        lblUserID.Text = e.Item.DataItem("user_id")
        lblUserName.Text = e.Item.DataItem("user_login")
    End Sub

    Private Function GetListSelectedUser() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("user_id", GetType(Long))
        dt.Columns.Add("user_login")

        For Each itm As RepeaterItem In rptUserList.Items
            Dim lblUserName As Label = itm.FindControl("lblUserName")
            Dim lblUserID As Label = itm.FindControl("lblUserID")

            Dim dr As DataRow = dt.NewRow
            dr("user_id") = lblUserID.Text
            dr("user_login") = lblUserName.Text
            dt.Rows.Add(dr)
        Next
        Return dt
    End Function

    Private Sub rptUserList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptUserList.ItemCommand
        Dim lblUserID As Label = e.Item.FindControl("lblUserID")
        Dim lblUserName As Label = e.Item.FindControl("lblUserName")
        Select Case e.CommandName
            Case "Delete"
                Dim dt As DataTable = GetListSelectedUser()
                dt.Rows.RemoveAt(e.Item.ItemIndex)

                LogFileData.LogTrans(UserSession, "Delete User " & lblUserName.Text)

                rptUserList.DataSource = dt
                rptUserList.DataBind()
        End Select
    End Sub

#Region "Dialog Add User"
    Private Sub btnOpenDialogAddUser_Click(sender As Object, e As EventArgs) Handles btnOpenDialogAddUser.Click
        pnlDialogAddUser.Visible = True
        lblDialogRoleName.Text = txtRoleName.Text
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Add User เพื่อ Open User Dialog")
    End Sub

    Private Sub btnCloseDialogAddUser_Click(sender As Object, e As EventArgs) Handles btnCloseDialogAddUser.Click
        pnlDialogAddUser.Visible = False
        LogFileData.LogTrans(UserSession, "คลิกปุ่มปิด Dialog")
    End Sub

    Private Sub btnDialogAddUser_Click(sender As Object, e As EventArgs) Handles btnDialogAddUser.Click
        Dim dt As DataTable = GetListGrantUser()

        rptUserList.DataSource = dt
        rptUserList.DataBind()

        pnlDialogAddUser.Visible = False
        LogFileData.LogTrans(UserSession, "คลิกปุ่มปิด Add User")
    End Sub

    Private Sub rptDialogUnselectUser_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptDialogUnselectUser.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim lblUserID As Label = e.Item.FindControl("lblUserID")
        Dim lblUserFullName As Label = e.Item.FindControl("lblUserFullName")
        Dim lblFirstName As Label = e.Item.FindControl("lblFirstName")
        Dim lblLastName As Label = e.Item.FindControl("lblLastName")
        Dim lblUserLogin As Label = e.Item.FindControl("lblUserLogin")

        lblUserID.Text = e.Item.DataItem("user_id")
        lblUserFullName.Text = e.Item.DataItem("first_name") & " " & e.Item.DataItem("last_name")
        lblFirstName.Text = e.Item.DataItem("first_name")
        lblLastName.Text = e.Item.DataItem("last_name")
        lblUserLogin.Text = e.Item.DataItem("user_login")
    End Sub

    Private Sub rptDialogUnselectUser_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptDialogUnselectUser.ItemCommand
        Dim lblUserFullName As Label = e.Item.FindControl("lblUserFullName")
        Dim lblUserID As Label = e.Item.FindControl("lblUserID")
        Dim lblFirstName As Label = e.Item.FindControl("lblFirstName")
        Dim lblLastName As Label = e.Item.FindControl("lblLastName")
        Dim lblUserLogin As Label = e.Item.FindControl("lblUserLogin")

        Select Case e.CommandName
            Case "GRANT"
                'เพิ่มชื่อในฝั่งกำหนดสิทธิ์
                Dim dt As DataTable = GetListGrantUser()
                Dim dr As DataRow = dt.NewRow
                dr("user_id") = lblUserID.Text
                dr("first_name") = lblFirstName.Text
                dr("last_name") = lblLastName.Text
                dr("user_login") = lblUserLogin.Text

                dt.Rows.Add(dr)

                rptDialogGrantUser.DataSource = dt
                rptDialogGrantUser.DataBind()
                LogFileData.LogTrans(UserSession, "กำหนดสิทธิ์ให้ User " & lblUserLogin.Text)

                'ลบชื่อออจากฝั่งที่ไม่มีสิทธิ์
                Dim dtAllUser As DataTable = GetListUnselectUser()
                dtAllUser.Rows.RemoveAt(e.Item.ItemIndex)

                rptDialogUnselectUser.DataSource = dtAllUser
                rptDialogUnselectUser.DataBind()

        End Select
    End Sub

    Private Sub rptDialogGrantUser_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptDialogGrantUser.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblUserID As Label = e.Item.FindControl("lblUserID")
        Dim lblUserFullName As Label = e.Item.FindControl("lblUserFullName")
        Dim lblFirstName As Label = e.Item.FindControl("lblFirstName")
        Dim lblLastName As Label = e.Item.FindControl("lblLastName")
        Dim lblUserLogin As Label = e.Item.FindControl("lblUserLogin")

        lblUserID.Text = e.Item.DataItem("user_id")
        lblUserFullName.Text = e.Item.DataItem("first_name") & " " & e.Item.DataItem("last_name")
        lblFirstName.Text = e.Item.DataItem("first_name")
        lblLastName.Text = e.Item.DataItem("last_name")
        lblUserLogin.Text = e.Item.DataItem("user_login")
    End Sub

    Private Sub rptDialogGrantUser_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptDialogGrantUser.ItemCommand
        Dim lblUserFullName As Label = e.Item.FindControl("lblUserFullName")
        Dim lblUserID As Label = e.Item.FindControl("lblUserID")
        Dim lblFirstName As Label = e.Item.FindControl("lblFirstName")
        Dim lblLastName As Label = e.Item.FindControl("lblLastName")
        Dim lblUserLogin As Label = e.Item.FindControl("lblUserLogin")

        Select Case e.CommandName
            Case "DENY"
                'เพิ่มชื่อในฝั่งที่ไม่มีสิทธิ์
                Dim dt As DataTable = GetListUnselectUser()
                Dim dr As DataRow = dt.NewRow
                dr("user_id") = lblUserID.Text
                dr("first_name") = lblFirstName.Text
                dr("last_name") = lblLastName.Text
                dr("user_login") = lblUserLogin.Text

                dt.Rows.Add(dr)

                rptDialogUnselectUser.DataSource = dt
                rptDialogUnselectUser.DataBind()
                LogFileData.LogTrans(UserSession, "ยกเลิกสิทธิ์ของ User " & lblUserLogin.Text)

                'ลบชื่อออกจากฝั่งที่มีสิทธิ์
                Dim dtGrantUser As DataTable = GetListGrantUser()
                dtGrantUser.Rows.RemoveAt(e.Item.ItemIndex)

                rptDialogGrantUser.DataSource = dtGrantUser
                rptDialogGrantUser.DataBind()
        End Select
    End Sub

    Private Function GetListGrantUser() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("user_id", GetType(Long))
        dt.Columns.Add("first_name")
        dt.Columns.Add("last_name")
        dt.Columns.Add("user_login")

        For Each itm As RepeaterItem In rptDialogGrantUser.Items
            Dim lblUserID As Label = itm.FindControl("lblUserID")
            Dim lblFirstName As Label = itm.FindControl("lblFirstName")
            Dim lblLastName As Label = itm.FindControl("lblLastName")
            Dim lblUserLogin As Label = itm.FindControl("lblUserLogin")

            Dim dr As DataRow = dt.NewRow
            dr("user_id") = lblUserID.Text
            dr("first_name") = lblFirstName.Text
            dr("last_name") = lblLastName.Text
            dr("user_login") = lblUserLogin.Text

            dt.Rows.Add(dr)
        Next

        Return dt
    End Function

    Private Function GetListUnselectUser() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("user_id", GetType(Long))
        dt.Columns.Add("first_name")
        dt.Columns.Add("last_name")
        dt.Columns.Add("user_login")

        For Each itm As RepeaterItem In rptDialogUnselectUser.Items
            Dim lblUserID As Label = itm.FindControl("lblUserID")
            Dim lblFirstName As Label = itm.FindControl("lblFirstName")
            Dim lblLastName As Label = itm.FindControl("lblLastName")
            Dim lblUserLogin As Label = itm.FindControl("lblUserLogin")

            Dim dr As DataRow = dt.NewRow
            dr("user_id") = lblUserID.Text
            dr("first_name") = lblFirstName.Text
            dr("last_name") = lblLastName.Text
            dr("user_login") = lblUserLogin.Text

            dt.Rows.Add(dr)
        Next

        Return dt
    End Function

#End Region

#End Region
    Private Function ValidateData()
        If txtRoleName.Text.Trim = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุสิทธิ์การใช้งาน');", True)
            Return False
        End If

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.CheckDuplicateRole(lblRoleID.Text, txtRoleName.Text)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');", True)
            Return False
        End If
        ret = Nothing

        Return True
    End Function

    Private Sub ClearForm()
        lblRoleID.Text = "0"
        txtRoleName.Text = ""
        chkActive.Checked = True

        BindRoleMenuList()
        BindRoleUserList()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save")
        If ValidateData() = True Then
            Dim lnq As MsRoleLinqDB = MasterDataBL.SaveRole(UserSession.UserName, lblRoleID.Text, txtRoleName.Text, chkActive.Checked, GetMenuList(), GetListSelectedUser())
            If lnq.ROLE_ID > 0 Then
                lblRoleID.Text = lnq.ROLE_ID
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Data Complete');", True)
                pnlEdit.Visible = False
                pnlList.Visible = True
                BindList()

                LogFileData.LogTrans(UserSession, "Save Role " & txtRoleName.Text)
            Else
                LogFileData.LogError(UserSession, "Save Role " & txtRoleName.Text & " ไม่สำเร็จ Error :" & lnq.ErrorMessage)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Fail');", True)
            End If
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Cancel")
        ClearForm()
        pnlEdit.Visible = False
        pnlList.Visible = True
        BindList()
    End Sub


End Class