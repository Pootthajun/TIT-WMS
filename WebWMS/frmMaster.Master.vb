﻿Imports WMS_BL
Public Class frmMaster
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl & "?rnd=" & DateTime.Now.Millisecond)
                Exit Sub
            Else
                Dim UserSession As LoginSessionData = Session(CommonBL.UserLoginSessionName)
                If UserSession.ForceChangePsswd = "Y" Then
                    pnlLeftMenu.Visible = False
                Else
                    Dim dt As DataTable = MasterDataBL.GetList_Menu(UserSession.UserID)
                    CreateMainMenu(dt, UserSession)
                End If

                lblLoginHistoryID.Text = UserSession.LoginHistoryID

            End If
        End If
    End Sub

#Region "Generate Menu"
    Private Sub CreateSubMenu(dt As DataTable, ParentID As Long, UserSession As LoginSessionData)
        dt.DefaultView.RowFilter = "parent_menu_id=" & ParentID
        If dt.DefaultView.Count > 0 Then
            For Each dr As DataRowView In dt.DefaultView
                'ตรวจสอบว่ามี Sub Menu อีกหรือไม่
                Dim HasSub As String = ""
                dt.DefaultView.RowFilter = "parent_menu_id=" & dr("menu_id")
                If dt.DefaultView.Count > 0 Then
                    HasSub = " has-sub"
                End If
                dt.DefaultView.RowFilter = ""

                lblMenu.Text += "   <li class=""site-menu-item" & HasSub & """>" & Environment.NewLine
                If dr("menu_url") <> "#" Then
                    lblMenu.Text += "       <a class=""animsition-link"" href='" & dr("menu_url") & "' onClick=""return CreateLogClickMenu('" & UserSession.LoginHistoryID & "','คลิกเมนู " & dr("menu_name") & "')"" >" & Environment.NewLine
                Else
                    lblMenu.Text += "       <a href=""javascript:void(0)"">" & Environment.NewLine
                End If

                Dim MenuIcon As String = ""
                If Convert.IsDBNull(dt.DefaultView(0)("menu_icon")) = False Then MenuIcon = dt.DefaultView(0)("menu_icon")
                If MenuIcon <> "" Then
                    lblMenu.Text += "           <i class=""" & MenuIcon & """ aria-hidden="" true""></i>" & Environment.NewLine
                End If
                lblMenu.Text += "           <span class="" site-menu-title"">" & dr("menu_name") & "</span>" & Environment.NewLine
                If HasSub.Trim <> "" Then
                    lblMenu.Text += "           <span class="" site-menu-arrow""></span>" & Environment.NewLine
                End If
                lblMenu.Text += "       </a>" & Environment.NewLine

                If HasSub.Trim <> "" Then
                    lblMenu.Text += "       <ul class="" site-menu-sub"">" & Environment.NewLine
                    CreateSubMenu(dt, dr("menu_id"), UserSession)
                    lblMenu.Text += "       </ul>" & Environment.NewLine
                Else
                    CreateSubMenu(dt, dr("menu_id"), UserSession)
                End If

                lblMenu.Text += "</li>" & Environment.NewLine
            Next
        End If
        dt.DefaultView.RowFilter = ""
    End Sub

    Private Sub CreateMainMenu(dt As DataTable, UserSession As LoginSessionData)
        Try
            dt.DefaultView.RowFilter = "parent_menu_id=0"
            If dt.DefaultView.Count > 0 Then
                lblMenu.Text += "<ul class="" site-menu"">"
                For Each dr As DataRowView In dt.DefaultView
                    lblMenu.Text += "<li Class="" site-menu-category"">"
                    If dr("menu_url") <> "#" Then
                        lblMenu.Text += Environment.NewLine
                        lblMenu.Text += "   <a class="" animsition-link"" href='" & dr("menu_url") & "' onClick="" return CreateLogClickMenu('" & UserSession.LoginHistoryID & "','คลิกเมนู " & dr("menu_name") & "')"" >" & Environment.NewLine

                        Dim MenuIcon As String = ""
                        If Convert.IsDBNull(dr("menu_icon")) = False Then MenuIcon = dr("menu_icon")
                        If MenuIcon <> "" Then
                            lblMenu.Text += "           <i class=""" & MenuIcon & """ aria-hidden="" true""></i>" & Environment.NewLine
                        End If
                        lblMenu.Text += "       <span class="" site-menu-title"">" & dr("menu_name") & "</span>" & Environment.NewLine
                        lblMenu.Text += "   </a>"
                    Else
                        lblMenu.Text += dr("menu_name")
                    End If
                    lblMenu.Text += "</li>" & Environment.NewLine


                    'Create Sub Menu
                    CreateSubMenu(dt, dr("menu_id"), UserSession)
                Next
                lblMenu.Text += "</div>"
            End If
            dt.DefaultView.RowFilter = ""
        Catch ex As Exception
            LogFileData.LogException(UserSession, ex.Message, ex.StackTrace)
        End Try
    End Sub
#End Region

End Class