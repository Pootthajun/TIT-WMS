﻿Imports LinqDB.TABLE
Imports WMS_BL

Public Class MS_Address_Country
    Inherits System.Web.UI.Page


    Private ReadOnly Property UserSession As LoginSessionData
        Get
            Return Session(CommonBL.UserLoginSessionName)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect("frmLogin.aspx?rnd=" & DateTime.Now.Millisecond)
                Exit Sub
            Else
                BindList()
                ClearForm()
            End If
        End If
    End Sub

    Protected Sub chkActiveStatus_CheckedChanged(sender As ucSwitchButton, e As System.EventArgs)
        Dim chkActiveStatus As ucSwitchButton = sender
        Dim itm As RepeaterItem = chkActiveStatus.Parent
        Dim lblCountryID As Label = itm.FindControl("lblCountryID")
        Dim lblCountryName As Label = itm.FindControl("lblCountryName")

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.UpdateCountrystatus(UserSession.UserName, lblCountryID.Text, chkActiveStatus.Checked)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Update status fail');", True)
            LogFileData.LogError(UserSession, ret.ErrorMessage)
        Else
            LogFileData.LogTrans(UserSession, "Update Country Status " & lblCountryName.Text & " Status : " & chkActiveStatus.Checked.ToString)
        End If
    End Sub

    Private Sub BindList()
        Dim dt As DataTable = MasterDataBL.GetList_Country(txtSearch.Text.Trim, ddlstatus.SelectedValue)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblCountryName As Label = e.Item.FindControl("lblCountryName")
        Dim lblCountryID As Label = e.Item.FindControl("lblCountryID")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        lblCountryName.Text = e.Item.DataItem("Country_Name")
        lblCountryID.Text = e.Item.DataItem("Country_ID")
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblCountryName As Label = e.Item.FindControl("lblCountryName")
        Dim lblCountryID As Label = e.Item.FindControl("lblCountryID")

        Select Case e.CommandName
            Case "EDIT"
                ClearForm()
                FillInData(lblCountryID.Text)

                pnlList.Visible = False
                pnlEdit.Visible = True

                LogFileData.LogTrans(UserSession, "คลิกปุ่ม Edit Country " & lblCountryName.Text)
            Case "DELETE"
                Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.DeleteCountry(lblCountryID.Text)
                If ret.IsSuccess = True Then
                    LogFileData.LogTrans(UserSession, "Delete Country " & lblCountryName.Text)

                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Delete Data Fail');", True)
                    LogFileData.LogError(UserSession, ret.ErrorMessage)
                End If
        End Select
    End Sub

    Private Sub FillInData(CountryID As Long)
        Dim ret As MsAddressCountryLinqDB = MasterDataBL.GetCountryData(CountryID)
        If ret.COUNTRY_ID > 0 Then
            lblCountryID.Text = ret.COUNTRY_ID
            txtCountryName.Text = ret.COUNTRY_NAME
            chkActive.Checked = (ret.ACTIVE_STATUS = "Y")
        End If
        ret = Nothing
    End Sub

    Private Sub ClearForm()
        lblCountryID.Text = "0"
        txtCountryName.Text = ""
        chkActive.Checked = True
    End Sub

    Private Sub btnAddCountry_Click(sender As Object, e As EventArgs) Handles btnAddCountry.Click
        ClearForm()
        pnlEdit.Visible = True
        pnlList.Visible = False
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Add Country")
    End Sub

    Private Function ValidateData() As Boolean
        If lblCountryID.Text = "0" Then
            'กรณีเป็นการเพิ่มใหม่ ให้ตรวจสอบข้อมูลการเข้าระบบด้วย
            If txtCountryName.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุชื่อประเทศ');", True)
                Return False
            End If
        End If

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.CheckDuplicateCountry(lblCountryID.Text, txtCountryName.Text)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');", True)
            Return False
        End If

        Return True
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save Country")
        If ValidateData() = True Then

            Dim lnq As MsAddressCountryLinqDB = MasterDataBL.SaveCountry(UserSession.UserName, lblCountryID.Text, txtCountryName.Text, chkActive.Checked)
            If lnq.COUNTRY_ID > 0 Then
                lblCountryID.Text = lnq.COUNTRY_ID
                LogFileData.LogTrans(UserSession, "Save Country " & txtCountryName.Text & " สำเร็จ")
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Data Complete');", True)
                pnlEdit.Visible = False
                pnlList.Visible = True
                BindList()
            Else
                LogFileData.LogError(UserSession, "Save Country " & txtCountryName.Text & " ไม่สำเร็จ Error :" & lnq.ErrorMessage)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Fail');", True)
            End If
            lnq = Nothing
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Cancel")
        ClearForm()
        pnlEdit.Visible = False
        pnlList.Visible = True
        BindList()
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click, txtSearch.TextChanged
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม ค้นหา")
        BindList()
    End Sub

    Private Sub ddlstatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlstatus.SelectedIndexChanged
        BindList()
    End Sub
End Class