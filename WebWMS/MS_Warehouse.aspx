﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="MS_Warehouse.aspx.vb" Inherits="WebWMS.MS_Warehouse" %>

<%@ Register Src="~/UserControl/ucSwitchButton.ascx" TagPrefix="uc1" TagName="ucSwitchButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="page-header">
        <h4 class="page-title">ข้อมูลคลังสินค้า</h4>
    </div>
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <asp:Panel ID="pnlList" runat="server" CssClass="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-2">
                                <asp:LinkButton ID="btnAddWarehouse" runat="server" CssClass="btn btn-warning">
                                    <i class="icon md-plus-circle-o"></i>
                                    <span>เพื่มข้อมูลคลังสินค้า</span>
                                </asp:LinkButton>
                            </div>
                        </div>

                        <div class="clearfix visible-md-block"></div>
                        <br />
                        <table class="tablesaw table-striped table-bordered table-hover">
                            <thead>
                                <tr class="bg-blue-grey-100">
                                    <th class="text-center">รหัส</th>
                                    <th class="text-center">ประเภท</th>
                                    <th class="text-center">ชื่อ</th>
                                    <th class="text-center">รายละเอียด</th>
                                    <th class="text-center">ใช้งาน</th>
                                    <th class="text-center">แก้ไข</th>
                                    <th class="text-center">ลบ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptList" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td class="text-center">
                                                <asp:Label ID="lblWarehouseCode" runat="server"></asp:Label>
                                                <asp:Label ID="lblWarehouseID" runat="server" Visible="false"></asp:Label>
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="lblWarehouseTypeName" runat="server"></asp:Label>
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="lblWarehouseName" runat="server"></asp:Label>
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="lblWarehouseDetial" runat="server"></asp:Label>
                                                <asp:Label ID="lblWarehouseSize" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblWarehouseBuilding" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblWarehouseFloor" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblWarehouseRoom" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblWarehouseZone" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblWarehouseLock" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblWarehouseRow" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblWarehouseShelf" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblWarehouseDeep" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblWarehouseType" runat="server" Visible="false"></asp:Label>
                                            </td>
                                            <td class="text-center">
                                                <uc1:ucSwitchButton runat="server" ID="chkActiveStatus" />
                                            </td>
                                            <td class="text-center">
                                                <asp:LinkButton ID="btnEdit" runat="server" CommandName="EDIT">
                                                    <i class="icon md-edit" style="font-size: 20px;"></i>
                                                </asp:LinkButton>
                                            </td>
                                            <td class="text-center">
                                                <asp:LinkButton ID="btnDelete" runat="server" CommandName="DELETE" OnClientClick="return confirm('Do you want delete data?');">
                                                    <i class="icon md-delete" style="font-size: 20px;"></i>
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlEdit" runat="server" CssClass="panel" Visible="false">
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>WareHouse ID :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-10">
                                <asp:TextBox ID="txtWarehouseCode" runat="server" CssClass="form-control" MaxLength="100" placeholder="รหัสคลังสินค้า"></asp:TextBox>
                                <asp:Label ID="lblWarehouseID" runat="server" Visible="false" Text="0"></asp:Label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Warehouse Name :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-10">
                                <asp:TextBox ID="txtWarehouseName" runat="server" CssClass="form-control" MaxLength="100" placeholder="ชื่อคลังสินค้า"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Warehouse Type :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-10">
                                <asp:DropDownList ID="ddlWarehouseType" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="ประเภทคลังสินค้า"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Size :</label>
                            </div>

                            <div class="col-sm-4">
                                <asp:TextBox ID="txtWarehouseSize" runat="server" CssClass="form-control" MaxLength="100" placeholder="ขนาด"></asp:TextBox>
                            </div>
                            <div class="form-group col-sm-2">
                                <label>Build :</label>
                            </div>

                            <div class="col-sm-4">
                                <asp:TextBox ID="txtWarehouseBuild" runat="server" CssClass="form-control" MaxLength="100" placeholder="อาคาร"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Floor :</label>
                            </div>

                            <div class="col-sm-4">
                                <asp:TextBox ID="txtWarehouseFloor" runat="server" CssClass="form-control" MaxLength="100" placeholder="ชั้น"></asp:TextBox>
                            </div>
                            <div class="form-group col-sm-2">
                                <label>Room :</label>
                            </div>

                            <div class="col-sm-4">
                                <asp:TextBox ID="txtWarehouseRoom" runat="server" CssClass="form-control" MaxLength="100" placeholder="ห้อง"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Zone :</label>
                            </div>

                            <div class="col-sm-4">
                                <asp:TextBox ID="txtWarehouseZone" runat="server" CssClass="form-control" MaxLength="100" placeholder="โซน"></asp:TextBox>
                            </div>
                            <div class="form-group col-sm-2">
                                <label>Lock :</label>
                            </div>

                            <div class="col-sm-4">
                                <asp:TextBox ID="txtWarehouseLock" runat="server" CssClass="form-control" MaxLength="100" placeholder="ล็อก"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Row :</label>
                            </div>

                            <div class="col-sm-4">
                                <asp:TextBox ID="txtWarehouseRow" runat="server" CssClass="form-control" MaxLength="100" placeholder="แถว"></asp:TextBox>
                            </div>
                            <div class="form-group col-sm-2">
                                <label>Shelf :</label>
                            </div>

                            <div class="col-sm-4">
                                <asp:TextBox ID="txtWarehouseShelf" runat="server" CssClass="form-control" MaxLength="100" placeholder="เชลส์"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Deep :</label>
                            </div>

                            <div class="col-sm-4">
                                <asp:TextBox ID="txtWarehouseDeep" runat="server" CssClass="form-control" MaxLength="100" placeholder="ลึก"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <br />
                            <div class="form-group col-sm-11">
                                <label>Address :</label>
                            </div>
                            <div style="text-align: right">
                                <asp:LinkButton ID="btnAddAddress" runat="server" CssClass="btn btn-primary">
                                    <span>+</span>
                                </asp:LinkButton>
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <table class="tablesaw table-striped table-bordered table-hover">
                                <thead>
                                    <tr class="bg-blue-grey-100">
                                        <th class="text-center" style="width: 70%">ที่อยู่</th>
                                        <th class="text-center" style="width: 10%">ใช้งาน</th>
                                        <th class="text-center" style="width: 10%">แก้ไข</th>
                                        <th class="text-center" style="width: 10%">ลบ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptListAddress" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <asp:Label ID="lblRowIndexAd" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblSequence_ID" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressWID" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressHouseNo" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressBuildingName" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressMoo" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressSoi" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressRoad" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressTumbonID" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressDistrictID" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressProvinceID" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressCountryID" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressPascode" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressTel" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressMobile" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressStatus" runat="server" Visible="false"></asp:Label>
                                                <td>
                                                    <asp:Label ID="lblAddressDetial" runat="server"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <uc1:ucSwitchButton runat="server" ID="chkActiveStatus" />
                                                </td>
                                                <td class="text-center">
                                                    <asp:LinkButton ID="btnEdit" runat="server" CommandName="EDIT">
                                                    <i class="icon md-edit" style="font-size: 20px;"></i>
                                                    </asp:LinkButton>
                                                </td>
                                                <td class="text-center">
                                                    <asp:LinkButton ID="btnDelete" runat="server" CommandName="DELETE" OnClientClick="return confirm('Do you want delete data?');">
                                                    <i class="icon md-delete" style="font-size: 20px;"></i>
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>ใช้งาน</label>
                            </div>

                            <div class="col-sm-9">
                                <uc1:ucSwitchButton runat="server" ID="chkActive" />
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <hr />
                        <div class="form-group form-material">
                            <div class="col-sm-12 ">
                                <div class="col-sm-10"></div>
                                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary">
                                    <span>Save</span>
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-warning">
                                    <span>Cancel</span>
                                </asp:LinkButton>
                            </div>
                        </div>

                    </div>
                </asp:Panel>

                 <asp:Panel ID="pnlEditAddress" runat="server" CssClass="panel" Visible="false">
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>บ้านเลขที่ :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-4">
                                <asp:TextBox ID="txthouseNo" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="บ้านเลขที่"></asp:TextBox>
                            </div>
                            <div class="form-group col-sm-2">
                                <label>อาคารที่ตั้ง :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtBuildingName" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="อาคารที่ตั้ง"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>หมู่ :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtmoo" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="หมู่"></asp:TextBox>
                            </div>
                            <div class="form-group col-sm-2">
                                <label>ซอย :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtsoi" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="ซอย"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>ถนน :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtroad" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="ถนน"></asp:TextBox>
                            </div>
                            </div> 

                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>จังหวัด :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-4">
                                <asp:DropDownList ID="ddlProvince" runat="server" data-plugin="selectpicker" CssClass="selectpicker"  Autopostback="true"  data-live-search="true" title="จังหวัด"></asp:DropDownList>
                                
                            </div>
                            <div class="form-group col-sm-2">
                                <label>อำเภอ/เขต :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-4">
                                <asp:DropDownList ID="ddlDistrict" runat="server" data-plugin="selectpicker" CssClass="selectpicker"  Autopostback="true"  data-live-search="true" title="อำเภอ/เขต"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>ตำบล/แขวง :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-4">
                                <asp:DropDownList ID="ddlTumbon" runat="server" data-plugin="selectpicker" CssClass="selectpicker" Autopostback="true" data-live-search="true" title="ตำบล/แขวง"></asp:DropDownList>
                            </div>
                            <div class="form-group col-sm-2">
                                <label>ประเทศ :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-4">
                                <asp:DropDownList ID="ddlCountry" runat="server" data-plugin="selectpicker" CssClass="selectpicker" Autopostback="true"  data-live-search="true" title="ประเทศ"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>รหัสไปรษณีย์ :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-10">
                                <asp:TextBox ID="txtPoscode" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="รหัสไปรษณีย์"></asp:TextBox>
                            </div>
                        </div>
                         <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>เบอร์โทรศัพท์ :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-4">
                               <asp:TextBox ID="txtTel" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="เบอร์โทรศัพท์"></asp:TextBox>
                            </div>
                            <div class="form-group col-sm-2">
                                <label>เบอร์โทรศัพท์มือถือ :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-4">
                               <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="เบอร์โทรศัพท์มือถือ"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <br />
                            <div class="form-group col-sm-2">
                                <label>ใช้งาน :</label>
                            </div>

                            <div class="col-sm-9">
                                <uc1:ucSwitchButton runat="server" ID="chkActiveAddress" />
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr />
                        <div class="form-group form-material">
                            <div class="col-sm-12 col-sm-offset-10">
                                <asp:LinkButton ID="btnSaveAddress" runat="server" CssClass="btn btn-primary">
                                    <span>Save</span>
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnCancelAddress" runat="server" CssClass="btn btn-warning">
                                    <span>Cancel</span>
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </asp:Panel>

            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpFooter" runat="server">
</asp:Content>
