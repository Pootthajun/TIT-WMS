﻿Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports WMS_BL

Public Class TB_Recevice_Test
    Inherits System.Web.UI.Page

    Private ReadOnly Property UserSession As LoginSessionData
        Get
            Return Session(CommonBL.UserLoginSessionName)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect("frmLogin.aspx?rnd=" & DateTime.Now.Millisecond)
                Exit Sub
            Else
                BindList(0)
                ClearForm()
                Bindddl()
                pnlSearch.Visible = False
            End If
        End If
    End Sub

    Private Sub BindList(DocumentID As String)

        Dim dt As DataTable = MasterDataBL.GetList_DocumentReceive(txtSearchDocID.Text, ddlSearchReceiveType.SelectedValue, ddlSearchPO.SelectedValue, txtSearchRDate.Text, txtSearchRTime.Text, ddlSearchStaff.SelectedValue, txtSearchApprover.Text)
        rptList.DataSource = dt
        rptList.DataBind()
        pnlSearch.Visible = False

        Dim dtskufree As DataTable = MasterDataBL.GetList_sku_free(DocumentID)
        rptskufree.DataSource = dtskufree
        rptskufree.DataBind()
    End Sub

    Private Sub Bindddl()
        MasterDataBL.GetListDDL_ReceiveType(ddlReceiveType)
        MasterDataBL.GetListDDL_ReceiveType(ddlSearchReceiveType)
        MasterDataBL.GetListDDL_Staff(ddlStaff)
        MasterDataBL.GetListDDL_Staff(ddlSearchStaff)
    End Sub
    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblSequenceID As Label = e.Item.FindControl("lblSequenceID")
        Dim lblDocumentID As Label = e.Item.FindControl("lblDocumentID")
        Dim lblDocumentType As Label = e.Item.FindControl("lblDocumentType") 'ReceiveType
        Dim lblPOReference As Label = e.Item.FindControl("lblPOReference")
        Dim lblReceiveDate As Label = e.Item.FindControl("lblReceiveDate")
        Dim lblReceiveTime As Label = e.Item.FindControl("lblReceiveTime")
        Dim lblOfficer As Label = e.Item.FindControl("lblOfficer")
        Dim lblApprover As Label = e.Item.FindControl("lblApprover")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        lblSequenceID.Text = e.Item.DataItem("Sequence_ID")
        lblDocumentID.Text = e.Item.DataItem("Document_ID")
        lblDocumentType.Text = e.Item.DataItem("Recieve_Type")
        lblPOReference.Text = e.Item.DataItem("PO_Reference_Document")
        lblReceiveDate.Text = e.Item.DataItem("Document_Date")
        lblReceiveTime.Text = e.Item.DataItem("Document_Time")
        lblOfficer.Text = e.Item.DataItem("Checker_ID")
        lblApprover.Text = e.Item.DataItem("Approver")
        'chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblSequenceID As Label = e.Item.FindControl("lblSequenceID")
        Dim lblDocumentID As Label = e.Item.FindControl("lblDocumentID")

        Select Case e.CommandName
            Case "EDIT"
                ClearForm()
                FillInData(lblSequenceID.Text)

                pnlList.Visible = False
                pnlEdit.Visible = True

                LogFileData.LogTrans(UserSession, "คลิกปุ่ม Edit Document Recieve " & lblDocumentID.Text)

                BindList(lblDocumentID.Text)
            Case "DELETE"
                Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.DeleteDocumentReceive(lblSequenceID.Text, lblDocumentID.Text)
                If ret.IsSuccess = True Then
                    LogFileData.LogTrans(UserSession, "Delete Document Recieve " & lblDocumentID.Text)

                    BindList(0)
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Delete Data Fail');", True)
                    LogFileData.LogError(UserSession, ret.ErrorMessage)
                End If
        End Select
    End Sub

    Private Sub FillInData(DocumentReceiveID As Long)
        Dim ret As TbRcstep1ReceiveLinqDB = MasterDataBL.GetDocumentReceiveData(DocumentReceiveID)

        If ret.SEQUENCE_ID > 0 Then
            lblSequenceID.Text = ret.SEQUENCE_ID
            txtDocumentID.Text = ret.DOCUMENT_ID

            Dim Rdate As String = ""
            Rdate = Convert.ToDateTime(ret.DOCUMENT_DATE).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
            txtReceieveDate.Text = Rdate
            txtReceieveTime.Text = ret.DOCUMENT_TIME

            txtReferNo.Text = ret.PO_REFERENCE_DOCUMENT
            txtReferNo_TextChanged(Nothing, Nothing)

            txtDoNo.Text = ret.DO_REFERENCE_DOCUMENT1
            txtDoNo_TextChanged(Nothing, Nothing)

            txtReferenceDoc.Text = ret.REFERENCE_DOCUMENT1
            ddlStaff.SelectedValue = ret.CHECKER_ID
            txtApprover.Text = ret.CREATED_BY
            chkActive.Checked = (ret.ACTIVE_STATUS = "Y")

            Dim sql As String = "SELECT Distinct R.Recieve_Type  From TB_PO_Test PO LEFT JOIN MS_Receive_Type R ON PO.Receive_Type_ID = R.Receive_Type_ID Where Reference_No = '" & txtReferNo.Text & "'"
            Dim dtPO As DataTable = SqlDB.ExecuteTable(sql)

            For i As Integer = 0 To dtPO.Rows.Count - 1
                Dim Recieve_Type As String = dtPO.Rows(i)("Recieve_Type").ToString()
                lblReferenceType.Text = Recieve_Type
            Next
        End If
        ret = Nothing
    End Sub

    Private Sub ClearForm()
        Bindddl()
        ddlReceiveType.SelectedValue = 0
        lblSequenceID.Text = "0"
        txtDocumentID.Text = ""
        Dim Rdate As String = ""
        Rdate = Convert.ToDateTime(DateTime.Now.ToShortDateString()).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
        txtReceieveDate.Text = Rdate
        txtReceieveTime.Text = Format(Now, "hh:mm")

        txtReferNo.Text = ""
        txtDateReference.Text = ""
        txtReferNo_TextChanged(Nothing, Nothing)
        txtDoNo.Text = ""
        txtDateDONo.Text = ""

        txtDateReference.Enabled = False
        txtApprover.Enabled = False
        txtApprover.Text = UserSession.UserName
        chkActive.Checked = True

    End Sub

    Private Sub btnAddDocumentRecieve_Click(sender As Object, e As EventArgs) Handles btnAddDocumentRecieve.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Add")
        If ddlReceiveType.SelectedValue = "0" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุประเภทการรับ');", True)
        Else
            pnlEdit.Visible = True
            pnlList.Visible = False

            Session("ReceiveTypeID") = ddlReceiveType.SelectedValue
            Session("ReceiveType") = ddlReceiveType.SelectedItem.ToString()
            lblReferenceType.Text = Session("ReceiveType").ToString()
            ClearForm()
        End If

    End Sub

    Private Function ValidateData() As Boolean
        If lblSequenceID.Text <> "0" Then
            'กรณีเป็นการเพิ่มใหม่ ให้ตรวจสอบข้อมูลการเข้าระบบด้วย
            If txtDocumentID.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุรหัสเอกสารรับสินค้า');", True)
                Return False
            End If
        End If

        If txtReferNo.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุรหัสใบสั่งซื้อสินค้า');", True)
            Return False
        End If

        If txtDateReference.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุวันที่ออกใบสินค้า');", True)
            Return False
        End If

        If ddlStaff.SelectedValue = "0" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุผู้ตรวจ');", True)
            Return False
        End If

        If txtApprover.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุผู้อัตนุมัติ');", True)
            Return False
        End If

        If txtDoNo.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุรหัสใบส่งของ');", True)
            Return False
        End If



        Return True
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save Document Receive")
        If ValidateData() = True Then

            If txtDocumentID.Text = "" Then

                Dim sql As String = "Select Count(Sequence_ID) As MCount From TB_RCStep1_Receive"
                Dim dt As DataTable = SqlDB.ExecuteTable(sql)

                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim MCount As String = dt.Rows(i)("MCount").ToString()
                    MCount = CInt(MCount) + 1
                    txtDocumentID.Text = "DR" + Session("ReceiveType").ToString() + Format(Now, "MMddyyyy") + "#" + MCount
                Next
            End If

            Dim DTSKU As New DataTable
            DTSKU = GetSKUList()

            Dim lnq As TbRcstep1ReceiveLinqDB = MasterDataBL.SaveDocumentReceive(UserSession.UserName, lblSequenceID.Text, txtDocumentID.Text, txtReceieveDate.Text, txtReceieveTime.Text, Session("ReceiveTypeID").ToString(), txtReferNo.Text, txtDateReference.Text, txtDoNo.Text, txtDateDONo.Text, ddlStaff.SelectedValue, txtReferenceDoc.Text, txtnote.Text, Nothing, Nothing, Nothing, chkActive.Checked)

            Dim lnqSku As TbPoTestLinqDB = MasterDataBL.UpdateReceiveSku(UserSession.UserName, DTSKU)

            Dim lnqSKUDetial As TbRcstep1ReceiveDetialLinqDB = MasterDataBL.SaveDocumentReceive_detial(UserSession.UserName, txtDocumentID.Text, txtReferNo.Text, txtDoNo.Text, DTSKU)

            Dim DTSKUFree As New DataTable
            DTSKUFree = GetSKUFreeList()

            Dim lnqSKUFree As TbRcstep1ReceiveDetialFreeLinqDB = MasterDataBL.SaveDocumentReceive_detial_Free(UserSession.UserName, txtDocumentID.Text, txtReferNo.Text, txtDoNo.Text, DTSKUFree)

            If lnq.SEQUENCE_ID > 0 Then
                lblSequenceID.Text = lnq.SEQUENCE_ID
                LogFileData.LogTrans(UserSession, "Save DocumentReceive " & txtDocumentID.Text & " สำเร็จ")
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Data Complete');", True)
                pnlEdit.Visible = False
                pnlList.Visible = True
                BindList(0)
            Else
                LogFileData.LogError(UserSession, "Save DocumentReceive " & txtDocumentID.Text & " ไม่สำเร็จ Error :" & lnq.ErrorMessage)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Fail');", True)
            End If
            lnq = Nothing
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Cancel")
        ClearForm()
        pnlEdit.Visible = False
        pnlList.Visible = True
        BindList(0)
    End Sub

#Region "รายการ SKU"

    Private Function GetSKUList() As DataTable
        Dim dt As New DataTable

        dt.Columns.Add("Sequence_ID")
        dt.Columns.Add("SKU_ID")
        dt.Columns.Add("AmountReceive")
        dt.Columns.Add("AmountReceiveAccess")
        dt.Columns.Add("AmountSent")
        dt.Columns.Add("AmountBalance")
        dt.Columns.Add("Receive_Type_ID")
        dt.Columns.Add("Reference_No")
        dt.Columns.Add("Product_Status_ID")
        dt.Columns.Add("Receive")


        For Each itm As RepeaterItem In rptsku.Items

            Dim lblSequenceID As Label = itm.FindControl("lblSequenceID")
            Dim lblSKUID As Label = itm.FindControl("lblSKUID")
            Dim lblProductName As Label = itm.FindControl("lblProductName")
            Dim lblLotBatch As Label = itm.FindControl("lblLotBatch")
            Dim lblAmountReceive As Label = itm.FindControl("lblAmountReceive")
            Dim lblAmountReceiveAccess As Label = itm.FindControl("lblAmountReceiveAccess")
            Dim txtAmountSent As TextBox = itm.FindControl("txtAmountSent")
            Dim lblAmountBalance As Label = itm.FindControl("lblAmountBalance")
            Dim chkReceive As ucSwitchButton = itm.FindControl("chkReceive")

            Dim dr As DataRow = dt.NewRow
            dr("Sequence_ID") = lblSequenceID.Text
            dr("SKU_ID") = lblSKUID.Text
            dr("AmountReceive") = lblAmountReceive.Text
            If txtAmountSent.Text = "" Then txtAmountSent.Text = 0
            dr("AmountReceiveAccess") = CInt(lblAmountReceiveAccess.Text) + CInt(txtAmountSent.Text)
            dr("AmountSent") = txtAmountSent.Text

            Dim AReceive As Integer = CInt(lblAmountReceive.Text)
            Dim AReceiveAccess As Integer = CInt(lblAmountReceiveAccess.Text)
            Dim ASent As Integer = CInt(txtAmountSent.Text)

            dr("AmountBalance") = AReceive - (AReceiveAccess + ASent)
            dr("Receive_Type_ID") = Session("ReceiveTypeID").ToString()
            dr("Reference_No") = txtReferNo.Text
            If chkReceive.Checked Then
                dr("Receive") = "Y"
            Else
                dr("Receive") = "N"
            End If


            dt.Rows.Add(dr)
        Next
        Return dt
    End Function

    Private Sub rptsku_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptsku.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblSKUID As Label = e.Item.FindControl("lblSKUID")
        Dim lblSequenceID As Label = e.Item.FindControl("lblSequenceID")
        Dim lblProductName As Label = e.Item.FindControl("lblProductName")
        Dim lblLotBatch As Label = e.Item.FindControl("lblLotBatch")
        Dim lblAmountReceive As Label = e.Item.FindControl("lblAmountReceive")
        Dim lblAmountReceiveAccess As Label = e.Item.FindControl("lblAmountReceiveAccess")
        Dim lblAmountBalance As Label = e.Item.FindControl("lblAmountBalance")
        Dim txtAmountSent As TextBox = e.Item.FindControl("txtAmountSent")
        Dim lblUnit As Label = e.Item.FindControl("lblUnit")
        Dim lblProductStatus As Label = e.Item.FindControl("lblProductStatus")
        Dim lblExpireDate As Label = e.Item.FindControl("lblExpireDate")
        Dim chkReceive As ucSwitchButton = e.Item.FindControl("chkReceive")

        lblSequenceID.Text = e.Item.DataItem("Sequence_ID")
        lblSKUID.Text = e.Item.DataItem("SKU_ID")
        lblProductName.Text = e.Item.DataItem("ProductName")
        lblLotBatch.Text = e.Item.DataItem("Lot_Batch")
        lblAmountReceive.Text = e.Item.DataItem("AmountReceive")
        lblAmountReceiveAccess.Text = e.Item.DataItem("AmountReceiveAccess")
        lblAmountBalance.Text = e.Item.DataItem("AmountBalance")
        txtAmountSent.Text = 0
        lblUnit.Text = e.Item.DataItem("Unit")
        lblProductStatus.Text = e.Item.DataItem("Product_Status")
        lblExpireDate.Text = e.Item.DataItem("Expire_Date")
        chkReceive.Checked = (e.Item.DataItem("Receive") = "Y")
    End Sub

    Private Sub rptsku_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptsku.ItemCommand
        Dim lblAmountReceive As Label = e.Item.FindControl("lblAmountReceive")
        Dim txtAmountSent As TextBox = e.Item.FindControl("txtAmountSent")

    End Sub

    Private Sub txtReferNo_TextChanged(sender As Object, e As EventArgs) Handles txtReferNo.TextChanged
        If txtReferNo.Text <> "" Then
            If txtDocumentID.Text = "" Then 'Add
                Dim sql As String = "SELECT Distinct Receive_Type_ID,Reference_date From TB_PO_Test Where Reference_No = '" & txtReferNo.Text & "'"
                Dim dtPO As DataTable = SqlDB.ExecuteTable(sql)

                If dtPO.Rows.Count <> 0 Then
                    For i As Integer = 0 To dtPO.Rows.Count - 1
                        Dim Reference_date As String = dtPO.Rows(i)("Reference_date").ToString()
                        Dim Receive_Type_ID As String = dtPO.Rows(i)("Receive_Type_ID").ToString()
                        txtDateReference.Text = Convert.ToDateTime(Reference_date).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))

                        If Session("ReceiveTypeID").ToString() = Receive_Type_ID Then
                            Dim dt As DataTable = MasterDataBL.GetList_ReceiveSKU(txtReferNo.Text, txtDocumentID.Text, "")
                            rptsku.DataSource = dt
                            rptsku.DataBind()

                            'แสดงจำนวนของรายการสินค้าแต่ละเคส

                            Dim Rec As Integer = 0
                            Dim NonRec As Integer = 0
                            For i1 As Integer = 0 To dt.Rows.Count - 1
                                Dim Receive As String = dt.Rows(i1)("Receive").ToString()
                                If Receive = "Y" Then
                                    Rec += 1
                                Else
                                    NonRec += 1
                                End If
                            Next

                            Dim total As String = dt.Rows.Count
                            lblTotal.Text = "<br/>จำนวนสินค้าทั้งหมด : " + total + " รายการ " + "(รับแล้ว : " + CStr(Rec) + " รายการ " + "ยังไม่ได้รับ :" + CStr(NonRec) + " รายการ)"

                            lblSumAmountTotal.Text = Convert.ToDecimal(dt.Compute("SUM(AmountReceive)", "")).ToString()
                            lblSumAmountReceive.Text = Convert.ToDecimal(dt.Compute("SUM(AmountReceiveAccess)", "")).ToString()
                            lblSumAmountBalance.Text = Convert.ToDecimal(dt.Compute("SUM(AmountBalance)", "")).ToString()
                        Else
                            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('รหัสใบ PO ไม่สอดคล้องกับประเภทการรับ กรุณาระบุใหม่');", True)
                            ClearRptSKU()
                        End If
                    Next
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('ไม่มีรหัสใบ PO กรุณาระบุใหม่');", True)
                    ClearRptSKU()
                End If
            Else
                'edit
                Dim dt As DataTable = MasterDataBL.GetList_ReceiveSKU(txtReferNo.Text, txtDocumentID.Text, "")
                rptsku.DataSource = dt
                rptsku.DataBind()

                Dim sql As String = "SELECT Distinct Receive_Type_ID,Reference_date From TB_PO_Test Where Reference_No = '" & txtReferNo.Text & "'"
                Dim dtPO As DataTable = SqlDB.ExecuteTable(sql)

                For i As Integer = 0 To dtPO.Rows.Count - 1
                    Dim Reference_date As String = dtPO.Rows(i)("Reference_date").ToString()
                    txtDateReference.Text = Convert.ToDateTime(Reference_date).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
                Next

                Dim Rec As Integer = 0
                Dim NonRec As Integer = 0
                For i1 As Integer = 0 To dt.Rows.Count - 1
                    Dim Receive As String = dt.Rows(i1)("Receive").ToString()
                    If Receive = "Y" Then
                        Rec += 1
                    Else
                        NonRec += 1
                    End If
                Next

                Dim total As String = dt.Rows.Count
                lblTotal.Text = "<br/>จำนวนสินค้าทั้งหมด : " + total + " รายการ " + "(รับแล้ว : " + CStr(Rec) + " รายการ " + "ยังไม่ได้รับ :" + CStr(NonRec) + " รายการ)"

                lblSumAmountTotal.Text = Convert.ToDecimal(dt.Compute("SUM(AmountReceive)", "")).ToString()
                lblSumAmountReceive.Text = Convert.ToDecimal(dt.Compute("SUM(AmountReceiveAccess)", "")).ToString()
                lblSumAmountBalance.Text = Convert.ToDecimal(dt.Compute("SUM(AmountBalance)", "")).ToString()
            End If
        Else
            ClearRptSKU()
        End If

    End Sub

    Private Sub ClearRptSKU()
        rptsku.DataSource = ""
        rptsku.DataBind()
        txtDateReference.Text = ""
        lblTotal.Text = ""

        lblSumAmountTotal.Text = ""
        lblSumAmountReceive.Text = ""
        lblSumAmountBalance.Text = ""
    End Sub

    Private Sub btnOpenSearch_Click(sender As Object, e As EventArgs) Handles btnOpenSearch.Click
        pnlSearch.Visible = True
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindList(0)
    End Sub

    Private Sub txtDoNo_TextChanged(sender As Object, e As EventArgs) Handles txtDoNo.TextChanged
        If txtDoNo.Text <> "" Then
            txtDateDONo.Text = Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
        Else
            txtDateDONo.Text = ""
        End If
    End Sub
#End Region

#Region "Sku ของแถม"

    Private Function GetSKUFreeList() As DataTable
        Dim dt As New DataTable

        dt.Columns.Add("Sequence_ID")
        dt.Columns.Add("SKU_ID")
        dt.Columns.Add("ProductName")
        dt.Columns.Add("Amount")

        For Each itm As RepeaterItem In rptskufree.Items

            Dim lblSequenceID As Label = itm.FindControl("lblSequenceID")
            Dim ddlSkuIDFree As DropDownList = itm.FindControl("ddlSkuIDFree")
            Dim lblProductIFree As Label = itm.FindControl("lblProductIFree")
            Dim txtAmountFree As TextBox = itm.FindControl("txtAmountFree")


            Dim dr As DataRow = dt.NewRow
            dr("Sequence_ID") = 0
            dr("SKU_ID") = ddlSkuIDFree.SelectedValue
            dr("ProductName") = lblProductIFree.Text
            dr("Amount") = txtAmountFree.Text

            dt.Rows.Add(dr)
        Next
        Return dt
    End Function

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)

        Dim dt As DataTable = GetSKUFreeList()
        Dim dr As DataRow
        dr = dt.NewRow
        dr("Sequence_ID") = dt.Rows.Count + 1
        dr("SKU_ID") = 0
        dr("ProductName") = ""
        dr("Amount") = 0
        dt.Rows.Add(dr)

        rptskufree.DataSource = dt
        rptskufree.DataBind()

    End Sub


    Private Sub rptskufree_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptskufree.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblSequenceID As Label = e.Item.FindControl("lblSequenceID")
        Dim ddlSkuIDFree As DropDownList = e.Item.FindControl("ddlSkuIDFree")
        Dim lblProductIFree As Label = e.Item.FindControl("lblProductIFree")
        Dim txtAmountFree As TextBox = e.Item.FindControl("txtAmountFree")

        lblSequenceID.Text = e.Item.DataItem("Sequence_ID")
        MasterDataBL.GetListDDL_SKU(ddlSkuIDFree, "")
        ddlSkuIDFree.SelectedValue = e.Item.DataItem("SKU_ID")
        ddlSkuIDFree_SelectedIndexChanged(ddlSkuIDFree, Nothing)
        txtAmountFree.Text = e.Item.DataItem("Amount")

    End Sub

    Private Sub rptskufree_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptskufree.ItemCommand
        If e.CommandName = "DELETE" Then

            Dim dt As DataTable = GetSKUFreeList()
            dt.Rows.RemoveAt(e.Item.ItemIndex)
            rptskufree.DataSource = dt
            rptskufree.DataBind()

        End If
    End Sub

    Public Sub ddlSkuIDFree_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim ddlSkuIDFree1 As DropDownList = DirectCast(sender, DropDownList)
        Dim ddlSkuIDFree As DropDownList = DirectCast(ddlSkuIDFree1.FindControl("ddlSkuIDFree"), DropDownList)
        Dim lblProductIFree As Label = DirectCast(ddlSkuIDFree1.FindControl("lblProductIFree"), Label)

        If ddlSkuIDFree.SelectedValue <> "0" Then
            Dim sql As String = "SELECT S.Product_ID,(P.Product_TName + '/' + P.Product_EName) ProductName  FROM MS_SKU S left join MS_Product P ON s.Product_ID = P.Product_ID WHERE S.SKU_ID = '" & ddlSkuIDFree.SelectedValue & "'"
            Dim dt As DataTable = SqlDB.ExecuteTable(sql)
            For i As Integer = 0 To dt.Rows.Count - 1
                Dim ProductName As String = dt.Rows(i)("ProductName").ToString()
                lblProductIFree.Text = ProductName
            Next
        Else
            lblProductIFree.Text = ""
        End If
    End Sub

#End Region

End Class