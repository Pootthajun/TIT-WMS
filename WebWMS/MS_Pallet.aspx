﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="MS_Pallet.aspx.vb" Inherits="WebWMS.MS_Pallet" %>

<%@ Register Src="~/UserControl/ucSwitchButton.ascx" TagPrefix="uc1" TagName="ucSwitchButton" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="page-header">
        <h4 class="page-title">Pallet</h4>
    </div>
    <div class="page-content container-fluid">
        <div class="page-header">
            <h4 class="page-title">ข้อมูลพาเลท</h4>
        </div>
        <div class="page-content container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <asp:Panel ID="pnlList" runat="server" CssClass="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-2">
                                    <asp:LinkButton ID="btnAddPallet" runat="server" CssClass="btn btn-warning">
                                    <i class="icon md-plus-circle-o"></i>
                                    <span>เพื่มพาเลท</span>
                                    </asp:LinkButton>
                                </div>
                                <div class="col-sm-3">
                                <asp:LinkButton ID="btnOpenSearch" runat="server" CssClass="btn btn-primary">
                                     <i class="icon fa-search"></i> <span>ค้นหา</span>
                                </asp:LinkButton>
                            </div>
                        </div>
                        <asp:Panel ID="pnlSearch" runat="server">
                            <br />
                            <div class="row">
                                <div class="col-sm-3">
                                     <asp:DropDownList ID="ddlPalletTypeSearch" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="ประเภท" AutoPostBack="true"></asp:DropDownList>
                                </div>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtSearchID" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="100" placeholder="รหัส"></asp:TextBox>
                                </div>
                                <div class="col-sm-2">
                                    <asp:TextBox ID="txtStatusPalletSearch" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="100" placeholder="สถานะ"></asp:TextBox>
                                </div>
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddlstatus" CssClass="form-control" AutoPostBack="true" runat="server">
                                        <asp:ListItem Text="สถานะการใช้งาน" Value=""></asp:ListItem>
                                        <asp:ListItem Text="ใช้งาน" Value="Y"></asp:ListItem>
                                        <asp:ListItem Text="ไม่ใช้งาน" Value="N"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-sm-1">
                                    <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn btn-warning">
                                        <i class="icon fa-search"></i>
                                    </asp:LinkButton>
                                </div>
                            </div>
                            
                        </asp:Panel>

                            <div class="clearfix visible-md-block"></div>
                            <br />
                            <table class="tablesaw table-striped table-bordered table-hover">
                                <thead>
                                    <tr class="bg-blue-grey-100">
                                        <th class="text-center">ประภท</th>
                                        <th class="text-center">รหัส</th>
                                        <th class="text-center">สถานะพาเลท</th>
                                        <th class="text-center">ใช้งาน</th>
                                        <th class="text-center">แก้ไข</th>
                                        <th class="text-center">ลบ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptList" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td class="text-center">
                                                    <asp:Label ID="lblPalletType" runat="server"></asp:Label>
                                                    <asp:Label ID="lblSequenceID" runat="server" Visible="false"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="lblPalletNo" runat="server"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="lblPalletStatus" runat="server"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <uc1:ucSwitchButton runat="server" ID="chkActiveStatus" AutoPostBack="true" OnCheckedChanged="chkActiveStatus_CheckedChanged" />
                                                </td>
                                                <td class="text-center">
                                                    <asp:LinkButton ID="btnEdit" runat="server" CommandName="EDIT">
                                                    <i class="icon md-edit" style="font-size: 20px;"></i>
                                                    </asp:LinkButton>
                                                </td>
                                                <td class="text-center">
                                                    <asp:LinkButton ID="btnDelete" runat="server" CommandName="DELETE" OnClientClick="return confirm('Do you want delete data?');">
                                                    <i class="icon md-delete" style="font-size: 20px;"></i>
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlEdit" runat="server" CssClass="panel" Visible="false">
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="form-group col-sm-2">
                                    <label>รหัส :<span style="color: red;">*</span></label>
                                </div>

                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtPalletNo" runat="server" CssClass="form-control" MaxLength="100" placeholder="รหัสพาเลท"></asp:TextBox>
                                    <asp:Label ID="lblSequenceID" runat="server" Visible="false" Text="0"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group col-sm-2">
                                    <label>ประเภทพาเลท :<span style="color: red;">*</span></label>
                                </div>

                                <div class="col-sm-9">
                                    <asp:DropDownList ID="ddlPalletType" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="ประเภทพาเลท" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group col-sm-2">
                                    <label>รายละเอียด :<span style="color: red;">*</span></label>
                                </div>

                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtPalletDescription" runat="server" CssClass="form-control" MaxLength="100" placeholder="รายละเอียด"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group col-sm-2">
                                    <label>สถานะพาเลท :<span style="color: red;">*</span></label>
                                </div>

                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtPalletStatus" runat="server" CssClass="form-control" MaxLength="100" placeholder="สถานะพาเลท"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group col-sm-2">
                                    <label>ใช้งาน</label>
                                </div>

                                <div class="col-sm-9">
                                    <uc1:ucSwitchButton runat="server" ID="chkActive" />
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />
                            <div class="form-group form-material">
                                <div class="col-sm-12">
                                    <div class="col-sm-10 "></div>
                                    <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary">
                                    <span>Save</span>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-warning">
                                    <span>Cancel</span>
                                    </asp:LinkButton>
                                </div>
                            </div>

                        </div>
                    </asp:Panel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpFooter" runat="server">
</asp:Content>
