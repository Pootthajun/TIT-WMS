﻿Imports LinqDB.TABLE
Imports WMS_BL

Public Class MS_Address_Province
    Inherits System.Web.UI.Page

    Private ReadOnly Property UserSession As LoginSessionData
        Get
            Return Session(CommonBL.UserLoginSessionName)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect("frmLogin.aspx?rnd=" & DateTime.Now.Millisecond)
                Exit Sub

            Else
                BindList()
                ClearForm()
            End If
        End If
    End Sub

    Protected Sub chkActiveStatus_CheckedChanged(sender As ucSwitchButton, e As System.EventArgs)
        Dim chkActiveStatus As ucSwitchButton = sender
        Dim itm As RepeaterItem = chkActiveStatus.Parent
        Dim lblProvinceID As Label = itm.FindControl("lblProvinceID")
        Dim lblProvinceName As Label = itm.FindControl("lblProvinceName")

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.UpdateProvincestatus(UserSession.UserName, lblProvinceID.Text, chkActiveStatus.Checked)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Update status fail');", True)
            LogFileData.LogError(UserSession, ret.ErrorMessage)
        Else
            LogFileData.LogTrans(UserSession, "Update Province Status " & lblProvinceName.Text & " Status : " & chkActiveStatus.Checked.ToString)
        End If
    End Sub

    Private Sub BindList()
        Dim dt As DataTable = MasterDataBL.GetList_Province(dllCountrySearch.SelectedValue, txtSearch.Text.Trim, ddlstatus.SelectedValue)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Private Sub Bindddl()
        MasterDataBL.GetListDDL_Country(ddlCountry)
        MasterDataBL.GetListDDL_Country(dllCountrySearch)
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblCountry As Label = e.Item.FindControl("lblCountry")
        Dim lblProvinceName As Label = e.Item.FindControl("lblProvinceName")
        Dim lblProvinceID As Label = e.Item.FindControl("lblProvinceID")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        lblCountry.Text = e.Item.DataItem("Country_Name")
        lblProvinceName.Text = e.Item.DataItem("Province_Name")
        lblProvinceID.Text = e.Item.DataItem("Province_ID")
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblProvinceName As Label = e.Item.FindControl("lblProvinceName")
        Dim lblProvinceID As Label = e.Item.FindControl("lblProvinceID")

        Select Case e.CommandName
            Case "EDIT"
                ClearForm()
                FillInData(lblProvinceID.Text)

                pnlList.Visible = False
                pnlEdit.Visible = True

                LogFileData.LogTrans(UserSession, "คลิกปุ่ม Edit Province " & lblProvinceName.Text)
            Case "DELETE"
                Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.DeleteProvince(lblProvinceID.Text)
                If ret.IsSuccess = True Then
                    LogFileData.LogTrans(UserSession, "Delete Province " & lblProvinceName.Text)

                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Delete Data Fail');", True)
                    LogFileData.LogError(UserSession, ret.ErrorMessage)
                End If
        End Select
    End Sub

    Private Sub FillInData(ProvinceID As Long)
        Dim ret As MsAddressProvinceLinqDB = MasterDataBL.GetProvinceData(ProvinceID)
        If ret.PROVINCE_ID > 0 Then
            lblProvinceID.Text = ret.PROVINCE_ID
            ddlCountry.SelectedValue = ret.COUNTRY_ID
            txtProvinceName.Text = ret.PROVINCE_NAME
            chkActive.Checked = (ret.ACTIVE_STATUS = "Y")
        End If
        ret = Nothing
    End Sub

    Private Sub ClearForm()
        lblProvinceID.Text = "0"
        txtProvinceName.Text = ""
        chkActive.Checked = True
        Bindddl()
        ddlCountry.SelectedValue = 0
        dllCountrySearch.SelectedValue = 0
    End Sub

    Private Sub btnAddProvince_Click(sender As Object, e As EventArgs) Handles btnAddProvince.Click
        ClearForm()
        pnlEdit.Visible = True
        pnlList.Visible = False
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Add Province")
    End Sub

    Private Function ValidateData() As Boolean
        If lblProvinceID.Text = "0" Then
            'กรณีเป็นการเพิ่มใหม่ ให้ตรวจสอบข้อมูลการเข้าระบบด้วย
            If ddlCountry.SelectedValue = 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกประเทศ');", True)
                Return False
            End If

            If txtProvinceName.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุชื่อจังหวัด');", True)
                Return False
            End If
        End If

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.CheckDuplicateProvince(ddlCountry.SelectedValue, txtProvinceName.Text)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');", True)
            Return False
        End If

        Return True
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save Province")
        If ValidateData() = True Then

            Dim lnq As MsAddressProvinceLinqDB = MasterDataBL.SaveProvince(UserSession.UserName, lblProvinceID.Text, ddlCountry.SelectedValue, txtProvinceName.Text, chkActive.Checked)
            If lnq.PROVINCE_ID > 0 Then
                lblProvinceID.Text = lnq.PROVINCE_ID
                LogFileData.LogTrans(UserSession, "Save Province " & txtProvinceName.Text & " สำเร็จ")
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Data Complete');", True)
                pnlEdit.Visible = False
                pnlList.Visible = True
                BindList()
            Else
                LogFileData.LogError(UserSession, "Save Province " & txtProvinceName.Text & " ไม่สำเร็จ Error :" & lnq.ErrorMessage)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Fail');", True)
            End If
            lnq = Nothing
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Cancel")
        ClearForm()
        pnlEdit.Visible = False
        pnlList.Visible = True
        BindList()
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click, txtSearch.TextChanged
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม ค้นหา")
        BindList()
    End Sub

    Private Sub ddlstatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlstatus.SelectedIndexChanged
        BindList()
    End Sub

    Private Sub dllCountrySearch_SelectedIndexChanged(sender As Object, e As EventArgs) Handles dllCountrySearch.SelectedIndexChanged
        BindList()
    End Sub
End Class