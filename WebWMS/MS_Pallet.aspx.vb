﻿Imports LinqDB.TABLE
Imports WMS_BL

Public Class MS_Pallet
    Inherits System.Web.UI.Page

    Private ReadOnly Property UserSession As LoginSessionData
        Get
            Return Session(CommonBL.UserLoginSessionName)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect("frmLogin.aspx?rnd=" & DateTime.Now.Millisecond)
                Exit Sub
            Else
                BindList()
                ClearForm()
            End If
        End If
    End Sub

    Protected Sub chkActiveStatus_CheckedChanged(sender As ucSwitchButton, e As System.EventArgs)
        Dim chkActiveStatus As ucSwitchButton = sender
        Dim itm As RepeaterItem = chkActiveStatus.Parent
        Dim lblSequenceID As Label = itm.FindControl("lblSequenceID")
        Dim lblPalletNo As Label = itm.FindControl("lblPalletNo")

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.UpdatePalletstatus(UserSession.UserName, lblSequenceID.Text, chkActiveStatus.Checked)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Update status fail');", True)
            LogFileData.LogError(UserSession, ret.ErrorMessage)
        Else
            LogFileData.LogTrans(UserSession, "Update Pallet Status " & lblPalletNo.Text & " Status : " & chkActiveStatus.Checked.ToString)
        End If
    End Sub

    Private Sub BindList()
        Dim dt As DataTable = MasterDataBL.GetList_Pallet(ddlPalletTypeSearch.SelectedValue, txtSearchID.Text, txtStatusPalletSearch.Text, ddlstatus.SelectedValue)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Private Sub Bindddl()
        MasterDataBL.GetListDDL_PalletType(ddlPalletType)
        MasterDataBL.GetListDDL_PalletType(ddlPalletTypeSearch)
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblSequenceID As Label = e.Item.FindControl("lblSequenceID")
        Dim lblPalletNo As Label = e.Item.FindControl("lblPalletNo")
        Dim lblPalletType As Label = e.Item.FindControl("lblPalletType")
        Dim lblPalletStatus As Label = e.Item.FindControl("lblPalletStatus")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        lblSequenceID.Text = e.Item.DataItem("Sequence_ID")
        lblPalletNo.Text = e.Item.DataItem("Pallet_No")
        lblPalletType.Text = e.Item.DataItem("Pallet_Type")
        lblPalletStatus.Text = e.Item.DataItem("Pallet_Status")
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblPalletNo As Label = e.Item.FindControl("lblPalletNo")
        Dim lblSequenceID As Label = e.Item.FindControl("lblSequenceID")

        Select Case e.CommandName
            Case "EDIT"
                ClearForm()
                FillInData(lblSequenceID.Text)

                pnlList.Visible = False
                pnlEdit.Visible = True

                LogFileData.LogTrans(UserSession, "คลิกปุ่ม Edit Pallet " & lblPalletNo.Text)
            Case "DELETE"
                Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.DeletePallet(lblSequenceID.Text)
                If ret.IsSuccess = True Then
                    LogFileData.LogTrans(UserSession, "Delete Pallet " & lblPalletNo.Text)

                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Delete Data Fail');", True)
                    LogFileData.LogError(UserSession, ret.ErrorMessage)
                End If
        End Select
    End Sub

    Private Sub FillInData(PalletID As Long)
        Dim ret As MsPalletLinqDB = MasterDataBL.GetPalletData(PalletID)
        If ret.SEQUENCE_ID > 0 Then
            lblSequenceID.Text = ret.SEQUENCE_ID
            txtPalletNo.Text = ret.PALLET_NO
            ddlPalletType.SelectedValue = ret.PALLET_TYPE_ID
            txtPalletDescription.Text = ret.PALLET_DESCRIPTION
            txtPalletStatus.Text = ret.PALLET_STATUS
            chkActive.Checked = (ret.ACTIVE_STATUS = "Y")
        End If
        ret = Nothing
    End Sub

    Private Sub ClearForm()
        Bindddl()
        lblSequenceID.Text = "0"
        txtPalletNo.Text = ""
        ddlPalletType.SelectedValue = 0
        txtPalletDescription.Text = ""
        txtPalletStatus.Text = ""
        chkActive.Checked = True
        txtPalletNo.Enabled = False

        pnlSearch.Visible = False
    End Sub

    Private Sub btnAddPallet_Click(sender As Object, e As EventArgs) Handles btnAddPallet.Click
        ClearForm()
        pnlEdit.Visible = True
        pnlList.Visible = False
        txtPalletNo.Enabled = True
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Add Pallet")
    End Sub

    Private Function ValidateData() As Boolean
        If lblSequenceID.Text = "0" Then
            'กรณีเป็นการเพิ่มใหม่ ให้ตรวจสอบข้อมูลการเข้าระบบด้วย
            If txtPalletNo.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุรหัสพาเลท');", True)
                Return False
            End If

            If ddlPalletType.SelectedValue = "0" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกประเภทพาเลท');", True)
                Return False
            End If
        End If

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.CheckDuplicatePallet(lblSequenceID.Text, txtPalletNo.Text)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');", True)
            Return False
        End If

        Return True
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save Pallet Type")
        If ValidateData() = True Then

            Dim lnq As MsPalletLinqDB = MasterDataBL.SavePallet(UserSession.UserName, lblSequenceID.Text, txtPalletNo.Text, ddlPalletType.SelectedValue, txtPalletDescription.Text, txtPalletStatus.Text, chkActive.Checked)
            If lnq.SEQUENCE_ID > 0 Then
                lblSequenceID.Text = lnq.SEQUENCE_ID
                LogFileData.LogTrans(UserSession, "Save Pallet " & txtPalletNo.Text & " สำเร็จ")
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Data Complete');", True)
                pnlEdit.Visible = False
                pnlList.Visible = True
                BindList()
            Else
                LogFileData.LogError(UserSession, "Save Pallet " & txtPalletNo.Text & " ไม่สำเร็จ Error :" & lnq.ErrorMessage)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Fail');", True)
            End If
            lnq = Nothing
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Cancel")
        ClearForm()
        pnlEdit.Visible = False
        pnlList.Visible = True
        BindList()
    End Sub

    Private Sub btnOpenSearch_Click(sender As Object, e As EventArgs) Handles btnOpenSearch.Click
        pnlSearch.Visible = True
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click, ddlPalletTypeSearch.SelectedIndexChanged, txtSearchID.TextChanged, txtStatusPalletSearch.TextChanged, ddlstatus.SelectedIndexChanged
        BindList()
    End Sub
End Class