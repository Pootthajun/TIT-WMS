﻿Public Class LoginSessionData
    Dim _UserID As Long = 0
    Dim _UserName As String = ""
    Dim _LoginHistoryID As Long = 0
    Dim _ForceChangePsswd As String = "N"
    Dim _LastLoginTime As DateTime = New Date(1, 1, 1)

    Public Property UserID As Long
        Get
            Return _UserID
        End Get
        Set(value As Long)
            _UserID = value
        End Set
    End Property
    Public Property UserName As String
        Get
            Return _UserName.Trim
        End Get
        Set(value As String)
            _UserName = value
        End Set
    End Property

    Public Property LoginHistoryID As Long
        Get
            Return _LoginHistoryID
        End Get
        Set(value As Long)
            _LoginHistoryID = value
        End Set
    End Property

    Public Property ForceChangePsswd As String
        Get
            Return _ForceChangePsswd.Trim
        End Get
        Set(value As String)
            _ForceChangePsswd = value
        End Set
    End Property
    Public Property LastLoginTime As DateTime
        Get
            Return _LastLoginTime
        End Get
        Set(value As DateTime)
            _LastLoginTime = value
        End Set
    End Property

End Class
