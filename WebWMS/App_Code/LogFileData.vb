﻿Imports System.Diagnostics
Imports WMS_BL
Public Class LogFileData
    Public Shared Function CreateLoginHistory(req As HttpRequest, Username As String, FirstName As String, LastName As String) As LinqDB.ConnectDB.ExecuteDataInfo
        Dim ret As New LinqDB.ConnectDB.ExecuteDataInfo

        Dim LoginTime As DateTime = DateTime.Now
        Dim lnq As New LinqDB.TABLE.TbLoginHistoryLinqDB
        lnq.USERNAME = Username
        lnq.FIRST_NAME = FirstName
        lnq.LAST_NAME = LastName
        lnq.LOGON_TIME = LoginTime
        lnq.CLIENT_IP = req.UserHostAddress
        lnq.CLIENT_BROWSER = "Browser : " & req.Browser.Browser & " Version : " & req.Browser.Version
        lnq.SERVER_URL = req.Url.AbsoluteUri

        Dim trans As New LinqDB.ConnectDB.TransactionDB
        ret = lnq.InsertData(Username, trans.Trans)
        If ret.IsSuccess = True Then
            Dim NewLoginHis As Long = lnq.LOGIN_HISTORY_ID

            'Update Last Login Time
            Dim uLnq As New LinqDB.TABLE.MsUserLinqDB
            uLnq.ChkDataByUSER_LOGIN(Username, trans.Trans)
            uLnq.LAST_LOGIN_TIME = LoginTime

            ret = uLnq.UpdateData(Username, trans.Trans)
            If ret.IsSuccess = True Then
                ret.NewID = NewLoginHis

                trans.CommitTransaction()
            Else
                trans.RollbackTransaction()
            End If
            uLnq = Nothing
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Sub LogTrans(UserSession As LoginSessionData, LogMsg As String)

        Dim frame As StackFrame = New StackFrame(1, True)
        Dim ClassName As String = frame.GetMethod.ReflectedType.Name
        Dim FunctionName As String = frame.GetMethod.Name
        Dim LineNo As Integer = frame.GetFileLineNumber
        LogFileBL.CreateUserActivityLog(UserSession.LoginHistoryID, UserSession.UserName, ClassName, FunctionName, LineNo, LogMsg, AgentLogType.TransLog)
    End Sub

    Public Shared Sub LogTrans(LoginHisID As Long, LogMsg As String)

        Dim frame As StackFrame = New StackFrame(1, True)
        Dim ClassName As String = frame.GetMethod.ReflectedType.Name
        Dim FunctionName As String = frame.GetMethod.Name
        Dim LineNo As Integer = frame.GetFileLineNumber

        Dim lnq As New LinqDB.TABLE.TbLoginHistoryLinqDB
        lnq.GetDataByPK(LoginHisID, Nothing)
        If lnq.LOGIN_HISTORY_ID > 0 Then
            LogFileBL.CreateUserActivityLog(LoginHisID, lnq.USERNAME, ClassName, FunctionName, LineNo, LogMsg, AgentLogType.TransLog)
        End If
        lnq = Nothing
    End Sub

    Public Shared Sub LogError(UserSession As LoginSessionData, LogMsg As String)
        Dim frame As StackFrame = New StackFrame(1, True)
        Dim ClassName As String = frame.GetMethod.ReflectedType.Name
        Dim FunctionName As String = frame.GetMethod.Name
        Dim LineNo As Integer = frame.GetFileLineNumber

        LogFileBL.CreateUserActivityLog(UserSession.LoginHistoryID, UserSession.UserName, ClassName, FunctionName, LineNo, LogMsg, AgentLogType.ErrorLog)
    End Sub

    Public Shared Sub LogException(UserSession As LoginSessionData, ExMessage As String, ExStackTrace As String)
        Dim frame As StackFrame = New StackFrame(1, True)
        Dim ClassName As String = frame.GetMethod.ReflectedType.Name
        Dim FunctionName As String = frame.GetMethod.Name
        Dim LineNo As Integer = frame.GetFileLineNumber

        LogFileBL.CreateUserActivityLog(UserSession.LoginHistoryID, UserSession.UserName, ClassName, FunctionName, LineNo, "Exception : " & ExMessage & vbNewLine & ExStackTrace, AgentLogType.ExceptionLog)
    End Sub

    Private Enum AgentLogType
        TransLog = 1
        ErrorLog = 2
        ExceptionLog = 3
    End Enum
End Class
