﻿Imports System.IO
Imports LinqDB.TABLE
Imports WMS_BL

Public Class MS_SKU
    Inherits System.Web.UI.Page

    Private ReadOnly Property UserSession As LoginSessionData
        Get
            Return Session(CommonBL.UserLoginSessionName)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect("frmLogin.aspx?rnd=" & DateTime.Now.Millisecond)
                Exit Sub
            Else
                BindList()
                ClearForm()
            End If
        End If
    End Sub

    Protected Sub chkActiveStatus_CheckedChanged(sender As ucSwitchButton, e As System.EventArgs)
        Dim chkActiveStatus As ucSwitchButton = sender
        Dim itm As RepeaterItem = chkActiveStatus.Parent
        Dim lblSequenceID As Label = itm.FindControl("lblSequenceID")
        Dim lblSKUID As Label = itm.FindControl("lblSKUID")

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.UpdateSKUGroupstatus(UserSession.UserName, lblSequenceID.Text, chkActiveStatus.Checked)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Update status fail');", True)
            LogFileData.LogError(UserSession, ret.ErrorMessage)
        Else
            LogFileData.LogTrans(UserSession, "Update SKU " & lblSKUID.Text & " Status : " & chkActiveStatus.Checked.ToString)
        End If
    End Sub

    Private Sub BindList()
        Dim dt As DataTable = MasterDataBL.GetList_SKU(txtSearchID.Text, ddlSKUGroupSearch.SelectedValue, txtSearchProductName.Text, ddlstatus.SelectedValue)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Private Sub Bindddl()
        MasterDataBL.GetListDDL_SKUGroup(ddlSKUGroup)
        MasterDataBL.GetListDDL_PalletType(ddlPallet)
        MasterDataBL.GetListDDL_PickOrder(ddlPick)
        MasterDataBL.GetListDDL_Unit(ddlUnit)
        MasterDataBL.GetListDDL_ProductType(ddlProductType)
        MasterDataBL.GetListDDL_Product(ddlProduct, ddlProductType.SelectedValue)

        MasterDataBL.GetListDDL_SKUGroup(ddlSKUGroupSearch)
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblSequenceID As Label = e.Item.FindControl("lblSequenceID")
        Dim lblSKUID As Label = e.Item.FindControl("lblSKUID")
        Dim lblSKUGroupName As Label = e.Item.FindControl("lblSKUGroupName")
        Dim lblProduct As Label = e.Item.FindControl("lblProduct")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        lblSequenceID.Text = e.Item.DataItem("Sequence_ID")
        lblSKUID.Text = e.Item.DataItem("SKU_ID")
        lblSKUGroupName.Text = e.Item.DataItem("SKU_Group_Name")
        lblProduct.Text = e.Item.DataItem("Project_Name")
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblSequenceID As Label = e.Item.FindControl("lblSequenceID")
        Dim lblSKUID As Label = e.Item.FindControl("lblSKUID")

        Select Case e.CommandName
            Case "EDIT"
                ClearForm()
                FillInData(lblSequenceID.Text)

                pnlList.Visible = False
                pnlEdit.Visible = True

                LogFileData.LogTrans(UserSession, "คลิกปุ่ม Edit SKU " & lblSKUID.Text)
            Case "DELETE"
                Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.DeleteSKU(lblSequenceID.Text)
                If ret.IsSuccess = True Then
                    LogFileData.LogTrans(UserSession, "Delete SKU " & lblSKUID.Text)

                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Delete Data Fail');", True)
                    LogFileData.LogError(UserSession, ret.ErrorMessage)
                End If
        End Select
    End Sub

    Private Sub FillInData(SKUGroupID As Long)
        Dim ret As MsSkuLinqDB = MasterDataBL.GetSKUData(SKUGroupID)
        If ret.SEQUENCE_ID > 0 Then
            lblSequenceID.Text = ret.SEQUENCE_ID
            txtSKUID.Text = ret.SKU_ID
            ddlSKUGroup.SelectedValue = ret.SKU_GROUP_ID
            txtSKUDetial.Text = ret.SKU_DESCRIPTION
            txtsize.Text = ret.SIZE
            txtcolor.Text = ret.COLOR
            txtmodel.Text = ret.MODEL
            txtBrand.Text = ret.BRAND
            ddlUnit.SelectedValue = ret.UNIT_ID
            txtWeightPerUnit.Text = ret.WEIGHT_PER_UNIT
            MasterDataBL.GetListDDL_ProductType(ddlProductType)
            ddlProductType.SelectedValue = ret.PRODUCT_TYPE_ID
            MasterDataBL.GetListDDL_Product(ddlProduct, ddlProductType.SelectedValue)
            ddlProduct.SelectedValue = ret.PRODUCT_ID
            txtVolumePerUnit.Text = ret.VOLUME_PER_UNIT
            txtwidth.Text = ret.VOLUME_WIDTH
            txtlength.Text = ret.VOLUME_LENGTH
            txtheight.Text = ret.VOLUME_HIGHT

            Dim ProductionDate As String = ""
            Dim ExpireDate As String = ""
            ProductionDate = Convert.ToDateTime(ret.PRODUCTION_DATE).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
            ExpireDate = Convert.ToDateTime(ret.EXPIRE_DATE).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
            txtProductionDate.Text = ProductionDate
            txtExpireDate.Text = ExpireDate

            txtAgeDay.Text = ret.AGE_PRODUCT_DAY
            txtAgeMonth.Text = ret.AGE_PRODUCT_MONTH
            txtAgeYear.Text = ret.AGE_PRODUCT_YEAR
            txtPicture.Text = ret.PRODUCT_PICTURE
            txtBuyPrice.Text = ret.BUY_PRICE
            txtSellPrice.Text = ret.SELL_PRICE
            txtOtherPrice.Text = ret.OTHER_PRICE
            txtAmountMax.Text = ret.MAXIMUM_AMOUNT
            txtAmountMin.Text = ret.MINIMUM_AMOUNT
            txtWeightMax.Text = ret.MAXIMUM_WEIGHT
            txtWeightMin.Text = ret.MINIMUM_WEIGHT
            txtVolumeMax.Text = ret.MAXIMUM_VOLUME
            txtVolumeMin.Text = ret.MINIMUM_VOLUME
            ddlPallet.SelectedValue = ret.PALLET_TYPE_ID
            txtMaxPallet.Text = ret.MAXIMUM_PER_PALLET
            ddlPick.SelectedValue = ret.PICK_ID
            txtAmountPerUnit.Text = ret.AMOUNT_PER_UNIT
            chkActive.Checked = (ret.ACTIVE_STATUS = "Y")
        End If
        ret = Nothing
    End Sub

    Private Sub ClearForm()
        Bindddl()
        lblSequenceID.Text = "0"
        txtSKUID.Text = ""
        txtSKUDetial.Text = ""
        txtsize.Text = ""
        txtcolor.Text = ""
        txtmodel.Text = ""
        txtBrand.Text = ""
         ddlUnit.SelectedValue = 0
        txtWeightPerUnit.Text = ""
        ddlSKUGroup.SelectedValue = 0
        ddlProductType.SelectedValue = 0
        ddlProduct.SelectedValue = 0
        txtVolumePerUnit.Text = ""
        txtwidth.Text = ""
        txtlength.Text = ""
        txtheight.Text = ""
        txtProductionDate.Text = ""
        txtExpireDate.Text = ""
        txtAgeDay.Text = ""
        txtAgeMonth.Text = ""
        txtAgeYear.Text = ""
        txtPicture.Text = ""
        txtBuyPrice.Text = ""
        txtSellPrice.Text = ""
        txtOtherPrice.Text = ""
        txtAmountMax.Text = ""
        txtAmountMin.Text = ""
        txtWeightMax.Text = ""
        txtWeightMin.Text = ""
        txtVolumeMax.Text = ""
        txtVolumeMin.Text = ""
        ddlPallet.SelectedValue = 0
        txtMaxPallet.Text = ""
        ddlPick.SelectedValue = 0
        txtAmountPerUnit.Text = ""
        chkActive.Checked = True
        txtSKUID.Enabled = False

        pnlSearch.Visible = False
    End Sub

    Private Sub btnAddSKU_Click(sender As Object, e As EventArgs) Handles btnAddSKU.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Add SKU ")
        pnlList.Visible = False
        pnlEdit.Visible = True
        txtSKUID.Enabled = True
    End Sub

    Private Function ValidateData() As Boolean
        If lblSequenceID.Text = "0" Then
            'กรณีเป็นการเพิ่มใหม่ ให้ตรวจสอบข้อมูลการเข้าระบบด้วย
            If txtSKUID.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุรหัสชนิดสินค้า');", True)
                Return False
            End If

            If txtsize.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุขนาด');", True)
                Return False
            End If

            If txtcolor.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุสี');", True)
                Return False
            End If

            If txtmodel.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุรุ่น');", True)
                Return False
            End If

            If txtBrand.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุแบรนด์');", True)
                Return False
            End If

            If ddlSKUGroup.SelectedValue = "0" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาลเลือกกลุ่มชนิดสินค้า');", True)
                Return False
            End If

            If ddlProductType.SelectedValue = "0" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาลเลือกประเภทสินค้า');", True)
                Return False
            End If

            If ddlProduct.SelectedValue = "0" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกสินค้า');", True)
                Return False
            End If

            If ddlPallet.SelectedValue = "0" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาลเลือกพาเลท');", True)
                Return False
            End If

            If ddlPick.SelectedValue = "0" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาลเลือกกิจกรรมการขาย');", True)
                Return False
            End If

            If txtProductionDate.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุวันผลิต');", True)
                Return False
            End If

            If txtExpireDate.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุวันหมดอายุ');", True)
                Return False
            End If

            If txtBuyPrice.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุราคาซื้อ');", True)
                Return False
            End If

            If txtSellPrice.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุราคาขาย');", True)
                Return False
            End If

            If txtMaxPallet.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุจำนวนพาเลท');", True)
                Return False
            End If
        End If

        Return True
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save SKU")
        If ValidateData() = True Then

            Dim lnq As MsSkuLinqDB = MasterDataBL.SaveSKU(UserSession.UserName, lblSequenceID.Text, txtSKUID.Text, txtSKUDetial.Text, ddlSKUGroup.SelectedValue, txtsize.Text, txtcolor.Text, txtmodel.Text, txtBrand.Text, ddlUnit.SelectedValue, txtWeightPerUnit.Text, ddlProductType.SelectedValue, ddlProduct.SelectedValue, txtVolumePerUnit.Text, txtwidth.Text, txtlength.Text, txtheight.Text, txtProductionDate.Text, txtExpireDate.Text, txtAgeDay.Text, txtAgeMonth.Text, txtAgeYear.Text, txtPicture.Text, txtBuyPrice.Text, txtSellPrice.Text, txtOtherPrice.Text, txtAmountMax.Text, txtAmountMin.Text, txtWeightMax.Text, txtWeightMin.Text, txtVolumeMax.Text, txtVolumeMin.Text, ddlPallet.SelectedValue, txtMaxPallet.Text, ddlPick.SelectedValue, txtAmountPerUnit.Text, chkActive.Checked)
            If lnq.SEQUENCE_ID > 0 Then
                lblSequenceID.Text = lnq.SEQUENCE_ID
                LogFileData.LogTrans(UserSession, "Save SKU " & txtSKUID.Text & " สำเร็จ")
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Data Complete');", True)
                pnlEdit.Visible = False
                pnlList.Visible = True
                BindList()
            Else
                LogFileData.LogError(UserSession, "Save SKU " & txtSKUID.Text & " ไม่สำเร็จ Error :" & lnq.ErrorMessage)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Fail');", True)
            End If
            lnq = Nothing
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Cancel")
        ClearForm()
        pnlEdit.Visible = False
        pnlList.Visible = True
        BindList()
    End Sub

    Private Sub ddlProductType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProductType.SelectedIndexChanged
        MasterDataBL.GetListDDL_Product(ddlProduct, ddlProductType.SelectedValue)
        ddlProduct.SelectedValue = 0
    End Sub

    Private Sub txtExpireDate_TextChanged(sender As Object, e As EventArgs) Handles txtExpireDate.TextChanged
        Dim ProductionDate As Date = MasterDataBL.StringToDate(txtProductionDate.Text, "dd/MM/yyyy")
        Dim ExpireDate As Date = MasterDataBL.StringToDate(txtExpireDate.Text, "dd/MM/yyyy")

        If ExpireDate < ProductionDate Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุช่วงวันที่ให้ถูกต้อง');", True)
        Else
            Dim DequalYear As Date
            Dim DequalMonth As Date

            Dim sYears As String
            Dim sDays As String
            Dim sMonths As String

            sYears = DateDiff(DateInterval.Year, ProductionDate, ExpireDate)
            DequalYear = ProductionDate.AddYears(sYears)
            sMonths = DateDiff(DateInterval.Month, DequalYear, ExpireDate)
            DequalMonth = DequalYear.AddMonths(sMonths)
            sDays = DateDiff(DateInterval.Day, DequalMonth, ExpireDate)

            If sDays < 0 Then
                sMonths = sMonths - 1
                sDays = 30 + sDays
            End If

            If sYears = "0" Then sYears = ""
            If sMonths = "0" Then sMonths = ""
            If sDays = "0" Then sDays = ""

            txtAgeDay.Text = sDays.ToString()
            txtAgeMonth.Text = sMonths.ToString()
            txtAgeYear.Text = sYears.ToString()
        End If
    End Sub

    Private Sub btnOpenSearch_Click(sender As Object, e As EventArgs) Handles btnOpenSearch.Click
        pnlSearch.Visible = True
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click, txtSearchID.TextChanged, ddlSKUGroupSearch.SelectedIndexChanged, txtSearchProductName.TextChanged, ddlstatus.SelectedIndexChanged
        BindList()
    End Sub
End Class