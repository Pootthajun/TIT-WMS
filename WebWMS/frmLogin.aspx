﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmLogin.aspx.vb" Inherits="WebWMS.frmLogin" %>

<!DOCTYPE html>

<html class="no-js css-menubar" lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Warehouse Management Sytem Login Page</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui" />
    <meta name="description" content="bootstrap admin template" />
    <meta name="author" content="" />

    <!-- Stylesheets -->
    <link rel="stylesheet" href="global/css/bootstrap.css" />
    <link rel="stylesheet" href="global/css/bootstrap-extend.min.css" />
    <link rel="stylesheet" href="assets/css/site.css" />
</head>
<body class="site-navbar-small ">
    <form id="form1" runat="server">
        <div class="align-middle text-center">
            <div class="row">
                <div class="col-lg-4"></div>
                <div class="col-lg-4">
                    <div class="panel">
                        <header class="panel-heading">
                            <h5 class="panel-title">
                                Welcome to Warehouse Management System
                            </h5>
                        </header>
                        <div class="panel-body">
                            <div class="form-group">
                                <asp:TextBox ID="txtUsername" runat="server" CssClass="form-control" placeholder="Username" autocomplete="off" required ></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="form-control" placeholder="Password" autocomplete="off" required ></asp:TextBox>
                            </div>
                            <asp:Button ID="btnLogin" runat="server" CssClass="btn btn-primary btn-block btn-lg" Text="Login" />
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <asp:Label ID="lblLoginFailCount" runat="server" Text="0" Visible="false"></asp:Label>
                    <asp:Label ID="lblMaxLoginFail" runat="server" Text="0" Visible="false"></asp:Label>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            function close_window() {
                window.close();
            }
        </script>
       <script type="text/javascript">
           function CloseWindow() {
               close();
           }
       
       </script>
    </form>
</body>
</html>
