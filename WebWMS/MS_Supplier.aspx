﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="MS_Supplier.aspx.vb" Inherits="WebWMS.MS_Supplier" %>

<%@ Register Src="~/UserControl/ucSwitchButton.ascx" TagPrefix="uc1" TagName="ucSwitchButton" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="page-header">
        <h4 class="page-title">ข้อมูลผู้ขาย</h4>
    </div>
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <asp:Panel ID="pnlList" runat="server" CssClass="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-2">
                                <asp:LinkButton ID="btnAddSupplier" runat="server" CssClass="btn btn-warning">
                            <i class="icon md-plus-circle-o"></i>
                            <span>เพื่มผู้ขาย</span>
                                </asp:LinkButton>
                            </div>
                            <div class="col-sm-3">
                                <asp:LinkButton ID="btnOpenSearch" runat="server" CssClass="btn btn-primary">
                            <i class="icon fa-search"></i>
                            <span>ค้นหา</span>
                                </asp:LinkButton>
                            </div>
                        </div>
                        <asp:Panel ID="pnlSearch" runat="server">
                            <br />
                             <div class="row">
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtSearchID" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="100" placeholder="รหัส"></asp:TextBox>
                                </div>
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddlSupplierTypeSearch" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="ประเภทลูกค้า" AutoPostBack="true"></asp:DropDownList>
                                </div>
                                <div class="col-sm-2">
                                    <asp:TextBox ID="txtSearchName" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="100" placeholder="ชื่อผู้ขาย"></asp:TextBox>
                                </div>
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddlstatus" CssClass="form-control" AutoPostBack="true" runat="server">
                                        <asp:ListItem Text="สถานะการใช้งาน" Value=""></asp:ListItem>
                                        <asp:ListItem Text="ใช้งาน" Value="Y"></asp:ListItem>
                                        <asp:ListItem Text="ไม่ใช้งาน" Value="N"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-sm-1">
                                    <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn btn-warning">
                                        <i class="icon fa-search"></i>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </asp:Panel>

                        <div class="clearfix visible-md-block"></div>
                        <br />
                        <table class="tablesaw table-striped table-bordered table-hover">
                            <thead>
                                <tr class="bg-blue-grey-100">
                                    <th class="text-center">รหัส</th>
                                    <th class="text-center">ประเภทผู้ขาย</th>
                                    <th class="text-center">ชื่อผู้ขาย</th>
                                    <th class="text-center">ใช้งาน</th>
                                    <th class="text-center">แก้ไข</th>
                                    <th class="text-center">ลบ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptList" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td class="text-center">
                                                <asp:Label ID="lblSupplierCode" runat="server"></asp:Label>
                                                <asp:Label ID="lblSupplierID" runat="server" Visible="false"></asp:Label>
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="lblSuppliertype" runat="server"></asp:Label>
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="lblSupplierName" runat="server"></asp:Label>
                                            </td>
                                            <td class="text-center">
                                                <uc1:ucSwitchButton runat="server" ID="chkActiveStatus" AutoPostBack="true" OnCheckedChanged="chkActiveStatus_CheckedChanged" />
                                            </td>
                                            <td class="text-center">
                                                <asp:LinkButton ID="btnEdit" runat="server" CommandName="EDIT">
                                                    <i class="icon md-edit" style="font-size: 20px;"></i>
                                                </asp:LinkButton>
                                            </td>
                                            <td class="text-center">
                                                <asp:LinkButton ID="btnDelete" runat="server" CommandName="DELETE" OnClientClick="return confirm('Do you want delete data?');">
                                                    <i class="icon md-delete" style="font-size: 20px;"></i>
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlEdit" runat="server" CssClass="panel" Visible="false">
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Supplier ID :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-9">
                                <asp:TextBox ID="txtSupplierID" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off"></asp:TextBox>
                                <asp:Label ID="lblSupplierID" runat="server" Visible="false" Text="0"></asp:Label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Supplier Type :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-9">
                                <asp:DropDownList ID="ddlSupplierType" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="ประเภทผู้ขาย" AutoPostBack="true"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Supplier Name :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-4">
                                <asp:TextBox ID="txtSupplierNameTH" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="ชื่อผู้ขาย"></asp:TextBox>
                            </div>
                            <div class="col-sm-5">
                                <asp:TextBox ID="txtSupplierNameEN" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="Supplier Name"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Supplier Alias :</label>
                            </div>

                            <div class="col-sm-9">
                                <asp:TextBox ID="txtAliasName" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="ชื่อเล่น"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Tax No :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-9">
                                <asp:TextBox ID="txtTaxNo" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="เลขประจำตัวผู้เสียภาษี"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Moblie :</label>
                            </div>

                            <div class="col-sm-9">
                                <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="เบอร์โทรศัพท์มือถือ"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Tel :</label>
                            </div>

                            <div class="col-sm-4">
                                <asp:TextBox ID="txtTel" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="เบอร์โทรศัพท์"></asp:TextBox>
                            </div>
                            <div class="form-group col-sm-1">
                                <label>Fax :</label>
                            </div>

                            <div class="col-sm-4">
                                <asp:TextBox ID="txtFax" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="เบอร์โทรสาร"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Email :</label>
                            </div>

                            <div class="col-sm-9">
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="อีเมล์"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <br />
                            <div class="form-group col-sm-11">
                                <label>Contact Name :</label>
                            </div>
                            <div style="text-align: right">
                                <asp:LinkButton ID="btnAddContact" runat="server" CssClass="btn btn-primary">
                                    <span>+</span>
                                </asp:LinkButton>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <table class="tablesaw table-striped table-bordered table-hover">
                                <thead>
                                    <tr class="bg-blue-grey-100">
                                        <th class="text-center" style="width: 20%">ชื่อ-นามสกุล</th>
                                        <th class="text-center" style="width: 20%">เบอร์โทรศัพท์</th>
                                        <th class="text-center" style="width: 20%">อีเมล์</th>
                                        <th class="text-center" style="width: 10%">ใช้งาน</th>
                                        <th class="text-center" style="width: 10%">แก้ไข</th>
                                        <th class="text-center" style="width: 10%">ลบ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptListContact" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <asp:Label ID="lblRowIndex" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblContactCode" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblContactID" runat="server" Visible="false"></asp:Label>
                                                <td class="text-center">
                                                    <asp:Label ID="lblPrefixID" runat="server" Visible="false"></asp:Label>
                                                    <asp:Label ID="lblPrefix" runat="server"></asp:Label>
                                                    <asp:Label ID="lblContactFName" runat="server"></asp:Label>
                                                    <asp:Label ID="lblContactLName" runat="server"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="lblContactTel" runat="server" Visible="false"></asp:Label>
                                                    <asp:Label ID="lblContactMobile" runat="server"></asp:Label>
                                                    <asp:Label ID="lblContactFax" runat="server" Visible="false"></asp:Label>

                                                    <asp:Label ID="lblContactStatus" runat="server" Visible="false"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="lblContactEmail" runat="server"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <uc1:ucSwitchButton runat="server" ID="chkActiveStatus" Enabled="false" />
                                                </td>
                                                <td class="text-center">
                                                    <asp:LinkButton ID="btnEdit" runat="server" CommandName="EDIT">
                                                    <i class="icon md-edit" style="font-size: 20px;"></i>
                                                    </asp:LinkButton>
                                                </td>
                                                <td class="text-center">
                                                    <asp:LinkButton ID="btnDelete" runat="server" CommandName="DELETE" OnClientClick="return confirm('Do you want delete data?');">
                                                    <i class="icon md-delete" style="font-size: 20px;"></i>
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-12">
                            <br />
                            <div class="form-group col-sm-11">
                                <label>Address :</label>
                            </div>
                            <div style="text-align: right">
                                <asp:LinkButton ID="btnAddAddress" runat="server" CssClass="btn btn-primary">
                                    <span>+</span>
                                </asp:LinkButton>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <table class="tablesaw table-striped table-bordered table-hover">
                                <thead>
                                    <tr class="bg-blue-grey-100">
                                        <th class="text-center" style="width: 70%">ที่อยู่</th>
                                        <th class="text-center" style="width: 10%">ใช้งาน</th>
                                        <th class="text-center" style="width: 10%">แก้ไข</th>
                                        <th class="text-center" style="width: 10%">ลบ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptListAddress" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <asp:Label ID="lblRowIndexAd" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressID" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressCode" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressHouseNo" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressBuildingName" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressMoo" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressSoi" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressRoad" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressTumbonID" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressDistrictID" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressProvinceID" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressCountryID" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressPascode" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblAddressStatus" runat="server" Visible="false"></asp:Label>
                                                <td>
                                                    <asp:Label ID="lblAddress" runat="server"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <uc1:ucSwitchButton runat="server" ID="chkActiveStatus" Enabled="false" />
                                                </td>
                                                <td class="text-center">
                                                    <asp:LinkButton ID="btnEdit" runat="server" CommandName="EDIT">
                                                    <i class="icon md-edit" style="font-size: 20px;"></i>
                                                    </asp:LinkButton>
                                                </td>
                                                <td class="text-center">
                                                    <asp:LinkButton ID="btnDelete" runat="server" CommandName="DELETE" OnClientClick="return confirm('Do you want delete data?');">
                                                    <i class="icon md-delete" style="font-size: 20px;"></i>
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-sm-12">
                            <br />
                            <div class="form-group col-sm-2">
                                <label>ใช้งาน</label>
                            </div>

                            <div class="col-sm-9">
                                <uc1:ucSwitchButton runat="server" ID="chkActive" />
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <hr />
                        <div class="form-group form-material">
                            <div class="col-sm-12 col-sm-offset-10">
                                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary">
                                    <span>Save</span>
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-warning">
                                    <span>Cancel</span>
                                </asp:LinkButton>
                            </div>
                        </div>

                    </div>
                </asp:Panel>

                <%--contact--%>
                <asp:Panel ID="pnlEditContact" runat="server" CssClass="panel" Visible="false">
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Contact Person :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-9">
                                <asp:DropDownList ID="ddlPrefixName" runat="server" data-plugin="selectpicker" CssClass="selectpicker" data-live-search="true" title="คำนำหน้าชื่อ"></asp:DropDownList>
                                <asp:Label ID="lblRowIndex" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Name :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-4">
                                <asp:TextBox ID="txtContactFName" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="ชื่อจริง"></asp:TextBox>
                            </div>
                            <div class="col-sm-5">
                                <asp:TextBox ID="txtContactLName" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="นามสกุล"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Mobile :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-9">
                                <asp:TextBox ID="txtContactMobile" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="เบอร์โทรศัพท์มือถือ"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Tel :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-4">
                                <asp:TextBox ID="txtContactTel" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="เบอร์โทรศัพท์"></asp:TextBox>
                            </div>
                            <div class="form-group col-sm-1">
                                <label>Fax :</label>
                            </div>

                            <div class="col-sm-4">
                                <asp:TextBox ID="txtContactFax" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="เบอร์โทรสาร"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>Email :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-9">
                                <asp:TextBox ID="txtContactEmail" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="อีเมลล์"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <br />
                            <div class="form-group col-sm-2">
                                <label>ใช้งาน</label>
                            </div>

                            <div class="col-sm-9">
                                <uc1:ucSwitchButton runat="server" ID="chkActiveContact" />
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr />
                        <div class="form-group form-material">
                            <div class="col-sm-12 col-sm-offset-10">
                                <asp:LinkButton ID="btnSaveContact" runat="server" CssClass="btn btn-primary">
                                    <span>Save</span>
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnCanelContact" runat="server" CssClass="btn btn-warning">
                                    <span>Cancel</span>
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <%--Address--%>
                <asp:Panel ID="pnlEditAddress" runat="server" CssClass="panel" Visible="false">
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>บ้านเลขที่ :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-4">
                                <asp:TextBox ID="txthouseNo" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="บ้านเลขที่"></asp:TextBox>
                            </div>
                            <div class="form-group col-sm-2">
                                <label>อาคารที่ตั้ง :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtBuildingName" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="อาคารที่ตั้ง"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>หมู่ :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtmoo" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="หมู่"></asp:TextBox>
                            </div>
                            <div class="form-group col-sm-2">
                                <label>ซอย :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtsoi" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="ซอย"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>ถนน :<span style="color: red;">*</span></label>
                            </div>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtroad" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="ถนน"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>จังหวัด :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-4">
                                <asp:DropDownList ID="ddlProvince" runat="server" data-plugin="selectpicker" CssClass="selectpicker" AutoPostBack="true" data-live-search="true" title="จังหวัด"></asp:DropDownList>

                            </div>
                            <div class="form-group col-sm-2">
                                <label>อำเภอ/เขต :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-4">
                                <asp:DropDownList ID="ddlDistrict" runat="server" data-plugin="selectpicker" CssClass="selectpicker" AutoPostBack="true" data-live-search="true" title="อำเภอ/เขต"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>ตำบล/แขวง :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-4">
                                <asp:DropDownList ID="ddlTumbon" runat="server" data-plugin="selectpicker" CssClass="selectpicker" AutoPostBack="true" data-live-search="true" title="ตำบล/แขวง"></asp:DropDownList>
                            </div>
                            <div class="form-group col-sm-2">
                                <label>ประเทศ :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-4">
                                <asp:DropDownList ID="ddlCountry" runat="server" data-plugin="selectpicker" CssClass="selectpicker" AutoPostBack="true" data-live-search="true" title="ประเทศ"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group col-sm-2">
                                <label>รหัสไปรษณีย์ :<span style="color: red;">*</span></label>
                            </div>

                            <div class="col-sm-10">
                                <asp:TextBox ID="txtPoscode" runat="server" CssClass="form-control" MaxLength="100" autocomplete="off" placeholder="รหัสไปรษณีย์"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <br />
                            <div class="form-group col-sm-2">
                                <label>ใช้งาน :</label>
                            </div>

                            <div class="col-sm-9">
                                <uc1:ucSwitchButton runat="server" ID="chkActiveAddress" />
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr />
                        <div class="form-group form-material">
                            <div class="col-sm-12 col-sm-offset-10">
                                <asp:LinkButton ID="btnSaveAddress" runat="server" CssClass="btn btn-primary">
                                    <span>Save</span>
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnCancelAddress" runat="server" CssClass="btn btn-warning">
                                    <span>Cancel</span>
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpFooter" runat="server">
</asp:Content>
