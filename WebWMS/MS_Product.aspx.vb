﻿Imports LinqDB.TABLE
Imports WMS_BL

Public Class MS_Product
    Inherits System.Web.UI.Page

    Private ReadOnly Property UserSession As LoginSessionData
        Get
            Return Session(CommonBL.UserLoginSessionName)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Session(CommonBL.UserLoginSessionName) Is Nothing Then
                Response.Redirect("frmLogin.aspx?rnd=" & DateTime.Now.Millisecond)
                Exit Sub
            Else
                BindList()
                ClearForm()
            End If
        End If
    End Sub

    Protected Sub chkActiveStatus_CheckedChanged(sender As ucSwitchButton, e As System.EventArgs)
        Dim chkActiveStatus As ucSwitchButton = sender
        Dim itm As RepeaterItem = chkActiveStatus.Parent
        Dim lblSequenceID As Label = itm.FindControl("lblSequenceID")
        Dim lblProductName As Label = itm.FindControl("lblProductName")

        Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.UpdateProductstatus(UserSession.UserName, lblSequenceID.Text, chkActiveStatus.Checked)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Update status fail');", True)
            LogFileData.LogError(UserSession, ret.ErrorMessage)
        Else
            LogFileData.LogTrans(UserSession, "Update Product Status " & lblProductName.Text & " Status : " & chkActiveStatus.Checked.ToString)
        End If
    End Sub

    Private Sub BindList()
        Dim dt As DataTable = MasterDataBL.GetList_Product(txtSearchID.Text, ddlProductTypeSeach.SelectedValue, ddlProductGroupSeach.SelectedValue, txtSearchProductName.Text, ddlstatus.SelectedValue)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Private Sub Bindddl()
        MasterDataBL.GetListDDL_ProductType(ddlProductType)
        MasterDataBL.GetListDDL_ProductGroup(ddlProductGroup)
        MasterDataBL.GetListDDL_Customet(ddlCustomer)
        MasterDataBL.GetListDDL_Supplier(ddlSupplier)

        MasterDataBL.GetListDDL_ProductType(ddlProductTypeSeach)
        MasterDataBL.GetListDDL_ProductGroup(ddlProductGroupSeach)
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblProductID As Label = e.Item.FindControl("lblProductID")
        Dim lblSequenceID As Label = e.Item.FindControl("lblSequenceID")
        Dim lblProductType As Label = e.Item.FindControl("lblProductType")
        Dim lblProductGroup As Label = e.Item.FindControl("lblProductGroup")
        Dim lblProductName As Label = e.Item.FindControl("lblProductName")
        Dim chkActiveStatus As ucSwitchButton = e.Item.FindControl("chkActiveStatus")

        lblSequenceID.Text = e.Item.DataItem("Sequence_ID")
        lblProductID.Text = e.Item.DataItem("Product_ID")
        lblProductType.Text = e.Item.DataItem("Product_Type")
        lblProductGroup.Text = e.Item.DataItem("Product_Group")

        Dim Tname As String = e.Item.DataItem("Product_TName")
        Dim Ename As String = e.Item.DataItem("Product_EName")

        lblProductName.Text = "" & Tname & "/ " & Ename & ""
        chkActiveStatus.Checked = (e.Item.DataItem("active_status") = "Y")
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblSequenceID As Label = e.Item.FindControl("lblSequenceID")
        Dim lblProductName As Label = e.Item.FindControl("lblProductName")

        Select Case e.CommandName
            Case "EDIT"
                ClearForm()
                FillInData(lblSequenceID.Text)

                pnlList.Visible = False
                pnlEdit.Visible = True

                LogFileData.LogTrans(UserSession, "Edit Product  " & lblProductName.Text)
            Case "DELETE"
                Dim ret As LinqDB.ConnectDB.ExecuteDataInfo = MasterDataBL.DeleteProduct(lblSequenceID.Text)
                If ret.IsSuccess = True Then
                    LogFileData.LogTrans(UserSession, "Delete Product  " & lblProductName.Text)

                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Delete Data Fail');", True)
                    LogFileData.LogError(UserSession, ret.ErrorMessage)
                End If
        End Select
    End Sub

    Private Sub FillInData(ID As Long)
        Dim ret As MsProductLinqDB = MasterDataBL.GetProductData(ID)
        If ret.SEQUENCE_ID > 0 Then
            lblSequenceID.Text = ret.SEQUENCE_ID
            txtProductID.Text = ret.PRODUCT_ID
            txtProducTName.Text = ret.PRODUCT_TNAME
            txtProducEName.Text = ret.PRODUCT_ENAME
            txtProductBrand.Text = ret.BRAND_NAME
            ddlProductType.SelectedValue = ret.PRODUCT_TYPE_ID
            ddlProductGroup.SelectedValue = ret.PRODUCT_GROUP_ID
            ddlCustomer.SelectedValue = ret.CUSTOMER_ID
            ddlSupplier.SelectedValue = ret.SUPPLIER_ID
            chkActive.Checked = (ret.ACTIVE_STATUS = "Y")
        End If
        ret = Nothing
    End Sub

    Private Sub ClearForm()
        Bindddl()
        lblSequenceID.Text = "0"
        txtProductID.Text = ""
        txtProducTName.Text = ""
        txtProducEName.Text = ""
        txtProductBrand.Text = ""
        ddlProductType.SelectedValue = 0
        ddlProductGroup.SelectedValue = 0
        ddlCustomer.SelectedValue = 0
        ddlSupplier.SelectedValue = 0
        chkActive.Checked = True
        txtProductID.Enabled = False

        pnlSearch.Visible = False
    End Sub

    Private Sub btnAddProduct_Click(sender As Object, e As EventArgs) Handles btnAddProduct.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Add")
        ClearForm()
        pnlList.Visible = False
        pnlEdit.Visible = True
        txtProductID.Enabled = True
    End Sub

    Private Function ValidateData() As Boolean

        If txtProductID.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุรหัสสินค้า');", True)
            Return False
        End If

        If txtProducTName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุชื่อสินค้าภาษาไทย');", True)
            Return False
        End If

        If ddlProductType.SelectedValue = "0" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), " ThenAlert", "alert('กรุณาเลือกประเภทสินค้า');", True)
            Return False
        End If

        If ddlProductGroup.SelectedValue = "0" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกกลุ่มสินค้า');", True)
            Return False
        End If
        'If ddlCustomer.SelectedValue = "0" Then
        '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกลูกค้า');", True)
        '    Return False
        'End If

        'If ddlSupplier.SelectedValue = "0" Then
        '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกผู้ขาย');", True)
        '    Return False
        'End If

        Return True
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Save Product ")
        If ValidateData() = True Then

            Dim lnq As MsProductLinqDB = MasterDataBL.SaveProduct(UserSession.UserName, lblSequenceID.Text, txtProductID.Text, txtProducTName.Text, txtProducEName.Text, txtProductBrand.Text, ddlProductType.SelectedValue, ddlProductGroup.SelectedValue, ddlCustomer.SelectedValue, ddlSupplier.SelectedValue, chkActive.Checked)
            If lnq.SEQUENCE_ID > 0 Then
                lblSequenceID.Text = lnq.SEQUENCE_ID
                LogFileData.LogTrans(UserSession, "Save Product " & txtProducTName.Text & " / " & txtProducEName.Text & "  สำเร็จ")
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Data Complete');", True)
                pnlEdit.Visible = False
                pnlList.Visible = True
                BindList()
            Else
                LogFileData.LogError(UserSession, "Save Product " & txtProducTName.Text & " / " & txtProducEName.Text & " ไม่สำเร็จ Error :" & lnq.ErrorMessage)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save Fail');", True)
            End If
            lnq = Nothing
            txtProductID.Enabled = False
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        LogFileData.LogTrans(UserSession, "คลิกปุ่ม Cancel")
        ClearForm()
        pnlEdit.Visible = False
        pnlList.Visible = True
        BindList()
    End Sub

    Private Sub ddlCustomer_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCustomer.SelectedIndexChanged
        If ddlCustomer.SelectedValue <> "0" Then
            ddlSupplier.Enabled = False
        Else
            ddlSupplier.Enabled = True
        End If
    End Sub

    Private Sub ddlSupplier_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSupplier.SelectedIndexChanged
        If ddlSupplier.SelectedValue <> "0" Then
            ddlCustomer.Enabled = False
        Else
            ddlCustomer.Enabled = True
        End If
    End Sub

    Private Sub btnOpenSearch_Click(sender As Object, e As EventArgs) Handles btnOpenSearch.Click
        pnlSearch.Visible = True
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click, txtSearchID.TextChanged, txtSearchProductName.TextChanged, ddlProductGroupSeach.SelectedIndexChanged, ddlProductTypeSeach.SelectedIndexChanged, ddlstatus.SelectedIndexChanged
        BindList()
    End Sub
End Class