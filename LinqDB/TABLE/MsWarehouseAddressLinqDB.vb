Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for MS_Warehouse_Address table LinqDB.
    '[Create by  on June, 9 2017]
    Public Class MsWarehouseAddressLinqDB
        Public sub MsWarehouseAddressLinqDB()

        End Sub 
        ' MS_Warehouse_Address
        Const _tableName As String = "MS_Warehouse_Address"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _SEQUENCE_ID As Long = 0
        Dim _WAREHOUSE_ID As String = ""
        Dim _WAREHOUSE_HOUSE_NO As  String  = ""
        Dim _WAREHOUSE_BUILDING As  String  = ""
        Dim _WAREHOUSE_MOO As  String  = ""
        Dim _WAREHOUSE_SOI As  String  = ""
        Dim _WAREHOUSE_ROAD As  String  = ""
        Dim _WAREHOUSE_TUMBON As  String  = ""
        Dim _WAREHOUSE_DISTRICT As  String  = ""
        Dim _WAREHOUSE_PROVINCE As  String  = ""
        Dim _WAREHOUSE_COUNTRY As  String  = ""
        Dim _WAREHOUSE_POSTCODE As  String  = ""
        Dim _WAREHOUSE_BRANCH_NO As  String  = ""
        Dim _WAREHOUSE_TELEPHONE As  String  = ""
        Dim _WAREHOUSE_MOBILE As  String  = ""
        Dim _ACTIVE_STATUS As Char = "Y"
        Dim _CREATED_BY As String = ""
        Dim _CREATED_DATE As DateTime = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)

        'Generate Field Property 
        <Column(Storage:="_SEQUENCE_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property SEQUENCE_ID() As Long
            Get
                Return _SEQUENCE_ID
            End Get
            Set(ByVal value As Long)
               _SEQUENCE_ID = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_ID", DbType:="VarChar(10) NOT NULL ",CanBeNull:=false)>  _
        Public Property WAREHOUSE_ID() As String
            Get
                Return _WAREHOUSE_ID
            End Get
            Set(ByVal value As String)
               _WAREHOUSE_ID = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_HOUSE_NO", DbType:="VarChar(10)")>  _
        Public Property WAREHOUSE_HOUSE_NO() As  String 
            Get
                Return _WAREHOUSE_HOUSE_NO
            End Get
            Set(ByVal value As  String )
               _WAREHOUSE_HOUSE_NO = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_BUILDING", DbType:="VarChar(50)")>  _
        Public Property WAREHOUSE_BUILDING() As  String 
            Get
                Return _WAREHOUSE_BUILDING
            End Get
            Set(ByVal value As  String )
               _WAREHOUSE_BUILDING = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_MOO", DbType:="VarChar(50)")>  _
        Public Property WAREHOUSE_MOO() As  String 
            Get
                Return _WAREHOUSE_MOO
            End Get
            Set(ByVal value As  String )
               _WAREHOUSE_MOO = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_SOI", DbType:="VarChar(50)")>  _
        Public Property WAREHOUSE_SOI() As  String 
            Get
                Return _WAREHOUSE_SOI
            End Get
            Set(ByVal value As  String )
               _WAREHOUSE_SOI = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_ROAD", DbType:="VarChar(50)")>  _
        Public Property WAREHOUSE_ROAD() As  String 
            Get
                Return _WAREHOUSE_ROAD
            End Get
            Set(ByVal value As  String )
               _WAREHOUSE_ROAD = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_TUMBON", DbType:="VarChar(50)")>  _
        Public Property WAREHOUSE_TUMBON() As  String 
            Get
                Return _WAREHOUSE_TUMBON
            End Get
            Set(ByVal value As  String )
               _WAREHOUSE_TUMBON = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_DISTRICT", DbType:="VarChar(50)")>  _
        Public Property WAREHOUSE_DISTRICT() As  String 
            Get
                Return _WAREHOUSE_DISTRICT
            End Get
            Set(ByVal value As  String )
               _WAREHOUSE_DISTRICT = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_PROVINCE", DbType:="VarChar(50)")>  _
        Public Property WAREHOUSE_PROVINCE() As  String 
            Get
                Return _WAREHOUSE_PROVINCE
            End Get
            Set(ByVal value As  String )
               _WAREHOUSE_PROVINCE = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_COUNTRY", DbType:="VarChar(50)")>  _
        Public Property WAREHOUSE_COUNTRY() As  String 
            Get
                Return _WAREHOUSE_COUNTRY
            End Get
            Set(ByVal value As  String )
               _WAREHOUSE_COUNTRY = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_POSTCODE", DbType:="VarChar(50)")>  _
        Public Property WAREHOUSE_POSTCODE() As  String 
            Get
                Return _WAREHOUSE_POSTCODE
            End Get
            Set(ByVal value As  String )
               _WAREHOUSE_POSTCODE = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_BRANCH_NO", DbType:="VarChar(50)")>  _
        Public Property WAREHOUSE_BRANCH_NO() As  String 
            Get
                Return _WAREHOUSE_BRANCH_NO
            End Get
            Set(ByVal value As  String )
               _WAREHOUSE_BRANCH_NO = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_TELEPHONE", DbType:="VarChar(50)")>  _
        Public Property WAREHOUSE_TELEPHONE() As  String 
            Get
                Return _WAREHOUSE_TELEPHONE
            End Get
            Set(ByVal value As  String )
               _WAREHOUSE_TELEPHONE = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_MOBILE", DbType:="VarChar(50)")>  _
        Public Property WAREHOUSE_MOBILE() As  String 
            Get
                Return _WAREHOUSE_MOBILE
            End Get
            Set(ByVal value As  String )
               _WAREHOUSE_MOBILE = value
            End Set
        End Property 
        <Column(Storage:="_ACTIVE_STATUS", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ACTIVE_STATUS() As Char
            Get
                Return _ACTIVE_STATUS
            End Get
            Set(ByVal value As Char)
               _ACTIVE_STATUS = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_BY() As String
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As String)
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_DATE() As DateTime
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As DateTime)
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _SEQUENCE_ID = 0
            _WAREHOUSE_ID = ""
            _WAREHOUSE_HOUSE_NO = ""
            _WAREHOUSE_BUILDING = ""
            _WAREHOUSE_MOO = ""
            _WAREHOUSE_SOI = ""
            _WAREHOUSE_ROAD = ""
            _WAREHOUSE_TUMBON = ""
            _WAREHOUSE_DISTRICT = ""
            _WAREHOUSE_PROVINCE = ""
            _WAREHOUSE_COUNTRY = ""
            _WAREHOUSE_POSTCODE = ""
            _WAREHOUSE_BRANCH_NO = ""
            _WAREHOUSE_TELEPHONE = ""
            _WAREHOUSE_MOBILE = ""
            _ACTIVE_STATUS = "Y"
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into MS_Warehouse_Address table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to MS_Warehouse_Address table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _Sequence_ID > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("SEQUENCE_ID = @_SEQUENCE_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to MS_Warehouse_Address table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from MS_Warehouse_Address table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cSEQUENCE_ID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_SEQUENCE_ID", cSEQUENCE_ID)
                Return doDelete("SEQUENCE_ID = @_SEQUENCE_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of MS_Warehouse_Address by specified SEQUENCE_ID key is retrieved successfully.
        '/// <param name=cSEQUENCE_ID>The SEQUENCE_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cSEQUENCE_ID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_SEQUENCE_ID", cSEQUENCE_ID)
            Return doChkData("SEQUENCE_ID = @_SEQUENCE_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of MS_Warehouse_Address by specified SEQUENCE_ID key is retrieved successfully.
        '/// <param name=cSEQUENCE_ID>The SEQUENCE_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cSEQUENCE_ID As Long, trans As SQLTransaction) As MsWarehouseAddressLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_SEQUENCE_ID", cSEQUENCE_ID)
            Return doGetData("SEQUENCE_ID = @_SEQUENCE_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of MS_Warehouse_Address by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into MS_Warehouse_Address table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _SEQUENCE_ID = dt.Rows(0)("SEQUENCE_ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                        ret.NewID = _SEQUENCE_ID
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to MS_Warehouse_Address table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                            ret.NewID = _SEQUENCE_ID
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from MS_Warehouse_Address table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(19) As SqlParameter
            cmbParam(0) = New SqlParameter("@_SEQUENCE_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _SEQUENCE_ID

            cmbParam(1) = New SqlParameter("@_WAREHOUSE_ID", SqlDbType.VarChar)
            cmbParam(1).Value = _WAREHOUSE_ID.Trim

            cmbParam(2) = New SqlParameter("@_WAREHOUSE_HOUSE_NO", SqlDbType.VarChar)
            If _WAREHOUSE_HOUSE_NO.Trim <> "" Then 
                cmbParam(2).Value = _WAREHOUSE_HOUSE_NO.Trim
            Else
                cmbParam(2).Value = DBNull.value
            End If

            cmbParam(3) = New SqlParameter("@_WAREHOUSE_BUILDING", SqlDbType.VarChar)
            If _WAREHOUSE_BUILDING.Trim <> "" Then 
                cmbParam(3).Value = _WAREHOUSE_BUILDING.Trim
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_WAREHOUSE_MOO", SqlDbType.VarChar)
            If _WAREHOUSE_MOO.Trim <> "" Then 
                cmbParam(4).Value = _WAREHOUSE_MOO.Trim
            Else
                cmbParam(4).Value = DBNull.value
            End If

            cmbParam(5) = New SqlParameter("@_WAREHOUSE_SOI", SqlDbType.VarChar)
            If _WAREHOUSE_SOI.Trim <> "" Then 
                cmbParam(5).Value = _WAREHOUSE_SOI.Trim
            Else
                cmbParam(5).Value = DBNull.value
            End If

            cmbParam(6) = New SqlParameter("@_WAREHOUSE_ROAD", SqlDbType.VarChar)
            If _WAREHOUSE_ROAD.Trim <> "" Then 
                cmbParam(6).Value = _WAREHOUSE_ROAD.Trim
            Else
                cmbParam(6).Value = DBNull.value
            End If

            cmbParam(7) = New SqlParameter("@_WAREHOUSE_TUMBON", SqlDbType.VarChar)
            If _WAREHOUSE_TUMBON.Trim <> "" Then 
                cmbParam(7).Value = _WAREHOUSE_TUMBON.Trim
            Else
                cmbParam(7).Value = DBNull.value
            End If

            cmbParam(8) = New SqlParameter("@_WAREHOUSE_DISTRICT", SqlDbType.VarChar)
            If _WAREHOUSE_DISTRICT.Trim <> "" Then 
                cmbParam(8).Value = _WAREHOUSE_DISTRICT.Trim
            Else
                cmbParam(8).Value = DBNull.value
            End If

            cmbParam(9) = New SqlParameter("@_WAREHOUSE_PROVINCE", SqlDbType.VarChar)
            If _WAREHOUSE_PROVINCE.Trim <> "" Then 
                cmbParam(9).Value = _WAREHOUSE_PROVINCE.Trim
            Else
                cmbParam(9).Value = DBNull.value
            End If

            cmbParam(10) = New SqlParameter("@_WAREHOUSE_COUNTRY", SqlDbType.VarChar)
            If _WAREHOUSE_COUNTRY.Trim <> "" Then 
                cmbParam(10).Value = _WAREHOUSE_COUNTRY.Trim
            Else
                cmbParam(10).Value = DBNull.value
            End If

            cmbParam(11) = New SqlParameter("@_WAREHOUSE_POSTCODE", SqlDbType.VarChar)
            If _WAREHOUSE_POSTCODE.Trim <> "" Then 
                cmbParam(11).Value = _WAREHOUSE_POSTCODE.Trim
            Else
                cmbParam(11).Value = DBNull.value
            End If

            cmbParam(12) = New SqlParameter("@_WAREHOUSE_BRANCH_NO", SqlDbType.VarChar)
            If _WAREHOUSE_BRANCH_NO.Trim <> "" Then 
                cmbParam(12).Value = _WAREHOUSE_BRANCH_NO.Trim
            Else
                cmbParam(12).Value = DBNull.value
            End If

            cmbParam(13) = New SqlParameter("@_WAREHOUSE_TELEPHONE", SqlDbType.VarChar)
            If _WAREHOUSE_TELEPHONE.Trim <> "" Then 
                cmbParam(13).Value = _WAREHOUSE_TELEPHONE.Trim
            Else
                cmbParam(13).Value = DBNull.value
            End If

            cmbParam(14) = New SqlParameter("@_WAREHOUSE_MOBILE", SqlDbType.VarChar)
            If _WAREHOUSE_MOBILE.Trim <> "" Then 
                cmbParam(14).Value = _WAREHOUSE_MOBILE.Trim
            Else
                cmbParam(14).Value = DBNull.value
            End If

            cmbParam(15) = New SqlParameter("@_ACTIVE_STATUS", SqlDbType.Char)
            cmbParam(15).Value = _ACTIVE_STATUS

            cmbParam(16) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            cmbParam(16).Value = _CREATED_BY.Trim

            cmbParam(17) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            cmbParam(17).Value = _CREATED_DATE

            cmbParam(18) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(18).Value = _UPDATED_BY.Trim
            Else
                cmbParam(18).Value = DBNull.value
            End If

            cmbParam(19) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(19).Value = _UPDATED_DATE.Value
            Else
                cmbParam(19).Value = DBNull.value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of MS_Warehouse_Address by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Sequence_ID")) = False Then _Sequence_ID = Convert.ToInt64(Rdr("Sequence_ID"))
                        If Convert.IsDBNull(Rdr("Warehouse_ID")) = False Then _Warehouse_ID = Rdr("Warehouse_ID").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_House_No")) = False Then _Warehouse_House_No = Rdr("Warehouse_House_No").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Building")) = False Then _Warehouse_Building = Rdr("Warehouse_Building").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Moo")) = False Then _Warehouse_Moo = Rdr("Warehouse_Moo").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Soi")) = False Then _Warehouse_Soi = Rdr("Warehouse_Soi").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Road")) = False Then _Warehouse_Road = Rdr("Warehouse_Road").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Tumbon")) = False Then _Warehouse_Tumbon = Rdr("Warehouse_Tumbon").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_District")) = False Then _Warehouse_District = Rdr("Warehouse_District").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Province")) = False Then _Warehouse_Province = Rdr("Warehouse_Province").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Country")) = False Then _Warehouse_Country = Rdr("Warehouse_Country").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Postcode")) = False Then _Warehouse_Postcode = Rdr("Warehouse_Postcode").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Branch_No")) = False Then _Warehouse_Branch_No = Rdr("Warehouse_Branch_No").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Telephone")) = False Then _Warehouse_Telephone = Rdr("Warehouse_Telephone").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Mobile")) = False Then _Warehouse_Mobile = Rdr("Warehouse_Mobile").ToString()
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _Active_Status = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of MS_Warehouse_Address by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As MsWarehouseAddressLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Sequence_ID")) = False Then _Sequence_ID = Convert.ToInt64(Rdr("Sequence_ID"))
                        If Convert.IsDBNull(Rdr("Warehouse_ID")) = False Then _Warehouse_ID = Rdr("Warehouse_ID").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_House_No")) = False Then _Warehouse_House_No = Rdr("Warehouse_House_No").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Building")) = False Then _Warehouse_Building = Rdr("Warehouse_Building").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Moo")) = False Then _Warehouse_Moo = Rdr("Warehouse_Moo").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Soi")) = False Then _Warehouse_Soi = Rdr("Warehouse_Soi").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Road")) = False Then _Warehouse_Road = Rdr("Warehouse_Road").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Tumbon")) = False Then _Warehouse_Tumbon = Rdr("Warehouse_Tumbon").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_District")) = False Then _Warehouse_District = Rdr("Warehouse_District").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Province")) = False Then _Warehouse_Province = Rdr("Warehouse_Province").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Country")) = False Then _Warehouse_Country = Rdr("Warehouse_Country").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Postcode")) = False Then _Warehouse_Postcode = Rdr("Warehouse_Postcode").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Branch_No")) = False Then _Warehouse_Branch_No = Rdr("Warehouse_Branch_No").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Telephone")) = False Then _Warehouse_Telephone = Rdr("Warehouse_Telephone").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Mobile")) = False Then _Warehouse_Mobile = Rdr("Warehouse_Mobile").ToString()
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _Active_Status = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table MS_Warehouse_Address
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (WAREHOUSE_ID, WAREHOUSE_HOUSE_NO, WAREHOUSE_BUILDING, WAREHOUSE_MOO, WAREHOUSE_SOI, WAREHOUSE_ROAD, WAREHOUSE_TUMBON, WAREHOUSE_DISTRICT, WAREHOUSE_PROVINCE, WAREHOUSE_COUNTRY, WAREHOUSE_POSTCODE, WAREHOUSE_BRANCH_NO, WAREHOUSE_TELEPHONE, WAREHOUSE_MOBILE, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE)"
                Sql += " OUTPUT INSERTED.SEQUENCE_ID, INSERTED.WAREHOUSE_ID, INSERTED.WAREHOUSE_HOUSE_NO, INSERTED.WAREHOUSE_BUILDING, INSERTED.WAREHOUSE_MOO, INSERTED.WAREHOUSE_SOI, INSERTED.WAREHOUSE_ROAD, INSERTED.WAREHOUSE_TUMBON, INSERTED.WAREHOUSE_DISTRICT, INSERTED.WAREHOUSE_PROVINCE, INSERTED.WAREHOUSE_COUNTRY, INSERTED.WAREHOUSE_POSTCODE, INSERTED.WAREHOUSE_BRANCH_NO, INSERTED.WAREHOUSE_TELEPHONE, INSERTED.WAREHOUSE_MOBILE, INSERTED.ACTIVE_STATUS, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE"
                Sql += " VALUES("
                sql += "@_WAREHOUSE_ID" & ", "
                sql += "@_WAREHOUSE_HOUSE_NO" & ", "
                sql += "@_WAREHOUSE_BUILDING" & ", "
                sql += "@_WAREHOUSE_MOO" & ", "
                sql += "@_WAREHOUSE_SOI" & ", "
                sql += "@_WAREHOUSE_ROAD" & ", "
                sql += "@_WAREHOUSE_TUMBON" & ", "
                sql += "@_WAREHOUSE_DISTRICT" & ", "
                sql += "@_WAREHOUSE_PROVINCE" & ", "
                sql += "@_WAREHOUSE_COUNTRY" & ", "
                sql += "@_WAREHOUSE_POSTCODE" & ", "
                sql += "@_WAREHOUSE_BRANCH_NO" & ", "
                sql += "@_WAREHOUSE_TELEPHONE" & ", "
                sql += "@_WAREHOUSE_MOBILE" & ", "
                sql += "@_ACTIVE_STATUS" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE" & ", "
                sql += "@_UPDATED_BY" & ", "
                sql += "@_UPDATED_DATE"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table MS_Warehouse_Address
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "WAREHOUSE_ID = " & "@_WAREHOUSE_ID" & ", "
                Sql += "WAREHOUSE_HOUSE_NO = " & "@_WAREHOUSE_HOUSE_NO" & ", "
                Sql += "WAREHOUSE_BUILDING = " & "@_WAREHOUSE_BUILDING" & ", "
                Sql += "WAREHOUSE_MOO = " & "@_WAREHOUSE_MOO" & ", "
                Sql += "WAREHOUSE_SOI = " & "@_WAREHOUSE_SOI" & ", "
                Sql += "WAREHOUSE_ROAD = " & "@_WAREHOUSE_ROAD" & ", "
                Sql += "WAREHOUSE_TUMBON = " & "@_WAREHOUSE_TUMBON" & ", "
                Sql += "WAREHOUSE_DISTRICT = " & "@_WAREHOUSE_DISTRICT" & ", "
                Sql += "WAREHOUSE_PROVINCE = " & "@_WAREHOUSE_PROVINCE" & ", "
                Sql += "WAREHOUSE_COUNTRY = " & "@_WAREHOUSE_COUNTRY" & ", "
                Sql += "WAREHOUSE_POSTCODE = " & "@_WAREHOUSE_POSTCODE" & ", "
                Sql += "WAREHOUSE_BRANCH_NO = " & "@_WAREHOUSE_BRANCH_NO" & ", "
                Sql += "WAREHOUSE_TELEPHONE = " & "@_WAREHOUSE_TELEPHONE" & ", "
                Sql += "WAREHOUSE_MOBILE = " & "@_WAREHOUSE_MOBILE" & ", "
                Sql += "ACTIVE_STATUS = " & "@_ACTIVE_STATUS" & ", "
                Sql += "CREATED_BY = " & "@_CREATED_BY" & ", "
                Sql += "CREATED_DATE = " & "@_CREATED_DATE" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table MS_Warehouse_Address
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table MS_Warehouse_Address
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT SEQUENCE_ID, WAREHOUSE_ID, WAREHOUSE_HOUSE_NO, WAREHOUSE_BUILDING, WAREHOUSE_MOO, WAREHOUSE_SOI, WAREHOUSE_ROAD, WAREHOUSE_TUMBON, WAREHOUSE_DISTRICT, WAREHOUSE_PROVINCE, WAREHOUSE_COUNTRY, WAREHOUSE_POSTCODE, WAREHOUSE_BRANCH_NO, WAREHOUSE_TELEPHONE, WAREHOUSE_MOBILE, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
