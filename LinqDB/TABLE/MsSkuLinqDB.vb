Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for MS_SKU table LinqDB.
    '[Create by  on July, 3 2017]
    Public Class MsSkuLinqDB
        Public Sub MsSkuLinqDB()

        End Sub
        ' MS_SKU
        Const _tableName As String = "MS_SKU"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _SEQUENCE_ID As Long = 0
        Dim _SKU_ID As String = ""
        Dim _SKU_DESCRIPTION As String = ""
        Dim _SKU_GROUP_ID As String = ""
        Dim _SIZE As String = ""
        Dim _COLOR As String = ""
        Dim _MODEL As String = ""
        Dim _BRAND As String = ""
        Dim _UNIT_ID As System.Nullable(Of Long)
        Dim _WEIGHT_PER_UNIT As System.Nullable(Of Long)
        Dim _BARCODE1 As String = ""
        Dim _BARCODE2 As String = ""
        Dim _PRODUCT_ID As String = ""
        Dim _PRODUCT_TYPE_ID As System.Nullable(Of Long)
        Dim _VOLUME_PER_UNIT As String = ""
        Dim _VOLUME_WIDTH As String = ""
        Dim _VOLUME_LENGTH As String = ""
        Dim _VOLUME_HIGHT As String = ""
        Dim _AGE_PRODUCT_DAY As String = ""
        Dim _AGE_PRODUCT_MONTH As String = ""
        Dim _AGE_PRODUCT_YEAR As String = ""
        Dim _PRODUCTION_DATE As System.Nullable(Of Date) = New DateTime(1, 1, 1)
        Dim _EXPIRE_DATE As System.Nullable(Of Date) = New DateTime(1, 1, 1)
        Dim _PRODUCT_PICTURE As String = ""
        Dim _PRODUCT_GROUP_ID As String = ""
        Dim _BUY_PRICE As System.Nullable(Of Long)
        Dim _SELL_PRICE As System.Nullable(Of Long)
        Dim _OTHER_PRICE As System.Nullable(Of Long)
        Dim _MINIMUM_AMOUNT As String = ""
        Dim _MAXIMUM_AMOUNT As String = ""
        Dim _MINIMUM_WEIGHT As String = ""
        Dim _MAXIMUM_WEIGHT As String = ""
        Dim _MINIMUM_VOLUME As String = ""
        Dim _MAXIMUM_VOLUME As String = ""
        Dim _PALLET_TYPE_ID As String = ""
        Dim _MAXIMUM_PER_PALLET As String = ""
        Dim _PICK_ID As String = ""
        Dim _AMOUNT_PER_UNIT As System.Nullable(Of Long)
        Dim _WEIGHT As String = ""
        Dim _ACTIVE_STATUS As Char = "Y"
        Dim _CREATED_BY As String = ""
        Dim _CREATED_DATE As DateTime = New DateTime(1, 1, 1)
        Dim _UPDATED_BY As String = ""
        Dim _UPDATED_DATE As System.Nullable(Of DateTime) = New DateTime(1, 1, 1)

        'Generate Field Property 
        <Column(Storage:="_SEQUENCE_ID", DbType:="BigInt NOT NULL ", CanBeNull:=False)>
        Public Property SEQUENCE_ID() As Long
            Get
                Return _SEQUENCE_ID
            End Get
            Set(ByVal value As Long)
                _SEQUENCE_ID = value
            End Set
        End Property
        <Column(Storage:="_SKU_ID", DbType:="VarChar(100) NOT NULL ", CanBeNull:=False)>
        Public Property SKU_ID() As String
            Get
                Return _SKU_ID
            End Get
            Set(ByVal value As String)
                _SKU_ID = value
            End Set
        End Property
        <Column(Storage:="_SKU_DESCRIPTION", DbType:="VarChar(300)")>
        Public Property SKU_DESCRIPTION() As String
            Get
                Return _SKU_DESCRIPTION
            End Get
            Set(ByVal value As String)
                _SKU_DESCRIPTION = value
            End Set
        End Property
        <Column(Storage:="_SKU_GROUP_ID", DbType:="VarChar(100)")>
        Public Property SKU_GROUP_ID() As String
            Get
                Return _SKU_GROUP_ID
            End Get
            Set(ByVal value As String)
                _SKU_GROUP_ID = value
            End Set
        End Property
        <Column(Storage:="_SIZE", DbType:="VarChar(50)")>
        Public Property SIZE() As String
            Get
                Return _SIZE
            End Get
            Set(ByVal value As String)
                _SIZE = value
            End Set
        End Property
        <Column(Storage:="_COLOR", DbType:="VarChar(50)")>
        Public Property COLOR() As String
            Get
                Return _COLOR
            End Get
            Set(ByVal value As String)
                _COLOR = value
            End Set
        End Property
        <Column(Storage:="_MODEL", DbType:="VarChar(100)")>
        Public Property MODEL() As String
            Get
                Return _MODEL
            End Get
            Set(ByVal value As String)
                _MODEL = value
            End Set
        End Property
        <Column(Storage:="_BRAND", DbType:="VarChar(100)")>
        Public Property BRAND() As String
            Get
                Return _BRAND
            End Get
            Set(ByVal value As String)
                _BRAND = value
            End Set
        End Property
        <Column(Storage:="_UNIT_ID", DbType:="BigInt")>
        Public Property UNIT_ID() As System.Nullable(Of Long)
            Get
                Return _UNIT_ID
            End Get
            Set(ByVal value As System.Nullable(Of Long))
                _UNIT_ID = value
            End Set
        End Property
        <Column(Storage:="_WEIGHT_PER_UNIT", DbType:="Int")>
        Public Property WEIGHT_PER_UNIT() As System.Nullable(Of Long)
            Get
                Return _WEIGHT_PER_UNIT
            End Get
            Set(ByVal value As System.Nullable(Of Long))
                _WEIGHT_PER_UNIT = value
            End Set
        End Property
        <Column(Storage:="_BARCODE1", DbType:="VarChar(100)")>
        Public Property BARCODE1() As String
            Get
                Return _BARCODE1
            End Get
            Set(ByVal value As String)
                _BARCODE1 = value
            End Set
        End Property
        <Column(Storage:="_BARCODE2", DbType:="VarChar(100)")>
        Public Property BARCODE2() As String
            Get
                Return _BARCODE2
            End Get
            Set(ByVal value As String)
                _BARCODE2 = value
            End Set
        End Property
        <Column(Storage:="_PRODUCT_ID", DbType:="VarChar(100)")>
        Public Property PRODUCT_ID() As String
            Get
                Return _PRODUCT_ID
            End Get
            Set(ByVal value As String)
                _PRODUCT_ID = value
            End Set
        End Property
        <Column(Storage:="_PRODUCT_TYPE_ID", DbType:="BigInt")>
        Public Property PRODUCT_TYPE_ID() As System.Nullable(Of Long)
            Get
                Return _PRODUCT_TYPE_ID
            End Get
            Set(ByVal value As System.Nullable(Of Long))
                _PRODUCT_TYPE_ID = value
            End Set
        End Property
        <Column(Storage:="_VOLUME_PER_UNIT", DbType:="VarChar(50)")>
        Public Property VOLUME_PER_UNIT() As String
            Get
                Return _VOLUME_PER_UNIT
            End Get
            Set(ByVal value As String)
                _VOLUME_PER_UNIT = value
            End Set
        End Property
        <Column(Storage:="_VOLUME_WIDTH", DbType:="VarChar(50)")>
        Public Property VOLUME_WIDTH() As String
            Get
                Return _VOLUME_WIDTH
            End Get
            Set(ByVal value As String)
                _VOLUME_WIDTH = value
            End Set
        End Property
        <Column(Storage:="_VOLUME_LENGTH", DbType:="VarChar(50)")>
        Public Property VOLUME_LENGTH() As String
            Get
                Return _VOLUME_LENGTH
            End Get
            Set(ByVal value As String)
                _VOLUME_LENGTH = value
            End Set
        End Property
        <Column(Storage:="_VOLUME_HIGHT", DbType:="VarChar(50)")>
        Public Property VOLUME_HIGHT() As String
            Get
                Return _VOLUME_HIGHT
            End Get
            Set(ByVal value As String)
                _VOLUME_HIGHT = value
            End Set
        End Property
        <Column(Storage:="_AGE_PRODUCT_DAY", DbType:="VarChar(50)")>
        Public Property AGE_PRODUCT_DAY() As String
            Get
                Return _AGE_PRODUCT_DAY
            End Get
            Set(ByVal value As String)
                _AGE_PRODUCT_DAY = value
            End Set
        End Property
        <Column(Storage:="_AGE_PRODUCT_MONTH", DbType:="VarChar(50)")>
        Public Property AGE_PRODUCT_MONTH() As String
            Get
                Return _AGE_PRODUCT_MONTH
            End Get
            Set(ByVal value As String)
                _AGE_PRODUCT_MONTH = value
            End Set
        End Property
        <Column(Storage:="_AGE_PRODUCT_YEAR", DbType:="VarChar(50)")>
        Public Property AGE_PRODUCT_YEAR() As String
            Get
                Return _AGE_PRODUCT_YEAR
            End Get
            Set(ByVal value As String)
                _AGE_PRODUCT_YEAR = value
            End Set
        End Property
        <Column(Storage:="_PRODUCTION_DATE", DbType:="Date")>
        Public Property PRODUCTION_DATE() As System.Nullable(Of Date)
            Get
                Return _PRODUCTION_DATE
            End Get
            Set(ByVal value As System.Nullable(Of Date))
                _PRODUCTION_DATE = value
            End Set
        End Property
        <Column(Storage:="_EXPIRE_DATE", DbType:="Date")>
        Public Property EXPIRE_DATE() As System.Nullable(Of Date)
            Get
                Return _EXPIRE_DATE
            End Get
            Set(ByVal value As System.Nullable(Of Date))
                _EXPIRE_DATE = value
            End Set
        End Property
        <Column(Storage:="_PRODUCT_PICTURE", DbType:="VarChar(500)")>
        Public Property PRODUCT_PICTURE() As String
            Get
                Return _PRODUCT_PICTURE
            End Get
            Set(ByVal value As String)
                _PRODUCT_PICTURE = value
            End Set
        End Property
        <Column(Storage:="_PRODUCT_GROUP_ID", DbType:="VarChar(100)")>
        Public Property PRODUCT_GROUP_ID() As String
            Get
                Return _PRODUCT_GROUP_ID
            End Get
            Set(ByVal value As String)
                _PRODUCT_GROUP_ID = value
            End Set
        End Property
        <Column(Storage:="_BUY_PRICE", DbType:="Int")>
        Public Property BUY_PRICE() As System.Nullable(Of Long)
            Get
                Return _BUY_PRICE
            End Get
            Set(ByVal value As System.Nullable(Of Long))
                _BUY_PRICE = value
            End Set
        End Property
        <Column(Storage:="_SELL_PRICE", DbType:="Int")>
        Public Property SELL_PRICE() As System.Nullable(Of Long)
            Get
                Return _SELL_PRICE
            End Get
            Set(ByVal value As System.Nullable(Of Long))
                _SELL_PRICE = value
            End Set
        End Property
        <Column(Storage:="_OTHER_PRICE", DbType:="Int")>
        Public Property OTHER_PRICE() As System.Nullable(Of Long)
            Get
                Return _OTHER_PRICE
            End Get
            Set(ByVal value As System.Nullable(Of Long))
                _OTHER_PRICE = value
            End Set
        End Property
        <Column(Storage:="_MINIMUM_AMOUNT", DbType:="VarChar(50)")>
        Public Property MINIMUM_AMOUNT() As String
            Get
                Return _MINIMUM_AMOUNT
            End Get
            Set(ByVal value As String)
                _MINIMUM_AMOUNT = value
            End Set
        End Property
        <Column(Storage:="_MAXIMUM_AMOUNT", DbType:="VarChar(50)")>
        Public Property MAXIMUM_AMOUNT() As String
            Get
                Return _MAXIMUM_AMOUNT
            End Get
            Set(ByVal value As String)
                _MAXIMUM_AMOUNT = value
            End Set
        End Property
        <Column(Storage:="_MINIMUM_WEIGHT", DbType:="VarChar(50)")>
        Public Property MINIMUM_WEIGHT() As String
            Get
                Return _MINIMUM_WEIGHT
            End Get
            Set(ByVal value As String)
                _MINIMUM_WEIGHT = value
            End Set
        End Property
        <Column(Storage:="_MAXIMUM_WEIGHT", DbType:="VarChar(50)")>
        Public Property MAXIMUM_WEIGHT() As String
            Get
                Return _MAXIMUM_WEIGHT
            End Get
            Set(ByVal value As String)
                _MAXIMUM_WEIGHT = value
            End Set
        End Property
        <Column(Storage:="_MINIMUM_VOLUME", DbType:="VarChar(50)")>
        Public Property MINIMUM_VOLUME() As String
            Get
                Return _MINIMUM_VOLUME
            End Get
            Set(ByVal value As String)
                _MINIMUM_VOLUME = value
            End Set
        End Property
        <Column(Storage:="_MAXIMUM_VOLUME", DbType:="VarChar(50)")>
        Public Property MAXIMUM_VOLUME() As String
            Get
                Return _MAXIMUM_VOLUME
            End Get
            Set(ByVal value As String)
                _MAXIMUM_VOLUME = value
            End Set
        End Property
        <Column(Storage:="_PALLET_TYPE_ID", DbType:="VarChar(50)")>
        Public Property PALLET_TYPE_ID() As String
            Get
                Return _PALLET_TYPE_ID
            End Get
            Set(ByVal value As String)
                _PALLET_TYPE_ID = value
            End Set
        End Property
        <Column(Storage:="_MAXIMUM_PER_PALLET", DbType:="VarChar(50)")>
        Public Property MAXIMUM_PER_PALLET() As String
            Get
                Return _MAXIMUM_PER_PALLET
            End Get
            Set(ByVal value As String)
                _MAXIMUM_PER_PALLET = value
            End Set
        End Property
        <Column(Storage:="_PICK_ID", DbType:="VarChar(10)")>
        Public Property PICK_ID() As String
            Get
                Return _PICK_ID
            End Get
            Set(ByVal value As String)
                _PICK_ID = value
            End Set
        End Property
        <Column(Storage:="_AMOUNT_PER_UNIT", DbType:="Int")>
        Public Property AMOUNT_PER_UNIT() As System.Nullable(Of Long)
            Get
                Return _AMOUNT_PER_UNIT
            End Get
            Set(ByVal value As System.Nullable(Of Long))
                _AMOUNT_PER_UNIT = value
            End Set
        End Property
        <Column(Storage:="_WEIGHT", DbType:="VarChar(50)")>
        Public Property WEIGHT() As String
            Get
                Return _WEIGHT
            End Get
            Set(ByVal value As String)
                _WEIGHT = value
            End Set
        End Property
        <Column(Storage:="_ACTIVE_STATUS", DbType:="Char(1) NOT NULL ", CanBeNull:=False)>
        Public Property ACTIVE_STATUS() As Char
            Get
                Return _ACTIVE_STATUS
            End Get
            Set(ByVal value As Char)
                _ACTIVE_STATUS = value
            End Set
        End Property
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100) NOT NULL ", CanBeNull:=False)>
        Public Property CREATED_BY() As String
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As String)
                _CREATED_BY = value
            End Set
        End Property
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime NOT NULL ", CanBeNull:=False)>
        Public Property CREATED_DATE() As DateTime
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As DateTime)
                _CREATED_DATE = value
            End Set
        End Property
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>
        Public Property UPDATED_BY() As String
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As String)
                _UPDATED_BY = value
            End Set
        End Property
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>
        Public Property UPDATED_DATE() As System.Nullable(Of DateTime)
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As System.Nullable(Of DateTime))
                _UPDATED_DATE = value
            End Set
        End Property


        'Clear All Data
        Private Sub ClearData()
            _SEQUENCE_ID = 0
            _SKU_ID = ""
            _SKU_DESCRIPTION = ""
            _SKU_GROUP_ID = ""
            _SIZE = ""
            _COLOR = ""
            _MODEL = ""
            _BRAND = ""
            _UNIT_ID = Nothing
            _WEIGHT_PER_UNIT = Nothing
            _BARCODE1 = ""
            _BARCODE2 = ""
            _PRODUCT_ID = ""
            _PRODUCT_TYPE_ID = Nothing
            _VOLUME_PER_UNIT = ""
            _VOLUME_WIDTH = ""
            _VOLUME_LENGTH = ""
            _VOLUME_HIGHT = ""
            _AGE_PRODUCT_DAY = ""
            _AGE_PRODUCT_MONTH = ""
            _AGE_PRODUCT_YEAR = ""
            _PRODUCTION_DATE = New DateTime(1, 1, 1)
            _EXPIRE_DATE = New DateTime(1, 1, 1)
            _PRODUCT_PICTURE = ""
            _PRODUCT_GROUP_ID = ""
            _BUY_PRICE = Nothing
            _SELL_PRICE = Nothing
            _OTHER_PRICE = Nothing
            _MINIMUM_AMOUNT = ""
            _MAXIMUM_AMOUNT = ""
            _MINIMUM_WEIGHT = ""
            _MAXIMUM_WEIGHT = ""
            _MINIMUM_VOLUME = ""
            _MAXIMUM_VOLUME = ""
            _PALLET_TYPE_ID = ""
            _MAXIMUM_PER_PALLET = ""
            _PICK_ID = ""
            _AMOUNT_PER_UNIT = Nothing
            _WEIGHT = ""
            _ACTIVE_STATUS = "Y"
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1, 1, 1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1, 1, 1)
        End Sub

        'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SqlTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIf(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SqlTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into MS_SKU table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String, trans As SqlTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then
                _CREATED_BY = CreatedBy
                _CREATED_DATE = DateTime.Now
                Return doInsert(trans)
            Else
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If
        End Function


        '/// Returns an indication whether the current data is updated to MS_SKU table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String, trans As SqlTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then
                If _SEQUENCE_ID > 0 Then
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("SEQUENCE_ID = @_SEQUENCE_ID", trans)
                Else
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If
            Else
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If
        End Function


        '/// Returns an indication whether the current data is updated to MS_SKU table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SqlTransaction, cmbParm() As SqlParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If
        End Function


        '/// Returns an indication whether the current data is deleted from MS_SKU table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cSEQUENCE_ID As Long, trans As SqlTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then
                Dim p(1) As SqlParameter
                p(0) = DB.SetBigInt("@_SEQUENCE_ID", cSEQUENCE_ID)
                Return doDelete("SEQUENCE_ID = @_SEQUENCE_ID", trans, p)
            Else
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If
        End Function


        '/// Returns an indication whether the record of MS_SKU by specified SEQUENCE_ID key is retrieved successfully.
        '/// <param name=cSEQUENCE_ID>The SEQUENCE_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cSEQUENCE_ID As Long, trans As SqlTransaction) As Boolean
            Dim p(1) As SqlParameter
            p(0) = DB.SetBigInt("@_SEQUENCE_ID", cSEQUENCE_ID)
            Return doChkData("SEQUENCE_ID = @_SEQUENCE_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of MS_SKU by specified SEQUENCE_ID key is retrieved successfully.
        '/// <param name=cSEQUENCE_ID>The SEQUENCE_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cSEQUENCE_ID As Long, trans As SqlTransaction) As MsSkuLinqDB
            Dim p(1) As SqlParameter
            p(0) = DB.SetBigInt("@_SEQUENCE_ID", cSEQUENCE_ID)
            Return doGetData("SEQUENCE_ID = @_SEQUENCE_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of MS_SKU by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SqlTransaction, cmdPara() As SqlParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into MS_SKU table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SqlTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt As DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _SEQUENCE_ID = dt.Rows(0)("SEQUENCE_ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                        ret.NewID = _SEQUENCE_ID
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = False
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to MS_SKU table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SqlTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> "" Then

                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                            ret.NewID = _SEQUENCE_ID
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString()
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 & ex.ToString()
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from MS_SKU table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SqlTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            Dim sql As String = SqlDelete & tmpWhere
            If whText.Trim() <> "" Then

                Try
                    ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                    If ret.IsSuccess = False Then
                        _error = MessageResources.MSGED001
                    Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                    End If
                Catch ex As ApplicationException
                    _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                    ret.IsSuccess = False
                    ret.ErrorMessage = _error
                    ret.SqlStatement = sql
                Catch ex As Exception
                    _error = " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                    ret.IsSuccess = False
                    ret.ErrorMessage = _error
                    ret.SqlStatement = sql
                End Try
            Else
                _error = MessageResources.MSGED003 & "### SQL: " & sql
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                ret.SqlStatement = sql
            End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(43) As SqlParameter
            cmbParam(0) = New SqlParameter("@_SEQUENCE_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _SEQUENCE_ID

            cmbParam(1) = New SqlParameter("@_SKU_ID", SqlDbType.VarChar)
            cmbParam(1).Value = _SKU_ID.Trim

            cmbParam(2) = New SqlParameter("@_SKU_DESCRIPTION", SqlDbType.VarChar)
            If _SKU_DESCRIPTION.Trim <> "" Then
                cmbParam(2).Value = _SKU_DESCRIPTION.Trim
            Else
                cmbParam(2).Value = DBNull.Value
            End If

            cmbParam(3) = New SqlParameter("@_SKU_GROUP_ID", SqlDbType.VarChar)
            If _SKU_GROUP_ID.Trim <> "" Then
                cmbParam(3).Value = _SKU_GROUP_ID.Trim
            Else
                cmbParam(3).Value = DBNull.Value
            End If

            cmbParam(4) = New SqlParameter("@_SIZE", SqlDbType.VarChar)
            If _SIZE.Trim <> "" Then
                cmbParam(4).Value = _SIZE.Trim
            Else
                cmbParam(4).Value = DBNull.Value
            End If

            cmbParam(5) = New SqlParameter("@_COLOR", SqlDbType.VarChar)
            If _COLOR.Trim <> "" Then
                cmbParam(5).Value = _COLOR.Trim
            Else
                cmbParam(5).Value = DBNull.Value
            End If

            cmbParam(6) = New SqlParameter("@_MODEL", SqlDbType.VarChar)
            If _MODEL.Trim <> "" Then
                cmbParam(6).Value = _MODEL.Trim
            Else
                cmbParam(6).Value = DBNull.Value
            End If

            cmbParam(7) = New SqlParameter("@_BRAND", SqlDbType.VarChar)
            If _BRAND.Trim <> "" Then
                cmbParam(7).Value = _BRAND.Trim
            Else
                cmbParam(7).Value = DBNull.Value
            End If

            cmbParam(8) = New SqlParameter("@_UNIT_ID", SqlDbType.BigInt)
            If _UNIT_ID IsNot Nothing Then
                cmbParam(8).Value = _UNIT_ID.Value
            Else
                cmbParam(8).Value = DBNull.Value
            End If

            cmbParam(9) = New SqlParameter("@_WEIGHT_PER_UNIT", SqlDbType.Int)
            If _WEIGHT_PER_UNIT IsNot Nothing Then
                cmbParam(9).Value = _WEIGHT_PER_UNIT.Value
            Else
                cmbParam(9).Value = DBNull.Value
            End If

            cmbParam(10) = New SqlParameter("@_BARCODE1", SqlDbType.VarChar)
            If _BARCODE1.Trim <> "" Then
                cmbParam(10).Value = _BARCODE1.Trim
            Else
                cmbParam(10).Value = DBNull.Value
            End If

            cmbParam(11) = New SqlParameter("@_BARCODE2", SqlDbType.VarChar)
            If _BARCODE2.Trim <> "" Then
                cmbParam(11).Value = _BARCODE2.Trim
            Else
                cmbParam(11).Value = DBNull.Value
            End If

            cmbParam(12) = New SqlParameter("@_PRODUCT_ID", SqlDbType.VarChar)
            If _PRODUCT_ID.Trim <> "" Then
                cmbParam(12).Value = _PRODUCT_ID.Trim
            Else
                cmbParam(12).Value = DBNull.Value
            End If

            cmbParam(13) = New SqlParameter("@_PRODUCT_TYPE_ID", SqlDbType.BigInt)
            If _PRODUCT_TYPE_ID IsNot Nothing Then
                cmbParam(13).Value = _PRODUCT_TYPE_ID.Value
            Else
                cmbParam(13).Value = DBNull.Value
            End If

            cmbParam(14) = New SqlParameter("@_VOLUME_PER_UNIT", SqlDbType.VarChar)
            If _VOLUME_PER_UNIT.Trim <> "" Then
                cmbParam(14).Value = _VOLUME_PER_UNIT.Trim
            Else
                cmbParam(14).Value = DBNull.Value
            End If

            cmbParam(15) = New SqlParameter("@_VOLUME_WIDTH", SqlDbType.VarChar)
            If _VOLUME_WIDTH.Trim <> "" Then
                cmbParam(15).Value = _VOLUME_WIDTH.Trim
            Else
                cmbParam(15).Value = DBNull.Value
            End If

            cmbParam(16) = New SqlParameter("@_VOLUME_LENGTH", SqlDbType.VarChar)
            If _VOLUME_LENGTH.Trim <> "" Then
                cmbParam(16).Value = _VOLUME_LENGTH.Trim
            Else
                cmbParam(16).Value = DBNull.Value
            End If

            cmbParam(17) = New SqlParameter("@_VOLUME_HIGHT", SqlDbType.VarChar)
            If _VOLUME_HIGHT.Trim <> "" Then
                cmbParam(17).Value = _VOLUME_HIGHT.Trim
            Else
                cmbParam(17).Value = DBNull.Value
            End If

            cmbParam(18) = New SqlParameter("@_AGE_PRODUCT_DAY", SqlDbType.VarChar)
            If _AGE_PRODUCT_DAY.Trim <> "" Then
                cmbParam(18).Value = _AGE_PRODUCT_DAY.Trim
            Else
                cmbParam(18).Value = DBNull.Value
            End If

            cmbParam(19) = New SqlParameter("@_AGE_PRODUCT_MONTH", SqlDbType.VarChar)
            If _AGE_PRODUCT_MONTH.Trim <> "" Then
                cmbParam(19).Value = _AGE_PRODUCT_MONTH.Trim
            Else
                cmbParam(19).Value = DBNull.Value
            End If

            cmbParam(20) = New SqlParameter("@_AGE_PRODUCT_YEAR", SqlDbType.VarChar)
            If _AGE_PRODUCT_YEAR.Trim <> "" Then
                cmbParam(20).Value = _AGE_PRODUCT_YEAR.Trim
            Else
                cmbParam(20).Value = DBNull.Value
            End If

            cmbParam(21) = New SqlParameter("@_PRODUCTION_DATE", SqlDbType.Date)
            If _PRODUCTION_DATE.Value.Year > 1 Then
                cmbParam(21).Value = _PRODUCTION_DATE.Value
            Else
                cmbParam(21).Value = DBNull.Value
            End If

            cmbParam(22) = New SqlParameter("@_EXPIRE_DATE", SqlDbType.Date)
            If _EXPIRE_DATE.Value.Year > 1 Then
                cmbParam(22).Value = _EXPIRE_DATE.Value
            Else
                cmbParam(22).Value = DBNull.Value
            End If

            cmbParam(23) = New SqlParameter("@_PRODUCT_PICTURE", SqlDbType.VarChar)
            If _PRODUCT_PICTURE.Trim <> "" Then
                cmbParam(23).Value = _PRODUCT_PICTURE.Trim
            Else
                cmbParam(23).Value = DBNull.Value
            End If

            cmbParam(24) = New SqlParameter("@_PRODUCT_GROUP_ID", SqlDbType.VarChar)
            If _PRODUCT_GROUP_ID.Trim <> "" Then
                cmbParam(24).Value = _PRODUCT_GROUP_ID.Trim
            Else
                cmbParam(24).Value = DBNull.Value
            End If

            cmbParam(25) = New SqlParameter("@_BUY_PRICE", SqlDbType.Int)
            If _BUY_PRICE IsNot Nothing Then
                cmbParam(25).Value = _BUY_PRICE.Value
            Else
                cmbParam(25).Value = DBNull.Value
            End If

            cmbParam(26) = New SqlParameter("@_SELL_PRICE", SqlDbType.Int)
            If _SELL_PRICE IsNot Nothing Then
                cmbParam(26).Value = _SELL_PRICE.Value
            Else
                cmbParam(26).Value = DBNull.Value
            End If

            cmbParam(27) = New SqlParameter("@_OTHER_PRICE", SqlDbType.Int)
            If _OTHER_PRICE IsNot Nothing Then
                cmbParam(27).Value = _OTHER_PRICE.Value
            Else
                cmbParam(27).Value = DBNull.Value
            End If

            cmbParam(28) = New SqlParameter("@_MINIMUM_AMOUNT", SqlDbType.VarChar)
            If _MINIMUM_AMOUNT.Trim <> "" Then
                cmbParam(28).Value = _MINIMUM_AMOUNT.Trim
            Else
                cmbParam(28).Value = DBNull.Value
            End If

            cmbParam(29) = New SqlParameter("@_MAXIMUM_AMOUNT", SqlDbType.VarChar)
            If _MAXIMUM_AMOUNT.Trim <> "" Then
                cmbParam(29).Value = _MAXIMUM_AMOUNT.Trim
            Else
                cmbParam(29).Value = DBNull.Value
            End If

            cmbParam(30) = New SqlParameter("@_MINIMUM_WEIGHT", SqlDbType.VarChar)
            If _MINIMUM_WEIGHT.Trim <> "" Then
                cmbParam(30).Value = _MINIMUM_WEIGHT.Trim
            Else
                cmbParam(30).Value = DBNull.Value
            End If

            cmbParam(31) = New SqlParameter("@_MAXIMUM_WEIGHT", SqlDbType.VarChar)
            If _MAXIMUM_WEIGHT.Trim <> "" Then
                cmbParam(31).Value = _MAXIMUM_WEIGHT.Trim
            Else
                cmbParam(31).Value = DBNull.Value
            End If

            cmbParam(32) = New SqlParameter("@_MINIMUM_VOLUME", SqlDbType.VarChar)
            If _MINIMUM_VOLUME.Trim <> "" Then
                cmbParam(32).Value = _MINIMUM_VOLUME.Trim
            Else
                cmbParam(32).Value = DBNull.Value
            End If

            cmbParam(33) = New SqlParameter("@_MAXIMUM_VOLUME", SqlDbType.VarChar)
            If _MAXIMUM_VOLUME.Trim <> "" Then
                cmbParam(33).Value = _MAXIMUM_VOLUME.Trim
            Else
                cmbParam(33).Value = DBNull.Value
            End If

            cmbParam(34) = New SqlParameter("@_PALLET_TYPE_ID", SqlDbType.VarChar)
            If _PALLET_TYPE_ID.Trim <> "" Then
                cmbParam(34).Value = _PALLET_TYPE_ID.Trim
            Else
                cmbParam(34).Value = DBNull.Value
            End If

            cmbParam(35) = New SqlParameter("@_MAXIMUM_PER_PALLET", SqlDbType.VarChar)
            If _MAXIMUM_PER_PALLET.Trim <> "" Then
                cmbParam(35).Value = _MAXIMUM_PER_PALLET.Trim
            Else
                cmbParam(35).Value = DBNull.Value
            End If

            cmbParam(36) = New SqlParameter("@_PICK_ID", SqlDbType.VarChar)
            If _PICK_ID.Trim <> "" Then
                cmbParam(36).Value = _PICK_ID.Trim
            Else
                cmbParam(36).Value = DBNull.Value
            End If

            cmbParam(37) = New SqlParameter("@_AMOUNT_PER_UNIT", SqlDbType.Int)
            If _AMOUNT_PER_UNIT IsNot Nothing Then
                cmbParam(37).Value = _AMOUNT_PER_UNIT.Value
            Else
                cmbParam(37).Value = DBNull.Value
            End If

            cmbParam(38) = New SqlParameter("@_WEIGHT", SqlDbType.VarChar)
            If _WEIGHT.Trim <> "" Then
                cmbParam(38).Value = _WEIGHT.Trim
            Else
                cmbParam(38).Value = DBNull.Value
            End If

            cmbParam(39) = New SqlParameter("@_ACTIVE_STATUS", SqlDbType.Char)
            cmbParam(39).Value = _ACTIVE_STATUS

            cmbParam(40) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            cmbParam(40).Value = _CREATED_BY.Trim

            cmbParam(41) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            cmbParam(41).Value = _CREATED_DATE

            cmbParam(42) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then
                cmbParam(42).Value = _UPDATED_BY.Trim
            Else
                cmbParam(42).Value = DBNull.Value
            End If

            cmbParam(43) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then
                cmbParam(43).Value = _UPDATED_DATE.Value
            Else
                cmbParam(43).Value = DBNull.Value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of MS_SKU by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SqlTransaction, cmdPara() As SqlParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData = False
            If whText.Trim() <> "" Then
                Dim Rdr As SqlDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Sequence_ID")) = False Then _SEQUENCE_ID = Convert.ToInt64(Rdr("Sequence_ID"))
                        If Convert.IsDBNull(Rdr("SKU_ID")) = False Then _SKU_ID = Rdr("SKU_ID").ToString()
                        If Convert.IsDBNull(Rdr("SKU_Description")) = False Then _SKU_DESCRIPTION = Rdr("SKU_Description").ToString()
                        If Convert.IsDBNull(Rdr("SKU_Group_ID")) = False Then _SKU_GROUP_ID = Rdr("SKU_Group_ID").ToString()
                        If Convert.IsDBNull(Rdr("Size")) = False Then _SIZE = Rdr("Size").ToString()
                        If Convert.IsDBNull(Rdr("Color")) = False Then _COLOR = Rdr("Color").ToString()
                        If Convert.IsDBNull(Rdr("Model")) = False Then _MODEL = Rdr("Model").ToString()
                        If Convert.IsDBNull(Rdr("Brand")) = False Then _BRAND = Rdr("Brand").ToString()
                        If Convert.IsDBNull(Rdr("Unit_ID")) = False Then _UNIT_ID = Convert.ToInt64(Rdr("Unit_ID"))
                        If Convert.IsDBNull(Rdr("Weight_per_Unit")) = False Then _WEIGHT_PER_UNIT = Convert.ToInt32(Rdr("Weight_per_Unit"))
                        If Convert.IsDBNull(Rdr("Barcode1")) = False Then _BARCODE1 = Rdr("Barcode1").ToString()
                        If Convert.IsDBNull(Rdr("Barcode2")) = False Then _BARCODE2 = Rdr("Barcode2").ToString()
                        If Convert.IsDBNull(Rdr("Product_ID")) = False Then _PRODUCT_ID = Rdr("Product_ID").ToString()
                        If Convert.IsDBNull(Rdr("Product_Type_ID")) = False Then _PRODUCT_TYPE_ID = Convert.ToInt64(Rdr("Product_Type_ID"))
                        If Convert.IsDBNull(Rdr("Volume_per_Unit")) = False Then _VOLUME_PER_UNIT = Rdr("Volume_per_Unit").ToString()
                        If Convert.IsDBNull(Rdr("Volume_Width")) = False Then _VOLUME_WIDTH = Rdr("Volume_Width").ToString()
                        If Convert.IsDBNull(Rdr("Volume_Length")) = False Then _VOLUME_LENGTH = Rdr("Volume_Length").ToString()
                        If Convert.IsDBNull(Rdr("Volume_Hight")) = False Then _VOLUME_HIGHT = Rdr("Volume_Hight").ToString()
                        If Convert.IsDBNull(Rdr("Age_Product_Day")) = False Then _AGE_PRODUCT_DAY = Rdr("Age_Product_Day").ToString()
                        If Convert.IsDBNull(Rdr("Age_Product_Month")) = False Then _AGE_PRODUCT_MONTH = Rdr("Age_Product_Month").ToString()
                        If Convert.IsDBNull(Rdr("Age_Product_Year")) = False Then _AGE_PRODUCT_YEAR = Rdr("Age_Product_Year").ToString()
                        If Convert.IsDBNull(Rdr("Production_Date")) = False Then _PRODUCTION_DATE = Convert.ToDateTime(Rdr("Production_Date"))
                        If Convert.IsDBNull(Rdr("Expire_Date")) = False Then _EXPIRE_DATE = Convert.ToDateTime(Rdr("Expire_Date"))
                        If Convert.IsDBNull(Rdr("Product_Picture")) = False Then _PRODUCT_PICTURE = Rdr("Product_Picture").ToString()
                        If Convert.IsDBNull(Rdr("Product_Group_ID")) = False Then _PRODUCT_GROUP_ID = Rdr("Product_Group_ID").ToString()
                        If Convert.IsDBNull(Rdr("Buy_Price")) = False Then _BUY_PRICE = Convert.ToInt32(Rdr("Buy_Price"))
                        If Convert.IsDBNull(Rdr("Sell_Price")) = False Then _SELL_PRICE = Convert.ToInt32(Rdr("Sell_Price"))
                        If Convert.IsDBNull(Rdr("Other_Price")) = False Then _OTHER_PRICE = Convert.ToInt32(Rdr("Other_Price"))
                        If Convert.IsDBNull(Rdr("Minimum_Amount")) = False Then _MINIMUM_AMOUNT = Rdr("Minimum_Amount").ToString()
                        If Convert.IsDBNull(Rdr("Maximum_Amount")) = False Then _MAXIMUM_AMOUNT = Rdr("Maximum_Amount").ToString()
                        If Convert.IsDBNull(Rdr("Minimum_Weight")) = False Then _MINIMUM_WEIGHT = Rdr("Minimum_Weight").ToString()
                        If Convert.IsDBNull(Rdr("Maximum_Weight")) = False Then _MAXIMUM_WEIGHT = Rdr("Maximum_Weight").ToString()
                        If Convert.IsDBNull(Rdr("Minimum_Volume")) = False Then _MINIMUM_VOLUME = Rdr("Minimum_Volume").ToString()
                        If Convert.IsDBNull(Rdr("Maximum_Volume")) = False Then _MAXIMUM_VOLUME = Rdr("Maximum_Volume").ToString()
                        If Convert.IsDBNull(Rdr("Pallet_Type_ID")) = False Then _PALLET_TYPE_ID = Rdr("Pallet_Type_ID").ToString()
                        If Convert.IsDBNull(Rdr("Maximum_Per_Pallet")) = False Then _MAXIMUM_PER_PALLET = Rdr("Maximum_Per_Pallet").ToString()
                        If Convert.IsDBNull(Rdr("Pick_ID")) = False Then _PICK_ID = Rdr("Pick_ID").ToString()
                        If Convert.IsDBNull(Rdr("Amount_Per_Unit")) = False Then _AMOUNT_PER_UNIT = Convert.ToInt32(Rdr("Amount_Per_Unit"))
                        If Convert.IsDBNull(Rdr("Weight")) = False Then _WEIGHT = Rdr("Weight").ToString()
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _ACTIVE_STATUS = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _CREATED_BY = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _CREATED_DATE = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _UPDATED_BY = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _UPDATED_DATE = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of MS_SKU by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SqlTransaction, cmdPara() As SqlParameter) As MsSkuLinqDB
            ClearData()
            _haveData = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SqlDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Sequence_ID")) = False Then _SEQUENCE_ID = Convert.ToInt64(Rdr("Sequence_ID"))
                        If Convert.IsDBNull(Rdr("SKU_ID")) = False Then _SKU_ID = Rdr("SKU_ID").ToString()
                        If Convert.IsDBNull(Rdr("SKU_Description")) = False Then _SKU_DESCRIPTION = Rdr("SKU_Description").ToString()
                        If Convert.IsDBNull(Rdr("SKU_Group_ID")) = False Then _SKU_GROUP_ID = Rdr("SKU_Group_ID").ToString()
                        If Convert.IsDBNull(Rdr("Size")) = False Then _SIZE = Rdr("Size").ToString()
                        If Convert.IsDBNull(Rdr("Color")) = False Then _COLOR = Rdr("Color").ToString()
                        If Convert.IsDBNull(Rdr("Model")) = False Then _MODEL = Rdr("Model").ToString()
                        If Convert.IsDBNull(Rdr("Brand")) = False Then _BRAND = Rdr("Brand").ToString()
                        If Convert.IsDBNull(Rdr("Unit_ID")) = False Then _UNIT_ID = Convert.ToInt64(Rdr("Unit_ID"))
                        If Convert.IsDBNull(Rdr("Weight_per_Unit")) = False Then _WEIGHT_PER_UNIT = Convert.ToInt32(Rdr("Weight_per_Unit"))
                        If Convert.IsDBNull(Rdr("Barcode1")) = False Then _BARCODE1 = Rdr("Barcode1").ToString()
                        If Convert.IsDBNull(Rdr("Barcode2")) = False Then _BARCODE2 = Rdr("Barcode2").ToString()
                        If Convert.IsDBNull(Rdr("Product_ID")) = False Then _PRODUCT_ID = Rdr("Product_ID").ToString()
                        If Convert.IsDBNull(Rdr("Product_Type_ID")) = False Then _PRODUCT_TYPE_ID = Convert.ToInt64(Rdr("Product_Type_ID"))
                        If Convert.IsDBNull(Rdr("Volume_per_Unit")) = False Then _VOLUME_PER_UNIT = Rdr("Volume_per_Unit").ToString()
                        If Convert.IsDBNull(Rdr("Volume_Width")) = False Then _VOLUME_WIDTH = Rdr("Volume_Width").ToString()
                        If Convert.IsDBNull(Rdr("Volume_Length")) = False Then _VOLUME_LENGTH = Rdr("Volume_Length").ToString()
                        If Convert.IsDBNull(Rdr("Volume_Hight")) = False Then _VOLUME_HIGHT = Rdr("Volume_Hight").ToString()
                        If Convert.IsDBNull(Rdr("Age_Product_Day")) = False Then _AGE_PRODUCT_DAY = Rdr("Age_Product_Day").ToString()
                        If Convert.IsDBNull(Rdr("Age_Product_Month")) = False Then _AGE_PRODUCT_MONTH = Rdr("Age_Product_Month").ToString()
                        If Convert.IsDBNull(Rdr("Age_Product_Year")) = False Then _AGE_PRODUCT_YEAR = Rdr("Age_Product_Year").ToString()
                        If Convert.IsDBNull(Rdr("Production_Date")) = False Then _PRODUCTION_DATE = Convert.ToDateTime(Rdr("Production_Date"))
                        If Convert.IsDBNull(Rdr("Expire_Date")) = False Then _EXPIRE_DATE = Convert.ToDateTime(Rdr("Expire_Date"))
                        If Convert.IsDBNull(Rdr("Product_Picture")) = False Then _PRODUCT_PICTURE = Rdr("Product_Picture").ToString()
                        If Convert.IsDBNull(Rdr("Product_Group_ID")) = False Then _PRODUCT_GROUP_ID = Rdr("Product_Group_ID").ToString()
                        If Convert.IsDBNull(Rdr("Buy_Price")) = False Then _BUY_PRICE = Convert.ToInt32(Rdr("Buy_Price"))
                        If Convert.IsDBNull(Rdr("Sell_Price")) = False Then _SELL_PRICE = Convert.ToInt32(Rdr("Sell_Price"))
                        If Convert.IsDBNull(Rdr("Other_Price")) = False Then _OTHER_PRICE = Convert.ToInt32(Rdr("Other_Price"))
                        If Convert.IsDBNull(Rdr("Minimum_Amount")) = False Then _MINIMUM_AMOUNT = Rdr("Minimum_Amount").ToString()
                        If Convert.IsDBNull(Rdr("Maximum_Amount")) = False Then _MAXIMUM_AMOUNT = Rdr("Maximum_Amount").ToString()
                        If Convert.IsDBNull(Rdr("Minimum_Weight")) = False Then _MINIMUM_WEIGHT = Rdr("Minimum_Weight").ToString()
                        If Convert.IsDBNull(Rdr("Maximum_Weight")) = False Then _MAXIMUM_WEIGHT = Rdr("Maximum_Weight").ToString()
                        If Convert.IsDBNull(Rdr("Minimum_Volume")) = False Then _MINIMUM_VOLUME = Rdr("Minimum_Volume").ToString()
                        If Convert.IsDBNull(Rdr("Maximum_Volume")) = False Then _MAXIMUM_VOLUME = Rdr("Maximum_Volume").ToString()
                        If Convert.IsDBNull(Rdr("Pallet_Type_ID")) = False Then _PALLET_TYPE_ID = Rdr("Pallet_Type_ID").ToString()
                        If Convert.IsDBNull(Rdr("Maximum_Per_Pallet")) = False Then _MAXIMUM_PER_PALLET = Rdr("Maximum_Per_Pallet").ToString()
                        If Convert.IsDBNull(Rdr("Pick_ID")) = False Then _PICK_ID = Rdr("Pick_ID").ToString()
                        If Convert.IsDBNull(Rdr("Amount_Per_Unit")) = False Then _AMOUNT_PER_UNIT = Convert.ToInt32(Rdr("Amount_Per_Unit"))
                        If Convert.IsDBNull(Rdr("Weight")) = False Then _WEIGHT = Rdr("Weight").ToString()
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _ACTIVE_STATUS = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _CREATED_BY = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _CREATED_DATE = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _UPDATED_BY = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _UPDATED_DATE = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table MS_SKU
        Private ReadOnly Property SqlInsert() As String
            Get
                Dim Sql As String = ""
                Sql += "INSERT INTO " & TableName & " (SKU_ID, SKU_DESCRIPTION, SKU_GROUP_ID, SIZE, COLOR, MODEL, BRAND, UNIT_ID, WEIGHT_PER_UNIT, BARCODE1, BARCODE2, PRODUCT_ID, PRODUCT_TYPE_ID, VOLUME_PER_UNIT, VOLUME_WIDTH, VOLUME_LENGTH, VOLUME_HIGHT, AGE_PRODUCT_DAY, AGE_PRODUCT_MONTH, AGE_PRODUCT_YEAR, PRODUCTION_DATE, EXPIRE_DATE, PRODUCT_PICTURE, PRODUCT_GROUP_ID, BUY_PRICE, SELL_PRICE, OTHER_PRICE, MINIMUM_AMOUNT, MAXIMUM_AMOUNT, MINIMUM_WEIGHT, MAXIMUM_WEIGHT, MINIMUM_VOLUME, MAXIMUM_VOLUME, PALLET_TYPE_ID, MAXIMUM_PER_PALLET, PICK_ID, AMOUNT_PER_UNIT, WEIGHT, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE)"
                Sql += " OUTPUT INSERTED.SEQUENCE_ID, INSERTED.SKU_ID, INSERTED.SKU_DESCRIPTION, INSERTED.SKU_GROUP_ID, INSERTED.SIZE, INSERTED.COLOR, INSERTED.MODEL, INSERTED.BRAND, INSERTED.UNIT_ID, INSERTED.WEIGHT_PER_UNIT, INSERTED.BARCODE1, INSERTED.BARCODE2, INSERTED.PRODUCT_ID, INSERTED.PRODUCT_TYPE_ID, INSERTED.VOLUME_PER_UNIT, INSERTED.VOLUME_WIDTH, INSERTED.VOLUME_LENGTH, INSERTED.VOLUME_HIGHT, INSERTED.AGE_PRODUCT_DAY, INSERTED.AGE_PRODUCT_MONTH, INSERTED.AGE_PRODUCT_YEAR, INSERTED.PRODUCTION_DATE, INSERTED.EXPIRE_DATE, INSERTED.PRODUCT_PICTURE, INSERTED.PRODUCT_GROUP_ID, INSERTED.BUY_PRICE, INSERTED.SELL_PRICE, INSERTED.OTHER_PRICE, INSERTED.MINIMUM_AMOUNT, INSERTED.MAXIMUM_AMOUNT, INSERTED.MINIMUM_WEIGHT, INSERTED.MAXIMUM_WEIGHT, INSERTED.MINIMUM_VOLUME, INSERTED.MAXIMUM_VOLUME, INSERTED.PALLET_TYPE_ID, INSERTED.MAXIMUM_PER_PALLET, INSERTED.PICK_ID, INSERTED.AMOUNT_PER_UNIT, INSERTED.WEIGHT, INSERTED.ACTIVE_STATUS, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE"
                Sql += " VALUES("
                Sql += "@_SKU_ID" & ", "
                Sql += "@_SKU_DESCRIPTION" & ", "
                Sql += "@_SKU_GROUP_ID" & ", "
                Sql += "@_SIZE" & ", "
                Sql += "@_COLOR" & ", "
                Sql += "@_MODEL" & ", "
                Sql += "@_BRAND" & ", "
                Sql += "@_UNIT_ID" & ", "
                Sql += "@_WEIGHT_PER_UNIT" & ", "
                Sql += "@_BARCODE1" & ", "
                Sql += "@_BARCODE2" & ", "
                Sql += "@_PRODUCT_ID" & ", "
                Sql += "@_PRODUCT_TYPE_ID" & ", "
                Sql += "@_VOLUME_PER_UNIT" & ", "
                Sql += "@_VOLUME_WIDTH" & ", "
                Sql += "@_VOLUME_LENGTH" & ", "
                Sql += "@_VOLUME_HIGHT" & ", "
                Sql += "@_AGE_PRODUCT_DAY" & ", "
                Sql += "@_AGE_PRODUCT_MONTH" & ", "
                Sql += "@_AGE_PRODUCT_YEAR" & ", "
                Sql += "@_PRODUCTION_DATE" & ", "
                Sql += "@_EXPIRE_DATE" & ", "
                Sql += "@_PRODUCT_PICTURE" & ", "
                Sql += "@_PRODUCT_GROUP_ID" & ", "
                Sql += "@_BUY_PRICE" & ", "
                Sql += "@_SELL_PRICE" & ", "
                Sql += "@_OTHER_PRICE" & ", "
                Sql += "@_MINIMUM_AMOUNT" & ", "
                Sql += "@_MAXIMUM_AMOUNT" & ", "
                Sql += "@_MINIMUM_WEIGHT" & ", "
                Sql += "@_MAXIMUM_WEIGHT" & ", "
                Sql += "@_MINIMUM_VOLUME" & ", "
                Sql += "@_MAXIMUM_VOLUME" & ", "
                Sql += "@_PALLET_TYPE_ID" & ", "
                Sql += "@_MAXIMUM_PER_PALLET" & ", "
                Sql += "@_PICK_ID" & ", "
                Sql += "@_AMOUNT_PER_UNIT" & ", "
                Sql += "@_WEIGHT" & ", "
                Sql += "@_ACTIVE_STATUS" & ", "
                Sql += "@_CREATED_BY" & ", "
                Sql += "@_CREATED_DATE" & ", "
                Sql += "@_UPDATED_BY" & ", "
                Sql += "@_UPDATED_DATE"
                Sql += ")"
                Return Sql
            End Get
        End Property


        'Get update statement form table MS_SKU
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & TableName & " SET "
                Sql += "SKU_ID = " & "@_SKU_ID" & ", "
                Sql += "SKU_DESCRIPTION = " & "@_SKU_DESCRIPTION" & ", "
                Sql += "SKU_GROUP_ID = " & "@_SKU_GROUP_ID" & ", "
                Sql += "SIZE = " & "@_SIZE" & ", "
                Sql += "COLOR = " & "@_COLOR" & ", "
                Sql += "MODEL = " & "@_MODEL" & ", "
                Sql += "BRAND = " & "@_BRAND" & ", "
                Sql += "UNIT_ID = " & "@_UNIT_ID" & ", "
                Sql += "WEIGHT_PER_UNIT = " & "@_WEIGHT_PER_UNIT" & ", "
                Sql += "BARCODE1 = " & "@_BARCODE1" & ", "
                Sql += "BARCODE2 = " & "@_BARCODE2" & ", "
                Sql += "PRODUCT_ID = " & "@_PRODUCT_ID" & ", "
                Sql += "PRODUCT_TYPE_ID = " & "@_PRODUCT_TYPE_ID" & ", "
                Sql += "VOLUME_PER_UNIT = " & "@_VOLUME_PER_UNIT" & ", "
                Sql += "VOLUME_WIDTH = " & "@_VOLUME_WIDTH" & ", "
                Sql += "VOLUME_LENGTH = " & "@_VOLUME_LENGTH" & ", "
                Sql += "VOLUME_HIGHT = " & "@_VOLUME_HIGHT" & ", "
                Sql += "AGE_PRODUCT_DAY = " & "@_AGE_PRODUCT_DAY" & ", "
                Sql += "AGE_PRODUCT_MONTH = " & "@_AGE_PRODUCT_MONTH" & ", "
                Sql += "AGE_PRODUCT_YEAR = " & "@_AGE_PRODUCT_YEAR" & ", "
                Sql += "PRODUCTION_DATE = " & "@_PRODUCTION_DATE" & ", "
                Sql += "EXPIRE_DATE = " & "@_EXPIRE_DATE" & ", "
                Sql += "PRODUCT_PICTURE = " & "@_PRODUCT_PICTURE" & ", "
                Sql += "PRODUCT_GROUP_ID = " & "@_PRODUCT_GROUP_ID" & ", "
                Sql += "BUY_PRICE = " & "@_BUY_PRICE" & ", "
                Sql += "SELL_PRICE = " & "@_SELL_PRICE" & ", "
                Sql += "OTHER_PRICE = " & "@_OTHER_PRICE" & ", "
                Sql += "MINIMUM_AMOUNT = " & "@_MINIMUM_AMOUNT" & ", "
                Sql += "MAXIMUM_AMOUNT = " & "@_MAXIMUM_AMOUNT" & ", "
                Sql += "MINIMUM_WEIGHT = " & "@_MINIMUM_WEIGHT" & ", "
                Sql += "MAXIMUM_WEIGHT = " & "@_MAXIMUM_WEIGHT" & ", "
                Sql += "MINIMUM_VOLUME = " & "@_MINIMUM_VOLUME" & ", "
                Sql += "MAXIMUM_VOLUME = " & "@_MAXIMUM_VOLUME" & ", "
                Sql += "PALLET_TYPE_ID = " & "@_PALLET_TYPE_ID" & ", "
                Sql += "MAXIMUM_PER_PALLET = " & "@_MAXIMUM_PER_PALLET" & ", "
                Sql += "PICK_ID = " & "@_PICK_ID" & ", "
                Sql += "AMOUNT_PER_UNIT = " & "@_AMOUNT_PER_UNIT" & ", "
                Sql += "WEIGHT = " & "@_WEIGHT" & ", "
                Sql += "ACTIVE_STATUS = " & "@_ACTIVE_STATUS" & ", "
                Sql += "CREATED_BY = " & "@_CREATED_BY" & ", "
                Sql += "CREATED_DATE = " & "@_CREATED_DATE" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table MS_SKU
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & TableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table MS_SKU
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT SEQUENCE_ID, SKU_ID, SKU_DESCRIPTION, SKU_GROUP_ID, SIZE, COLOR, MODEL, BRAND, UNIT_ID, WEIGHT_PER_UNIT, BARCODE1, BARCODE2, PRODUCT_ID, PRODUCT_TYPE_ID, VOLUME_PER_UNIT, VOLUME_WIDTH, VOLUME_LENGTH, VOLUME_HIGHT, AGE_PRODUCT_DAY, AGE_PRODUCT_MONTH, AGE_PRODUCT_YEAR, PRODUCTION_DATE, EXPIRE_DATE, PRODUCT_PICTURE, PRODUCT_GROUP_ID, BUY_PRICE, SELL_PRICE, OTHER_PRICE, MINIMUM_AMOUNT, MAXIMUM_AMOUNT, MINIMUM_WEIGHT, MAXIMUM_WEIGHT, MINIMUM_VOLUME, MAXIMUM_VOLUME, PALLET_TYPE_ID, MAXIMUM_PER_PALLET, PICK_ID, AMOUNT_PER_UNIT, WEIGHT, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE FROM " & TableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
