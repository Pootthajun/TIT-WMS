Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for MS_SKU_Ingredient_By table LinqDB.
    '[Create by  on June, 21 2017]
    Public Class MsSkuIngredientByLinqDB
        Public sub MsSkuIngredientByLinqDB()

        End Sub 
        ' MS_SKU_Ingredient_By
        Const _tableName As String = "MS_SKU_Ingredient_By"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _SEQUENCE_ID As Long = 0
        Dim _SKU_INGREDIENT_ID As String = ""
        Dim _SKU_GROUP_ID As String = ""
        Dim _SKU_ID As String = ""
        Dim _AMOUNT As String = ""
        Dim _QUANTITY As String = ""
        Dim _VOLUME As String = ""
        Dim _ACTIVE_STATUS As Char = "Y"
        Dim _CREATED_BY As String = ""
        Dim _CREATED_DATE As DateTime = New DateTime(1, 1, 1)
        Dim _UPDATED_BY As String = ""
        Dim _UPDATED_DATE As System.Nullable(Of DateTime) = New DateTime(1, 1, 1)

        'Generate Field Property 
        <Column(Storage:="_SEQUENCE_ID", DbType:="BigInt NOT NULL ", CanBeNull:=False)>
        Public Property SEQUENCE_ID() As Long
            Get
                Return _SEQUENCE_ID
            End Get
            Set(ByVal value As Long)
                _SEQUENCE_ID = value
            End Set
        End Property
        <Column(Storage:="_SKU_INGREDIENT_ID", DbType:="VarChar(100) NOT NULL ", CanBeNull:=False)>
        Public Property SKU_INGREDIENT_ID() As String
            Get
                Return _SKU_INGREDIENT_ID
            End Get
            Set(ByVal value As String)
                _SKU_INGREDIENT_ID = value
            End Set
        End Property
        <Column(Storage:="_SKU_GROUP_ID", DbType:="VarChar(100) NOT NULL ", CanBeNull:=False)>
        Public Property SKU_GROUP_ID() As String
            Get
                Return _SKU_GROUP_ID
            End Get
            Set(ByVal value As String)
                _SKU_GROUP_ID = value
            End Set
        End Property
        <Column(Storage:="_SKU_ID", DbType:="VarChar(100) NOT NULL ", CanBeNull:=False)>
        Public Property SKU_ID() As String
            Get
                Return _SKU_ID
            End Get
            Set(ByVal value As String)
                _SKU_ID = value
            End Set
        End Property
        <Column(Storage:="_AMOUNT", DbType:="VarChar(50)")>
        Public Property AMOUNT() As String
            Get
                Return _AMOUNT
            End Get
            Set(ByVal value As String)
                _AMOUNT = value
            End Set
        End Property
        <Column(Storage:="_QUANTITY", DbType:="VarChar(100)")>
        Public Property QUANTITY() As String
            Get
                Return _QUANTITY
            End Get
            Set(ByVal value As String)
                _QUANTITY = value
            End Set
        End Property
        <Column(Storage:="_VOLUME", DbType:="VarChar(50)")>
        Public Property VOLUME() As String
            Get
                Return _VOLUME
            End Get
            Set(ByVal value As String)
                _VOLUME = value
            End Set
        End Property
        <Column(Storage:="_ACTIVE_STATUS", DbType:="Char(1) NOT NULL ", CanBeNull:=False)>
        Public Property ACTIVE_STATUS() As Char
            Get
                Return _ACTIVE_STATUS
            End Get
            Set(ByVal value As Char)
                _ACTIVE_STATUS = value
            End Set
        End Property
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100) NOT NULL ", CanBeNull:=False)>
        Public Property CREATED_BY() As String
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As String)
                _CREATED_BY = value
            End Set
        End Property
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime NOT NULL ", CanBeNull:=False)>
        Public Property CREATED_DATE() As DateTime
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As DateTime)
                _CREATED_DATE = value
            End Set
        End Property
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>
        Public Property UPDATED_BY() As String
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As String)
                _UPDATED_BY = value
            End Set
        End Property
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>
        Public Property UPDATED_DATE() As System.Nullable(Of DateTime)
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As System.Nullable(Of DateTime))
                _UPDATED_DATE = value
            End Set
        End Property


        'Clear All Data
        Private Sub ClearData()
            _SEQUENCE_ID = 0
            _SKU_INGREDIENT_ID = ""
            _SKU_GROUP_ID = ""
            _SKU_ID = ""
            _AMOUNT = ""
            _QUANTITY = ""
            _VOLUME = ""
            _ACTIVE_STATUS = "Y"
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1, 1, 1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1, 1, 1)
        End Sub

        'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SqlTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIf(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SqlTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into MS_SKU_Ingredient_By table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String, trans As SqlTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then
                _CREATED_BY = CreatedBy
                _CREATED_DATE = DateTime.Now
                Return doInsert(trans)
            Else
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If
        End Function


        '/// Returns an indication whether the current data is updated to MS_SKU_Ingredient_By table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String, trans As SqlTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then
                If _SEQUENCE_ID > 0 Then
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("SEQUENCE_ID = @_SEQUENCE_ID", trans)
                Else
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If
            Else
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If
        End Function


        '/// Returns an indication whether the current data is updated to MS_SKU_Ingredient_By table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SqlTransaction, cmbParm() As SqlParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If
        End Function


        '/// Returns an indication whether the current data is deleted from MS_SKU_Ingredient_By table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cSEQUENCE_ID As Long, trans As SqlTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then
                Dim p(1) As SqlParameter
                p(0) = DB.SetBigInt("@_SEQUENCE_ID", cSEQUENCE_ID)
                Return doDelete("SEQUENCE_ID = @_SEQUENCE_ID", trans, p)
            Else
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If
        End Function


        '/// Returns an indication whether the record of MS_SKU_Ingredient_By by specified SEQUENCE_ID key is retrieved successfully.
        '/// <param name=cSEQUENCE_ID>The SEQUENCE_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cSEQUENCE_ID As Long, trans As SqlTransaction) As Boolean
            Dim p(1) As SqlParameter
            p(0) = DB.SetBigInt("@_SEQUENCE_ID", cSEQUENCE_ID)
            Return doChkData("SEQUENCE_ID = @_SEQUENCE_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of MS_SKU_Ingredient_By by specified SEQUENCE_ID key is retrieved successfully.
        '/// <param name=cSEQUENCE_ID>The SEQUENCE_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cSEQUENCE_ID As Long, trans As SqlTransaction) As MsSkuIngredientByLinqDB
            Dim p(1) As SqlParameter
            p(0) = DB.SetBigInt("@_SEQUENCE_ID", cSEQUENCE_ID)
            Return doGetData("SEQUENCE_ID = @_SEQUENCE_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of MS_SKU_Ingredient_By by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SqlTransaction, cmdPara() As SqlParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into MS_SKU_Ingredient_By table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SqlTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt As DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _SEQUENCE_ID = dt.Rows(0)("SEQUENCE_ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                        ret.NewID = _SEQUENCE_ID
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = False
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to MS_SKU_Ingredient_By table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SqlTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> "" Then

                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                            ret.NewID = _SEQUENCE_ID
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString()
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 & ex.ToString()
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from MS_SKU_Ingredient_By table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SqlTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            Dim sql As String = SqlDelete & tmpWhere
            If whText.Trim() <> "" Then

                Try
                    ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                    If ret.IsSuccess = False Then
                        _error = MessageResources.MSGED001
                    Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                    End If
                Catch ex As ApplicationException
                    _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                    ret.IsSuccess = False
                    ret.ErrorMessage = _error
                    ret.SqlStatement = sql
                Catch ex As Exception
                    _error = " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                    ret.IsSuccess = False
                    ret.ErrorMessage = _error
                    ret.SqlStatement = sql
                End Try
            Else
                _error = MessageResources.MSGED003 & "### SQL: " & sql
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                ret.SqlStatement = sql
            End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(11) As SqlParameter
            cmbParam(0) = New SqlParameter("@_SEQUENCE_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _SEQUENCE_ID

            cmbParam(1) = New SqlParameter("@_SKU_INGREDIENT_ID", SqlDbType.VarChar)
            cmbParam(1).Value = _SKU_INGREDIENT_ID.Trim

            cmbParam(2) = New SqlParameter("@_SKU_GROUP_ID", SqlDbType.VarChar)
            cmbParam(2).Value = _SKU_GROUP_ID.Trim

            cmbParam(3) = New SqlParameter("@_SKU_ID", SqlDbType.VarChar)
            cmbParam(3).Value = _SKU_ID.Trim

            cmbParam(4) = New SqlParameter("@_AMOUNT", SqlDbType.VarChar)
            If _AMOUNT.Trim <> "" Then
                cmbParam(4).Value = _AMOUNT.Trim
            Else
                cmbParam(4).Value = DBNull.Value
            End If

            cmbParam(5) = New SqlParameter("@_QUANTITY", SqlDbType.VarChar)
            If _QUANTITY.Trim <> "" Then
                cmbParam(5).Value = _QUANTITY.Trim
            Else
                cmbParam(5).Value = DBNull.Value
            End If

            cmbParam(6) = New SqlParameter("@_VOLUME", SqlDbType.VarChar)
            If _VOLUME.Trim <> "" Then
                cmbParam(6).Value = _VOLUME.Trim
            Else
                cmbParam(6).Value = DBNull.Value
            End If

            cmbParam(7) = New SqlParameter("@_ACTIVE_STATUS", SqlDbType.Char)
            cmbParam(7).Value = _ACTIVE_STATUS

            cmbParam(8) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            cmbParam(8).Value = _CREATED_BY.Trim

            cmbParam(9) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            cmbParam(9).Value = _CREATED_DATE

            cmbParam(10) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then
                cmbParam(10).Value = _UPDATED_BY.Trim
            Else
                cmbParam(10).Value = DBNull.Value
            End If

            cmbParam(11) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then
                cmbParam(11).Value = _UPDATED_DATE.Value
            Else
                cmbParam(11).Value = DBNull.Value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of MS_SKU_Ingredient_By by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SqlTransaction, cmdPara() As SqlParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData = False
            If whText.Trim() <> "" Then
                Dim Rdr As SqlDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Sequence_ID")) = False Then _SEQUENCE_ID = Convert.ToInt64(Rdr("Sequence_ID"))
                        If Convert.IsDBNull(Rdr("SKU_Ingredient_ID")) = False Then _SKU_INGREDIENT_ID = Rdr("SKU_Ingredient_ID").ToString()
                        If Convert.IsDBNull(Rdr("SKU_Group_ID")) = False Then _SKU_GROUP_ID = Rdr("SKU_Group_ID").ToString()
                        If Convert.IsDBNull(Rdr("SKU_ID")) = False Then _SKU_ID = Rdr("SKU_ID").ToString()
                        If Convert.IsDBNull(Rdr("Amount")) = False Then _AMOUNT = Rdr("Amount").ToString()
                        If Convert.IsDBNull(Rdr("Quantity")) = False Then _QUANTITY = Rdr("Quantity").ToString()
                        If Convert.IsDBNull(Rdr("Volume")) = False Then _VOLUME = Rdr("Volume").ToString()
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _ACTIVE_STATUS = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _CREATED_BY = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _CREATED_DATE = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _UPDATED_BY = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _UPDATED_DATE = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of MS_SKU_Ingredient_By by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SqlTransaction, cmdPara() As SqlParameter) As MsSkuIngredientByLinqDB
            ClearData()
            _haveData = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SqlDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Sequence_ID")) = False Then _SEQUENCE_ID = Convert.ToInt64(Rdr("Sequence_ID"))
                        If Convert.IsDBNull(Rdr("SKU_Ingredient_ID")) = False Then _SKU_INGREDIENT_ID = Rdr("SKU_Ingredient_ID").ToString()
                        If Convert.IsDBNull(Rdr("SKU_Group_ID")) = False Then _SKU_Group_ID = Rdr("SKU_Group_ID").ToString()
                        If Convert.IsDBNull(Rdr("SKU_ID")) = False Then _SKU_ID = Rdr("SKU_ID").ToString()
                        If Convert.IsDBNull(Rdr("Amount")) = False Then _Amount = Rdr("Amount").ToString()
                        If Convert.IsDBNull(Rdr("Quantity")) = False Then _Quantity = Rdr("Quantity").ToString()
                        If Convert.IsDBNull(Rdr("Volume")) = False Then _Volume = Rdr("Volume").ToString()
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _Active_Status = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table MS_SKU_Ingredient_By
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (SKU_INGREDIENT_ID, SKU_GROUP_ID, SKU_ID, AMOUNT, QUANTITY, VOLUME, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE)"
                Sql += " OUTPUT INSERTED.SEQUENCE_ID, INSERTED.SKU_INGREDIENT_ID, INSERTED.SKU_GROUP_ID, INSERTED.SKU_ID, INSERTED.AMOUNT, INSERTED.QUANTITY, INSERTED.VOLUME, INSERTED.ACTIVE_STATUS, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE"
                Sql += " VALUES("
                sql += "@_SKU_INGREDIENT_ID" & ", "
                sql += "@_SKU_GROUP_ID" & ", "
                sql += "@_SKU_ID" & ", "
                sql += "@_AMOUNT" & ", "
                sql += "@_QUANTITY" & ", "
                sql += "@_VOLUME" & ", "
                sql += "@_ACTIVE_STATUS" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE" & ", "
                sql += "@_UPDATED_BY" & ", "
                sql += "@_UPDATED_DATE"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table MS_SKU_Ingredient_By
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "SKU_INGREDIENT_ID = " & "@_SKU_INGREDIENT_ID" & ", "
                Sql += "SKU_GROUP_ID = " & "@_SKU_GROUP_ID" & ", "
                Sql += "SKU_ID = " & "@_SKU_ID" & ", "
                Sql += "AMOUNT = " & "@_AMOUNT" & ", "
                Sql += "QUANTITY = " & "@_QUANTITY" & ", "
                Sql += "VOLUME = " & "@_VOLUME" & ", "
                Sql += "ACTIVE_STATUS = " & "@_ACTIVE_STATUS" & ", "
                Sql += "CREATED_BY = " & "@_CREATED_BY" & ", "
                Sql += "CREATED_DATE = " & "@_CREATED_DATE" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table MS_SKU_Ingredient_By
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table MS_SKU_Ingredient_By
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT SEQUENCE_ID, SKU_INGREDIENT_ID, SKU_GROUP_ID, SKU_ID, AMOUNT, QUANTITY, VOLUME, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
