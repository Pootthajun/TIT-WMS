Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for MS_Warehouse table LinqDB.
    '[Create by  on July, 4 2017]
    Public Class MsWarehouseLinqDB
        Public sub MsWarehouseLinqDB()

        End Sub 
        ' MS_Warehouse
        Const _tableName As String = "MS_Warehouse"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _WAREHOUSE_ID As Long = 0
        Dim _WAREHOUSE_CODE As String = ""
        Dim _WAREHOUSE_NAME As  String  = ""
        Dim _WAREHOUSE_SIZE As  String  = ""
        Dim _WAREHOUSE_BUILDING As  String  = ""
        Dim _WAREHOUSE_FLOOR As  String  = ""
        Dim _WAREHOUSE_ROOM As  String  = ""
        Dim _WAREHOUSE_ZONE As  String  = ""
        Dim _WAREHOUSE_LOCK As  String  = ""
        Dim _WAREHOUSE_ROW As  String  = ""
        Dim _WAREHOUSE_SHELF As  String  = ""
        Dim _WAREHOUSE_DEEP As  String  = ""
        Dim _WAREHOUSE_TYPE_ID As  System.Nullable(Of Long) 
        Dim _ACTIVE_STATUS As Char = "Y"
        Dim _CREATED_BY As String = ""
        Dim _CREATED_DATE As DateTime = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)

        'Generate Field Property 
        <Column(Storage:="_WAREHOUSE_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property WAREHOUSE_ID() As Long
            Get
                Return _WAREHOUSE_ID
            End Get
            Set(ByVal value As Long)
               _WAREHOUSE_ID = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_CODE", DbType:="VarChar(10) NOT NULL ",CanBeNull:=false)>  _
        Public Property WAREHOUSE_CODE() As String
            Get
                Return _WAREHOUSE_CODE
            End Get
            Set(ByVal value As String)
               _WAREHOUSE_CODE = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_NAME", DbType:="VarChar(100)")>  _
        Public Property WAREHOUSE_NAME() As  String 
            Get
                Return _WAREHOUSE_NAME
            End Get
            Set(ByVal value As  String )
               _WAREHOUSE_NAME = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_SIZE", DbType:="VarChar(50)")>  _
        Public Property WAREHOUSE_SIZE() As  String 
            Get
                Return _WAREHOUSE_SIZE
            End Get
            Set(ByVal value As  String )
               _WAREHOUSE_SIZE = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_BUILDING", DbType:="VarChar(50)")>  _
        Public Property WAREHOUSE_BUILDING() As  String 
            Get
                Return _WAREHOUSE_BUILDING
            End Get
            Set(ByVal value As  String )
               _WAREHOUSE_BUILDING = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_FLOOR", DbType:="VarChar(50)")>  _
        Public Property WAREHOUSE_FLOOR() As  String 
            Get
                Return _WAREHOUSE_FLOOR
            End Get
            Set(ByVal value As  String )
               _WAREHOUSE_FLOOR = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_ROOM", DbType:="VarChar(50)")>  _
        Public Property WAREHOUSE_ROOM() As  String 
            Get
                Return _WAREHOUSE_ROOM
            End Get
            Set(ByVal value As  String )
               _WAREHOUSE_ROOM = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_ZONE", DbType:="VarChar(50)")>  _
        Public Property WAREHOUSE_ZONE() As  String 
            Get
                Return _WAREHOUSE_ZONE
            End Get
            Set(ByVal value As  String )
               _WAREHOUSE_ZONE = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_LOCK", DbType:="VarChar(50)")>  _
        Public Property WAREHOUSE_LOCK() As  String 
            Get
                Return _WAREHOUSE_LOCK
            End Get
            Set(ByVal value As  String )
               _WAREHOUSE_LOCK = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_ROW", DbType:="VarChar(50)")>  _
        Public Property WAREHOUSE_ROW() As  String 
            Get
                Return _WAREHOUSE_ROW
            End Get
            Set(ByVal value As  String )
               _WAREHOUSE_ROW = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_SHELF", DbType:="VarChar(50)")>  _
        Public Property WAREHOUSE_SHELF() As  String 
            Get
                Return _WAREHOUSE_SHELF
            End Get
            Set(ByVal value As  String )
               _WAREHOUSE_SHELF = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_DEEP", DbType:="VarChar(50)")>  _
        Public Property WAREHOUSE_DEEP() As  String 
            Get
                Return _WAREHOUSE_DEEP
            End Get
            Set(ByVal value As  String )
               _WAREHOUSE_DEEP = value
            End Set
        End Property 
        <Column(Storage:="_WAREHOUSE_TYPE_ID", DbType:="Int")>  _
        Public Property WAREHOUSE_TYPE_ID() As  System.Nullable(Of Long) 
            Get
                Return _WAREHOUSE_TYPE_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _WAREHOUSE_TYPE_ID = value
            End Set
        End Property 
        <Column(Storage:="_ACTIVE_STATUS", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ACTIVE_STATUS() As Char
            Get
                Return _ACTIVE_STATUS
            End Get
            Set(ByVal value As Char)
               _ACTIVE_STATUS = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_BY() As String
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As String)
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_DATE() As DateTime
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As DateTime)
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _WAREHOUSE_ID = 0
            _WAREHOUSE_CODE = ""
            _WAREHOUSE_NAME = ""
            _WAREHOUSE_SIZE = ""
            _WAREHOUSE_BUILDING = ""
            _WAREHOUSE_FLOOR = ""
            _WAREHOUSE_ROOM = ""
            _WAREHOUSE_ZONE = ""
            _WAREHOUSE_LOCK = ""
            _WAREHOUSE_ROW = ""
            _WAREHOUSE_SHELF = ""
            _WAREHOUSE_DEEP = ""
            _WAREHOUSE_TYPE_ID = Nothing
            _ACTIVE_STATUS = "Y"
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into MS_Warehouse table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to MS_Warehouse table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _Warehouse_ID > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("WAREHOUSE_ID = @_WAREHOUSE_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to MS_Warehouse table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from MS_Warehouse table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cWAREHOUSE_ID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_WAREHOUSE_ID", cWAREHOUSE_ID)
                Return doDelete("WAREHOUSE_ID = @_WAREHOUSE_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of MS_Warehouse by specified WAREHOUSE_ID key is retrieved successfully.
        '/// <param name=cWAREHOUSE_ID>The WAREHOUSE_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cWAREHOUSE_ID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_WAREHOUSE_ID", cWAREHOUSE_ID)
            Return doChkData("WAREHOUSE_ID = @_WAREHOUSE_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of MS_Warehouse by specified WAREHOUSE_ID key is retrieved successfully.
        '/// <param name=cWAREHOUSE_ID>The WAREHOUSE_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cWAREHOUSE_ID As Long, trans As SQLTransaction) As MsWarehouseLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_WAREHOUSE_ID", cWAREHOUSE_ID)
            Return doGetData("WAREHOUSE_ID = @_WAREHOUSE_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of MS_Warehouse by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into MS_Warehouse table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _WAREHOUSE_ID = dt.Rows(0)("WAREHOUSE_ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                        ret.NewID = _WAREHOUSE_ID
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to MS_Warehouse table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                            ret.NewID = _WAREHOUSE_ID
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from MS_Warehouse table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(17) As SqlParameter
            cmbParam(0) = New SqlParameter("@_WAREHOUSE_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _WAREHOUSE_ID

            cmbParam(1) = New SqlParameter("@_WAREHOUSE_CODE", SqlDbType.VarChar)
            cmbParam(1).Value = _WAREHOUSE_CODE.Trim

            cmbParam(2) = New SqlParameter("@_WAREHOUSE_NAME", SqlDbType.VarChar)
            If _WAREHOUSE_NAME.Trim <> "" Then 
                cmbParam(2).Value = _WAREHOUSE_NAME.Trim
            Else
                cmbParam(2).Value = DBNull.value
            End If

            cmbParam(3) = New SqlParameter("@_WAREHOUSE_SIZE", SqlDbType.VarChar)
            If _WAREHOUSE_SIZE.Trim <> "" Then 
                cmbParam(3).Value = _WAREHOUSE_SIZE.Trim
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_WAREHOUSE_BUILDING", SqlDbType.VarChar)
            If _WAREHOUSE_BUILDING.Trim <> "" Then 
                cmbParam(4).Value = _WAREHOUSE_BUILDING.Trim
            Else
                cmbParam(4).Value = DBNull.value
            End If

            cmbParam(5) = New SqlParameter("@_WAREHOUSE_FLOOR", SqlDbType.VarChar)
            If _WAREHOUSE_FLOOR.Trim <> "" Then 
                cmbParam(5).Value = _WAREHOUSE_FLOOR.Trim
            Else
                cmbParam(5).Value = DBNull.value
            End If

            cmbParam(6) = New SqlParameter("@_WAREHOUSE_ROOM", SqlDbType.VarChar)
            If _WAREHOUSE_ROOM.Trim <> "" Then 
                cmbParam(6).Value = _WAREHOUSE_ROOM.Trim
            Else
                cmbParam(6).Value = DBNull.value
            End If

            cmbParam(7) = New SqlParameter("@_WAREHOUSE_ZONE", SqlDbType.VarChar)
            If _WAREHOUSE_ZONE.Trim <> "" Then 
                cmbParam(7).Value = _WAREHOUSE_ZONE.Trim
            Else
                cmbParam(7).Value = DBNull.value
            End If

            cmbParam(8) = New SqlParameter("@_WAREHOUSE_LOCK", SqlDbType.VarChar)
            If _WAREHOUSE_LOCK.Trim <> "" Then 
                cmbParam(8).Value = _WAREHOUSE_LOCK.Trim
            Else
                cmbParam(8).Value = DBNull.value
            End If

            cmbParam(9) = New SqlParameter("@_WAREHOUSE_ROW", SqlDbType.VarChar)
            If _WAREHOUSE_ROW.Trim <> "" Then 
                cmbParam(9).Value = _WAREHOUSE_ROW.Trim
            Else
                cmbParam(9).Value = DBNull.value
            End If

            cmbParam(10) = New SqlParameter("@_WAREHOUSE_SHELF", SqlDbType.VarChar)
            If _WAREHOUSE_SHELF.Trim <> "" Then 
                cmbParam(10).Value = _WAREHOUSE_SHELF.Trim
            Else
                cmbParam(10).Value = DBNull.value
            End If

            cmbParam(11) = New SqlParameter("@_WAREHOUSE_DEEP", SqlDbType.VarChar)
            If _WAREHOUSE_DEEP.Trim <> "" Then 
                cmbParam(11).Value = _WAREHOUSE_DEEP.Trim
            Else
                cmbParam(11).Value = DBNull.value
            End If

            cmbParam(12) = New SqlParameter("@_WAREHOUSE_TYPE_ID", SqlDbType.Int)
            If _WAREHOUSE_TYPE_ID IsNot Nothing Then 
                cmbParam(12).Value = _WAREHOUSE_TYPE_ID.Value
            Else
                cmbParam(12).Value = DBNull.value
            End IF

            cmbParam(13) = New SqlParameter("@_ACTIVE_STATUS", SqlDbType.Char)
            cmbParam(13).Value = _ACTIVE_STATUS

            cmbParam(14) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            cmbParam(14).Value = _CREATED_BY.Trim

            cmbParam(15) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            cmbParam(15).Value = _CREATED_DATE

            cmbParam(16) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(16).Value = _UPDATED_BY.Trim
            Else
                cmbParam(16).Value = DBNull.value
            End If

            cmbParam(17) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(17).Value = _UPDATED_DATE.Value
            Else
                cmbParam(17).Value = DBNull.value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of MS_Warehouse by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Warehouse_ID")) = False Then _Warehouse_ID = Convert.ToInt64(Rdr("Warehouse_ID"))
                        If Convert.IsDBNull(Rdr("Warehouse_Code")) = False Then _Warehouse_Code = Rdr("Warehouse_Code").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Name")) = False Then _Warehouse_Name = Rdr("Warehouse_Name").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Size")) = False Then _Warehouse_Size = Rdr("Warehouse_Size").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Building")) = False Then _Warehouse_Building = Rdr("Warehouse_Building").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Floor")) = False Then _Warehouse_Floor = Rdr("Warehouse_Floor").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Room")) = False Then _Warehouse_Room = Rdr("Warehouse_Room").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Zone")) = False Then _Warehouse_Zone = Rdr("Warehouse_Zone").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Lock")) = False Then _Warehouse_Lock = Rdr("Warehouse_Lock").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Row")) = False Then _Warehouse_Row = Rdr("Warehouse_Row").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Shelf")) = False Then _Warehouse_Shelf = Rdr("Warehouse_Shelf").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Deep")) = False Then _Warehouse_Deep = Rdr("Warehouse_Deep").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Type_ID")) = False Then _Warehouse_Type_ID = Convert.ToInt32(Rdr("Warehouse_Type_ID"))
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _Active_Status = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of MS_Warehouse by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As MsWarehouseLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Warehouse_ID")) = False Then _Warehouse_ID = Convert.ToInt64(Rdr("Warehouse_ID"))
                        If Convert.IsDBNull(Rdr("Warehouse_Code")) = False Then _Warehouse_Code = Rdr("Warehouse_Code").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Name")) = False Then _Warehouse_Name = Rdr("Warehouse_Name").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Size")) = False Then _Warehouse_Size = Rdr("Warehouse_Size").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Building")) = False Then _Warehouse_Building = Rdr("Warehouse_Building").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Floor")) = False Then _Warehouse_Floor = Rdr("Warehouse_Floor").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Room")) = False Then _Warehouse_Room = Rdr("Warehouse_Room").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Zone")) = False Then _Warehouse_Zone = Rdr("Warehouse_Zone").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Lock")) = False Then _Warehouse_Lock = Rdr("Warehouse_Lock").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Row")) = False Then _Warehouse_Row = Rdr("Warehouse_Row").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Shelf")) = False Then _Warehouse_Shelf = Rdr("Warehouse_Shelf").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Deep")) = False Then _Warehouse_Deep = Rdr("Warehouse_Deep").ToString()
                        If Convert.IsDBNull(Rdr("Warehouse_Type_ID")) = False Then _Warehouse_Type_ID = Convert.ToInt32(Rdr("Warehouse_Type_ID"))
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _Active_Status = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table MS_Warehouse
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (WAREHOUSE_CODE, WAREHOUSE_NAME, WAREHOUSE_SIZE, WAREHOUSE_BUILDING, WAREHOUSE_FLOOR, WAREHOUSE_ROOM, WAREHOUSE_ZONE, WAREHOUSE_LOCK, WAREHOUSE_ROW, WAREHOUSE_SHELF, WAREHOUSE_DEEP, WAREHOUSE_TYPE_ID, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE)"
                Sql += " OUTPUT INSERTED.WAREHOUSE_ID, INSERTED.WAREHOUSE_CODE, INSERTED.WAREHOUSE_NAME, INSERTED.WAREHOUSE_SIZE, INSERTED.WAREHOUSE_BUILDING, INSERTED.WAREHOUSE_FLOOR, INSERTED.WAREHOUSE_ROOM, INSERTED.WAREHOUSE_ZONE, INSERTED.WAREHOUSE_LOCK, INSERTED.WAREHOUSE_ROW, INSERTED.WAREHOUSE_SHELF, INSERTED.WAREHOUSE_DEEP, INSERTED.WAREHOUSE_TYPE_ID, INSERTED.ACTIVE_STATUS, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE"
                Sql += " VALUES("
                sql += "@_WAREHOUSE_CODE" & ", "
                sql += "@_WAREHOUSE_NAME" & ", "
                sql += "@_WAREHOUSE_SIZE" & ", "
                sql += "@_WAREHOUSE_BUILDING" & ", "
                sql += "@_WAREHOUSE_FLOOR" & ", "
                sql += "@_WAREHOUSE_ROOM" & ", "
                sql += "@_WAREHOUSE_ZONE" & ", "
                sql += "@_WAREHOUSE_LOCK" & ", "
                sql += "@_WAREHOUSE_ROW" & ", "
                sql += "@_WAREHOUSE_SHELF" & ", "
                sql += "@_WAREHOUSE_DEEP" & ", "
                sql += "@_WAREHOUSE_TYPE_ID" & ", "
                sql += "@_ACTIVE_STATUS" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE" & ", "
                sql += "@_UPDATED_BY" & ", "
                sql += "@_UPDATED_DATE"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table MS_Warehouse
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "WAREHOUSE_CODE = " & "@_WAREHOUSE_CODE" & ", "
                Sql += "WAREHOUSE_NAME = " & "@_WAREHOUSE_NAME" & ", "
                Sql += "WAREHOUSE_SIZE = " & "@_WAREHOUSE_SIZE" & ", "
                Sql += "WAREHOUSE_BUILDING = " & "@_WAREHOUSE_BUILDING" & ", "
                Sql += "WAREHOUSE_FLOOR = " & "@_WAREHOUSE_FLOOR" & ", "
                Sql += "WAREHOUSE_ROOM = " & "@_WAREHOUSE_ROOM" & ", "
                Sql += "WAREHOUSE_ZONE = " & "@_WAREHOUSE_ZONE" & ", "
                Sql += "WAREHOUSE_LOCK = " & "@_WAREHOUSE_LOCK" & ", "
                Sql += "WAREHOUSE_ROW = " & "@_WAREHOUSE_ROW" & ", "
                Sql += "WAREHOUSE_SHELF = " & "@_WAREHOUSE_SHELF" & ", "
                Sql += "WAREHOUSE_DEEP = " & "@_WAREHOUSE_DEEP" & ", "
                Sql += "WAREHOUSE_TYPE_ID = " & "@_WAREHOUSE_TYPE_ID" & ", "
                Sql += "ACTIVE_STATUS = " & "@_ACTIVE_STATUS" & ", "
                Sql += "CREATED_BY = " & "@_CREATED_BY" & ", "
                Sql += "CREATED_DATE = " & "@_CREATED_DATE" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table MS_Warehouse
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table MS_Warehouse
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT WAREHOUSE_ID, WAREHOUSE_CODE, WAREHOUSE_NAME, WAREHOUSE_SIZE, WAREHOUSE_BUILDING, WAREHOUSE_FLOOR, WAREHOUSE_ROOM, WAREHOUSE_ZONE, WAREHOUSE_LOCK, WAREHOUSE_ROW, WAREHOUSE_SHELF, WAREHOUSE_DEEP, WAREHOUSE_TYPE_ID, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
