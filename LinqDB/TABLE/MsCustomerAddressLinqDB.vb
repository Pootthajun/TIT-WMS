Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for MS_Customer_Address table LinqDB.
    '[Create by  on May, 30 2017]
    Public Class MsCustomerAddressLinqDB
        Public sub MsCustomerAddressLinqDB()

        End Sub 
        ' MS_Customer_Address
        Const _tableName As String = "MS_Customer_Address"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _CUSTOMER_ADDRESS_ID As Long = 0
        Dim _CUSTOMER_ID As Long = 0
        Dim _HOUSE_NO As  String  = ""
        Dim _BUILDING_NAME As  String  = ""
        Dim _MOO As  String  = ""
        Dim _SOI_NAME As  String  = ""
        Dim _ROAD_NAME As  String  = ""
        Dim _TUMBON_ID As  System.Nullable(Of Long) 
        Dim _DISTRICT_ID As  System.Nullable(Of Long) 
        Dim _PROVINCE_ID As  System.Nullable(Of Long) 
        Dim _COUNTRY_ID As  System.Nullable(Of Long) 
        Dim _POSTCODE As  String  = ""
        Dim _BRANCH_NO As  String  = ""
        Dim _ACTIVE_STATUS As Char = "Y"
        Dim _CREATED_BY As String = ""
        Dim _CREATED_DATE As DateTime = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)

        'Generate Field Property 
        <Column(Storage:="_CUSTOMER_ADDRESS_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property CUSTOMER_ADDRESS_ID() As Long
            Get
                Return _CUSTOMER_ADDRESS_ID
            End Get
            Set(ByVal value As Long)
               _CUSTOMER_ADDRESS_ID = value
            End Set
        End Property 
        <Column(Storage:="_CUSTOMER_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property CUSTOMER_ID() As Long
            Get
                Return _CUSTOMER_ID
            End Get
            Set(ByVal value As Long)
               _CUSTOMER_ID = value
            End Set
        End Property 
        <Column(Storage:="_HOUSE_NO", DbType:="VarChar(50)")>  _
        Public Property HOUSE_NO() As  String 
            Get
                Return _HOUSE_NO
            End Get
            Set(ByVal value As  String )
               _HOUSE_NO = value
            End Set
        End Property 
        <Column(Storage:="_BUILDING_NAME", DbType:="VarChar(255)")>  _
        Public Property BUILDING_NAME() As  String 
            Get
                Return _BUILDING_NAME
            End Get
            Set(ByVal value As  String )
               _BUILDING_NAME = value
            End Set
        End Property 
        <Column(Storage:="_MOO", DbType:="VarChar(50)")>  _
        Public Property MOO() As  String 
            Get
                Return _MOO
            End Get
            Set(ByVal value As  String )
               _MOO = value
            End Set
        End Property 
        <Column(Storage:="_SOI_NAME", DbType:="VarChar(255)")>  _
        Public Property SOI_NAME() As  String 
            Get
                Return _SOI_NAME
            End Get
            Set(ByVal value As  String )
               _SOI_NAME = value
            End Set
        End Property 
        <Column(Storage:="_ROAD_NAME", DbType:="VarChar(255)")>  _
        Public Property ROAD_NAME() As  String 
            Get
                Return _ROAD_NAME
            End Get
            Set(ByVal value As  String )
               _ROAD_NAME = value
            End Set
        End Property 
        <Column(Storage:="_TUMBON_ID", DbType:="BigInt")>  _
        Public Property TUMBON_ID() As  System.Nullable(Of Long) 
            Get
                Return _TUMBON_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _TUMBON_ID = value
            End Set
        End Property 
        <Column(Storage:="_DISTRICT_ID", DbType:="BigInt")>  _
        Public Property DISTRICT_ID() As  System.Nullable(Of Long) 
            Get
                Return _DISTRICT_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _DISTRICT_ID = value
            End Set
        End Property 
        <Column(Storage:="_PROVINCE_ID", DbType:="BigInt")>  _
        Public Property PROVINCE_ID() As  System.Nullable(Of Long) 
            Get
                Return _PROVINCE_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _PROVINCE_ID = value
            End Set
        End Property 
        <Column(Storage:="_COUNTRY_ID", DbType:="BigInt")>  _
        Public Property COUNTRY_ID() As  System.Nullable(Of Long) 
            Get
                Return _COUNTRY_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _COUNTRY_ID = value
            End Set
        End Property 
        <Column(Storage:="_POSTCODE", DbType:="VarChar(50)")>  _
        Public Property POSTCODE() As  String 
            Get
                Return _POSTCODE
            End Get
            Set(ByVal value As  String )
               _POSTCODE = value
            End Set
        End Property 
        <Column(Storage:="_BRANCH_NO", DbType:="VarChar(50)")>  _
        Public Property BRANCH_NO() As  String 
            Get
                Return _BRANCH_NO
            End Get
            Set(ByVal value As  String )
               _BRANCH_NO = value
            End Set
        End Property 
        <Column(Storage:="_ACTIVE_STATUS", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ACTIVE_STATUS() As Char
            Get
                Return _ACTIVE_STATUS
            End Get
            Set(ByVal value As Char)
               _ACTIVE_STATUS = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_BY() As String
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As String)
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_DATE() As DateTime
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As DateTime)
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _CUSTOMER_ADDRESS_ID = 0
            _CUSTOMER_ID = 0
            _HOUSE_NO = ""
            _BUILDING_NAME = ""
            _MOO = ""
            _SOI_NAME = ""
            _ROAD_NAME = ""
            _TUMBON_ID = Nothing
            _DISTRICT_ID = Nothing
            _PROVINCE_ID = Nothing
            _COUNTRY_ID = Nothing
            _POSTCODE = ""
            _BRANCH_NO = ""
            _ACTIVE_STATUS = "Y"
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into MS_Customer_Address table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to MS_Customer_Address table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _Customer_Address_ID > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("CUSTOMER_ADDRESS_ID = @_CUSTOMER_ADDRESS_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to MS_Customer_Address table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from MS_Customer_Address table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cCUSTOMER_ADDRESS_ID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_CUSTOMER_ADDRESS_ID", cCUSTOMER_ADDRESS_ID)
                Return doDelete("CUSTOMER_ADDRESS_ID = @_CUSTOMER_ADDRESS_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of MS_Customer_Address by specified CUSTOMER_ADDRESS_ID key is retrieved successfully.
        '/// <param name=cCUSTOMER_ADDRESS_ID>The CUSTOMER_ADDRESS_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cCUSTOMER_ADDRESS_ID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_CUSTOMER_ADDRESS_ID", cCUSTOMER_ADDRESS_ID)
            Return doChkData("CUSTOMER_ADDRESS_ID = @_CUSTOMER_ADDRESS_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of MS_Customer_Address by specified CUSTOMER_ADDRESS_ID key is retrieved successfully.
        '/// <param name=cCUSTOMER_ADDRESS_ID>The CUSTOMER_ADDRESS_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cCUSTOMER_ADDRESS_ID As Long, trans As SQLTransaction) As MsCustomerAddressLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_CUSTOMER_ADDRESS_ID", cCUSTOMER_ADDRESS_ID)
            Return doGetData("CUSTOMER_ADDRESS_ID = @_CUSTOMER_ADDRESS_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of MS_Customer_Address by specified CUSTOMER_ID key is retrieved successfully.
        '/// <param name=cCUSTOMER_ID>The CUSTOMER_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByCUSTOMER_ID(cCUSTOMER_ID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_CUSTOMER_ID", cCUSTOMER_ID) 
            Return doChkData("CUSTOMER_ID = @_CUSTOMER_ID", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of MS_Customer_Address by specified CUSTOMER_ID key is retrieved successfully.
        '/// <param name=cCUSTOMER_ID>The CUSTOMER_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByCUSTOMER_ID(cCUSTOMER_ID As Long, cCUSTOMER_ADDRESS_ID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_CUSTOMER_ID", cCUSTOMER_ID) 
            cmdPara(1) = DB.SetBigInt("@_CUSTOMER_ADDRESS_ID", cCUSTOMER_ADDRESS_ID) 
            Return doChkData("CUSTOMER_ID = @_CUSTOMER_ID And CUSTOMER_ADDRESS_ID <> @_CUSTOMER_ADDRESS_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of MS_Customer_Address by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into MS_Customer_Address table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _CUSTOMER_ADDRESS_ID = dt.Rows(0)("CUSTOMER_ADDRESS_ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                        ret.NewID = _CUSTOMER_ADDRESS_ID
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to MS_Customer_Address table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                            ret.NewID = _CUSTOMER_ADDRESS_ID
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from MS_Customer_Address table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(17) As SqlParameter
            cmbParam(0) = New SqlParameter("@_CUSTOMER_ADDRESS_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _CUSTOMER_ADDRESS_ID

            cmbParam(1) = New SqlParameter("@_CUSTOMER_ID", SqlDbType.BigInt)
            cmbParam(1).Value = _CUSTOMER_ID

            cmbParam(2) = New SqlParameter("@_HOUSE_NO", SqlDbType.VarChar)
            If _HOUSE_NO.Trim <> "" Then 
                cmbParam(2).Value = _HOUSE_NO.Trim
            Else
                cmbParam(2).Value = DBNull.value
            End If

            cmbParam(3) = New SqlParameter("@_BUILDING_NAME", SqlDbType.VarChar)
            If _BUILDING_NAME.Trim <> "" Then 
                cmbParam(3).Value = _BUILDING_NAME.Trim
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_MOO", SqlDbType.VarChar)
            If _MOO.Trim <> "" Then 
                cmbParam(4).Value = _MOO.Trim
            Else
                cmbParam(4).Value = DBNull.value
            End If

            cmbParam(5) = New SqlParameter("@_SOI_NAME", SqlDbType.VarChar)
            If _SOI_NAME.Trim <> "" Then 
                cmbParam(5).Value = _SOI_NAME.Trim
            Else
                cmbParam(5).Value = DBNull.value
            End If

            cmbParam(6) = New SqlParameter("@_ROAD_NAME", SqlDbType.VarChar)
            If _ROAD_NAME.Trim <> "" Then 
                cmbParam(6).Value = _ROAD_NAME.Trim
            Else
                cmbParam(6).Value = DBNull.value
            End If

            cmbParam(7) = New SqlParameter("@_TUMBON_ID", SqlDbType.BigInt)
            If _TUMBON_ID IsNot Nothing Then 
                cmbParam(7).Value = _TUMBON_ID.Value
            Else
                cmbParam(7).Value = DBNull.value
            End IF

            cmbParam(8) = New SqlParameter("@_DISTRICT_ID", SqlDbType.BigInt)
            If _DISTRICT_ID IsNot Nothing Then 
                cmbParam(8).Value = _DISTRICT_ID.Value
            Else
                cmbParam(8).Value = DBNull.value
            End IF

            cmbParam(9) = New SqlParameter("@_PROVINCE_ID", SqlDbType.BigInt)
            If _PROVINCE_ID IsNot Nothing Then 
                cmbParam(9).Value = _PROVINCE_ID.Value
            Else
                cmbParam(9).Value = DBNull.value
            End IF

            cmbParam(10) = New SqlParameter("@_COUNTRY_ID", SqlDbType.BigInt)
            If _COUNTRY_ID IsNot Nothing Then 
                cmbParam(10).Value = _COUNTRY_ID.Value
            Else
                cmbParam(10).Value = DBNull.value
            End IF

            cmbParam(11) = New SqlParameter("@_POSTCODE", SqlDbType.VarChar)
            If _POSTCODE.Trim <> "" Then 
                cmbParam(11).Value = _POSTCODE.Trim
            Else
                cmbParam(11).Value = DBNull.value
            End If

            cmbParam(12) = New SqlParameter("@_BRANCH_NO", SqlDbType.VarChar)
            If _BRANCH_NO.Trim <> "" Then 
                cmbParam(12).Value = _BRANCH_NO.Trim
            Else
                cmbParam(12).Value = DBNull.value
            End If

            cmbParam(13) = New SqlParameter("@_ACTIVE_STATUS", SqlDbType.Char)
            cmbParam(13).Value = _ACTIVE_STATUS

            cmbParam(14) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            cmbParam(14).Value = _CREATED_BY.Trim

            cmbParam(15) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            cmbParam(15).Value = _CREATED_DATE

            cmbParam(16) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(16).Value = _UPDATED_BY.Trim
            Else
                cmbParam(16).Value = DBNull.value
            End If

            cmbParam(17) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(17).Value = _UPDATED_DATE.Value
            Else
                cmbParam(17).Value = DBNull.value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of MS_Customer_Address by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Customer_Address_ID")) = False Then _Customer_Address_ID = Convert.ToInt64(Rdr("Customer_Address_ID"))
                        If Convert.IsDBNull(Rdr("Customer_ID")) = False Then _Customer_ID = Convert.ToInt64(Rdr("Customer_ID"))
                        If Convert.IsDBNull(Rdr("House_No")) = False Then _House_No = Rdr("House_No").ToString()
                        If Convert.IsDBNull(Rdr("Building_Name")) = False Then _Building_Name = Rdr("Building_Name").ToString()
                        If Convert.IsDBNull(Rdr("moo")) = False Then _moo = Rdr("moo").ToString()
                        If Convert.IsDBNull(Rdr("soi_name")) = False Then _soi_name = Rdr("soi_name").ToString()
                        If Convert.IsDBNull(Rdr("road_name")) = False Then _road_name = Rdr("road_name").ToString()
                        If Convert.IsDBNull(Rdr("Tumbon_ID")) = False Then _Tumbon_ID = Convert.ToInt64(Rdr("Tumbon_ID"))
                        If Convert.IsDBNull(Rdr("District_ID")) = False Then _District_ID = Convert.ToInt64(Rdr("District_ID"))
                        If Convert.IsDBNull(Rdr("Province_ID")) = False Then _Province_ID = Convert.ToInt64(Rdr("Province_ID"))
                        If Convert.IsDBNull(Rdr("Country_ID")) = False Then _Country_ID = Convert.ToInt64(Rdr("Country_ID"))
                        If Convert.IsDBNull(Rdr("Postcode")) = False Then _Postcode = Rdr("Postcode").ToString()
                        If Convert.IsDBNull(Rdr("Branch_No")) = False Then _Branch_No = Rdr("Branch_No").ToString()
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _Active_Status = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of MS_Customer_Address by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As MsCustomerAddressLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Customer_Address_ID")) = False Then _Customer_Address_ID = Convert.ToInt64(Rdr("Customer_Address_ID"))
                        If Convert.IsDBNull(Rdr("Customer_ID")) = False Then _Customer_ID = Convert.ToInt64(Rdr("Customer_ID"))
                        If Convert.IsDBNull(Rdr("House_No")) = False Then _House_No = Rdr("House_No").ToString()
                        If Convert.IsDBNull(Rdr("Building_Name")) = False Then _Building_Name = Rdr("Building_Name").ToString()
                        If Convert.IsDBNull(Rdr("moo")) = False Then _moo = Rdr("moo").ToString()
                        If Convert.IsDBNull(Rdr("soi_name")) = False Then _soi_name = Rdr("soi_name").ToString()
                        If Convert.IsDBNull(Rdr("road_name")) = False Then _road_name = Rdr("road_name").ToString()
                        If Convert.IsDBNull(Rdr("Tumbon_ID")) = False Then _Tumbon_ID = Convert.ToInt64(Rdr("Tumbon_ID"))
                        If Convert.IsDBNull(Rdr("District_ID")) = False Then _District_ID = Convert.ToInt64(Rdr("District_ID"))
                        If Convert.IsDBNull(Rdr("Province_ID")) = False Then _Province_ID = Convert.ToInt64(Rdr("Province_ID"))
                        If Convert.IsDBNull(Rdr("Country_ID")) = False Then _Country_ID = Convert.ToInt64(Rdr("Country_ID"))
                        If Convert.IsDBNull(Rdr("Postcode")) = False Then _Postcode = Rdr("Postcode").ToString()
                        If Convert.IsDBNull(Rdr("Branch_No")) = False Then _Branch_No = Rdr("Branch_No").ToString()
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _Active_Status = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table MS_Customer_Address
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (CUSTOMER_ID, HOUSE_NO, BUILDING_NAME, MOO, SOI_NAME, ROAD_NAME, TUMBON_ID, DISTRICT_ID, PROVINCE_ID, COUNTRY_ID, POSTCODE, BRANCH_NO, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE)"
                Sql += " OUTPUT INSERTED.CUSTOMER_ADDRESS_ID, INSERTED.CUSTOMER_ID, INSERTED.HOUSE_NO, INSERTED.BUILDING_NAME, INSERTED.MOO, INSERTED.SOI_NAME, INSERTED.ROAD_NAME, INSERTED.TUMBON_ID, INSERTED.DISTRICT_ID, INSERTED.PROVINCE_ID, INSERTED.COUNTRY_ID, INSERTED.POSTCODE, INSERTED.BRANCH_NO, INSERTED.ACTIVE_STATUS, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE"
                Sql += " VALUES("
                sql += "@_CUSTOMER_ID" & ", "
                sql += "@_HOUSE_NO" & ", "
                sql += "@_BUILDING_NAME" & ", "
                sql += "@_MOO" & ", "
                sql += "@_SOI_NAME" & ", "
                sql += "@_ROAD_NAME" & ", "
                sql += "@_TUMBON_ID" & ", "
                sql += "@_DISTRICT_ID" & ", "
                sql += "@_PROVINCE_ID" & ", "
                sql += "@_COUNTRY_ID" & ", "
                sql += "@_POSTCODE" & ", "
                sql += "@_BRANCH_NO" & ", "
                sql += "@_ACTIVE_STATUS" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE" & ", "
                sql += "@_UPDATED_BY" & ", "
                sql += "@_UPDATED_DATE"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table MS_Customer_Address
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "CUSTOMER_ID = " & "@_CUSTOMER_ID" & ", "
                Sql += "HOUSE_NO = " & "@_HOUSE_NO" & ", "
                Sql += "BUILDING_NAME = " & "@_BUILDING_NAME" & ", "
                Sql += "MOO = " & "@_MOO" & ", "
                Sql += "SOI_NAME = " & "@_SOI_NAME" & ", "
                Sql += "ROAD_NAME = " & "@_ROAD_NAME" & ", "
                Sql += "TUMBON_ID = " & "@_TUMBON_ID" & ", "
                Sql += "DISTRICT_ID = " & "@_DISTRICT_ID" & ", "
                Sql += "PROVINCE_ID = " & "@_PROVINCE_ID" & ", "
                Sql += "COUNTRY_ID = " & "@_COUNTRY_ID" & ", "
                Sql += "POSTCODE = " & "@_POSTCODE" & ", "
                Sql += "BRANCH_NO = " & "@_BRANCH_NO" & ", "
                Sql += "ACTIVE_STATUS = " & "@_ACTIVE_STATUS" & ", "
                Sql += "CREATED_BY = " & "@_CREATED_BY" & ", "
                Sql += "CREATED_DATE = " & "@_CREATED_DATE" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table MS_Customer_Address
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table MS_Customer_Address
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT CUSTOMER_ADDRESS_ID, CUSTOMER_ID, HOUSE_NO, BUILDING_NAME, MOO, SOI_NAME, ROAD_NAME, TUMBON_ID, DISTRICT_ID, PROVINCE_ID, COUNTRY_ID, POSTCODE, BRANCH_NO, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
