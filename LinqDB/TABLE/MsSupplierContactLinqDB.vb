Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for MS_Supplier_Contact table LinqDB.
    '[Create by  on June, 5 2017]
    Public Class MsSupplierContactLinqDB
        Public sub MsSupplierContactLinqDB()

        End Sub 
        ' MS_Supplier_Contact
        Const _tableName As String = "MS_Supplier_Contact"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _SUPPLIER_CONTACT_ID As Long = 0
        Dim _SUPPLIER_ID As Long = 0
        Dim _PREFIX_ID As  System.Nullable(Of Long) 
        Dim _FIRST_NAME As  String  = ""
        Dim _LAST_NAME As  String  = ""
        Dim _EMAIL As  String  = ""
        Dim _TELEPHONENO As  String  = ""
        Dim _MOBILENO As  String  = ""
        Dim _FAXNO As  String  = ""
        Dim _ACTIVE_STATUS As Char = "Y"
        Dim _CREATED_BY As String = ""
        Dim _CREATED_DATE As DateTime = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)

        'Generate Field Property 
        <Column(Storage:="_SUPPLIER_CONTACT_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property SUPPLIER_CONTACT_ID() As Long
            Get
                Return _SUPPLIER_CONTACT_ID
            End Get
            Set(ByVal value As Long)
               _SUPPLIER_CONTACT_ID = value
            End Set
        End Property 
        <Column(Storage:="_SUPPLIER_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property SUPPLIER_ID() As Long
            Get
                Return _SUPPLIER_ID
            End Get
            Set(ByVal value As Long)
               _SUPPLIER_ID = value
            End Set
        End Property 
        <Column(Storage:="_PREFIX_ID", DbType:="BigInt")>  _
        Public Property PREFIX_ID() As  System.Nullable(Of Long) 
            Get
                Return _PREFIX_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _PREFIX_ID = value
            End Set
        End Property 
        <Column(Storage:="_FIRST_NAME", DbType:="VarChar(100)")>  _
        Public Property FIRST_NAME() As  String 
            Get
                Return _FIRST_NAME
            End Get
            Set(ByVal value As  String )
               _FIRST_NAME = value
            End Set
        End Property 
        <Column(Storage:="_LAST_NAME", DbType:="VarChar(100)")>  _
        Public Property LAST_NAME() As  String 
            Get
                Return _LAST_NAME
            End Get
            Set(ByVal value As  String )
               _LAST_NAME = value
            End Set
        End Property 
        <Column(Storage:="_EMAIL", DbType:="VarChar(255)")>  _
        Public Property EMAIL() As  String 
            Get
                Return _EMAIL
            End Get
            Set(ByVal value As  String )
               _EMAIL = value
            End Set
        End Property 
        <Column(Storage:="_TELEPHONENO", DbType:="VarChar(50)")>  _
        Public Property TELEPHONENO() As  String 
            Get
                Return _TELEPHONENO
            End Get
            Set(ByVal value As  String )
               _TELEPHONENO = value
            End Set
        End Property 
        <Column(Storage:="_MOBILENO", DbType:="VarChar(50)")>  _
        Public Property MOBILENO() As  String 
            Get
                Return _MOBILENO
            End Get
            Set(ByVal value As  String )
               _MOBILENO = value
            End Set
        End Property 
        <Column(Storage:="_FAXNO", DbType:="VarChar(50)")>  _
        Public Property FAXNO() As  String 
            Get
                Return _FAXNO
            End Get
            Set(ByVal value As  String )
               _FAXNO = value
            End Set
        End Property 
        <Column(Storage:="_ACTIVE_STATUS", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ACTIVE_STATUS() As Char
            Get
                Return _ACTIVE_STATUS
            End Get
            Set(ByVal value As Char)
               _ACTIVE_STATUS = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_BY() As String
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As String)
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_DATE() As DateTime
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As DateTime)
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _SUPPLIER_CONTACT_ID = 0
            _SUPPLIER_ID = 0
            _PREFIX_ID = Nothing
            _FIRST_NAME = ""
            _LAST_NAME = ""
            _EMAIL = ""
            _TELEPHONENO = ""
            _MOBILENO = ""
            _FAXNO = ""
            _ACTIVE_STATUS = "Y"
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into MS_Supplier_Contact table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to MS_Supplier_Contact table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _Supplier_Contact_ID > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("SUPPLIER_CONTACT_ID = @_SUPPLIER_CONTACT_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to MS_Supplier_Contact table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from MS_Supplier_Contact table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cSUPPLIER_CONTACT_ID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_SUPPLIER_CONTACT_ID", cSUPPLIER_CONTACT_ID)
                Return doDelete("SUPPLIER_CONTACT_ID = @_SUPPLIER_CONTACT_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of MS_Supplier_Contact by specified SUPPLIER_CONTACT_ID key is retrieved successfully.
        '/// <param name=cSUPPLIER_CONTACT_ID>The SUPPLIER_CONTACT_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cSUPPLIER_CONTACT_ID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_SUPPLIER_CONTACT_ID", cSUPPLIER_CONTACT_ID)
            Return doChkData("SUPPLIER_CONTACT_ID = @_SUPPLIER_CONTACT_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of MS_Supplier_Contact by specified SUPPLIER_CONTACT_ID key is retrieved successfully.
        '/// <param name=cSUPPLIER_CONTACT_ID>The SUPPLIER_CONTACT_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cSUPPLIER_CONTACT_ID As Long, trans As SQLTransaction) As MsSupplierContactLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_SUPPLIER_CONTACT_ID", cSUPPLIER_CONTACT_ID)
            Return doGetData("SUPPLIER_CONTACT_ID = @_SUPPLIER_CONTACT_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of MS_Supplier_Contact by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into MS_Supplier_Contact table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _SUPPLIER_CONTACT_ID = dt.Rows(0)("SUPPLIER_CONTACT_ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                        ret.NewID = _SUPPLIER_CONTACT_ID
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to MS_Supplier_Contact table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                            ret.NewID = _SUPPLIER_CONTACT_ID
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from MS_Supplier_Contact table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(13) As SqlParameter
            cmbParam(0) = New SqlParameter("@_SUPPLIER_CONTACT_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _SUPPLIER_CONTACT_ID

            cmbParam(1) = New SqlParameter("@_SUPPLIER_ID", SqlDbType.BigInt)
            cmbParam(1).Value = _SUPPLIER_ID

            cmbParam(2) = New SqlParameter("@_PREFIX_ID", SqlDbType.BigInt)
            If _PREFIX_ID IsNot Nothing Then 
                cmbParam(2).Value = _PREFIX_ID.Value
            Else
                cmbParam(2).Value = DBNull.value
            End IF

            cmbParam(3) = New SqlParameter("@_FIRST_NAME", SqlDbType.VarChar)
            If _FIRST_NAME.Trim <> "" Then 
                cmbParam(3).Value = _FIRST_NAME.Trim
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_LAST_NAME", SqlDbType.VarChar)
            If _LAST_NAME.Trim <> "" Then 
                cmbParam(4).Value = _LAST_NAME.Trim
            Else
                cmbParam(4).Value = DBNull.value
            End If

            cmbParam(5) = New SqlParameter("@_EMAIL", SqlDbType.VarChar)
            If _EMAIL.Trim <> "" Then 
                cmbParam(5).Value = _EMAIL.Trim
            Else
                cmbParam(5).Value = DBNull.value
            End If

            cmbParam(6) = New SqlParameter("@_TELEPHONENO", SqlDbType.VarChar)
            If _TELEPHONENO.Trim <> "" Then 
                cmbParam(6).Value = _TELEPHONENO.Trim
            Else
                cmbParam(6).Value = DBNull.value
            End If

            cmbParam(7) = New SqlParameter("@_MOBILENO", SqlDbType.VarChar)
            If _MOBILENO.Trim <> "" Then 
                cmbParam(7).Value = _MOBILENO.Trim
            Else
                cmbParam(7).Value = DBNull.value
            End If

            cmbParam(8) = New SqlParameter("@_FAXNO", SqlDbType.VarChar)
            If _FAXNO.Trim <> "" Then 
                cmbParam(8).Value = _FAXNO.Trim
            Else
                cmbParam(8).Value = DBNull.value
            End If

            cmbParam(9) = New SqlParameter("@_ACTIVE_STATUS", SqlDbType.Char)
            cmbParam(9).Value = _ACTIVE_STATUS

            cmbParam(10) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            cmbParam(10).Value = _CREATED_BY.Trim

            cmbParam(11) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            cmbParam(11).Value = _CREATED_DATE

            cmbParam(12) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(12).Value = _UPDATED_BY.Trim
            Else
                cmbParam(12).Value = DBNull.value
            End If

            cmbParam(13) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(13).Value = _UPDATED_DATE.Value
            Else
                cmbParam(13).Value = DBNull.value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of MS_Supplier_Contact by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Supplier_Contact_ID")) = False Then _Supplier_Contact_ID = Convert.ToInt64(Rdr("Supplier_Contact_ID"))
                        If Convert.IsDBNull(Rdr("Supplier_ID")) = False Then _Supplier_ID = Convert.ToInt64(Rdr("Supplier_ID"))
                        If Convert.IsDBNull(Rdr("Prefix_ID")) = False Then _Prefix_ID = Convert.ToInt64(Rdr("Prefix_ID"))
                        If Convert.IsDBNull(Rdr("First_Name")) = False Then _First_Name = Rdr("First_Name").ToString()
                        If Convert.IsDBNull(Rdr("Last_Name")) = False Then _Last_Name = Rdr("Last_Name").ToString()
                        If Convert.IsDBNull(Rdr("Email")) = False Then _Email = Rdr("Email").ToString()
                        If Convert.IsDBNull(Rdr("TelephoneNo")) = False Then _TelephoneNo = Rdr("TelephoneNo").ToString()
                        If Convert.IsDBNull(Rdr("MobileNo")) = False Then _MobileNo = Rdr("MobileNo").ToString()
                        If Convert.IsDBNull(Rdr("FaxNo")) = False Then _FaxNo = Rdr("FaxNo").ToString()
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _Active_Status = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of MS_Supplier_Contact by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As MsSupplierContactLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Supplier_Contact_ID")) = False Then _Supplier_Contact_ID = Convert.ToInt64(Rdr("Supplier_Contact_ID"))
                        If Convert.IsDBNull(Rdr("Supplier_ID")) = False Then _Supplier_ID = Convert.ToInt64(Rdr("Supplier_ID"))
                        If Convert.IsDBNull(Rdr("Prefix_ID")) = False Then _Prefix_ID = Convert.ToInt64(Rdr("Prefix_ID"))
                        If Convert.IsDBNull(Rdr("First_Name")) = False Then _First_Name = Rdr("First_Name").ToString()
                        If Convert.IsDBNull(Rdr("Last_Name")) = False Then _Last_Name = Rdr("Last_Name").ToString()
                        If Convert.IsDBNull(Rdr("Email")) = False Then _Email = Rdr("Email").ToString()
                        If Convert.IsDBNull(Rdr("TelephoneNo")) = False Then _TelephoneNo = Rdr("TelephoneNo").ToString()
                        If Convert.IsDBNull(Rdr("MobileNo")) = False Then _MobileNo = Rdr("MobileNo").ToString()
                        If Convert.IsDBNull(Rdr("FaxNo")) = False Then _FaxNo = Rdr("FaxNo").ToString()
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _Active_Status = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table MS_Supplier_Contact
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (SUPPLIER_ID, PREFIX_ID, FIRST_NAME, LAST_NAME, EMAIL, TELEPHONENO, MOBILENO, FAXNO, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE)"
                Sql += " OUTPUT INSERTED.SUPPLIER_CONTACT_ID, INSERTED.SUPPLIER_ID, INSERTED.PREFIX_ID, INSERTED.FIRST_NAME, INSERTED.LAST_NAME, INSERTED.EMAIL, INSERTED.TELEPHONENO, INSERTED.MOBILENO, INSERTED.FAXNO, INSERTED.ACTIVE_STATUS, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE"
                Sql += " VALUES("
                sql += "@_SUPPLIER_ID" & ", "
                sql += "@_PREFIX_ID" & ", "
                sql += "@_FIRST_NAME" & ", "
                sql += "@_LAST_NAME" & ", "
                sql += "@_EMAIL" & ", "
                sql += "@_TELEPHONENO" & ", "
                sql += "@_MOBILENO" & ", "
                sql += "@_FAXNO" & ", "
                sql += "@_ACTIVE_STATUS" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE" & ", "
                sql += "@_UPDATED_BY" & ", "
                sql += "@_UPDATED_DATE"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table MS_Supplier_Contact
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "SUPPLIER_ID = " & "@_SUPPLIER_ID" & ", "
                Sql += "PREFIX_ID = " & "@_PREFIX_ID" & ", "
                Sql += "FIRST_NAME = " & "@_FIRST_NAME" & ", "
                Sql += "LAST_NAME = " & "@_LAST_NAME" & ", "
                Sql += "EMAIL = " & "@_EMAIL" & ", "
                Sql += "TELEPHONENO = " & "@_TELEPHONENO" & ", "
                Sql += "MOBILENO = " & "@_MOBILENO" & ", "
                Sql += "FAXNO = " & "@_FAXNO" & ", "
                Sql += "ACTIVE_STATUS = " & "@_ACTIVE_STATUS" & ", "
                Sql += "CREATED_BY = " & "@_CREATED_BY" & ", "
                Sql += "CREATED_DATE = " & "@_CREATED_DATE" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table MS_Supplier_Contact
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table MS_Supplier_Contact
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT SUPPLIER_CONTACT_ID, SUPPLIER_ID, PREFIX_ID, FIRST_NAME, LAST_NAME, EMAIL, TELEPHONENO, MOBILENO, FAXNO, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
