Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for TB_RCStep1_Receive_Detial table LinqDB.
    '[Create by  on July, 3 2017]
    Public Class TbRcstep1ReceiveDetialLinqDB
        Public sub TbRcstep1ReceiveDetialLinqDB()

        End Sub 
        ' TB_RCStep1_Receive_Detial
        Const _tableName As String = "TB_RCStep1_Receive_Detial"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _SEQUENCE_ID As Long = 0
        Dim _DOCUMENT_ID As String = ""
        Dim _PO_REFERENCE_DOCUMENT As  String  = ""
        Dim _DO_REFERENCE_DOCUMENT1 As  String  = ""
        Dim _SKU_ID As  String  = ""
        Dim _AMOUNTRECEIVE As  System.Nullable(Of Long) 
        Dim _AMOUNTRECEIVEACCESS As  System.Nullable(Of Long) 
        Dim _AMOUNTSENT As  System.Nullable(Of Long) 
        Dim _AMOUNTBALANCE As  System.Nullable(Of Long) 
        Dim _CREATED_BY As String = ""
        Dim _CREATED_DATE As DateTime = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)

        'Generate Field Property 
        <Column(Storage:="_SEQUENCE_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property SEQUENCE_ID() As Long
            Get
                Return _SEQUENCE_ID
            End Get
            Set(ByVal value As Long)
               _SEQUENCE_ID = value
            End Set
        End Property 
        <Column(Storage:="_DOCUMENT_ID", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property DOCUMENT_ID() As String
            Get
                Return _DOCUMENT_ID
            End Get
            Set(ByVal value As String)
               _DOCUMENT_ID = value
            End Set
        End Property 
        <Column(Storage:="_PO_REFERENCE_DOCUMENT", DbType:="VarChar(100)")>  _
        Public Property PO_REFERENCE_DOCUMENT() As  String 
            Get
                Return _PO_REFERENCE_DOCUMENT
            End Get
            Set(ByVal value As  String )
               _PO_REFERENCE_DOCUMENT = value
            End Set
        End Property 
        <Column(Storage:="_DO_REFERENCE_DOCUMENT1", DbType:="VarChar(100)")>  _
        Public Property DO_REFERENCE_DOCUMENT1() As  String 
            Get
                Return _DO_REFERENCE_DOCUMENT1
            End Get
            Set(ByVal value As  String )
               _DO_REFERENCE_DOCUMENT1 = value
            End Set
        End Property 
        <Column(Storage:="_SKU_ID", DbType:="VarChar(100)")>  _
        Public Property SKU_ID() As  String 
            Get
                Return _SKU_ID
            End Get
            Set(ByVal value As  String )
               _SKU_ID = value
            End Set
        End Property 
        <Column(Storage:="_AMOUNTRECEIVE", DbType:="Int")>  _
        Public Property AMOUNTRECEIVE() As  System.Nullable(Of Long) 
            Get
                Return _AMOUNTRECEIVE
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _AMOUNTRECEIVE = value
            End Set
        End Property 
        <Column(Storage:="_AMOUNTRECEIVEACCESS", DbType:="Int")>  _
        Public Property AMOUNTRECEIVEACCESS() As  System.Nullable(Of Long) 
            Get
                Return _AMOUNTRECEIVEACCESS
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _AMOUNTRECEIVEACCESS = value
            End Set
        End Property 
        <Column(Storage:="_AMOUNTSENT", DbType:="Int")>  _
        Public Property AMOUNTSENT() As  System.Nullable(Of Long) 
            Get
                Return _AMOUNTSENT
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _AMOUNTSENT = value
            End Set
        End Property 
        <Column(Storage:="_AMOUNTBALANCE", DbType:="Int")>  _
        Public Property AMOUNTBALANCE() As  System.Nullable(Of Long) 
            Get
                Return _AMOUNTBALANCE
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _AMOUNTBALANCE = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_BY() As String
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As String)
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_DATE() As DateTime
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As DateTime)
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _SEQUENCE_ID = 0
            _DOCUMENT_ID = ""
            _PO_REFERENCE_DOCUMENT = ""
            _DO_REFERENCE_DOCUMENT1 = ""
            _SKU_ID = ""
            _AMOUNTRECEIVE = Nothing
            _AMOUNTRECEIVEACCESS = Nothing
            _AMOUNTSENT = Nothing
            _AMOUNTBALANCE = Nothing
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into TB_RCStep1_Receive_Detial table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_RCStep1_Receive_Detial table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _Sequence_ID > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("SEQUENCE_ID = @_SEQUENCE_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_RCStep1_Receive_Detial table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from TB_RCStep1_Receive_Detial table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cSEQUENCE_ID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_SEQUENCE_ID", cSEQUENCE_ID)
                Return doDelete("SEQUENCE_ID = @_SEQUENCE_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of TB_RCStep1_Receive_Detial by specified SEQUENCE_ID key is retrieved successfully.
        '/// <param name=cSEQUENCE_ID>The SEQUENCE_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cSEQUENCE_ID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_SEQUENCE_ID", cSEQUENCE_ID)
            Return doChkData("SEQUENCE_ID = @_SEQUENCE_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of TB_RCStep1_Receive_Detial by specified SEQUENCE_ID key is retrieved successfully.
        '/// <param name=cSEQUENCE_ID>The SEQUENCE_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cSEQUENCE_ID As Long, trans As SQLTransaction) As TbRcstep1ReceiveDetialLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_SEQUENCE_ID", cSEQUENCE_ID)
            Return doGetData("SEQUENCE_ID = @_SEQUENCE_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of TB_RCStep1_Receive_Detial by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into TB_RCStep1_Receive_Detial table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _SEQUENCE_ID = dt.Rows(0)("SEQUENCE_ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                        ret.NewID = _SEQUENCE_ID
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to TB_RCStep1_Receive_Detial table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                            ret.NewID = _SEQUENCE_ID
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from TB_RCStep1_Receive_Detial table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(12) As SqlParameter
            cmbParam(0) = New SqlParameter("@_SEQUENCE_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _SEQUENCE_ID

            cmbParam(1) = New SqlParameter("@_DOCUMENT_ID", SqlDbType.VarChar)
            cmbParam(1).Value = _DOCUMENT_ID.Trim

            cmbParam(2) = New SqlParameter("@_PO_REFERENCE_DOCUMENT", SqlDbType.VarChar)
            If _PO_REFERENCE_DOCUMENT.Trim <> "" Then 
                cmbParam(2).Value = _PO_REFERENCE_DOCUMENT.Trim
            Else
                cmbParam(2).Value = DBNull.value
            End If

            cmbParam(3) = New SqlParameter("@_DO_REFERENCE_DOCUMENT1", SqlDbType.VarChar)
            If _DO_REFERENCE_DOCUMENT1.Trim <> "" Then 
                cmbParam(3).Value = _DO_REFERENCE_DOCUMENT1.Trim
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_SKU_ID", SqlDbType.VarChar)
            If _SKU_ID.Trim <> "" Then 
                cmbParam(4).Value = _SKU_ID.Trim
            Else
                cmbParam(4).Value = DBNull.value
            End If

            cmbParam(5) = New SqlParameter("@_AMOUNTRECEIVE", SqlDbType.Int)
            If _AMOUNTRECEIVE IsNot Nothing Then 
                cmbParam(5).Value = _AMOUNTRECEIVE.Value
            Else
                cmbParam(5).Value = DBNull.value
            End IF

            cmbParam(6) = New SqlParameter("@_AMOUNTRECEIVEACCESS", SqlDbType.Int)
            If _AMOUNTRECEIVEACCESS IsNot Nothing Then 
                cmbParam(6).Value = _AMOUNTRECEIVEACCESS.Value
            Else
                cmbParam(6).Value = DBNull.value
            End IF

            cmbParam(7) = New SqlParameter("@_AMOUNTSENT", SqlDbType.Int)
            If _AMOUNTSENT IsNot Nothing Then 
                cmbParam(7).Value = _AMOUNTSENT.Value
            Else
                cmbParam(7).Value = DBNull.value
            End IF

            cmbParam(8) = New SqlParameter("@_AMOUNTBALANCE", SqlDbType.Int)
            If _AMOUNTBALANCE IsNot Nothing Then 
                cmbParam(8).Value = _AMOUNTBALANCE.Value
            Else
                cmbParam(8).Value = DBNull.value
            End IF

            cmbParam(9) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            cmbParam(9).Value = _CREATED_BY.Trim

            cmbParam(10) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            cmbParam(10).Value = _CREATED_DATE

            cmbParam(11) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(11).Value = _UPDATED_BY.Trim
            Else
                cmbParam(11).Value = DBNull.value
            End If

            cmbParam(12) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(12).Value = _UPDATED_DATE.Value
            Else
                cmbParam(12).Value = DBNull.value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of TB_RCStep1_Receive_Detial by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Sequence_ID")) = False Then _Sequence_ID = Convert.ToInt64(Rdr("Sequence_ID"))
                        If Convert.IsDBNull(Rdr("Document_ID")) = False Then _Document_ID = Rdr("Document_ID").ToString()
                        If Convert.IsDBNull(Rdr("PO_Reference_Document")) = False Then _PO_Reference_Document = Rdr("PO_Reference_Document").ToString()
                        If Convert.IsDBNull(Rdr("DO_Reference_Document1")) = False Then _DO_Reference_Document1 = Rdr("DO_Reference_Document1").ToString()
                        If Convert.IsDBNull(Rdr("SKU_ID")) = False Then _SKU_ID = Rdr("SKU_ID").ToString()
                        If Convert.IsDBNull(Rdr("AmountReceive")) = False Then _AmountReceive = Convert.ToInt32(Rdr("AmountReceive"))
                        If Convert.IsDBNull(Rdr("AmountReceiveAccess")) = False Then _AmountReceiveAccess = Convert.ToInt32(Rdr("AmountReceiveAccess"))
                        If Convert.IsDBNull(Rdr("AmountSent")) = False Then _AmountSent = Convert.ToInt32(Rdr("AmountSent"))
                        If Convert.IsDBNull(Rdr("AmountBalance")) = False Then _AmountBalance = Convert.ToInt32(Rdr("AmountBalance"))
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of TB_RCStep1_Receive_Detial by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As TbRcstep1ReceiveDetialLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Sequence_ID")) = False Then _Sequence_ID = Convert.ToInt64(Rdr("Sequence_ID"))
                        If Convert.IsDBNull(Rdr("Document_ID")) = False Then _Document_ID = Rdr("Document_ID").ToString()
                        If Convert.IsDBNull(Rdr("PO_Reference_Document")) = False Then _PO_Reference_Document = Rdr("PO_Reference_Document").ToString()
                        If Convert.IsDBNull(Rdr("DO_Reference_Document1")) = False Then _DO_Reference_Document1 = Rdr("DO_Reference_Document1").ToString()
                        If Convert.IsDBNull(Rdr("SKU_ID")) = False Then _SKU_ID = Rdr("SKU_ID").ToString()
                        If Convert.IsDBNull(Rdr("AmountReceive")) = False Then _AmountReceive = Convert.ToInt32(Rdr("AmountReceive"))
                        If Convert.IsDBNull(Rdr("AmountReceiveAccess")) = False Then _AmountReceiveAccess = Convert.ToInt32(Rdr("AmountReceiveAccess"))
                        If Convert.IsDBNull(Rdr("AmountSent")) = False Then _AmountSent = Convert.ToInt32(Rdr("AmountSent"))
                        If Convert.IsDBNull(Rdr("AmountBalance")) = False Then _AmountBalance = Convert.ToInt32(Rdr("AmountBalance"))
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table TB_RCStep1_Receive_Detial
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (DOCUMENT_ID, PO_REFERENCE_DOCUMENT, DO_REFERENCE_DOCUMENT1, SKU_ID, AMOUNTRECEIVE, AMOUNTRECEIVEACCESS, AMOUNTSENT, AMOUNTBALANCE, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE)"
                Sql += " OUTPUT INSERTED.SEQUENCE_ID, INSERTED.DOCUMENT_ID, INSERTED.PO_REFERENCE_DOCUMENT, INSERTED.DO_REFERENCE_DOCUMENT1, INSERTED.SKU_ID, INSERTED.AMOUNTRECEIVE, INSERTED.AMOUNTRECEIVEACCESS, INSERTED.AMOUNTSENT, INSERTED.AMOUNTBALANCE, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE"
                Sql += " VALUES("
                sql += "@_DOCUMENT_ID" & ", "
                sql += "@_PO_REFERENCE_DOCUMENT" & ", "
                sql += "@_DO_REFERENCE_DOCUMENT1" & ", "
                sql += "@_SKU_ID" & ", "
                sql += "@_AMOUNTRECEIVE" & ", "
                sql += "@_AMOUNTRECEIVEACCESS" & ", "
                sql += "@_AMOUNTSENT" & ", "
                sql += "@_AMOUNTBALANCE" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE" & ", "
                sql += "@_UPDATED_BY" & ", "
                sql += "@_UPDATED_DATE"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table TB_RCStep1_Receive_Detial
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "DOCUMENT_ID = " & "@_DOCUMENT_ID" & ", "
                Sql += "PO_REFERENCE_DOCUMENT = " & "@_PO_REFERENCE_DOCUMENT" & ", "
                Sql += "DO_REFERENCE_DOCUMENT1 = " & "@_DO_REFERENCE_DOCUMENT1" & ", "
                Sql += "SKU_ID = " & "@_SKU_ID" & ", "
                Sql += "AMOUNTRECEIVE = " & "@_AMOUNTRECEIVE" & ", "
                Sql += "AMOUNTRECEIVEACCESS = " & "@_AMOUNTRECEIVEACCESS" & ", "
                Sql += "AMOUNTSENT = " & "@_AMOUNTSENT" & ", "
                Sql += "AMOUNTBALANCE = " & "@_AMOUNTBALANCE" & ", "
                Sql += "CREATED_BY = " & "@_CREATED_BY" & ", "
                Sql += "CREATED_DATE = " & "@_CREATED_DATE" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table TB_RCStep1_Receive_Detial
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table TB_RCStep1_Receive_Detial
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT SEQUENCE_ID, DOCUMENT_ID, PO_REFERENCE_DOCUMENT, DO_REFERENCE_DOCUMENT1, SKU_ID, AMOUNTRECEIVE, AMOUNTRECEIVEACCESS, AMOUNTSENT, AMOUNTBALANCE, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
