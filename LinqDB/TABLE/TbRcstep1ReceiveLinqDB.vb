Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for TB_RCStep1_Receive table LinqDB.
    '[Create by  on July, 5 2017]
    Public Class TbRcstep1ReceiveLinqDB
        Public sub TbRcstep1ReceiveLinqDB()

        End Sub 
        ' TB_RCStep1_Receive
        Const _tableName As String = "TB_RCStep1_Receive"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _SEQUENCE_ID As Long = 0
        Dim _DOCUMENT_ID As String = ""
        Dim _DOCUMENT_DATE As  System.Nullable(Of Date)  = New DateTime(1,1,1)
        Dim _DOCUMENT_TIME As  String  = ""
        Dim _RECEIVE_TYPE_ID As Long = 0
        Dim _PO_REFERENCE_DOCUMENT As  String  = ""
        Dim _PO_REFERENCE_DATE As  System.Nullable(Of Date)  = New DateTime(1,1,1)
        Dim _DO_REFERENCE_DOCUMENT1 As  String  = ""
        Dim _DO_REFERENCE_DATE As  System.Nullable(Of Date)  = New DateTime(1,1,1)
        Dim _CHECKER_ID As  String  = ""
        Dim _REFERENCE_DOCUMENT1 As  String  = ""
        Dim _REFERENCE_DOCUMENT2 As  String  = ""
        Dim _REFERENCE_DOCUMENT3 As  String  = ""
        Dim _RECEIVE_REASON As  String  = ""
        Dim _TOTAL_UNIT As  String  = ""
        Dim _GROSS_WEIGHT As  String  = ""
        Dim _TOTAL_PRICE As String = ""
        Dim _SKU_ID As  String  = ""
        Dim _PRODUCT_ID As  String  = ""
        Dim _VOLUME As  String  = ""
        Dim _AMOUNT As  String  = ""
        Dim _UNIT As  String  = ""
        Dim _PRODUCT_STATUS_ID As  String  = ""
        Dim _PRODUCTION_DATE As  System.Nullable(Of Date)  = New DateTime(1,1,1)
        Dim _EXPIRE_DATE As  System.Nullable(Of Date)  = New DateTime(1,1,1)
        Dim _TIME_START As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _TIME_END As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _TIME4JOB As  System.Nullable(Of Long) 
        Dim _VALUE_PER_PACK As  String  = ""
        Dim _VALUE As  String  = ""
        Dim _NET_WEIGHT As  String  = ""
        Dim _COUNT_STATUS As  String  = ""
        Dim _ACTIVE_STATUS As Char = "Y"
        Dim _CREATED_BY As String = ""
        Dim _CREATED_DATE As DateTime = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)

        'Generate Field Property 
        <Column(Storage:="_SEQUENCE_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property SEQUENCE_ID() As Long
            Get
                Return _SEQUENCE_ID
            End Get
            Set(ByVal value As Long)
               _SEQUENCE_ID = value
            End Set
        End Property 
        <Column(Storage:="_DOCUMENT_ID", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property DOCUMENT_ID() As String
            Get
                Return _DOCUMENT_ID
            End Get
            Set(ByVal value As String)
               _DOCUMENT_ID = value
            End Set
        End Property 
        <Column(Storage:="_DOCUMENT_DATE", DbType:="Date")>  _
        Public Property DOCUMENT_DATE() As  System.Nullable(Of Date) 
            Get
                Return _DOCUMENT_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of Date) )
               _DOCUMENT_DATE = value
            End Set
        End Property 
        <Column(Storage:="_DOCUMENT_TIME", DbType:="VarChar(50)")>  _
        Public Property DOCUMENT_TIME() As  String 
            Get
                Return _DOCUMENT_TIME
            End Get
            Set(ByVal value As  String )
               _DOCUMENT_TIME = value
            End Set
        End Property 
        <Column(Storage:="_RECEIVE_TYPE_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property RECEIVE_TYPE_ID() As Long
            Get
                Return _RECEIVE_TYPE_ID
            End Get
            Set(ByVal value As Long)
               _RECEIVE_TYPE_ID = value
            End Set
        End Property 
        <Column(Storage:="_PO_REFERENCE_DOCUMENT", DbType:="VarChar(100)")>  _
        Public Property PO_REFERENCE_DOCUMENT() As  String 
            Get
                Return _PO_REFERENCE_DOCUMENT
            End Get
            Set(ByVal value As  String )
               _PO_REFERENCE_DOCUMENT = value
            End Set
        End Property 
        <Column(Storage:="_PO_REFERENCE_DATE", DbType:="Date")>  _
        Public Property PO_REFERENCE_DATE() As  System.Nullable(Of Date) 
            Get
                Return _PO_REFERENCE_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of Date) )
               _PO_REFERENCE_DATE = value
            End Set
        End Property 
        <Column(Storage:="_DO_REFERENCE_DOCUMENT1", DbType:="VarChar(100)")>  _
        Public Property DO_REFERENCE_DOCUMENT1() As  String 
            Get
                Return _DO_REFERENCE_DOCUMENT1
            End Get
            Set(ByVal value As  String )
               _DO_REFERENCE_DOCUMENT1 = value
            End Set
        End Property 
        <Column(Storage:="_DO_REFERENCE_DATE", DbType:="Date")>  _
        Public Property DO_REFERENCE_DATE() As  System.Nullable(Of Date) 
            Get
                Return _DO_REFERENCE_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of Date) )
               _DO_REFERENCE_DATE = value
            End Set
        End Property 
        <Column(Storage:="_CHECKER_ID", DbType:="VarChar(100)")>  _
        Public Property CHECKER_ID() As  String 
            Get
                Return _CHECKER_ID
            End Get
            Set(ByVal value As  String )
               _CHECKER_ID = value
            End Set
        End Property 
        <Column(Storage:="_REFERENCE_DOCUMENT1", DbType:="VarChar(100)")>  _
        Public Property REFERENCE_DOCUMENT1() As  String 
            Get
                Return _REFERENCE_DOCUMENT1
            End Get
            Set(ByVal value As  String )
               _REFERENCE_DOCUMENT1 = value
            End Set
        End Property 
        <Column(Storage:="_REFERENCE_DOCUMENT2", DbType:="VarChar(100)")>  _
        Public Property REFERENCE_DOCUMENT2() As  String 
            Get
                Return _REFERENCE_DOCUMENT2
            End Get
            Set(ByVal value As  String )
               _REFERENCE_DOCUMENT2 = value
            End Set
        End Property 
        <Column(Storage:="_REFERENCE_DOCUMENT3", DbType:="VarChar(100)")>  _
        Public Property REFERENCE_DOCUMENT3() As  String 
            Get
                Return _REFERENCE_DOCUMENT3
            End Get
            Set(ByVal value As  String )
               _REFERENCE_DOCUMENT3 = value
            End Set
        End Property 
        <Column(Storage:="_RECEIVE_REASON", DbType:="VarChar(200)")>  _
        Public Property RECEIVE_REASON() As  String 
            Get
                Return _RECEIVE_REASON
            End Get
            Set(ByVal value As  String )
               _RECEIVE_REASON = value
            End Set
        End Property 
        <Column(Storage:="_TOTAL_UNIT", DbType:="VarChar(50)")>  _
        Public Property TOTAL_UNIT() As  String 
            Get
                Return _TOTAL_UNIT
            End Get
            Set(ByVal value As  String )
               _TOTAL_UNIT = value
            End Set
        End Property 
        <Column(Storage:="_GROSS_WEIGHT", DbType:="VarChar(50)")>  _
        Public Property GROSS_WEIGHT() As  String 
            Get
                Return _GROSS_WEIGHT
            End Get
            Set(ByVal value As  String )
               _GROSS_WEIGHT = value
            End Set
        End Property 
        <Column(Storage:="_TOTAL_PRICE", DbType:="VarChar(50) NOT NULL ",CanBeNull:=false)>  _
        Public Property TOTAL_PRICE() As String
            Get
                Return _TOTAL_PRICE
            End Get
            Set(ByVal value As String)
               _TOTAL_PRICE = value
            End Set
        End Property 
        <Column(Storage:="_SKU_ID", DbType:="VarChar(100)")>  _
        Public Property SKU_ID() As  String 
            Get
                Return _SKU_ID
            End Get
            Set(ByVal value As  String )
               _SKU_ID = value
            End Set
        End Property 
        <Column(Storage:="_PRODUCT_ID", DbType:="VarChar(100)")>  _
        Public Property PRODUCT_ID() As  String 
            Get
                Return _PRODUCT_ID
            End Get
            Set(ByVal value As  String )
               _PRODUCT_ID = value
            End Set
        End Property 
        <Column(Storage:="_VOLUME", DbType:="VarChar(50)")>  _
        Public Property VOLUME() As  String 
            Get
                Return _VOLUME
            End Get
            Set(ByVal value As  String )
               _VOLUME = value
            End Set
        End Property 
        <Column(Storage:="_AMOUNT", DbType:="VarChar(50)")>  _
        Public Property AMOUNT() As  String 
            Get
                Return _AMOUNT
            End Get
            Set(ByVal value As  String )
               _AMOUNT = value
            End Set
        End Property 
        <Column(Storage:="_UNIT", DbType:="VarChar(50)")>  _
        Public Property UNIT() As  String 
            Get
                Return _UNIT
            End Get
            Set(ByVal value As  String )
               _UNIT = value
            End Set
        End Property 
        <Column(Storage:="_PRODUCT_STATUS_ID", DbType:="VarChar(10)")>  _
        Public Property PRODUCT_STATUS_ID() As  String 
            Get
                Return _PRODUCT_STATUS_ID
            End Get
            Set(ByVal value As  String )
               _PRODUCT_STATUS_ID = value
            End Set
        End Property 
        <Column(Storage:="_PRODUCTION_DATE", DbType:="Date")>  _
        Public Property PRODUCTION_DATE() As  System.Nullable(Of Date) 
            Get
                Return _PRODUCTION_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of Date) )
               _PRODUCTION_DATE = value
            End Set
        End Property 
        <Column(Storage:="_EXPIRE_DATE", DbType:="Date")>  _
        Public Property EXPIRE_DATE() As  System.Nullable(Of Date) 
            Get
                Return _EXPIRE_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of Date) )
               _EXPIRE_DATE = value
            End Set
        End Property 
        <Column(Storage:="_TIME_START", DbType:="DateTime")>  _
        Public Property TIME_START() As  System.Nullable(Of DateTime) 
            Get
                Return _TIME_START
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _TIME_START = value
            End Set
        End Property 
        <Column(Storage:="_TIME_END", DbType:="DateTime")>  _
        Public Property TIME_END() As  System.Nullable(Of DateTime) 
            Get
                Return _TIME_END
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _TIME_END = value
            End Set
        End Property 
        <Column(Storage:="_TIME4JOB", DbType:="Int")>  _
        Public Property TIME4JOB() As  System.Nullable(Of Long) 
            Get
                Return _TIME4JOB
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _TIME4JOB = value
            End Set
        End Property 
        <Column(Storage:="_VALUE_PER_PACK", DbType:="VarChar(50)")>  _
        Public Property VALUE_PER_PACK() As  String 
            Get
                Return _VALUE_PER_PACK
            End Get
            Set(ByVal value As  String )
               _VALUE_PER_PACK = value
            End Set
        End Property 
        <Column(Storage:="_VALUE", DbType:="VarChar(50)")>  _
        Public Property VALUE() As  String 
            Get
                Return _VALUE
            End Get
            Set(ByVal value As  String )
               _VALUE = value
            End Set
        End Property 
        <Column(Storage:="_NET_WEIGHT", DbType:="VarChar(50)")>  _
        Public Property NET_WEIGHT() As  String 
            Get
                Return _NET_WEIGHT
            End Get
            Set(ByVal value As  String )
               _NET_WEIGHT = value
            End Set
        End Property 
        <Column(Storage:="_COUNT_STATUS", DbType:="VarChar(1)")>  _
        Public Property COUNT_STATUS() As  String 
            Get
                Return _COUNT_STATUS
            End Get
            Set(ByVal value As  String )
               _COUNT_STATUS = value
            End Set
        End Property 
        <Column(Storage:="_ACTIVE_STATUS", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ACTIVE_STATUS() As Char
            Get
                Return _ACTIVE_STATUS
            End Get
            Set(ByVal value As Char)
               _ACTIVE_STATUS = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_BY() As String
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As String)
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_DATE() As DateTime
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As DateTime)
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _SEQUENCE_ID = 0
            _DOCUMENT_ID = ""
            _DOCUMENT_DATE = New DateTime(1,1,1)
            _DOCUMENT_TIME = ""
            _RECEIVE_TYPE_ID = 0
            _PO_REFERENCE_DOCUMENT = ""
            _PO_REFERENCE_DATE = New DateTime(1,1,1)
            _DO_REFERENCE_DOCUMENT1 = ""
            _DO_REFERENCE_DATE = New DateTime(1,1,1)
            _CHECKER_ID = ""
            _REFERENCE_DOCUMENT1 = ""
            _REFERENCE_DOCUMENT2 = ""
            _REFERENCE_DOCUMENT3 = ""
            _RECEIVE_REASON = ""
            _TOTAL_UNIT = ""
            _GROSS_WEIGHT = ""
            _TOTAL_PRICE = ""
            _SKU_ID = ""
            _PRODUCT_ID = ""
            _VOLUME = ""
            _AMOUNT = ""
            _UNIT = ""
            _PRODUCT_STATUS_ID = ""
            _PRODUCTION_DATE = New DateTime(1,1,1)
            _EXPIRE_DATE = New DateTime(1,1,1)
            _TIME_START = New DateTime(1,1,1)
            _TIME_END = New DateTime(1,1,1)
            _TIME4JOB = Nothing
            _VALUE_PER_PACK = ""
            _VALUE = ""
            _NET_WEIGHT = ""
            _COUNT_STATUS = ""
            _ACTIVE_STATUS = "Y"
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into TB_RCStep1_Receive table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_RCStep1_Receive table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _Sequence_ID > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("SEQUENCE_ID = @_SEQUENCE_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_RCStep1_Receive table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from TB_RCStep1_Receive table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cSEQUENCE_ID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_SEQUENCE_ID", cSEQUENCE_ID)
                Return doDelete("SEQUENCE_ID = @_SEQUENCE_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of TB_RCStep1_Receive by specified SEQUENCE_ID key is retrieved successfully.
        '/// <param name=cSEQUENCE_ID>The SEQUENCE_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cSEQUENCE_ID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_SEQUENCE_ID", cSEQUENCE_ID)
            Return doChkData("SEQUENCE_ID = @_SEQUENCE_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of TB_RCStep1_Receive by specified SEQUENCE_ID key is retrieved successfully.
        '/// <param name=cSEQUENCE_ID>The SEQUENCE_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cSEQUENCE_ID As Long, trans As SQLTransaction) As TbRcstep1ReceiveLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_SEQUENCE_ID", cSEQUENCE_ID)
            Return doGetData("SEQUENCE_ID = @_SEQUENCE_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of TB_RCStep1_Receive by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into TB_RCStep1_Receive table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _SEQUENCE_ID = dt.Rows(0)("SEQUENCE_ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                        ret.NewID = _SEQUENCE_ID
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to TB_RCStep1_Receive table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                            ret.NewID = _SEQUENCE_ID
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from TB_RCStep1_Receive table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(36) As SqlParameter
            cmbParam(0) = New SqlParameter("@_SEQUENCE_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _SEQUENCE_ID

            cmbParam(1) = New SqlParameter("@_DOCUMENT_ID", SqlDbType.VarChar)
            cmbParam(1).Value = _DOCUMENT_ID.Trim

            cmbParam(2) = New SqlParameter("@_DOCUMENT_DATE", SqlDbType.Date)
            If _DOCUMENT_DATE.Value.Year > 1 Then 
                cmbParam(2).Value = _DOCUMENT_DATE.Value
            Else
                cmbParam(2).Value = DBNull.value
            End If

            cmbParam(3) = New SqlParameter("@_DOCUMENT_TIME", SqlDbType.VarChar)
            If _DOCUMENT_TIME.Trim <> "" Then 
                cmbParam(3).Value = _DOCUMENT_TIME.Trim
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_RECEIVE_TYPE_ID", SqlDbType.BigInt)
            cmbParam(4).Value = _RECEIVE_TYPE_ID

            cmbParam(5) = New SqlParameter("@_PO_REFERENCE_DOCUMENT", SqlDbType.VarChar)
            If _PO_REFERENCE_DOCUMENT.Trim <> "" Then 
                cmbParam(5).Value = _PO_REFERENCE_DOCUMENT.Trim
            Else
                cmbParam(5).Value = DBNull.value
            End If

            cmbParam(6) = New SqlParameter("@_PO_REFERENCE_DATE", SqlDbType.Date)
            If _PO_REFERENCE_DATE.Value.Year > 1 Then 
                cmbParam(6).Value = _PO_REFERENCE_DATE.Value
            Else
                cmbParam(6).Value = DBNull.value
            End If

            cmbParam(7) = New SqlParameter("@_DO_REFERENCE_DOCUMENT1", SqlDbType.VarChar)
            If _DO_REFERENCE_DOCUMENT1.Trim <> "" Then 
                cmbParam(7).Value = _DO_REFERENCE_DOCUMENT1.Trim
            Else
                cmbParam(7).Value = DBNull.value
            End If

            cmbParam(8) = New SqlParameter("@_DO_REFERENCE_DATE", SqlDbType.Date)
            If _DO_REFERENCE_DATE.Value.Year > 1 Then 
                cmbParam(8).Value = _DO_REFERENCE_DATE.Value
            Else
                cmbParam(8).Value = DBNull.value
            End If

            cmbParam(9) = New SqlParameter("@_CHECKER_ID", SqlDbType.VarChar)
            If _CHECKER_ID.Trim <> "" Then 
                cmbParam(9).Value = _CHECKER_ID.Trim
            Else
                cmbParam(9).Value = DBNull.value
            End If

            cmbParam(10) = New SqlParameter("@_REFERENCE_DOCUMENT1", SqlDbType.VarChar)
            If _REFERENCE_DOCUMENT1.Trim <> "" Then 
                cmbParam(10).Value = _REFERENCE_DOCUMENT1.Trim
            Else
                cmbParam(10).Value = DBNull.value
            End If

            cmbParam(11) = New SqlParameter("@_REFERENCE_DOCUMENT2", SqlDbType.VarChar)
            If _REFERENCE_DOCUMENT2.Trim <> "" Then 
                cmbParam(11).Value = _REFERENCE_DOCUMENT2.Trim
            Else
                cmbParam(11).Value = DBNull.value
            End If

            cmbParam(12) = New SqlParameter("@_REFERENCE_DOCUMENT3", SqlDbType.VarChar)
            If _REFERENCE_DOCUMENT3.Trim <> "" Then 
                cmbParam(12).Value = _REFERENCE_DOCUMENT3.Trim
            Else
                cmbParam(12).Value = DBNull.value
            End If

            cmbParam(13) = New SqlParameter("@_RECEIVE_REASON", SqlDbType.VarChar)
            If _RECEIVE_REASON.Trim <> "" Then 
                cmbParam(13).Value = _RECEIVE_REASON.Trim
            Else
                cmbParam(13).Value = DBNull.value
            End If

            cmbParam(14) = New SqlParameter("@_TOTAL_UNIT", SqlDbType.VarChar)
            If _TOTAL_UNIT.Trim <> "" Then 
                cmbParam(14).Value = _TOTAL_UNIT.Trim
            Else
                cmbParam(14).Value = DBNull.value
            End If

            cmbParam(15) = New SqlParameter("@_GROSS_WEIGHT", SqlDbType.VarChar)
            If _GROSS_WEIGHT.Trim <> "" Then 
                cmbParam(15).Value = _GROSS_WEIGHT.Trim
            Else
                cmbParam(15).Value = DBNull.value
            End If

            cmbParam(16) = New SqlParameter("@_TOTAL_PRICE", SqlDbType.VarChar)
            cmbParam(16).Value = _TOTAL_PRICE.Trim

            cmbParam(17) = New SqlParameter("@_SKU_ID", SqlDbType.VarChar)
            If _SKU_ID.Trim <> "" Then 
                cmbParam(17).Value = _SKU_ID.Trim
            Else
                cmbParam(17).Value = DBNull.value
            End If

            cmbParam(18) = New SqlParameter("@_PRODUCT_ID", SqlDbType.VarChar)
            If _PRODUCT_ID.Trim <> "" Then 
                cmbParam(18).Value = _PRODUCT_ID.Trim
            Else
                cmbParam(18).Value = DBNull.value
            End If

            cmbParam(19) = New SqlParameter("@_VOLUME", SqlDbType.VarChar)
            If _VOLUME.Trim <> "" Then 
                cmbParam(19).Value = _VOLUME.Trim
            Else
                cmbParam(19).Value = DBNull.value
            End If

            cmbParam(20) = New SqlParameter("@_AMOUNT", SqlDbType.VarChar)
            If _AMOUNT.Trim <> "" Then 
                cmbParam(20).Value = _AMOUNT.Trim
            Else
                cmbParam(20).Value = DBNull.value
            End If

            cmbParam(21) = New SqlParameter("@_UNIT", SqlDbType.VarChar)
            If _UNIT.Trim <> "" Then 
                cmbParam(21).Value = _UNIT.Trim
            Else
                cmbParam(21).Value = DBNull.value
            End If

            cmbParam(22) = New SqlParameter("@_PRODUCT_STATUS_ID", SqlDbType.VarChar)
            If _PRODUCT_STATUS_ID.Trim <> "" Then 
                cmbParam(22).Value = _PRODUCT_STATUS_ID.Trim
            Else
                cmbParam(22).Value = DBNull.value
            End If

            cmbParam(23) = New SqlParameter("@_PRODUCTION_DATE", SqlDbType.Date)
            If _PRODUCTION_DATE.Value.Year > 1 Then 
                cmbParam(23).Value = _PRODUCTION_DATE.Value
            Else
                cmbParam(23).Value = DBNull.value
            End If

            cmbParam(24) = New SqlParameter("@_EXPIRE_DATE", SqlDbType.Date)
            If _EXPIRE_DATE.Value.Year > 1 Then 
                cmbParam(24).Value = _EXPIRE_DATE.Value
            Else
                cmbParam(24).Value = DBNull.value
            End If

            cmbParam(25) = New SqlParameter("@_TIME_START", SqlDbType.DateTime)
            If _TIME_START.Value.Year > 1 Then 
                cmbParam(25).Value = _TIME_START.Value
            Else
                cmbParam(25).Value = DBNull.value
            End If

            cmbParam(26) = New SqlParameter("@_TIME_END", SqlDbType.DateTime)
            If _TIME_END.Value.Year > 1 Then 
                cmbParam(26).Value = _TIME_END.Value
            Else
                cmbParam(26).Value = DBNull.value
            End If

            cmbParam(27) = New SqlParameter("@_TIME4JOB", SqlDbType.Int)
            If _TIME4JOB IsNot Nothing Then 
                cmbParam(27).Value = _TIME4JOB.Value
            Else
                cmbParam(27).Value = DBNull.value
            End IF

            cmbParam(28) = New SqlParameter("@_VALUE_PER_PACK", SqlDbType.VarChar)
            If _VALUE_PER_PACK.Trim <> "" Then 
                cmbParam(28).Value = _VALUE_PER_PACK.Trim
            Else
                cmbParam(28).Value = DBNull.value
            End If

            cmbParam(29) = New SqlParameter("@_VALUE", SqlDbType.VarChar)
            If _VALUE.Trim <> "" Then 
                cmbParam(29).Value = _VALUE.Trim
            Else
                cmbParam(29).Value = DBNull.value
            End If

            cmbParam(30) = New SqlParameter("@_NET_WEIGHT", SqlDbType.VarChar)
            If _NET_WEIGHT.Trim <> "" Then 
                cmbParam(30).Value = _NET_WEIGHT.Trim
            Else
                cmbParam(30).Value = DBNull.value
            End If

            cmbParam(31) = New SqlParameter("@_COUNT_STATUS", SqlDbType.VarChar)
            If _COUNT_STATUS.Trim <> "" Then 
                cmbParam(31).Value = _COUNT_STATUS.Trim
            Else
                cmbParam(31).Value = DBNull.value
            End If

            cmbParam(32) = New SqlParameter("@_ACTIVE_STATUS", SqlDbType.Char)
            cmbParam(32).Value = _ACTIVE_STATUS

            cmbParam(33) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            cmbParam(33).Value = _CREATED_BY.Trim

            cmbParam(34) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            cmbParam(34).Value = _CREATED_DATE

            cmbParam(35) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(35).Value = _UPDATED_BY.Trim
            Else
                cmbParam(35).Value = DBNull.value
            End If

            cmbParam(36) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(36).Value = _UPDATED_DATE.Value
            Else
                cmbParam(36).Value = DBNull.value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of TB_RCStep1_Receive by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Sequence_ID")) = False Then _Sequence_ID = Convert.ToInt64(Rdr("Sequence_ID"))
                        If Convert.IsDBNull(Rdr("Document_ID")) = False Then _Document_ID = Rdr("Document_ID").ToString()
                        If Convert.IsDBNull(Rdr("Document_Date")) = False Then _Document_Date = Convert.ToDateTime(Rdr("Document_Date"))
                        If Convert.IsDBNull(Rdr("Document_Time")) = False Then _Document_Time = Rdr("Document_Time").ToString()
                        If Convert.IsDBNull(Rdr("Receive_Type_ID")) = False Then _Receive_Type_ID = Convert.ToInt64(Rdr("Receive_Type_ID"))
                        If Convert.IsDBNull(Rdr("PO_Reference_Document")) = False Then _PO_Reference_Document = Rdr("PO_Reference_Document").ToString()
                        If Convert.IsDBNull(Rdr("PO_Reference_Date")) = False Then _PO_Reference_Date = Convert.ToDateTime(Rdr("PO_Reference_Date"))
                        If Convert.IsDBNull(Rdr("DO_Reference_Document1")) = False Then _DO_Reference_Document1 = Rdr("DO_Reference_Document1").ToString()
                        If Convert.IsDBNull(Rdr("DO_Reference_Date")) = False Then _DO_Reference_Date = Convert.ToDateTime(Rdr("DO_Reference_Date"))
                        If Convert.IsDBNull(Rdr("Checker_ID")) = False Then _Checker_ID = Rdr("Checker_ID").ToString()
                        If Convert.IsDBNull(Rdr("Reference_Document1")) = False Then _Reference_Document1 = Rdr("Reference_Document1").ToString()
                        If Convert.IsDBNull(Rdr("Reference_Document2")) = False Then _Reference_Document2 = Rdr("Reference_Document2").ToString()
                        If Convert.IsDBNull(Rdr("Reference_Document3")) = False Then _Reference_Document3 = Rdr("Reference_Document3").ToString()
                        If Convert.IsDBNull(Rdr("Receive_Reason")) = False Then _Receive_Reason = Rdr("Receive_Reason").ToString()
                        If Convert.IsDBNull(Rdr("Total_Unit")) = False Then _Total_Unit = Rdr("Total_Unit").ToString()
                        If Convert.IsDBNull(Rdr("Gross_Weight")) = False Then _Gross_Weight = Rdr("Gross_Weight").ToString()
                        If Convert.IsDBNull(Rdr("Total_Price")) = False Then _Total_Price = Rdr("Total_Price").ToString()
                        If Convert.IsDBNull(Rdr("SKU_ID")) = False Then _SKU_ID = Rdr("SKU_ID").ToString()
                        If Convert.IsDBNull(Rdr("Product_ID")) = False Then _Product_ID = Rdr("Product_ID").ToString()
                        If Convert.IsDBNull(Rdr("Volume")) = False Then _Volume = Rdr("Volume").ToString()
                        If Convert.IsDBNull(Rdr("Amount")) = False Then _Amount = Rdr("Amount").ToString()
                        If Convert.IsDBNull(Rdr("Unit")) = False Then _Unit = Rdr("Unit").ToString()
                        If Convert.IsDBNull(Rdr("Product_Status_ID")) = False Then _Product_Status_ID = Rdr("Product_Status_ID").ToString()
                        If Convert.IsDBNull(Rdr("Production_Date")) = False Then _Production_Date = Convert.ToDateTime(Rdr("Production_Date"))
                        If Convert.IsDBNull(Rdr("Expire_Date")) = False Then _Expire_Date = Convert.ToDateTime(Rdr("Expire_Date"))
                        If Convert.IsDBNull(Rdr("Time_Start")) = False Then _Time_Start = Convert.ToDateTime(Rdr("Time_Start"))
                        If Convert.IsDBNull(Rdr("Time_End")) = False Then _Time_End = Convert.ToDateTime(Rdr("Time_End"))
                        If Convert.IsDBNull(Rdr("Time4Job")) = False Then _Time4Job = Convert.ToInt32(Rdr("Time4Job"))
                        If Convert.IsDBNull(Rdr("Value_Per_Pack")) = False Then _Value_Per_Pack = Rdr("Value_Per_Pack").ToString()
                        If Convert.IsDBNull(Rdr("Value")) = False Then _Value = Rdr("Value").ToString()
                        If Convert.IsDBNull(Rdr("Net_Weight")) = False Then _Net_Weight = Rdr("Net_Weight").ToString()
                        If Convert.IsDBNull(Rdr("Count_Status")) = False Then _Count_Status = Rdr("Count_Status").ToString()
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _Active_Status = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of TB_RCStep1_Receive by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As TbRcstep1ReceiveLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Sequence_ID")) = False Then _Sequence_ID = Convert.ToInt64(Rdr("Sequence_ID"))
                        If Convert.IsDBNull(Rdr("Document_ID")) = False Then _Document_ID = Rdr("Document_ID").ToString()
                        If Convert.IsDBNull(Rdr("Document_Date")) = False Then _Document_Date = Convert.ToDateTime(Rdr("Document_Date"))
                        If Convert.IsDBNull(Rdr("Document_Time")) = False Then _Document_Time = Rdr("Document_Time").ToString()
                        If Convert.IsDBNull(Rdr("Receive_Type_ID")) = False Then _Receive_Type_ID = Convert.ToInt64(Rdr("Receive_Type_ID"))
                        If Convert.IsDBNull(Rdr("PO_Reference_Document")) = False Then _PO_Reference_Document = Rdr("PO_Reference_Document").ToString()
                        If Convert.IsDBNull(Rdr("PO_Reference_Date")) = False Then _PO_Reference_Date = Convert.ToDateTime(Rdr("PO_Reference_Date"))
                        If Convert.IsDBNull(Rdr("DO_Reference_Document1")) = False Then _DO_Reference_Document1 = Rdr("DO_Reference_Document1").ToString()
                        If Convert.IsDBNull(Rdr("DO_Reference_Date")) = False Then _DO_Reference_Date = Convert.ToDateTime(Rdr("DO_Reference_Date"))
                        If Convert.IsDBNull(Rdr("Checker_ID")) = False Then _Checker_ID = Rdr("Checker_ID").ToString()
                        If Convert.IsDBNull(Rdr("Reference_Document1")) = False Then _Reference_Document1 = Rdr("Reference_Document1").ToString()
                        If Convert.IsDBNull(Rdr("Reference_Document2")) = False Then _Reference_Document2 = Rdr("Reference_Document2").ToString()
                        If Convert.IsDBNull(Rdr("Reference_Document3")) = False Then _Reference_Document3 = Rdr("Reference_Document3").ToString()
                        If Convert.IsDBNull(Rdr("Receive_Reason")) = False Then _Receive_Reason = Rdr("Receive_Reason").ToString()
                        If Convert.IsDBNull(Rdr("Total_Unit")) = False Then _Total_Unit = Rdr("Total_Unit").ToString()
                        If Convert.IsDBNull(Rdr("Gross_Weight")) = False Then _Gross_Weight = Rdr("Gross_Weight").ToString()
                        If Convert.IsDBNull(Rdr("Total_Price")) = False Then _Total_Price = Rdr("Total_Price").ToString()
                        If Convert.IsDBNull(Rdr("SKU_ID")) = False Then _SKU_ID = Rdr("SKU_ID").ToString()
                        If Convert.IsDBNull(Rdr("Product_ID")) = False Then _Product_ID = Rdr("Product_ID").ToString()
                        If Convert.IsDBNull(Rdr("Volume")) = False Then _Volume = Rdr("Volume").ToString()
                        If Convert.IsDBNull(Rdr("Amount")) = False Then _Amount = Rdr("Amount").ToString()
                        If Convert.IsDBNull(Rdr("Unit")) = False Then _Unit = Rdr("Unit").ToString()
                        If Convert.IsDBNull(Rdr("Product_Status_ID")) = False Then _Product_Status_ID = Rdr("Product_Status_ID").ToString()
                        If Convert.IsDBNull(Rdr("Production_Date")) = False Then _Production_Date = Convert.ToDateTime(Rdr("Production_Date"))
                        If Convert.IsDBNull(Rdr("Expire_Date")) = False Then _Expire_Date = Convert.ToDateTime(Rdr("Expire_Date"))
                        If Convert.IsDBNull(Rdr("Time_Start")) = False Then _Time_Start = Convert.ToDateTime(Rdr("Time_Start"))
                        If Convert.IsDBNull(Rdr("Time_End")) = False Then _Time_End = Convert.ToDateTime(Rdr("Time_End"))
                        If Convert.IsDBNull(Rdr("Time4Job")) = False Then _Time4Job = Convert.ToInt32(Rdr("Time4Job"))
                        If Convert.IsDBNull(Rdr("Value_Per_Pack")) = False Then _Value_Per_Pack = Rdr("Value_Per_Pack").ToString()
                        If Convert.IsDBNull(Rdr("Value")) = False Then _Value = Rdr("Value").ToString()
                        If Convert.IsDBNull(Rdr("Net_Weight")) = False Then _Net_Weight = Rdr("Net_Weight").ToString()
                        If Convert.IsDBNull(Rdr("Count_Status")) = False Then _Count_Status = Rdr("Count_Status").ToString()
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _Active_Status = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table TB_RCStep1_Receive
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (DOCUMENT_ID, DOCUMENT_DATE, DOCUMENT_TIME, RECEIVE_TYPE_ID, PO_REFERENCE_DOCUMENT, PO_REFERENCE_DATE, DO_REFERENCE_DOCUMENT1, DO_REFERENCE_DATE, CHECKER_ID, REFERENCE_DOCUMENT1, REFERENCE_DOCUMENT2, REFERENCE_DOCUMENT3, RECEIVE_REASON, TOTAL_UNIT, GROSS_WEIGHT, TOTAL_PRICE, SKU_ID, PRODUCT_ID, VOLUME, AMOUNT, UNIT, PRODUCT_STATUS_ID, PRODUCTION_DATE, EXPIRE_DATE, TIME_START, TIME_END, TIME4JOB, VALUE_PER_PACK, VALUE, NET_WEIGHT, COUNT_STATUS, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE)"
                Sql += " OUTPUT INSERTED.SEQUENCE_ID, INSERTED.DOCUMENT_ID, INSERTED.DOCUMENT_DATE, INSERTED.DOCUMENT_TIME, INSERTED.RECEIVE_TYPE_ID, INSERTED.PO_REFERENCE_DOCUMENT, INSERTED.PO_REFERENCE_DATE, INSERTED.DO_REFERENCE_DOCUMENT1, INSERTED.DO_REFERENCE_DATE, INSERTED.CHECKER_ID, INSERTED.REFERENCE_DOCUMENT1, INSERTED.REFERENCE_DOCUMENT2, INSERTED.REFERENCE_DOCUMENT3, INSERTED.RECEIVE_REASON, INSERTED.TOTAL_UNIT, INSERTED.GROSS_WEIGHT, INSERTED.TOTAL_PRICE, INSERTED.SKU_ID, INSERTED.PRODUCT_ID, INSERTED.VOLUME, INSERTED.AMOUNT, INSERTED.UNIT, INSERTED.PRODUCT_STATUS_ID, INSERTED.PRODUCTION_DATE, INSERTED.EXPIRE_DATE, INSERTED.TIME_START, INSERTED.TIME_END, INSERTED.TIME4JOB, INSERTED.VALUE_PER_PACK, INSERTED.VALUE, INSERTED.NET_WEIGHT, INSERTED.COUNT_STATUS, INSERTED.ACTIVE_STATUS, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE"
                Sql += " VALUES("
                sql += "@_DOCUMENT_ID" & ", "
                sql += "@_DOCUMENT_DATE" & ", "
                sql += "@_DOCUMENT_TIME" & ", "
                sql += "@_RECEIVE_TYPE_ID" & ", "
                sql += "@_PO_REFERENCE_DOCUMENT" & ", "
                sql += "@_PO_REFERENCE_DATE" & ", "
                sql += "@_DO_REFERENCE_DOCUMENT1" & ", "
                sql += "@_DO_REFERENCE_DATE" & ", "
                sql += "@_CHECKER_ID" & ", "
                sql += "@_REFERENCE_DOCUMENT1" & ", "
                sql += "@_REFERENCE_DOCUMENT2" & ", "
                sql += "@_REFERENCE_DOCUMENT3" & ", "
                sql += "@_RECEIVE_REASON" & ", "
                sql += "@_TOTAL_UNIT" & ", "
                sql += "@_GROSS_WEIGHT" & ", "
                sql += "@_TOTAL_PRICE" & ", "
                sql += "@_SKU_ID" & ", "
                sql += "@_PRODUCT_ID" & ", "
                sql += "@_VOLUME" & ", "
                sql += "@_AMOUNT" & ", "
                sql += "@_UNIT" & ", "
                sql += "@_PRODUCT_STATUS_ID" & ", "
                sql += "@_PRODUCTION_DATE" & ", "
                sql += "@_EXPIRE_DATE" & ", "
                sql += "@_TIME_START" & ", "
                sql += "@_TIME_END" & ", "
                sql += "@_TIME4JOB" & ", "
                sql += "@_VALUE_PER_PACK" & ", "
                sql += "@_VALUE" & ", "
                sql += "@_NET_WEIGHT" & ", "
                sql += "@_COUNT_STATUS" & ", "
                sql += "@_ACTIVE_STATUS" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE" & ", "
                sql += "@_UPDATED_BY" & ", "
                sql += "@_UPDATED_DATE"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table TB_RCStep1_Receive
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "DOCUMENT_ID = " & "@_DOCUMENT_ID" & ", "
                Sql += "DOCUMENT_DATE = " & "@_DOCUMENT_DATE" & ", "
                Sql += "DOCUMENT_TIME = " & "@_DOCUMENT_TIME" & ", "
                Sql += "RECEIVE_TYPE_ID = " & "@_RECEIVE_TYPE_ID" & ", "
                Sql += "PO_REFERENCE_DOCUMENT = " & "@_PO_REFERENCE_DOCUMENT" & ", "
                Sql += "PO_REFERENCE_DATE = " & "@_PO_REFERENCE_DATE" & ", "
                Sql += "DO_REFERENCE_DOCUMENT1 = " & "@_DO_REFERENCE_DOCUMENT1" & ", "
                Sql += "DO_REFERENCE_DATE = " & "@_DO_REFERENCE_DATE" & ", "
                Sql += "CHECKER_ID = " & "@_CHECKER_ID" & ", "
                Sql += "REFERENCE_DOCUMENT1 = " & "@_REFERENCE_DOCUMENT1" & ", "
                Sql += "REFERENCE_DOCUMENT2 = " & "@_REFERENCE_DOCUMENT2" & ", "
                Sql += "REFERENCE_DOCUMENT3 = " & "@_REFERENCE_DOCUMENT3" & ", "
                Sql += "RECEIVE_REASON = " & "@_RECEIVE_REASON" & ", "
                Sql += "TOTAL_UNIT = " & "@_TOTAL_UNIT" & ", "
                Sql += "GROSS_WEIGHT = " & "@_GROSS_WEIGHT" & ", "
                Sql += "TOTAL_PRICE = " & "@_TOTAL_PRICE" & ", "
                Sql += "SKU_ID = " & "@_SKU_ID" & ", "
                Sql += "PRODUCT_ID = " & "@_PRODUCT_ID" & ", "
                Sql += "VOLUME = " & "@_VOLUME" & ", "
                Sql += "AMOUNT = " & "@_AMOUNT" & ", "
                Sql += "UNIT = " & "@_UNIT" & ", "
                Sql += "PRODUCT_STATUS_ID = " & "@_PRODUCT_STATUS_ID" & ", "
                Sql += "PRODUCTION_DATE = " & "@_PRODUCTION_DATE" & ", "
                Sql += "EXPIRE_DATE = " & "@_EXPIRE_DATE" & ", "
                Sql += "TIME_START = " & "@_TIME_START" & ", "
                Sql += "TIME_END = " & "@_TIME_END" & ", "
                Sql += "TIME4JOB = " & "@_TIME4JOB" & ", "
                Sql += "VALUE_PER_PACK = " & "@_VALUE_PER_PACK" & ", "
                Sql += "VALUE = " & "@_VALUE" & ", "
                Sql += "NET_WEIGHT = " & "@_NET_WEIGHT" & ", "
                Sql += "COUNT_STATUS = " & "@_COUNT_STATUS" & ", "
                Sql += "ACTIVE_STATUS = " & "@_ACTIVE_STATUS" & ", "
                Sql += "CREATED_BY = " & "@_CREATED_BY" & ", "
                Sql += "CREATED_DATE = " & "@_CREATED_DATE" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table TB_RCStep1_Receive
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table TB_RCStep1_Receive
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT SEQUENCE_ID, DOCUMENT_ID, DOCUMENT_DATE, DOCUMENT_TIME, RECEIVE_TYPE_ID, PO_REFERENCE_DOCUMENT, PO_REFERENCE_DATE, DO_REFERENCE_DOCUMENT1, DO_REFERENCE_DATE, CHECKER_ID, REFERENCE_DOCUMENT1, REFERENCE_DOCUMENT2, REFERENCE_DOCUMENT3, RECEIVE_REASON, TOTAL_UNIT, GROSS_WEIGHT, TOTAL_PRICE, SKU_ID, PRODUCT_ID, VOLUME, AMOUNT, UNIT, PRODUCT_STATUS_ID, PRODUCTION_DATE, EXPIRE_DATE, TIME_START, TIME_END, TIME4JOB, VALUE_PER_PACK, VALUE, NET_WEIGHT, COUNT_STATUS, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
