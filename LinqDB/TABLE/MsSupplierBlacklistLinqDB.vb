Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for MS_SUPPLIER_BLACKLIST table LinqDB.
    '[Create by  on May, 30 2017]
    Public Class MsSupplierBlacklistLinqDB
        Public sub MsSupplierBlacklistLinqDB()

        End Sub 
        ' MS_SUPPLIER_BLACKLIST
        Const _tableName As String = "MS_SUPPLIER_BLACKLIST"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _SUPPLIER_BLACKLIST_ID As Long = 0
        Dim _SUPPLIER_ID As Long = 0
        Dim _BLACKLIST_STARTDATE As DateTime = New DateTime(1,1,1)
        Dim _BLACKLIST_ENDDATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _BLACKLIST_REASON As String = ""
        Dim _NOTATION As  String  = ""
        Dim _ACTIVE_STATUS As Char = "Y"
        Dim _CREATED_BY As String = ""
        Dim _CREATED_DATE As DateTime = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)

        'Generate Field Property 
        <Column(Storage:="_SUPPLIER_BLACKLIST_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property SUPPLIER_BLACKLIST_ID() As Long
            Get
                Return _SUPPLIER_BLACKLIST_ID
            End Get
            Set(ByVal value As Long)
               _SUPPLIER_BLACKLIST_ID = value
            End Set
        End Property 
        <Column(Storage:="_SUPPLIER_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property SUPPLIER_ID() As Long
            Get
                Return _SUPPLIER_ID
            End Get
            Set(ByVal value As Long)
               _SUPPLIER_ID = value
            End Set
        End Property 
        <Column(Storage:="_BLACKLIST_STARTDATE", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property BLACKLIST_STARTDATE() As DateTime
            Get
                Return _BLACKLIST_STARTDATE
            End Get
            Set(ByVal value As DateTime)
               _BLACKLIST_STARTDATE = value
            End Set
        End Property 
        <Column(Storage:="_BLACKLIST_ENDDATE", DbType:="DateTime")>  _
        Public Property BLACKLIST_ENDDATE() As  System.Nullable(Of DateTime) 
            Get
                Return _BLACKLIST_ENDDATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _BLACKLIST_ENDDATE = value
            End Set
        End Property 
        <Column(Storage:="_BLACKLIST_REASON", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property BLACKLIST_REASON() As String
            Get
                Return _BLACKLIST_REASON
            End Get
            Set(ByVal value As String)
               _BLACKLIST_REASON = value
            End Set
        End Property 
        <Column(Storage:="_NOTATION", DbType:="VarChar(200)")>  _
        Public Property NOTATION() As  String 
            Get
                Return _NOTATION
            End Get
            Set(ByVal value As  String )
               _NOTATION = value
            End Set
        End Property 
        <Column(Storage:="_ACTIVE_STATUS", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ACTIVE_STATUS() As Char
            Get
                Return _ACTIVE_STATUS
            End Get
            Set(ByVal value As Char)
               _ACTIVE_STATUS = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_BY() As String
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As String)
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_DATE() As DateTime
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As DateTime)
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _SUPPLIER_BLACKLIST_ID = 0
            _SUPPLIER_ID = 0
            _BLACKLIST_STARTDATE = New DateTime(1,1,1)
            _BLACKLIST_ENDDATE = New DateTime(1,1,1)
            _BLACKLIST_REASON = ""
            _NOTATION = ""
            _ACTIVE_STATUS = "Y"
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into MS_SUPPLIER_BLACKLIST table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to MS_SUPPLIER_BLACKLIST table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _Supplier_Blacklist_ID > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("SUPPLIER_BLACKLIST_ID = @_SUPPLIER_BLACKLIST_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to MS_SUPPLIER_BLACKLIST table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from MS_SUPPLIER_BLACKLIST table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cSUPPLIER_BLACKLIST_ID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_SUPPLIER_BLACKLIST_ID", cSUPPLIER_BLACKLIST_ID)
                Return doDelete("SUPPLIER_BLACKLIST_ID = @_SUPPLIER_BLACKLIST_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of MS_SUPPLIER_BLACKLIST by specified SUPPLIER_BLACKLIST_ID key is retrieved successfully.
        '/// <param name=cSUPPLIER_BLACKLIST_ID>The SUPPLIER_BLACKLIST_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cSUPPLIER_BLACKLIST_ID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_SUPPLIER_BLACKLIST_ID", cSUPPLIER_BLACKLIST_ID)
            Return doChkData("SUPPLIER_BLACKLIST_ID = @_SUPPLIER_BLACKLIST_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of MS_SUPPLIER_BLACKLIST by specified SUPPLIER_BLACKLIST_ID key is retrieved successfully.
        '/// <param name=cSUPPLIER_BLACKLIST_ID>The SUPPLIER_BLACKLIST_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cSUPPLIER_BLACKLIST_ID As Long, trans As SQLTransaction) As MsSupplierBlacklistLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_SUPPLIER_BLACKLIST_ID", cSUPPLIER_BLACKLIST_ID)
            Return doGetData("SUPPLIER_BLACKLIST_ID = @_SUPPLIER_BLACKLIST_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of MS_SUPPLIER_BLACKLIST by specified SUPPLIER_ID key is retrieved successfully.
        '/// <param name=cSUPPLIER_ID>The SUPPLIER_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataBySUPPLIER_ID(cSUPPLIER_ID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_SUPPLIER_ID", cSUPPLIER_ID) 
            Return doChkData("SUPPLIER_ID = @_SUPPLIER_ID", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of MS_SUPPLIER_BLACKLIST by specified SUPPLIER_ID key is retrieved successfully.
        '/// <param name=cSUPPLIER_ID>The SUPPLIER_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateBySUPPLIER_ID(cSUPPLIER_ID As Long, cSUPPLIER_BLACKLIST_ID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_SUPPLIER_ID", cSUPPLIER_ID) 
            cmdPara(1) = DB.SetBigInt("@_SUPPLIER_BLACKLIST_ID", cSUPPLIER_BLACKLIST_ID) 
            Return doChkData("SUPPLIER_ID = @_SUPPLIER_ID And SUPPLIER_BLACKLIST_ID <> @_SUPPLIER_BLACKLIST_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of MS_SUPPLIER_BLACKLIST by specified BLACKLIST_STARTDATE key is retrieved successfully.
        '/// <param name=cBLACKLIST_STARTDATE>The BLACKLIST_STARTDATE key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByBLACKLIST_STARTDATE(cBLACKLIST_STARTDATE As DateTime, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_BLACKLIST_STARTDATE", cBLACKLIST_STARTDATE) 
            Return doChkData("BLACKLIST_STARTDATE = @_BLACKLIST_STARTDATE", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of MS_SUPPLIER_BLACKLIST by specified BLACKLIST_STARTDATE key is retrieved successfully.
        '/// <param name=cBLACKLIST_STARTDATE>The BLACKLIST_STARTDATE key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByBLACKLIST_STARTDATE(cBLACKLIST_STARTDATE As DateTime, cSUPPLIER_BLACKLIST_ID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_BLACKLIST_STARTDATE", cBLACKLIST_STARTDATE) 
            cmdPara(1) = DB.SetBigInt("@_SUPPLIER_BLACKLIST_ID", cSUPPLIER_BLACKLIST_ID) 
            Return doChkData("BLACKLIST_STARTDATE = @_BLACKLIST_STARTDATE And SUPPLIER_BLACKLIST_ID <> @_SUPPLIER_BLACKLIST_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of MS_SUPPLIER_BLACKLIST by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into MS_SUPPLIER_BLACKLIST table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _SUPPLIER_BLACKLIST_ID = dt.Rows(0)("SUPPLIER_BLACKLIST_ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                        ret.NewID = _SUPPLIER_BLACKLIST_ID
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to MS_SUPPLIER_BLACKLIST table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                            ret.NewID = _SUPPLIER_BLACKLIST_ID
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from MS_SUPPLIER_BLACKLIST table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(10) As SqlParameter
            cmbParam(0) = New SqlParameter("@_SUPPLIER_BLACKLIST_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _SUPPLIER_BLACKLIST_ID

            cmbParam(1) = New SqlParameter("@_SUPPLIER_ID", SqlDbType.BigInt)
            cmbParam(1).Value = _SUPPLIER_ID

            cmbParam(2) = New SqlParameter("@_BLACKLIST_STARTDATE", SqlDbType.DateTime)
            cmbParam(2).Value = _BLACKLIST_STARTDATE

            cmbParam(3) = New SqlParameter("@_BLACKLIST_ENDDATE", SqlDbType.DateTime)
            If _BLACKLIST_ENDDATE.Value.Year > 1 Then 
                cmbParam(3).Value = _BLACKLIST_ENDDATE.Value
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_BLACKLIST_REASON", SqlDbType.VarChar)
            cmbParam(4).Value = _BLACKLIST_REASON.Trim

            cmbParam(5) = New SqlParameter("@_NOTATION", SqlDbType.VarChar)
            If _NOTATION.Trim <> "" Then 
                cmbParam(5).Value = _NOTATION.Trim
            Else
                cmbParam(5).Value = DBNull.value
            End If

            cmbParam(6) = New SqlParameter("@_ACTIVE_STATUS", SqlDbType.Char)
            cmbParam(6).Value = _ACTIVE_STATUS

            cmbParam(7) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            cmbParam(7).Value = _CREATED_BY.Trim

            cmbParam(8) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            cmbParam(8).Value = _CREATED_DATE

            cmbParam(9) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(9).Value = _UPDATED_BY.Trim
            Else
                cmbParam(9).Value = DBNull.value
            End If

            cmbParam(10) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(10).Value = _UPDATED_DATE.Value
            Else
                cmbParam(10).Value = DBNull.value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of MS_SUPPLIER_BLACKLIST by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Supplier_Blacklist_ID")) = False Then _Supplier_Blacklist_ID = Convert.ToInt64(Rdr("Supplier_Blacklist_ID"))
                        If Convert.IsDBNull(Rdr("Supplier_ID")) = False Then _Supplier_ID = Convert.ToInt64(Rdr("Supplier_ID"))
                        If Convert.IsDBNull(Rdr("Blacklist_StartDate")) = False Then _Blacklist_StartDate = Convert.ToDateTime(Rdr("Blacklist_StartDate"))
                        If Convert.IsDBNull(Rdr("Blacklist_EndDate")) = False Then _Blacklist_EndDate = Convert.ToDateTime(Rdr("Blacklist_EndDate"))
                        If Convert.IsDBNull(Rdr("Blacklist_Reason")) = False Then _Blacklist_Reason = Rdr("Blacklist_Reason").ToString()
                        If Convert.IsDBNull(Rdr("Notation")) = False Then _Notation = Rdr("Notation").ToString()
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _Active_Status = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of MS_SUPPLIER_BLACKLIST by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As MsSupplierBlacklistLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Supplier_Blacklist_ID")) = False Then _Supplier_Blacklist_ID = Convert.ToInt64(Rdr("Supplier_Blacklist_ID"))
                        If Convert.IsDBNull(Rdr("Supplier_ID")) = False Then _Supplier_ID = Convert.ToInt64(Rdr("Supplier_ID"))
                        If Convert.IsDBNull(Rdr("Blacklist_StartDate")) = False Then _Blacklist_StartDate = Convert.ToDateTime(Rdr("Blacklist_StartDate"))
                        If Convert.IsDBNull(Rdr("Blacklist_EndDate")) = False Then _Blacklist_EndDate = Convert.ToDateTime(Rdr("Blacklist_EndDate"))
                        If Convert.IsDBNull(Rdr("Blacklist_Reason")) = False Then _Blacklist_Reason = Rdr("Blacklist_Reason").ToString()
                        If Convert.IsDBNull(Rdr("Notation")) = False Then _Notation = Rdr("Notation").ToString()
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _Active_Status = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table MS_SUPPLIER_BLACKLIST
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (SUPPLIER_ID, BLACKLIST_STARTDATE, BLACKLIST_ENDDATE, BLACKLIST_REASON, NOTATION, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE)"
                Sql += " OUTPUT INSERTED.SUPPLIER_BLACKLIST_ID, INSERTED.SUPPLIER_ID, INSERTED.BLACKLIST_STARTDATE, INSERTED.BLACKLIST_ENDDATE, INSERTED.BLACKLIST_REASON, INSERTED.NOTATION, INSERTED.ACTIVE_STATUS, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE"
                Sql += " VALUES("
                sql += "@_SUPPLIER_ID" & ", "
                sql += "@_BLACKLIST_STARTDATE" & ", "
                sql += "@_BLACKLIST_ENDDATE" & ", "
                sql += "@_BLACKLIST_REASON" & ", "
                sql += "@_NOTATION" & ", "
                sql += "@_ACTIVE_STATUS" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE" & ", "
                sql += "@_UPDATED_BY" & ", "
                sql += "@_UPDATED_DATE"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table MS_SUPPLIER_BLACKLIST
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "SUPPLIER_ID = " & "@_SUPPLIER_ID" & ", "
                Sql += "BLACKLIST_STARTDATE = " & "@_BLACKLIST_STARTDATE" & ", "
                Sql += "BLACKLIST_ENDDATE = " & "@_BLACKLIST_ENDDATE" & ", "
                Sql += "BLACKLIST_REASON = " & "@_BLACKLIST_REASON" & ", "
                Sql += "NOTATION = " & "@_NOTATION" & ", "
                Sql += "ACTIVE_STATUS = " & "@_ACTIVE_STATUS" & ", "
                Sql += "CREATED_BY = " & "@_CREATED_BY" & ", "
                Sql += "CREATED_DATE = " & "@_CREATED_DATE" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table MS_SUPPLIER_BLACKLIST
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table MS_SUPPLIER_BLACKLIST
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT SUPPLIER_BLACKLIST_ID, SUPPLIER_ID, BLACKLIST_STARTDATE, BLACKLIST_ENDDATE, BLACKLIST_REASON, NOTATION, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
