Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for MS_Staff table LinqDB.
    '[Create by  on June, 29 2017]
    Public Class MsStaffLinqDB
        Public sub MsStaffLinqDB()

        End Sub 
        ' MS_Staff
        Const _tableName As String = "MS_Staff"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _SEQUENCE_ID As Long = 0
        Dim _STAFF_ID As String = ""
        Dim _PREFIX_NAME_ID As  System.Nullable(Of Long) 
        Dim _FISRT_NAME As  String  = ""
        Dim _LAST_NAME As  String  = ""
        Dim _POSITION_ID As  System.Nullable(Of Long) 
        Dim _DEPARTMENT_ID As  System.Nullable(Of Long) 
        Dim _ACTIVE_STATUS As Char = "Y"
        Dim _CREATED_BY As String = ""
        Dim _CREATED_DATE As DateTime = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)

        'Generate Field Property 
        <Column(Storage:="_SEQUENCE_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property SEQUENCE_ID() As Long
            Get
                Return _SEQUENCE_ID
            End Get
            Set(ByVal value As Long)
               _SEQUENCE_ID = value
            End Set
        End Property 
        <Column(Storage:="_STAFF_ID", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property STAFF_ID() As String
            Get
                Return _STAFF_ID
            End Get
            Set(ByVal value As String)
               _STAFF_ID = value
            End Set
        End Property 
        <Column(Storage:="_PREFIX_NAME_ID", DbType:="BigInt")>  _
        Public Property PREFIX_NAME_ID() As  System.Nullable(Of Long) 
            Get
                Return _PREFIX_NAME_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _PREFIX_NAME_ID = value
            End Set
        End Property 
        <Column(Storage:="_FISRT_NAME", DbType:="VarChar(100)")>  _
        Public Property FISRT_NAME() As  String 
            Get
                Return _FISRT_NAME
            End Get
            Set(ByVal value As  String )
               _FISRT_NAME = value
            End Set
        End Property 
        <Column(Storage:="_LAST_NAME", DbType:="VarChar(100)")>  _
        Public Property LAST_NAME() As  String 
            Get
                Return _LAST_NAME
            End Get
            Set(ByVal value As  String )
               _LAST_NAME = value
            End Set
        End Property 
        <Column(Storage:="_POSITION_ID", DbType:="BigInt")>  _
        Public Property POSITION_ID() As  System.Nullable(Of Long) 
            Get
                Return _POSITION_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _POSITION_ID = value
            End Set
        End Property 
        <Column(Storage:="_DEPARTMENT_ID", DbType:="BigInt")>  _
        Public Property DEPARTMENT_ID() As  System.Nullable(Of Long) 
            Get
                Return _DEPARTMENT_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _DEPARTMENT_ID = value
            End Set
        End Property 
        <Column(Storage:="_ACTIVE_STATUS", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ACTIVE_STATUS() As Char
            Get
                Return _ACTIVE_STATUS
            End Get
            Set(ByVal value As Char)
               _ACTIVE_STATUS = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_BY() As String
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As String)
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_DATE() As DateTime
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As DateTime)
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _SEQUENCE_ID = 0
            _STAFF_ID = ""
            _PREFIX_NAME_ID = Nothing
            _FISRT_NAME = ""
            _LAST_NAME = ""
            _POSITION_ID = Nothing
            _DEPARTMENT_ID = Nothing
            _ACTIVE_STATUS = "Y"
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into MS_Staff table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to MS_Staff table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _Sequence_ID > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("SEQUENCE_ID = @_SEQUENCE_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to MS_Staff table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from MS_Staff table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cSEQUENCE_ID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_SEQUENCE_ID", cSEQUENCE_ID)
                Return doDelete("SEQUENCE_ID = @_SEQUENCE_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of MS_Staff by specified SEQUENCE_ID key is retrieved successfully.
        '/// <param name=cSEQUENCE_ID>The SEQUENCE_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cSEQUENCE_ID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_SEQUENCE_ID", cSEQUENCE_ID)
            Return doChkData("SEQUENCE_ID = @_SEQUENCE_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of MS_Staff by specified SEQUENCE_ID key is retrieved successfully.
        '/// <param name=cSEQUENCE_ID>The SEQUENCE_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cSEQUENCE_ID As Long, trans As SQLTransaction) As MsStaffLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_SEQUENCE_ID", cSEQUENCE_ID)
            Return doGetData("SEQUENCE_ID = @_SEQUENCE_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of MS_Staff by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into MS_Staff table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _SEQUENCE_ID = dt.Rows(0)("SEQUENCE_ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                        ret.NewID = _SEQUENCE_ID
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to MS_Staff table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                            ret.NewID = _SEQUENCE_ID
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from MS_Staff table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(11) As SqlParameter
            cmbParam(0) = New SqlParameter("@_SEQUENCE_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _SEQUENCE_ID

            cmbParam(1) = New SqlParameter("@_STAFF_ID", SqlDbType.VarChar)
            cmbParam(1).Value = _STAFF_ID.Trim

            cmbParam(2) = New SqlParameter("@_PREFIX_NAME_ID", SqlDbType.BigInt)
            If _PREFIX_NAME_ID IsNot Nothing Then 
                cmbParam(2).Value = _PREFIX_NAME_ID.Value
            Else
                cmbParam(2).Value = DBNull.value
            End IF

            cmbParam(3) = New SqlParameter("@_FISRT_NAME", SqlDbType.VarChar)
            If _FISRT_NAME.Trim <> "" Then 
                cmbParam(3).Value = _FISRT_NAME.Trim
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_LAST_NAME", SqlDbType.VarChar)
            If _LAST_NAME.Trim <> "" Then 
                cmbParam(4).Value = _LAST_NAME.Trim
            Else
                cmbParam(4).Value = DBNull.value
            End If

            cmbParam(5) = New SqlParameter("@_POSITION_ID", SqlDbType.BigInt)
            If _POSITION_ID IsNot Nothing Then 
                cmbParam(5).Value = _POSITION_ID.Value
            Else
                cmbParam(5).Value = DBNull.value
            End IF

            cmbParam(6) = New SqlParameter("@_DEPARTMENT_ID", SqlDbType.BigInt)
            If _DEPARTMENT_ID IsNot Nothing Then 
                cmbParam(6).Value = _DEPARTMENT_ID.Value
            Else
                cmbParam(6).Value = DBNull.value
            End IF

            cmbParam(7) = New SqlParameter("@_ACTIVE_STATUS", SqlDbType.Char)
            cmbParam(7).Value = _ACTIVE_STATUS

            cmbParam(8) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            cmbParam(8).Value = _CREATED_BY.Trim

            cmbParam(9) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            cmbParam(9).Value = _CREATED_DATE

            cmbParam(10) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(10).Value = _UPDATED_BY.Trim
            Else
                cmbParam(10).Value = DBNull.value
            End If

            cmbParam(11) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(11).Value = _UPDATED_DATE.Value
            Else
                cmbParam(11).Value = DBNull.value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of MS_Staff by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Sequence_ID")) = False Then _Sequence_ID = Convert.ToInt64(Rdr("Sequence_ID"))
                        If Convert.IsDBNull(Rdr("Staff_ID")) = False Then _Staff_ID = Rdr("Staff_ID").ToString()
                        If Convert.IsDBNull(Rdr("Prefix_Name_ID")) = False Then _Prefix_Name_ID = Convert.ToInt64(Rdr("Prefix_Name_ID"))
                        If Convert.IsDBNull(Rdr("Fisrt_Name")) = False Then _Fisrt_Name = Rdr("Fisrt_Name").ToString()
                        If Convert.IsDBNull(Rdr("Last_Name")) = False Then _Last_Name = Rdr("Last_Name").ToString()
                        If Convert.IsDBNull(Rdr("Position_ID")) = False Then _Position_ID = Convert.ToInt64(Rdr("Position_ID"))
                        If Convert.IsDBNull(Rdr("Department_ID")) = False Then _Department_ID = Convert.ToInt64(Rdr("Department_ID"))
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _Active_Status = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of MS_Staff by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As MsStaffLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Sequence_ID")) = False Then _Sequence_ID = Convert.ToInt64(Rdr("Sequence_ID"))
                        If Convert.IsDBNull(Rdr("Staff_ID")) = False Then _Staff_ID = Rdr("Staff_ID").ToString()
                        If Convert.IsDBNull(Rdr("Prefix_Name_ID")) = False Then _Prefix_Name_ID = Convert.ToInt64(Rdr("Prefix_Name_ID"))
                        If Convert.IsDBNull(Rdr("Fisrt_Name")) = False Then _Fisrt_Name = Rdr("Fisrt_Name").ToString()
                        If Convert.IsDBNull(Rdr("Last_Name")) = False Then _Last_Name = Rdr("Last_Name").ToString()
                        If Convert.IsDBNull(Rdr("Position_ID")) = False Then _Position_ID = Convert.ToInt64(Rdr("Position_ID"))
                        If Convert.IsDBNull(Rdr("Department_ID")) = False Then _Department_ID = Convert.ToInt64(Rdr("Department_ID"))
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _Active_Status = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table MS_Staff
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (STAFF_ID, PREFIX_NAME_ID, FISRT_NAME, LAST_NAME, POSITION_ID, DEPARTMENT_ID, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE)"
                Sql += " OUTPUT INSERTED.SEQUENCE_ID, INSERTED.STAFF_ID, INSERTED.PREFIX_NAME_ID, INSERTED.FISRT_NAME, INSERTED.LAST_NAME, INSERTED.POSITION_ID, INSERTED.DEPARTMENT_ID, INSERTED.ACTIVE_STATUS, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE"
                Sql += " VALUES("
                sql += "@_STAFF_ID" & ", "
                sql += "@_PREFIX_NAME_ID" & ", "
                sql += "@_FISRT_NAME" & ", "
                sql += "@_LAST_NAME" & ", "
                sql += "@_POSITION_ID" & ", "
                sql += "@_DEPARTMENT_ID" & ", "
                sql += "@_ACTIVE_STATUS" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE" & ", "
                sql += "@_UPDATED_BY" & ", "
                sql += "@_UPDATED_DATE"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table MS_Staff
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "STAFF_ID = " & "@_STAFF_ID" & ", "
                Sql += "PREFIX_NAME_ID = " & "@_PREFIX_NAME_ID" & ", "
                Sql += "FISRT_NAME = " & "@_FISRT_NAME" & ", "
                Sql += "LAST_NAME = " & "@_LAST_NAME" & ", "
                Sql += "POSITION_ID = " & "@_POSITION_ID" & ", "
                Sql += "DEPARTMENT_ID = " & "@_DEPARTMENT_ID" & ", "
                Sql += "ACTIVE_STATUS = " & "@_ACTIVE_STATUS" & ", "
                Sql += "CREATED_BY = " & "@_CREATED_BY" & ", "
                Sql += "CREATED_DATE = " & "@_CREATED_DATE" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table MS_Staff
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table MS_Staff
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT SEQUENCE_ID, STAFF_ID, PREFIX_NAME_ID, FISRT_NAME, LAST_NAME, POSITION_ID, DEPARTMENT_ID, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
