Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for MS_Customer table LinqDB.
    '[Create by  on May, 30 2017]
    Public Class MsCustomerLinqDB
        Public sub MsCustomerLinqDB()

        End Sub 
        ' MS_Customer
        Const _tableName As String = "MS_Customer"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _CUSTOMER_ID As Long = 0
        Dim _CUSTOMER_CODE As String = ""
        Dim _PREFIX_ID As  System.Nullable(Of Long) 
        Dim _CUSTOMER_TNAME As  String  = ""
        Dim _CUSTOMER_ENAME As  String  = ""
        Dim _CUSTOMER_ALIAS As  String  = ""
        Dim _CUSTOMER_TAX_NO As  String  = ""
        Dim _CUSTOMER_TYPE_ID As  System.Nullable(Of Long) 
        Dim _BARCODE1 As  String  = ""
        Dim _BARCODE2 As  String  = ""
        Dim _CUSTOMER_PHONE As  String  = ""
        Dim _CUSTOMER_MOBILE As  String  = ""
        Dim _CUSTOMER_CONTACT_PERSON1 As  String  = ""
        Dim _CUSTOMER_CONTACT_PERSON2 As  String  = ""
        Dim _CUSTOMER_CONTACT_PERSON3 As  String  = ""
        Dim _CUSTOMER_FAX As  String  = ""
        Dim _CUSTOMER_EMAIL As  String  = ""
        Dim _ACTIVE_STATUS As Char = "Y"
        Dim _CREATED_BY As String = ""
        Dim _CREATED_DATE As DateTime = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)

        'Generate Field Property 
        <Column(Storage:="_CUSTOMER_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property CUSTOMER_ID() As Long
            Get
                Return _CUSTOMER_ID
            End Get
            Set(ByVal value As Long)
               _CUSTOMER_ID = value
            End Set
        End Property 
        <Column(Storage:="_CUSTOMER_CODE", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property CUSTOMER_CODE() As String
            Get
                Return _CUSTOMER_CODE
            End Get
            Set(ByVal value As String)
               _CUSTOMER_CODE = value
            End Set
        End Property 
        <Column(Storage:="_PREFIX_ID", DbType:="BigInt")>  _
        Public Property PREFIX_ID() As  System.Nullable(Of Long) 
            Get
                Return _PREFIX_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _PREFIX_ID = value
            End Set
        End Property 
        <Column(Storage:="_CUSTOMER_TNAME", DbType:="VarChar(300)")>  _
        Public Property CUSTOMER_TNAME() As  String 
            Get
                Return _CUSTOMER_TNAME
            End Get
            Set(ByVal value As  String )
               _CUSTOMER_TNAME = value
            End Set
        End Property 
        <Column(Storage:="_CUSTOMER_ENAME", DbType:="VarChar(300)")>  _
        Public Property CUSTOMER_ENAME() As  String 
            Get
                Return _CUSTOMER_ENAME
            End Get
            Set(ByVal value As  String )
               _CUSTOMER_ENAME = value
            End Set
        End Property 
        <Column(Storage:="_CUSTOMER_ALIAS", DbType:="VarChar(100)")>  _
        Public Property CUSTOMER_ALIAS() As  String 
            Get
                Return _CUSTOMER_ALIAS
            End Get
            Set(ByVal value As  String )
               _CUSTOMER_ALIAS = value
            End Set
        End Property 
        <Column(Storage:="_CUSTOMER_TAX_NO", DbType:="VarChar(50)")>  _
        Public Property CUSTOMER_TAX_NO() As  String 
            Get
                Return _CUSTOMER_TAX_NO
            End Get
            Set(ByVal value As  String )
               _CUSTOMER_TAX_NO = value
            End Set
        End Property 
        <Column(Storage:="_CUSTOMER_TYPE_ID", DbType:="BigInt")>  _
        Public Property CUSTOMER_TYPE_ID() As  System.Nullable(Of Long) 
            Get
                Return _CUSTOMER_TYPE_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _CUSTOMER_TYPE_ID = value
            End Set
        End Property 
        <Column(Storage:="_BARCODE1", DbType:="VarChar(50)")>  _
        Public Property BARCODE1() As  String 
            Get
                Return _BARCODE1
            End Get
            Set(ByVal value As  String )
               _BARCODE1 = value
            End Set
        End Property 
        <Column(Storage:="_BARCODE2", DbType:="VarChar(50)")>  _
        Public Property BARCODE2() As  String 
            Get
                Return _BARCODE2
            End Get
            Set(ByVal value As  String )
               _BARCODE2 = value
            End Set
        End Property 
        <Column(Storage:="_CUSTOMER_PHONE", DbType:="VarChar(50)")>  _
        Public Property CUSTOMER_PHONE() As  String 
            Get
                Return _CUSTOMER_PHONE
            End Get
            Set(ByVal value As  String )
               _CUSTOMER_PHONE = value
            End Set
        End Property 
        <Column(Storage:="_CUSTOMER_MOBILE", DbType:="VarChar(50)")>  _
        Public Property CUSTOMER_MOBILE() As  String 
            Get
                Return _CUSTOMER_MOBILE
            End Get
            Set(ByVal value As  String )
               _CUSTOMER_MOBILE = value
            End Set
        End Property 
        <Column(Storage:="_CUSTOMER_CONTACT_PERSON1", DbType:="VarChar(100)")>  _
        Public Property CUSTOMER_CONTACT_PERSON1() As  String 
            Get
                Return _CUSTOMER_CONTACT_PERSON1
            End Get
            Set(ByVal value As  String )
               _CUSTOMER_CONTACT_PERSON1 = value
            End Set
        End Property 
        <Column(Storage:="_CUSTOMER_CONTACT_PERSON2", DbType:="VarChar(100)")>  _
        Public Property CUSTOMER_CONTACT_PERSON2() As  String 
            Get
                Return _CUSTOMER_CONTACT_PERSON2
            End Get
            Set(ByVal value As  String )
               _CUSTOMER_CONTACT_PERSON2 = value
            End Set
        End Property 
        <Column(Storage:="_CUSTOMER_CONTACT_PERSON3", DbType:="VarChar(100)")>  _
        Public Property CUSTOMER_CONTACT_PERSON3() As  String 
            Get
                Return _CUSTOMER_CONTACT_PERSON3
            End Get
            Set(ByVal value As  String )
               _CUSTOMER_CONTACT_PERSON3 = value
            End Set
        End Property 
        <Column(Storage:="_CUSTOMER_FAX", DbType:="VarChar(50)")>  _
        Public Property CUSTOMER_FAX() As  String 
            Get
                Return _CUSTOMER_FAX
            End Get
            Set(ByVal value As  String )
               _CUSTOMER_FAX = value
            End Set
        End Property 
        <Column(Storage:="_CUSTOMER_EMAIL", DbType:="VarChar(50)")>  _
        Public Property CUSTOMER_EMAIL() As  String 
            Get
                Return _CUSTOMER_EMAIL
            End Get
            Set(ByVal value As  String )
               _CUSTOMER_EMAIL = value
            End Set
        End Property 
        <Column(Storage:="_ACTIVE_STATUS", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ACTIVE_STATUS() As Char
            Get
                Return _ACTIVE_STATUS
            End Get
            Set(ByVal value As Char)
               _ACTIVE_STATUS = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_BY() As String
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As String)
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_DATE() As DateTime
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As DateTime)
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _CUSTOMER_ID = 0
            _CUSTOMER_CODE = ""
            _PREFIX_ID = Nothing
            _CUSTOMER_TNAME = ""
            _CUSTOMER_ENAME = ""
            _CUSTOMER_ALIAS = ""
            _CUSTOMER_TAX_NO = ""
            _CUSTOMER_TYPE_ID = Nothing
            _BARCODE1 = ""
            _BARCODE2 = ""
            _CUSTOMER_PHONE = ""
            _CUSTOMER_MOBILE = ""
            _CUSTOMER_CONTACT_PERSON1 = ""
            _CUSTOMER_CONTACT_PERSON2 = ""
            _CUSTOMER_CONTACT_PERSON3 = ""
            _CUSTOMER_FAX = ""
            _CUSTOMER_EMAIL = ""
            _ACTIVE_STATUS = "Y"
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into MS_Customer table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to MS_Customer table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _Customer_ID > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("CUSTOMER_ID = @_CUSTOMER_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to MS_Customer table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from MS_Customer table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cCUSTOMER_ID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_CUSTOMER_ID", cCUSTOMER_ID)
                Return doDelete("CUSTOMER_ID = @_CUSTOMER_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of MS_Customer by specified CUSTOMER_ID key is retrieved successfully.
        '/// <param name=cCUSTOMER_ID>The CUSTOMER_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cCUSTOMER_ID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_CUSTOMER_ID", cCUSTOMER_ID)
            Return doChkData("CUSTOMER_ID = @_CUSTOMER_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of MS_Customer by specified CUSTOMER_ID key is retrieved successfully.
        '/// <param name=cCUSTOMER_ID>The CUSTOMER_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cCUSTOMER_ID As Long, trans As SQLTransaction) As MsCustomerLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_CUSTOMER_ID", cCUSTOMER_ID)
            Return doGetData("CUSTOMER_ID = @_CUSTOMER_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of MS_Customer by specified CUSTOMER_CODE key is retrieved successfully.
        '/// <param name=cCUSTOMER_CODE>The CUSTOMER_CODE key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByCUSTOMER_CODE(cCUSTOMER_CODE As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_CUSTOMER_CODE", cCUSTOMER_CODE) 
            Return doChkData("CUSTOMER_CODE = @_CUSTOMER_CODE", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of MS_Customer by specified CUSTOMER_CODE key is retrieved successfully.
        '/// <param name=cCUSTOMER_CODE>The CUSTOMER_CODE key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByCUSTOMER_CODE(cCUSTOMER_CODE As String, cCUSTOMER_ID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_CUSTOMER_CODE", cCUSTOMER_CODE) 
            cmdPara(1) = DB.SetBigInt("@_CUSTOMER_ID", cCUSTOMER_ID) 
            Return doChkData("CUSTOMER_CODE = @_CUSTOMER_CODE And CUSTOMER_ID <> @_CUSTOMER_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of MS_Customer by specified CUSTOMER_TAX_NO key is retrieved successfully.
        '/// <param name=cCUSTOMER_TAX_NO>The CUSTOMER_TAX_NO key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByCUSTOMER_TAX_NO(cCUSTOMER_TAX_NO As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_CUSTOMER_TAX_NO", cCUSTOMER_TAX_NO) 
            Return doChkData("CUSTOMER_TAX_NO = @_CUSTOMER_TAX_NO", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of MS_Customer by specified CUSTOMER_TAX_NO key is retrieved successfully.
        '/// <param name=cCUSTOMER_TAX_NO>The CUSTOMER_TAX_NO key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByCUSTOMER_TAX_NO(cCUSTOMER_TAX_NO As String, cCUSTOMER_ID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_CUSTOMER_TAX_NO", cCUSTOMER_TAX_NO) 
            cmdPara(1) = DB.SetBigInt("@_CUSTOMER_ID", cCUSTOMER_ID) 
            Return doChkData("CUSTOMER_TAX_NO = @_CUSTOMER_TAX_NO And CUSTOMER_ID <> @_CUSTOMER_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of MS_Customer by specified CUSTOMER_TNAME key is retrieved successfully.
        '/// <param name=cCUSTOMER_TNAME>The CUSTOMER_TNAME key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByCUSTOMER_TNAME(cCUSTOMER_TNAME As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_CUSTOMER_TNAME", cCUSTOMER_TNAME) 
            Return doChkData("CUSTOMER_TNAME = @_CUSTOMER_TNAME", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of MS_Customer by specified CUSTOMER_TNAME key is retrieved successfully.
        '/// <param name=cCUSTOMER_TNAME>The CUSTOMER_TNAME key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByCUSTOMER_TNAME(cCUSTOMER_TNAME As String, cCUSTOMER_ID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_CUSTOMER_TNAME", cCUSTOMER_TNAME) 
            cmdPara(1) = DB.SetBigInt("@_CUSTOMER_ID", cCUSTOMER_ID) 
            Return doChkData("CUSTOMER_TNAME = @_CUSTOMER_TNAME And CUSTOMER_ID <> @_CUSTOMER_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of MS_Customer by specified CUSTOMER_ENAME key is retrieved successfully.
        '/// <param name=cCUSTOMER_ENAME>The CUSTOMER_ENAME key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByCUSTOMER_ENAME(cCUSTOMER_ENAME As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_CUSTOMER_ENAME", cCUSTOMER_ENAME) 
            Return doChkData("CUSTOMER_ENAME = @_CUSTOMER_ENAME", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of MS_Customer by specified CUSTOMER_ENAME key is retrieved successfully.
        '/// <param name=cCUSTOMER_ENAME>The CUSTOMER_ENAME key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByCUSTOMER_ENAME(cCUSTOMER_ENAME As String, cCUSTOMER_ID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_CUSTOMER_ENAME", cCUSTOMER_ENAME) 
            cmdPara(1) = DB.SetBigInt("@_CUSTOMER_ID", cCUSTOMER_ID) 
            Return doChkData("CUSTOMER_ENAME = @_CUSTOMER_ENAME And CUSTOMER_ID <> @_CUSTOMER_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of MS_Customer by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into MS_Customer table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _CUSTOMER_ID = dt.Rows(0)("CUSTOMER_ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                        ret.NewID = _CUSTOMER_ID
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to MS_Customer table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                            ret.NewID = _CUSTOMER_ID
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from MS_Customer table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(21) As SqlParameter
            cmbParam(0) = New SqlParameter("@_CUSTOMER_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _CUSTOMER_ID

            cmbParam(1) = New SqlParameter("@_CUSTOMER_CODE", SqlDbType.VarChar)
            cmbParam(1).Value = _CUSTOMER_CODE.Trim

            cmbParam(2) = New SqlParameter("@_PREFIX_ID", SqlDbType.BigInt)
            If _PREFIX_ID IsNot Nothing Then 
                cmbParam(2).Value = _PREFIX_ID.Value
            Else
                cmbParam(2).Value = DBNull.value
            End IF

            cmbParam(3) = New SqlParameter("@_CUSTOMER_TNAME", SqlDbType.VarChar)
            If _CUSTOMER_TNAME.Trim <> "" Then 
                cmbParam(3).Value = _CUSTOMER_TNAME.Trim
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_CUSTOMER_ENAME", SqlDbType.VarChar)
            If _CUSTOMER_ENAME.Trim <> "" Then 
                cmbParam(4).Value = _CUSTOMER_ENAME.Trim
            Else
                cmbParam(4).Value = DBNull.value
            End If

            cmbParam(5) = New SqlParameter("@_CUSTOMER_ALIAS", SqlDbType.VarChar)
            If _CUSTOMER_ALIAS.Trim <> "" Then 
                cmbParam(5).Value = _CUSTOMER_ALIAS.Trim
            Else
                cmbParam(5).Value = DBNull.value
            End If

            cmbParam(6) = New SqlParameter("@_CUSTOMER_TAX_NO", SqlDbType.VarChar)
            If _CUSTOMER_TAX_NO.Trim <> "" Then 
                cmbParam(6).Value = _CUSTOMER_TAX_NO.Trim
            Else
                cmbParam(6).Value = DBNull.value
            End If

            cmbParam(7) = New SqlParameter("@_CUSTOMER_TYPE_ID", SqlDbType.BigInt)
            If _CUSTOMER_TYPE_ID IsNot Nothing Then 
                cmbParam(7).Value = _CUSTOMER_TYPE_ID.Value
            Else
                cmbParam(7).Value = DBNull.value
            End IF

            cmbParam(8) = New SqlParameter("@_BARCODE1", SqlDbType.VarChar)
            If _BARCODE1.Trim <> "" Then 
                cmbParam(8).Value = _BARCODE1.Trim
            Else
                cmbParam(8).Value = DBNull.value
            End If

            cmbParam(9) = New SqlParameter("@_BARCODE2", SqlDbType.VarChar)
            If _BARCODE2.Trim <> "" Then 
                cmbParam(9).Value = _BARCODE2.Trim
            Else
                cmbParam(9).Value = DBNull.value
            End If

            cmbParam(10) = New SqlParameter("@_CUSTOMER_PHONE", SqlDbType.VarChar)
            If _CUSTOMER_PHONE.Trim <> "" Then 
                cmbParam(10).Value = _CUSTOMER_PHONE.Trim
            Else
                cmbParam(10).Value = DBNull.value
            End If

            cmbParam(11) = New SqlParameter("@_CUSTOMER_MOBILE", SqlDbType.VarChar)
            If _CUSTOMER_MOBILE.Trim <> "" Then 
                cmbParam(11).Value = _CUSTOMER_MOBILE.Trim
            Else
                cmbParam(11).Value = DBNull.value
            End If

            cmbParam(12) = New SqlParameter("@_CUSTOMER_CONTACT_PERSON1", SqlDbType.VarChar)
            If _CUSTOMER_CONTACT_PERSON1.Trim <> "" Then 
                cmbParam(12).Value = _CUSTOMER_CONTACT_PERSON1.Trim
            Else
                cmbParam(12).Value = DBNull.value
            End If

            cmbParam(13) = New SqlParameter("@_CUSTOMER_CONTACT_PERSON2", SqlDbType.VarChar)
            If _CUSTOMER_CONTACT_PERSON2.Trim <> "" Then 
                cmbParam(13).Value = _CUSTOMER_CONTACT_PERSON2.Trim
            Else
                cmbParam(13).Value = DBNull.value
            End If

            cmbParam(14) = New SqlParameter("@_CUSTOMER_CONTACT_PERSON3", SqlDbType.VarChar)
            If _CUSTOMER_CONTACT_PERSON3.Trim <> "" Then 
                cmbParam(14).Value = _CUSTOMER_CONTACT_PERSON3.Trim
            Else
                cmbParam(14).Value = DBNull.value
            End If

            cmbParam(15) = New SqlParameter("@_CUSTOMER_FAX", SqlDbType.VarChar)
            If _CUSTOMER_FAX.Trim <> "" Then 
                cmbParam(15).Value = _CUSTOMER_FAX.Trim
            Else
                cmbParam(15).Value = DBNull.value
            End If

            cmbParam(16) = New SqlParameter("@_CUSTOMER_EMAIL", SqlDbType.VarChar)
            If _CUSTOMER_EMAIL.Trim <> "" Then 
                cmbParam(16).Value = _CUSTOMER_EMAIL.Trim
            Else
                cmbParam(16).Value = DBNull.value
            End If

            cmbParam(17) = New SqlParameter("@_ACTIVE_STATUS", SqlDbType.Char)
            cmbParam(17).Value = _ACTIVE_STATUS

            cmbParam(18) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            cmbParam(18).Value = _CREATED_BY.Trim

            cmbParam(19) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            cmbParam(19).Value = _CREATED_DATE

            cmbParam(20) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(20).Value = _UPDATED_BY.Trim
            Else
                cmbParam(20).Value = DBNull.value
            End If

            cmbParam(21) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(21).Value = _UPDATED_DATE.Value
            Else
                cmbParam(21).Value = DBNull.value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of MS_Customer by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Customer_ID")) = False Then _Customer_ID = Convert.ToInt64(Rdr("Customer_ID"))
                        If Convert.IsDBNull(Rdr("Customer_Code")) = False Then _Customer_Code = Rdr("Customer_Code").ToString()
                        If Convert.IsDBNull(Rdr("Prefix_ID")) = False Then _Prefix_ID = Convert.ToInt64(Rdr("Prefix_ID"))
                        If Convert.IsDBNull(Rdr("Customer_TName")) = False Then _Customer_TName = Rdr("Customer_TName").ToString()
                        If Convert.IsDBNull(Rdr("Customer_EName")) = False Then _Customer_EName = Rdr("Customer_EName").ToString()
                        If Convert.IsDBNull(Rdr("Customer_Alias")) = False Then _Customer_Alias = Rdr("Customer_Alias").ToString()
                        If Convert.IsDBNull(Rdr("Customer_Tax_No")) = False Then _Customer_Tax_No = Rdr("Customer_Tax_No").ToString()
                        If Convert.IsDBNull(Rdr("Customer_Type_ID")) = False Then _Customer_Type_ID = Convert.ToInt64(Rdr("Customer_Type_ID"))
                        If Convert.IsDBNull(Rdr("Barcode1")) = False Then _Barcode1 = Rdr("Barcode1").ToString()
                        If Convert.IsDBNull(Rdr("Barcode2")) = False Then _Barcode2 = Rdr("Barcode2").ToString()
                        If Convert.IsDBNull(Rdr("Customer_Phone")) = False Then _Customer_Phone = Rdr("Customer_Phone").ToString()
                        If Convert.IsDBNull(Rdr("Customer_Mobile")) = False Then _Customer_Mobile = Rdr("Customer_Mobile").ToString()
                        If Convert.IsDBNull(Rdr("Customer_Contact_Person1")) = False Then _Customer_Contact_Person1 = Rdr("Customer_Contact_Person1").ToString()
                        If Convert.IsDBNull(Rdr("Customer_Contact_Person2")) = False Then _Customer_Contact_Person2 = Rdr("Customer_Contact_Person2").ToString()
                        If Convert.IsDBNull(Rdr("Customer_Contact_Person3")) = False Then _Customer_Contact_Person3 = Rdr("Customer_Contact_Person3").ToString()
                        If Convert.IsDBNull(Rdr("Customer_Fax")) = False Then _Customer_Fax = Rdr("Customer_Fax").ToString()
                        If Convert.IsDBNull(Rdr("Customer_Email")) = False Then _Customer_Email = Rdr("Customer_Email").ToString()
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _Active_Status = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of MS_Customer by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As MsCustomerLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Customer_ID")) = False Then _Customer_ID = Convert.ToInt64(Rdr("Customer_ID"))
                        If Convert.IsDBNull(Rdr("Customer_Code")) = False Then _Customer_Code = Rdr("Customer_Code").ToString()
                        If Convert.IsDBNull(Rdr("Prefix_ID")) = False Then _Prefix_ID = Convert.ToInt64(Rdr("Prefix_ID"))
                        If Convert.IsDBNull(Rdr("Customer_TName")) = False Then _Customer_TName = Rdr("Customer_TName").ToString()
                        If Convert.IsDBNull(Rdr("Customer_EName")) = False Then _Customer_EName = Rdr("Customer_EName").ToString()
                        If Convert.IsDBNull(Rdr("Customer_Alias")) = False Then _Customer_Alias = Rdr("Customer_Alias").ToString()
                        If Convert.IsDBNull(Rdr("Customer_Tax_No")) = False Then _Customer_Tax_No = Rdr("Customer_Tax_No").ToString()
                        If Convert.IsDBNull(Rdr("Customer_Type_ID")) = False Then _Customer_Type_ID = Convert.ToInt64(Rdr("Customer_Type_ID"))
                        If Convert.IsDBNull(Rdr("Barcode1")) = False Then _Barcode1 = Rdr("Barcode1").ToString()
                        If Convert.IsDBNull(Rdr("Barcode2")) = False Then _Barcode2 = Rdr("Barcode2").ToString()
                        If Convert.IsDBNull(Rdr("Customer_Phone")) = False Then _Customer_Phone = Rdr("Customer_Phone").ToString()
                        If Convert.IsDBNull(Rdr("Customer_Mobile")) = False Then _Customer_Mobile = Rdr("Customer_Mobile").ToString()
                        If Convert.IsDBNull(Rdr("Customer_Contact_Person1")) = False Then _Customer_Contact_Person1 = Rdr("Customer_Contact_Person1").ToString()
                        If Convert.IsDBNull(Rdr("Customer_Contact_Person2")) = False Then _Customer_Contact_Person2 = Rdr("Customer_Contact_Person2").ToString()
                        If Convert.IsDBNull(Rdr("Customer_Contact_Person3")) = False Then _Customer_Contact_Person3 = Rdr("Customer_Contact_Person3").ToString()
                        If Convert.IsDBNull(Rdr("Customer_Fax")) = False Then _Customer_Fax = Rdr("Customer_Fax").ToString()
                        If Convert.IsDBNull(Rdr("Customer_Email")) = False Then _Customer_Email = Rdr("Customer_Email").ToString()
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _Active_Status = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table MS_Customer
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (CUSTOMER_CODE, PREFIX_ID, CUSTOMER_TNAME, CUSTOMER_ENAME, CUSTOMER_ALIAS, CUSTOMER_TAX_NO, CUSTOMER_TYPE_ID, BARCODE1, BARCODE2, CUSTOMER_PHONE, CUSTOMER_MOBILE, CUSTOMER_CONTACT_PERSON1, CUSTOMER_CONTACT_PERSON2, CUSTOMER_CONTACT_PERSON3, CUSTOMER_FAX, CUSTOMER_EMAIL, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE)"
                Sql += " OUTPUT INSERTED.CUSTOMER_ID, INSERTED.CUSTOMER_CODE, INSERTED.PREFIX_ID, INSERTED.CUSTOMER_TNAME, INSERTED.CUSTOMER_ENAME, INSERTED.CUSTOMER_ALIAS, INSERTED.CUSTOMER_TAX_NO, INSERTED.CUSTOMER_TYPE_ID, INSERTED.BARCODE1, INSERTED.BARCODE2, INSERTED.CUSTOMER_PHONE, INSERTED.CUSTOMER_MOBILE, INSERTED.CUSTOMER_CONTACT_PERSON1, INSERTED.CUSTOMER_CONTACT_PERSON2, INSERTED.CUSTOMER_CONTACT_PERSON3, INSERTED.CUSTOMER_FAX, INSERTED.CUSTOMER_EMAIL, INSERTED.ACTIVE_STATUS, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE"
                Sql += " VALUES("
                sql += "@_CUSTOMER_CODE" & ", "
                sql += "@_PREFIX_ID" & ", "
                sql += "@_CUSTOMER_TNAME" & ", "
                sql += "@_CUSTOMER_ENAME" & ", "
                sql += "@_CUSTOMER_ALIAS" & ", "
                sql += "@_CUSTOMER_TAX_NO" & ", "
                sql += "@_CUSTOMER_TYPE_ID" & ", "
                sql += "@_BARCODE1" & ", "
                sql += "@_BARCODE2" & ", "
                sql += "@_CUSTOMER_PHONE" & ", "
                sql += "@_CUSTOMER_MOBILE" & ", "
                sql += "@_CUSTOMER_CONTACT_PERSON1" & ", "
                sql += "@_CUSTOMER_CONTACT_PERSON2" & ", "
                sql += "@_CUSTOMER_CONTACT_PERSON3" & ", "
                sql += "@_CUSTOMER_FAX" & ", "
                sql += "@_CUSTOMER_EMAIL" & ", "
                sql += "@_ACTIVE_STATUS" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE" & ", "
                sql += "@_UPDATED_BY" & ", "
                sql += "@_UPDATED_DATE"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table MS_Customer
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "CUSTOMER_CODE = " & "@_CUSTOMER_CODE" & ", "
                Sql += "PREFIX_ID = " & "@_PREFIX_ID" & ", "
                Sql += "CUSTOMER_TNAME = " & "@_CUSTOMER_TNAME" & ", "
                Sql += "CUSTOMER_ENAME = " & "@_CUSTOMER_ENAME" & ", "
                Sql += "CUSTOMER_ALIAS = " & "@_CUSTOMER_ALIAS" & ", "
                Sql += "CUSTOMER_TAX_NO = " & "@_CUSTOMER_TAX_NO" & ", "
                Sql += "CUSTOMER_TYPE_ID = " & "@_CUSTOMER_TYPE_ID" & ", "
                Sql += "BARCODE1 = " & "@_BARCODE1" & ", "
                Sql += "BARCODE2 = " & "@_BARCODE2" & ", "
                Sql += "CUSTOMER_PHONE = " & "@_CUSTOMER_PHONE" & ", "
                Sql += "CUSTOMER_MOBILE = " & "@_CUSTOMER_MOBILE" & ", "
                Sql += "CUSTOMER_CONTACT_PERSON1 = " & "@_CUSTOMER_CONTACT_PERSON1" & ", "
                Sql += "CUSTOMER_CONTACT_PERSON2 = " & "@_CUSTOMER_CONTACT_PERSON2" & ", "
                Sql += "CUSTOMER_CONTACT_PERSON3 = " & "@_CUSTOMER_CONTACT_PERSON3" & ", "
                Sql += "CUSTOMER_FAX = " & "@_CUSTOMER_FAX" & ", "
                Sql += "CUSTOMER_EMAIL = " & "@_CUSTOMER_EMAIL" & ", "
                Sql += "ACTIVE_STATUS = " & "@_ACTIVE_STATUS" & ", "
                Sql += "CREATED_BY = " & "@_CREATED_BY" & ", "
                Sql += "CREATED_DATE = " & "@_CREATED_DATE" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table MS_Customer
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table MS_Customer
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT CUSTOMER_ID, CUSTOMER_CODE, PREFIX_ID, CUSTOMER_TNAME, CUSTOMER_ENAME, CUSTOMER_ALIAS, CUSTOMER_TAX_NO, CUSTOMER_TYPE_ID, BARCODE1, BARCODE2, CUSTOMER_PHONE, CUSTOMER_MOBILE, CUSTOMER_CONTACT_PERSON1, CUSTOMER_CONTACT_PERSON2, CUSTOMER_CONTACT_PERSON3, CUSTOMER_FAX, CUSTOMER_EMAIL, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
