Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for MS_Menu table LinqDB.
    '[Create by  on June, 1 2017]
    Public Class MsMenuLinqDB
        Public sub MsMenuLinqDB()

        End Sub 
        ' MS_Menu
        Const _tableName As String = "MS_Menu"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _MENU_ID As Long = 0
        Dim _MENU_NAME As String = ""
        Dim _CAN_EDIT As Char = ""
        Dim _CAN_DELETE As Char = ""
        Dim _MENU_URL As String = ""
        Dim _MENU_ICON As  String  = ""
        Dim _PARENT_MENU_ID As Long = 0
        Dim _MENU_ORDER As Long = 0
        Dim _ACTIVE_STATUS As Char = "Y"
        Dim _CREATED_BY As String = ""
        Dim _CREATED_DATE As DateTime = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)

        'Generate Field Property 
        <Column(Storage:="_MENU_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property MENU_ID() As Long
            Get
                Return _MENU_ID
            End Get
            Set(ByVal value As Long)
               _MENU_ID = value
            End Set
        End Property 
        <Column(Storage:="_MENU_NAME", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property MENU_NAME() As String
            Get
                Return _MENU_NAME
            End Get
            Set(ByVal value As String)
               _MENU_NAME = value
            End Set
        End Property 
        <Column(Storage:="_CAN_EDIT", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property CAN_EDIT() As Char
            Get
                Return _CAN_EDIT
            End Get
            Set(ByVal value As Char)
               _CAN_EDIT = value
            End Set
        End Property 
        <Column(Storage:="_CAN_DELETE", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property CAN_DELETE() As Char
            Get
                Return _CAN_DELETE
            End Get
            Set(ByVal value As Char)
               _CAN_DELETE = value
            End Set
        End Property 
        <Column(Storage:="_MENU_URL", DbType:="VarChar(500) NOT NULL ",CanBeNull:=false)>  _
        Public Property MENU_URL() As String
            Get
                Return _MENU_URL
            End Get
            Set(ByVal value As String)
               _MENU_URL = value
            End Set
        End Property 
        <Column(Storage:="_MENU_ICON", DbType:="VarChar(100)")>  _
        Public Property MENU_ICON() As  String 
            Get
                Return _MENU_ICON
            End Get
            Set(ByVal value As  String )
               _MENU_ICON = value
            End Set
        End Property 
        <Column(Storage:="_PARENT_MENU_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property PARENT_MENU_ID() As Long
            Get
                Return _PARENT_MENU_ID
            End Get
            Set(ByVal value As Long)
               _PARENT_MENU_ID = value
            End Set
        End Property 
        <Column(Storage:="_MENU_ORDER", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property MENU_ORDER() As Long
            Get
                Return _MENU_ORDER
            End Get
            Set(ByVal value As Long)
               _MENU_ORDER = value
            End Set
        End Property 
        <Column(Storage:="_ACTIVE_STATUS", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ACTIVE_STATUS() As Char
            Get
                Return _ACTIVE_STATUS
            End Get
            Set(ByVal value As Char)
               _ACTIVE_STATUS = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_BY() As String
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As String)
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_DATE() As DateTime
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As DateTime)
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _MENU_ID = 0
            _MENU_NAME = ""
            _CAN_EDIT = ""
            _CAN_DELETE = ""
            _MENU_URL = ""
            _MENU_ICON = ""
            _PARENT_MENU_ID = 0
            _MENU_ORDER = 0
            _ACTIVE_STATUS = "Y"
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into MS_Menu table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to MS_Menu table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _Menu_ID > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("MENU_ID = @_MENU_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to MS_Menu table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from MS_Menu table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cMENU_ID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_MENU_ID", cMENU_ID)
                Return doDelete("MENU_ID = @_MENU_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of MS_Menu by specified MENU_ID key is retrieved successfully.
        '/// <param name=cMENU_ID>The MENU_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cMENU_ID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_MENU_ID", cMENU_ID)
            Return doChkData("MENU_ID = @_MENU_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of MS_Menu by specified MENU_ID key is retrieved successfully.
        '/// <param name=cMENU_ID>The MENU_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cMENU_ID As Long, trans As SQLTransaction) As MsMenuLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_MENU_ID", cMENU_ID)
            Return doGetData("MENU_ID = @_MENU_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of MS_Menu by specified MENU_NAME_PARENT_MENU_ID key is retrieved successfully.
        '/// <param name=cMENU_NAME_PARENT_MENU_ID>The MENU_NAME_PARENT_MENU_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByMENU_NAME_PARENT_MENU_ID(cMENU_NAME As String, cPARENT_MENU_ID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(3)  As SQLParameter
            cmdPara(0) = DB.SetText("@_MENU_NAME", cMENU_NAME) 
            cmdPara(1) = DB.SetText("@_PARENT_MENU_ID", cPARENT_MENU_ID) 
            Return doChkData("MENU_NAME = @_MENU_NAME AND PARENT_MENU_ID = @_PARENT_MENU_ID", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of MS_Menu by specified MENU_NAME_PARENT_MENU_ID key is retrieved successfully.
        '/// <param name=cMENU_NAME_PARENT_MENU_ID>The MENU_NAME_PARENT_MENU_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByMENU_NAME_PARENT_MENU_ID(cMENU_NAME As String, cPARENT_MENU_ID As Long, cMENU_ID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(3)  As SQLParameter
            cmdPara(0) = DB.SetText("@_MENU_NAME", cMENU_NAME) 
            cmdPara(1) = DB.SetText("@_PARENT_MENU_ID", cPARENT_MENU_ID) 
            cmdPara(2) = DB.SetBigInt("@_MENU_ID", cMENU_ID) 
            Return doChkData("MENU_NAME = @_MENU_NAME AND PARENT_MENU_ID = @_PARENT_MENU_ID And MENU_ID <> @_MENU_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of MS_Menu by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into MS_Menu table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _MENU_ID = dt.Rows(0)("MENU_ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                        ret.NewID = _MENU_ID
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to MS_Menu table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                            ret.NewID = _MENU_ID
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from MS_Menu table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(12) As SqlParameter
            cmbParam(0) = New SqlParameter("@_MENU_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _MENU_ID

            cmbParam(1) = New SqlParameter("@_MENU_NAME", SqlDbType.VarChar)
            cmbParam(1).Value = _MENU_NAME.Trim

            cmbParam(2) = New SqlParameter("@_CAN_EDIT", SqlDbType.Char)
            cmbParam(2).Value = _CAN_EDIT

            cmbParam(3) = New SqlParameter("@_CAN_DELETE", SqlDbType.Char)
            cmbParam(3).Value = _CAN_DELETE

            cmbParam(4) = New SqlParameter("@_MENU_URL", SqlDbType.VarChar)
            cmbParam(4).Value = _MENU_URL.Trim

            cmbParam(5) = New SqlParameter("@_MENU_ICON", SqlDbType.VarChar)
            If _MENU_ICON.Trim <> "" Then 
                cmbParam(5).Value = _MENU_ICON.Trim
            Else
                cmbParam(5).Value = DBNull.value
            End If

            cmbParam(6) = New SqlParameter("@_PARENT_MENU_ID", SqlDbType.BigInt)
            cmbParam(6).Value = _PARENT_MENU_ID

            cmbParam(7) = New SqlParameter("@_MENU_ORDER", SqlDbType.Int)
            cmbParam(7).Value = _MENU_ORDER

            cmbParam(8) = New SqlParameter("@_ACTIVE_STATUS", SqlDbType.Char)
            cmbParam(8).Value = _ACTIVE_STATUS

            cmbParam(9) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            cmbParam(9).Value = _CREATED_BY.Trim

            cmbParam(10) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            cmbParam(10).Value = _CREATED_DATE

            cmbParam(11) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(11).Value = _UPDATED_BY.Trim
            Else
                cmbParam(11).Value = DBNull.value
            End If

            cmbParam(12) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(12).Value = _UPDATED_DATE.Value
            Else
                cmbParam(12).Value = DBNull.value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of MS_Menu by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Menu_ID")) = False Then _Menu_ID = Convert.ToInt64(Rdr("Menu_ID"))
                        If Convert.IsDBNull(Rdr("Menu_Name")) = False Then _Menu_Name = Rdr("Menu_Name").ToString()
                        If Convert.IsDBNull(Rdr("Can_Edit")) = False Then _Can_Edit = Rdr("Can_Edit").ToString()
                        If Convert.IsDBNull(Rdr("Can_Delete")) = False Then _Can_Delete = Rdr("Can_Delete").ToString()
                        If Convert.IsDBNull(Rdr("menu_url")) = False Then _menu_url = Rdr("menu_url").ToString()
                        If Convert.IsDBNull(Rdr("menu_icon")) = False Then _menu_icon = Rdr("menu_icon").ToString()
                        If Convert.IsDBNull(Rdr("parent_menu_id")) = False Then _parent_menu_id = Convert.ToInt64(Rdr("parent_menu_id"))
                        If Convert.IsDBNull(Rdr("menu_order")) = False Then _menu_order = Convert.ToInt32(Rdr("menu_order"))
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _Active_Status = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of MS_Menu by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As MsMenuLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Menu_ID")) = False Then _Menu_ID = Convert.ToInt64(Rdr("Menu_ID"))
                        If Convert.IsDBNull(Rdr("Menu_Name")) = False Then _Menu_Name = Rdr("Menu_Name").ToString()
                        If Convert.IsDBNull(Rdr("Can_Edit")) = False Then _Can_Edit = Rdr("Can_Edit").ToString()
                        If Convert.IsDBNull(Rdr("Can_Delete")) = False Then _Can_Delete = Rdr("Can_Delete").ToString()
                        If Convert.IsDBNull(Rdr("menu_url")) = False Then _menu_url = Rdr("menu_url").ToString()
                        If Convert.IsDBNull(Rdr("menu_icon")) = False Then _menu_icon = Rdr("menu_icon").ToString()
                        If Convert.IsDBNull(Rdr("parent_menu_id")) = False Then _parent_menu_id = Convert.ToInt64(Rdr("parent_menu_id"))
                        If Convert.IsDBNull(Rdr("menu_order")) = False Then _menu_order = Convert.ToInt32(Rdr("menu_order"))
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _Active_Status = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table MS_Menu
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (MENU_NAME, CAN_EDIT, CAN_DELETE, MENU_URL, MENU_ICON, PARENT_MENU_ID, MENU_ORDER, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE)"
                Sql += " OUTPUT INSERTED.MENU_ID, INSERTED.MENU_NAME, INSERTED.CAN_EDIT, INSERTED.CAN_DELETE, INSERTED.MENU_URL, INSERTED.MENU_ICON, INSERTED.PARENT_MENU_ID, INSERTED.MENU_ORDER, INSERTED.ACTIVE_STATUS, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE"
                Sql += " VALUES("
                sql += "@_MENU_NAME" & ", "
                sql += "@_CAN_EDIT" & ", "
                sql += "@_CAN_DELETE" & ", "
                sql += "@_MENU_URL" & ", "
                sql += "@_MENU_ICON" & ", "
                sql += "@_PARENT_MENU_ID" & ", "
                sql += "@_MENU_ORDER" & ", "
                sql += "@_ACTIVE_STATUS" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE" & ", "
                sql += "@_UPDATED_BY" & ", "
                sql += "@_UPDATED_DATE"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table MS_Menu
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "MENU_NAME = " & "@_MENU_NAME" & ", "
                Sql += "CAN_EDIT = " & "@_CAN_EDIT" & ", "
                Sql += "CAN_DELETE = " & "@_CAN_DELETE" & ", "
                Sql += "MENU_URL = " & "@_MENU_URL" & ", "
                Sql += "MENU_ICON = " & "@_MENU_ICON" & ", "
                Sql += "PARENT_MENU_ID = " & "@_PARENT_MENU_ID" & ", "
                Sql += "MENU_ORDER = " & "@_MENU_ORDER" & ", "
                Sql += "ACTIVE_STATUS = " & "@_ACTIVE_STATUS" & ", "
                Sql += "CREATED_BY = " & "@_CREATED_BY" & ", "
                Sql += "CREATED_DATE = " & "@_CREATED_DATE" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table MS_Menu
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table MS_Menu
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT MENU_ID, MENU_NAME, CAN_EDIT, CAN_DELETE, MENU_URL, MENU_ICON, PARENT_MENU_ID, MENU_ORDER, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
