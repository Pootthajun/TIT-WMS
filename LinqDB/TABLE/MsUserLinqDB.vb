Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for MS_User table LinqDB.
    '[Create by  on May, 26 2017]
    Public Class MsUserLinqDB
        Public sub MsUserLinqDB()

        End Sub 
        ' MS_User
        Const _tableName As String = "MS_User"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _USER_ID As Long = 0
        Dim _USER_LOGIN As String = ""
        Dim _USER_PASSWORD As String = ""
        Dim _DEPARTMENT_ID As Long = 0
        Dim _PREFIX_NAME_ID As Long = 0
        Dim _FIRST_NAME As String = ""
        Dim _LAST_NAME As String = ""
        Dim _GENDER As Char = ""
        Dim _POSITION_ID As Long = 0
        Dim _LAST_LOGIN_TIME As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _FORCE_CHANGE_PWD As Char = "Y"
        Dim _ACTIVE_STATUS As Char = "Y"
        Dim _CREATED_BY As String = ""
        Dim _CREATED_DATE As DateTime = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)

        'Generate Field Property 
        <Column(Storage:="_USER_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property USER_ID() As Long
            Get
                Return _USER_ID
            End Get
            Set(ByVal value As Long)
               _USER_ID = value
            End Set
        End Property 
        <Column(Storage:="_USER_LOGIN", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property USER_LOGIN() As String
            Get
                Return _USER_LOGIN
            End Get
            Set(ByVal value As String)
               _USER_LOGIN = value
            End Set
        End Property 
        <Column(Storage:="_USER_PASSWORD", DbType:="VarChar(255) NOT NULL ",CanBeNull:=false)>  _
        Public Property USER_PASSWORD() As String
            Get
                Return _USER_PASSWORD
            End Get
            Set(ByVal value As String)
               _USER_PASSWORD = value
            End Set
        End Property 
        <Column(Storage:="_DEPARTMENT_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property DEPARTMENT_ID() As Long
            Get
                Return _DEPARTMENT_ID
            End Get
            Set(ByVal value As Long)
               _DEPARTMENT_ID = value
            End Set
        End Property 
        <Column(Storage:="_PREFIX_NAME_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property PREFIX_NAME_ID() As Long
            Get
                Return _PREFIX_NAME_ID
            End Get
            Set(ByVal value As Long)
               _PREFIX_NAME_ID = value
            End Set
        End Property 
        <Column(Storage:="_FIRST_NAME", DbType:="VarChar(255) NOT NULL ",CanBeNull:=false)>  _
        Public Property FIRST_NAME() As String
            Get
                Return _FIRST_NAME
            End Get
            Set(ByVal value As String)
               _FIRST_NAME = value
            End Set
        End Property 
        <Column(Storage:="_LAST_NAME", DbType:="VarChar(255) NOT NULL ",CanBeNull:=false)>  _
        Public Property LAST_NAME() As String
            Get
                Return _LAST_NAME
            End Get
            Set(ByVal value As String)
               _LAST_NAME = value
            End Set
        End Property 
        <Column(Storage:="_GENDER", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property GENDER() As Char
            Get
                Return _GENDER
            End Get
            Set(ByVal value As Char)
               _GENDER = value
            End Set
        End Property 
        <Column(Storage:="_POSITION_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property POSITION_ID() As Long
            Get
                Return _POSITION_ID
            End Get
            Set(ByVal value As Long)
               _POSITION_ID = value
            End Set
        End Property 
        <Column(Storage:="_LAST_LOGIN_TIME", DbType:="DateTime")>  _
        Public Property LAST_LOGIN_TIME() As  System.Nullable(Of DateTime) 
            Get
                Return _LAST_LOGIN_TIME
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _LAST_LOGIN_TIME = value
            End Set
        End Property 
        <Column(Storage:="_FORCE_CHANGE_PWD", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property FORCE_CHANGE_PWD() As Char
            Get
                Return _FORCE_CHANGE_PWD
            End Get
            Set(ByVal value As Char)
               _FORCE_CHANGE_PWD = value
            End Set
        End Property 
        <Column(Storage:="_ACTIVE_STATUS", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ACTIVE_STATUS() As Char
            Get
                Return _ACTIVE_STATUS
            End Get
            Set(ByVal value As Char)
               _ACTIVE_STATUS = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_BY() As String
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As String)
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_DATE() As DateTime
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As DateTime)
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _USER_ID = 0
            _USER_LOGIN = ""
            _USER_PASSWORD = ""
            _DEPARTMENT_ID = 0
            _PREFIX_NAME_ID = 0
            _FIRST_NAME = ""
            _LAST_NAME = ""
            _GENDER = ""
            _POSITION_ID = 0
            _LAST_LOGIN_TIME = New DateTime(1,1,1)
            _FORCE_CHANGE_PWD = "Y"
            _ACTIVE_STATUS = "Y"
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into MS_User table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to MS_User table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _user_id > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("USER_ID = @_USER_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to MS_User table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from MS_User table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cUSER_ID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_USER_ID", cUSER_ID)
                Return doDelete("USER_ID = @_USER_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of MS_User by specified USER_ID key is retrieved successfully.
        '/// <param name=cUSER_ID>The USER_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cUSER_ID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_USER_ID", cUSER_ID)
            Return doChkData("USER_ID = @_USER_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of MS_User by specified USER_ID key is retrieved successfully.
        '/// <param name=cUSER_ID>The USER_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cUSER_ID As Long, trans As SQLTransaction) As MsUserLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_USER_ID", cUSER_ID)
            Return doGetData("USER_ID = @_USER_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of MS_User by specified USER_LOGIN key is retrieved successfully.
        '/// <param name=cUSER_LOGIN>The USER_LOGIN key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByUSER_LOGIN(cUSER_LOGIN As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_USER_LOGIN", cUSER_LOGIN) 
            Return doChkData("USER_LOGIN = @_USER_LOGIN", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of MS_User by specified USER_LOGIN key is retrieved successfully.
        '/// <param name=cUSER_LOGIN>The USER_LOGIN key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByUSER_LOGIN(cUSER_LOGIN As String, cUSER_ID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_USER_LOGIN", cUSER_LOGIN) 
            cmdPara(1) = DB.SetBigInt("@_USER_ID", cUSER_ID) 
            Return doChkData("USER_LOGIN = @_USER_LOGIN And USER_ID <> @_USER_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of MS_User by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into MS_User table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _USER_ID = dt.Rows(0)("USER_ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                        ret.NewID = _USER_ID
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to MS_User table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                            ret.NewID = _USER_ID
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from MS_User table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(15) As SqlParameter
            cmbParam(0) = New SqlParameter("@_USER_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _USER_ID

            cmbParam(1) = New SqlParameter("@_USER_LOGIN", SqlDbType.VarChar)
            cmbParam(1).Value = _USER_LOGIN.Trim

            cmbParam(2) = New SqlParameter("@_USER_PASSWORD", SqlDbType.VarChar)
            cmbParam(2).Value = _USER_PASSWORD.Trim

            cmbParam(3) = New SqlParameter("@_DEPARTMENT_ID", SqlDbType.BigInt)
            cmbParam(3).Value = _DEPARTMENT_ID

            cmbParam(4) = New SqlParameter("@_PREFIX_NAME_ID", SqlDbType.BigInt)
            cmbParam(4).Value = _PREFIX_NAME_ID

            cmbParam(5) = New SqlParameter("@_FIRST_NAME", SqlDbType.VarChar)
            cmbParam(5).Value = _FIRST_NAME.Trim

            cmbParam(6) = New SqlParameter("@_LAST_NAME", SqlDbType.VarChar)
            cmbParam(6).Value = _LAST_NAME.Trim

            cmbParam(7) = New SqlParameter("@_GENDER", SqlDbType.Char)
            cmbParam(7).Value = _GENDER

            cmbParam(8) = New SqlParameter("@_POSITION_ID", SqlDbType.BigInt)
            cmbParam(8).Value = _POSITION_ID

            cmbParam(9) = New SqlParameter("@_LAST_LOGIN_TIME", SqlDbType.DateTime)
            If _LAST_LOGIN_TIME.Value.Year > 1 Then 
                cmbParam(9).Value = _LAST_LOGIN_TIME.Value
            Else
                cmbParam(9).Value = DBNull.value
            End If

            cmbParam(10) = New SqlParameter("@_FORCE_CHANGE_PWD", SqlDbType.Char)
            cmbParam(10).Value = _FORCE_CHANGE_PWD

            cmbParam(11) = New SqlParameter("@_ACTIVE_STATUS", SqlDbType.Char)
            cmbParam(11).Value = _ACTIVE_STATUS

            cmbParam(12) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            cmbParam(12).Value = _CREATED_BY.Trim

            cmbParam(13) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            cmbParam(13).Value = _CREATED_DATE

            cmbParam(14) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(14).Value = _UPDATED_BY.Trim
            Else
                cmbParam(14).Value = DBNull.value
            End If

            cmbParam(15) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(15).Value = _UPDATED_DATE.Value
            Else
                cmbParam(15).Value = DBNull.value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of MS_User by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("user_id")) = False Then _user_id = Convert.ToInt64(Rdr("user_id"))
                        If Convert.IsDBNull(Rdr("User_Login")) = False Then _User_Login = Rdr("User_Login").ToString()
                        If Convert.IsDBNull(Rdr("User_Password")) = False Then _User_Password = Rdr("User_Password").ToString()
                        If Convert.IsDBNull(Rdr("Department_ID")) = False Then _Department_ID = Convert.ToInt64(Rdr("Department_ID"))
                        If Convert.IsDBNull(Rdr("Prefix_Name_ID")) = False Then _Prefix_Name_ID = Convert.ToInt64(Rdr("Prefix_Name_ID"))
                        If Convert.IsDBNull(Rdr("First_Name")) = False Then _First_Name = Rdr("First_Name").ToString()
                        If Convert.IsDBNull(Rdr("Last_Name")) = False Then _Last_Name = Rdr("Last_Name").ToString()
                        If Convert.IsDBNull(Rdr("gender")) = False Then _gender = Rdr("gender").ToString()
                        If Convert.IsDBNull(Rdr("Position_ID")) = False Then _Position_ID = Convert.ToInt64(Rdr("Position_ID"))
                        If Convert.IsDBNull(Rdr("last_login_time")) = False Then _last_login_time = Convert.ToDateTime(Rdr("last_login_time"))
                        If Convert.IsDBNull(Rdr("force_change_pwd")) = False Then _force_change_pwd = Rdr("force_change_pwd").ToString()
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _Active_Status = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of MS_User by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As MsUserLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("user_id")) = False Then _user_id = Convert.ToInt64(Rdr("user_id"))
                        If Convert.IsDBNull(Rdr("User_Login")) = False Then _User_Login = Rdr("User_Login").ToString()
                        If Convert.IsDBNull(Rdr("User_Password")) = False Then _User_Password = Rdr("User_Password").ToString()
                        If Convert.IsDBNull(Rdr("Department_ID")) = False Then _Department_ID = Convert.ToInt64(Rdr("Department_ID"))
                        If Convert.IsDBNull(Rdr("Prefix_Name_ID")) = False Then _Prefix_Name_ID = Convert.ToInt64(Rdr("Prefix_Name_ID"))
                        If Convert.IsDBNull(Rdr("First_Name")) = False Then _First_Name = Rdr("First_Name").ToString()
                        If Convert.IsDBNull(Rdr("Last_Name")) = False Then _Last_Name = Rdr("Last_Name").ToString()
                        If Convert.IsDBNull(Rdr("gender")) = False Then _gender = Rdr("gender").ToString()
                        If Convert.IsDBNull(Rdr("Position_ID")) = False Then _Position_ID = Convert.ToInt64(Rdr("Position_ID"))
                        If Convert.IsDBNull(Rdr("last_login_time")) = False Then _last_login_time = Convert.ToDateTime(Rdr("last_login_time"))
                        If Convert.IsDBNull(Rdr("force_change_pwd")) = False Then _force_change_pwd = Rdr("force_change_pwd").ToString()
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _Active_Status = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table MS_User
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (USER_LOGIN, USER_PASSWORD, DEPARTMENT_ID, PREFIX_NAME_ID, FIRST_NAME, LAST_NAME, GENDER, POSITION_ID, LAST_LOGIN_TIME, FORCE_CHANGE_PWD, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE)"
                Sql += " OUTPUT INSERTED.USER_ID, INSERTED.USER_LOGIN, INSERTED.USER_PASSWORD, INSERTED.DEPARTMENT_ID, INSERTED.PREFIX_NAME_ID, INSERTED.FIRST_NAME, INSERTED.LAST_NAME, INSERTED.GENDER, INSERTED.POSITION_ID, INSERTED.LAST_LOGIN_TIME, INSERTED.FORCE_CHANGE_PWD, INSERTED.ACTIVE_STATUS, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE"
                Sql += " VALUES("
                sql += "@_USER_LOGIN" & ", "
                sql += "@_USER_PASSWORD" & ", "
                sql += "@_DEPARTMENT_ID" & ", "
                sql += "@_PREFIX_NAME_ID" & ", "
                sql += "@_FIRST_NAME" & ", "
                sql += "@_LAST_NAME" & ", "
                sql += "@_GENDER" & ", "
                sql += "@_POSITION_ID" & ", "
                sql += "@_LAST_LOGIN_TIME" & ", "
                sql += "@_FORCE_CHANGE_PWD" & ", "
                sql += "@_ACTIVE_STATUS" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE" & ", "
                sql += "@_UPDATED_BY" & ", "
                sql += "@_UPDATED_DATE"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table MS_User
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "USER_LOGIN = " & "@_USER_LOGIN" & ", "
                Sql += "USER_PASSWORD = " & "@_USER_PASSWORD" & ", "
                Sql += "DEPARTMENT_ID = " & "@_DEPARTMENT_ID" & ", "
                Sql += "PREFIX_NAME_ID = " & "@_PREFIX_NAME_ID" & ", "
                Sql += "FIRST_NAME = " & "@_FIRST_NAME" & ", "
                Sql += "LAST_NAME = " & "@_LAST_NAME" & ", "
                Sql += "GENDER = " & "@_GENDER" & ", "
                Sql += "POSITION_ID = " & "@_POSITION_ID" & ", "
                Sql += "LAST_LOGIN_TIME = " & "@_LAST_LOGIN_TIME" & ", "
                Sql += "FORCE_CHANGE_PWD = " & "@_FORCE_CHANGE_PWD" & ", "
                Sql += "ACTIVE_STATUS = " & "@_ACTIVE_STATUS" & ", "
                Sql += "CREATED_BY = " & "@_CREATED_BY" & ", "
                Sql += "CREATED_DATE = " & "@_CREATED_DATE" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table MS_User
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table MS_User
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT USER_ID, USER_LOGIN, USER_PASSWORD, DEPARTMENT_ID, PREFIX_NAME_ID, FIRST_NAME, LAST_NAME, GENDER, POSITION_ID, LAST_LOGIN_TIME, FORCE_CHANGE_PWD, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
