Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for TB_PO_Test table LinqDB.
    '[Create by  on July, 6 2017]
    Public Class TbPoTestLinqDB
        Public Sub TbPoTestLinqDB()

        End Sub
        ' TB_PO_Test
        Const _tableName As String = "TB_PO_Test"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _SEQUENCE_ID As Long = 0
        Dim _RECEIVE_TYPE_ID As Long = 0
        Dim _REFERENCE_NO As String = ""
        Dim _REFERENCE_DATE As System.Nullable(Of Date) = New DateTime(1, 1, 1)
        Dim _SKU_ID As String = ""
        Dim _LOT_BATCH As String = ""
        Dim _AMOUNTRECEIVE As System.Nullable(Of Long)
        Dim _PRODUCT_STATUS_ID As System.Nullable(Of Long)
        Dim _RECEIVE As System.Nullable(Of Char) = ""
        Dim _ACTIVE_STATUS As Char = "Y"
        Dim _CREATED_BY As String = ""
        Dim _CREATED_DATE As DateTime = New DateTime(1, 1, 1)
        Dim _UPDATED_BY As String = ""
        Dim _UPDATED_DATE As System.Nullable(Of DateTime) = New DateTime(1, 1, 1)

        'Generate Field Property 
        <Column(Storage:="_SEQUENCE_ID", DbType:="BigInt NOT NULL ", CanBeNull:=False)>
        Public Property SEQUENCE_ID() As Long
            Get
                Return _SEQUENCE_ID
            End Get
            Set(ByVal value As Long)
                _SEQUENCE_ID = value
            End Set
        End Property
        <Column(Storage:="_RECEIVE_TYPE_ID", DbType:="BigInt NOT NULL ", CanBeNull:=False)>
        Public Property RECEIVE_TYPE_ID() As Long
            Get
                Return _RECEIVE_TYPE_ID
            End Get
            Set(ByVal value As Long)
                _RECEIVE_TYPE_ID = value
            End Set
        End Property
        <Column(Storage:="_REFERENCE_NO", DbType:="VarChar(100)")>
        Public Property REFERENCE_NO() As String
            Get
                Return _REFERENCE_NO
            End Get
            Set(ByVal value As String)
                _REFERENCE_NO = value
            End Set
        End Property
        <Column(Storage:="_REFERENCE_DATE", DbType:="Date")>
        Public Property REFERENCE_DATE() As System.Nullable(Of Date)
            Get
                Return _REFERENCE_DATE
            End Get
            Set(ByVal value As System.Nullable(Of Date))
                _REFERENCE_DATE = value
            End Set
        End Property
        <Column(Storage:="_SKU_ID", DbType:="VarChar(100)")>
        Public Property SKU_ID() As String
            Get
                Return _SKU_ID
            End Get
            Set(ByVal value As String)
                _SKU_ID = value
            End Set
        End Property
        <Column(Storage:="_LOT_BATCH", DbType:="VarChar(50)")>
        Public Property LOT_BATCH() As String
            Get
                Return _LOT_BATCH
            End Get
            Set(ByVal value As String)
                _LOT_BATCH = value
            End Set
        End Property
        <Column(Storage:="_AMOUNTRECEIVE", DbType:="Int")>
        Public Property AMOUNTRECEIVE() As System.Nullable(Of Long)
            Get
                Return _AMOUNTRECEIVE
            End Get
            Set(ByVal value As System.Nullable(Of Long))
                _AMOUNTRECEIVE = value
            End Set
        End Property
        <Column(Storage:="_PRODUCT_STATUS_ID", DbType:="BigInt")>
        Public Property PRODUCT_STATUS_ID() As System.Nullable(Of Long)
            Get
                Return _PRODUCT_STATUS_ID
            End Get
            Set(ByVal value As System.Nullable(Of Long))
                _PRODUCT_STATUS_ID = value
            End Set
        End Property
        <Column(Storage:="_RECEIVE", DbType:="Char(1)")>
        Public Property RECEIVE() As System.Nullable(Of Char)
            Get
                Return _RECEIVE
            End Get
            Set(ByVal value As System.Nullable(Of Char))
                _RECEIVE = value
            End Set
        End Property
        <Column(Storage:="_ACTIVE_STATUS", DbType:="Char(1) NOT NULL ", CanBeNull:=False)>
        Public Property ACTIVE_STATUS() As Char
            Get
                Return _ACTIVE_STATUS
            End Get
            Set(ByVal value As Char)
                _ACTIVE_STATUS = value
            End Set
        End Property
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100) NOT NULL ", CanBeNull:=False)>
        Public Property CREATED_BY() As String
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As String)
                _CREATED_BY = value
            End Set
        End Property
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime NOT NULL ", CanBeNull:=False)>
        Public Property CREATED_DATE() As DateTime
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As DateTime)
                _CREATED_DATE = value
            End Set
        End Property
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>
        Public Property UPDATED_BY() As String
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As String)
                _UPDATED_BY = value
            End Set
        End Property
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>
        Public Property UPDATED_DATE() As System.Nullable(Of DateTime)
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As System.Nullable(Of DateTime))
                _UPDATED_DATE = value
            End Set
        End Property


        'Clear All Data
        Private Sub ClearData()
            _SEQUENCE_ID = 0
            _RECEIVE_TYPE_ID = 0
            _REFERENCE_NO = ""
            _REFERENCE_DATE = New DateTime(1, 1, 1)
            _SKU_ID = ""
            _LOT_BATCH = ""
            _AMOUNTRECEIVE = Nothing
            _PRODUCT_STATUS_ID = Nothing
            _RECEIVE = ""
            _ACTIVE_STATUS = "Y"
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1, 1, 1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1, 1, 1)
        End Sub

        'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SqlTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIf(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SqlTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into TB_PO_Test table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String, trans As SqlTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then
                _CREATED_BY = CreatedBy
                _CREATED_DATE = DateTime.Now
                Return doInsert(trans)
            Else
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If
        End Function


        '/// Returns an indication whether the current data is updated to TB_PO_Test table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String, trans As SqlTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then
                If _SEQUENCE_ID > 0 Then
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("SEQUENCE_ID = @_SEQUENCE_ID", trans)
                Else
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If
            Else
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If
        End Function


        '/// Returns an indication whether the current data is updated to TB_PO_Test table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SqlTransaction, cmbParm() As SqlParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If
        End Function


        '/// Returns an indication whether the current data is deleted from TB_PO_Test table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cSEQUENCE_ID As Long, trans As SqlTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then
                Dim p(1) As SqlParameter
                p(0) = DB.SetBigInt("@_SEQUENCE_ID", cSEQUENCE_ID)
                Return doDelete("SEQUENCE_ID = @_SEQUENCE_ID", trans, p)
            Else
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If
        End Function


        '/// Returns an indication whether the record of TB_PO_Test by specified SEQUENCE_ID key is retrieved successfully.
        '/// <param name=cSEQUENCE_ID>The SEQUENCE_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cSEQUENCE_ID As Long, trans As SqlTransaction) As Boolean
            Dim p(1) As SqlParameter
            p(0) = DB.SetBigInt("@_SEQUENCE_ID", cSEQUENCE_ID)
            Return doChkData("SEQUENCE_ID = @_SEQUENCE_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of TB_PO_Test by specified SEQUENCE_ID key is retrieved successfully.
        '/// <param name=cSEQUENCE_ID>The SEQUENCE_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cSEQUENCE_ID As Long, trans As SqlTransaction) As TbPoTestLinqDB
            Dim p(1) As SqlParameter
            p(0) = DB.SetBigInt("@_SEQUENCE_ID", cSEQUENCE_ID)
            Return doGetData("SEQUENCE_ID = @_SEQUENCE_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of TB_PO_Test by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SqlTransaction, cmdPara() As SqlParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into TB_PO_Test table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SqlTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt As DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _SEQUENCE_ID = dt.Rows(0)("SEQUENCE_ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                        ret.NewID = _SEQUENCE_ID
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = False
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to TB_PO_Test table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SqlTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> "" Then

                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                            ret.NewID = _SEQUENCE_ID
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString()
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 & ex.ToString()
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from TB_PO_Test table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SqlTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            Dim sql As String = SqlDelete & tmpWhere
            If whText.Trim() <> "" Then

                Try
                    ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                    If ret.IsSuccess = False Then
                        _error = MessageResources.MSGED001
                    Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                    End If
                Catch ex As ApplicationException
                    _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                    ret.IsSuccess = False
                    ret.ErrorMessage = _error
                    ret.SqlStatement = sql
                Catch ex As Exception
                    _error = " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                    ret.IsSuccess = False
                    ret.ErrorMessage = _error
                    ret.SqlStatement = sql
                End Try
            Else
                _error = MessageResources.MSGED003 & "### SQL: " & sql
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                ret.SqlStatement = sql
            End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(13) As SqlParameter
            cmbParam(0) = New SqlParameter("@_SEQUENCE_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _SEQUENCE_ID

            cmbParam(1) = New SqlParameter("@_RECEIVE_TYPE_ID", SqlDbType.BigInt)
            cmbParam(1).Value = _RECEIVE_TYPE_ID

            cmbParam(2) = New SqlParameter("@_REFERENCE_NO", SqlDbType.VarChar)
            If _REFERENCE_NO.Trim <> "" Then
                cmbParam(2).Value = _REFERENCE_NO.Trim
            Else
                cmbParam(2).Value = DBNull.Value
            End If

            cmbParam(3) = New SqlParameter("@_REFERENCE_DATE", SqlDbType.Date)
            If _REFERENCE_DATE.Value.Year > 1 Then
                cmbParam(3).Value = _REFERENCE_DATE.Value
            Else
                cmbParam(3).Value = DBNull.Value
            End If

            cmbParam(4) = New SqlParameter("@_SKU_ID", SqlDbType.VarChar)
            If _SKU_ID.Trim <> "" Then
                cmbParam(4).Value = _SKU_ID.Trim
            Else
                cmbParam(4).Value = DBNull.Value
            End If

            cmbParam(5) = New SqlParameter("@_LOT_BATCH", SqlDbType.VarChar)
            If _LOT_BATCH.Trim <> "" Then
                cmbParam(5).Value = _LOT_BATCH.Trim
            Else
                cmbParam(5).Value = DBNull.Value
            End If

            cmbParam(6) = New SqlParameter("@_AMOUNTRECEIVE", SqlDbType.Int)
            If _AMOUNTRECEIVE IsNot Nothing Then
                cmbParam(6).Value = _AMOUNTRECEIVE.Value
            Else
                cmbParam(6).Value = DBNull.Value
            End If

            cmbParam(7) = New SqlParameter("@_PRODUCT_STATUS_ID", SqlDbType.BigInt)
            If _PRODUCT_STATUS_ID IsNot Nothing Then
                cmbParam(7).Value = _PRODUCT_STATUS_ID.Value
            Else
                cmbParam(7).Value = DBNull.Value
            End If

            cmbParam(8) = New SqlParameter("@_RECEIVE", SqlDbType.Char)
            If _RECEIVE.Value <> "" Then
                cmbParam(8).Value = _RECEIVE.Value
            Else
                cmbParam(8).Value = DBNull.Value
            End If

            cmbParam(9) = New SqlParameter("@_ACTIVE_STATUS", SqlDbType.Char)
            cmbParam(9).Value = _ACTIVE_STATUS

            cmbParam(10) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            cmbParam(10).Value = _CREATED_BY.Trim

            cmbParam(11) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            cmbParam(11).Value = _CREATED_DATE

            cmbParam(12) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then
                cmbParam(12).Value = _UPDATED_BY.Trim
            Else
                cmbParam(12).Value = DBNull.Value
            End If

            cmbParam(13) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then
                cmbParam(13).Value = _UPDATED_DATE.Value
            Else
                cmbParam(13).Value = DBNull.Value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of TB_PO_Test by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SqlTransaction, cmdPara() As SqlParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData = False
            If whText.Trim() <> "" Then
                Dim Rdr As SqlDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Sequence_ID")) = False Then _SEQUENCE_ID = Convert.ToInt64(Rdr("Sequence_ID"))
                        If Convert.IsDBNull(Rdr("Receive_Type_ID")) = False Then _RECEIVE_TYPE_ID = Convert.ToInt64(Rdr("Receive_Type_ID"))
                        If Convert.IsDBNull(Rdr("Reference_No")) = False Then _REFERENCE_NO = Rdr("Reference_No").ToString()
                        If Convert.IsDBNull(Rdr("Reference_date")) = False Then _REFERENCE_DATE = Convert.ToDateTime(Rdr("Reference_date"))
                        If Convert.IsDBNull(Rdr("SKU_ID")) = False Then _SKU_ID = Rdr("SKU_ID").ToString()
                        If Convert.IsDBNull(Rdr("Lot_Batch")) = False Then _LOT_BATCH = Rdr("Lot_Batch").ToString()
                        If Convert.IsDBNull(Rdr("AmountReceive")) = False Then _AMOUNTRECEIVE = Convert.ToInt32(Rdr("AmountReceive"))
                        If Convert.IsDBNull(Rdr("Product_Status_ID")) = False Then _PRODUCT_STATUS_ID = Convert.ToInt64(Rdr("Product_Status_ID"))
                        If Convert.IsDBNull(Rdr("Receive")) = False Then _RECEIVE = Rdr("Receive").ToString()
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _ACTIVE_STATUS = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _CREATED_BY = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _CREATED_DATE = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _UPDATED_BY = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _UPDATED_DATE = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of TB_PO_Test by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SqlTransaction, cmdPara() As SqlParameter) As TbPoTestLinqDB
            ClearData()
            _haveData = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SqlDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Sequence_ID")) = False Then _SEQUENCE_ID = Convert.ToInt64(Rdr("Sequence_ID"))
                        If Convert.IsDBNull(Rdr("Receive_Type_ID")) = False Then _RECEIVE_TYPE_ID = Convert.ToInt64(Rdr("Receive_Type_ID"))
                        If Convert.IsDBNull(Rdr("Reference_No")) = False Then _REFERENCE_NO = Rdr("Reference_No").ToString()
                        If Convert.IsDBNull(Rdr("Reference_date")) = False Then _REFERENCE_DATE = Convert.ToDateTime(Rdr("Reference_date"))
                        If Convert.IsDBNull(Rdr("SKU_ID")) = False Then _SKU_ID = Rdr("SKU_ID").ToString()
                        If Convert.IsDBNull(Rdr("Lot_Batch")) = False Then _LOT_BATCH = Rdr("Lot_Batch").ToString()
                        If Convert.IsDBNull(Rdr("AmountReceive")) = False Then _AMOUNTRECEIVE = Convert.ToInt32(Rdr("AmountReceive"))
                        If Convert.IsDBNull(Rdr("Product_Status_ID")) = False Then _PRODUCT_STATUS_ID = Convert.ToInt64(Rdr("Product_Status_ID"))
                        If Convert.IsDBNull(Rdr("Receive")) = False Then _RECEIVE = Rdr("Receive").ToString()
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _ACTIVE_STATUS = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _CREATED_BY = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _CREATED_DATE = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _UPDATED_BY = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _UPDATED_DATE = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table TB_PO_Test
        Private ReadOnly Property SqlInsert() As String
            Get
                Dim Sql As String = ""
                Sql += "INSERT INTO " & TableName & " (RECEIVE_TYPE_ID, REFERENCE_NO, REFERENCE_DATE, SKU_ID, LOT_BATCH, AMOUNTRECEIVE, PRODUCT_STATUS_ID, RECEIVE, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE)"
                Sql += " OUTPUT INSERTED.SEQUENCE_ID, INSERTED.RECEIVE_TYPE_ID, INSERTED.REFERENCE_NO, INSERTED.REFERENCE_DATE, INSERTED.SKU_ID, INSERTED.LOT_BATCH, INSERTED.AMOUNTRECEIVE, INSERTED.PRODUCT_STATUS_ID, INSERTED.RECEIVE, INSERTED.ACTIVE_STATUS, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE"
                Sql += " VALUES("
                Sql += "@_RECEIVE_TYPE_ID" & ", "
                Sql += "@_REFERENCE_NO" & ", "
                Sql += "@_REFERENCE_DATE" & ", "
                Sql += "@_SKU_ID" & ", "
                Sql += "@_LOT_BATCH" & ", "
                Sql += "@_AMOUNTRECEIVE" & ", "
                Sql += "@_PRODUCT_STATUS_ID" & ", "
                Sql += "@_RECEIVE" & ", "
                Sql += "@_ACTIVE_STATUS" & ", "
                Sql += "@_CREATED_BY" & ", "
                Sql += "@_CREATED_DATE" & ", "
                Sql += "@_UPDATED_BY" & ", "
                Sql += "@_UPDATED_DATE"
                Sql += ")"
                Return Sql
            End Get
        End Property


        'Get update statement form table TB_PO_Test
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & TableName & " SET "
                Sql += "RECEIVE_TYPE_ID = " & "@_RECEIVE_TYPE_ID" & ", "
                Sql += "REFERENCE_NO = " & "@_REFERENCE_NO" & ", "
                Sql += "REFERENCE_DATE = " & "@_REFERENCE_DATE" & ", "
                Sql += "SKU_ID = " & "@_SKU_ID" & ", "
                Sql += "LOT_BATCH = " & "@_LOT_BATCH" & ", "
                Sql += "AMOUNTRECEIVE = " & "@_AMOUNTRECEIVE" & ", "
                Sql += "PRODUCT_STATUS_ID = " & "@_PRODUCT_STATUS_ID" & ", "
                Sql += "RECEIVE = " & "@_RECEIVE" & ", "
                Sql += "ACTIVE_STATUS = " & "@_ACTIVE_STATUS" & ", "
                Sql += "CREATED_BY = " & "@_CREATED_BY" & ", "
                Sql += "CREATED_DATE = " & "@_CREATED_DATE" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table TB_PO_Test
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & TableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table TB_PO_Test
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT SEQUENCE_ID, RECEIVE_TYPE_ID, REFERENCE_NO, REFERENCE_DATE, SKU_ID, LOT_BATCH, AMOUNTRECEIVE, PRODUCT_STATUS_ID, RECEIVE, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE FROM " & TableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
