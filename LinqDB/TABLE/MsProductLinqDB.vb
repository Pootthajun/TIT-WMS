Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for MS_Product table LinqDB.
    '[Create by  on June, 13 2017]
    Public Class MsProductLinqDB
        Public sub MsProductLinqDB()

        End Sub 
        ' MS_Product
        Const _tableName As String = "MS_Product"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _SEQUENCE_ID As Long = 0
        Dim _PRODUCT_ID As String = ""
        Dim _PRODUCT_TNAME As  String  = ""
        Dim _PRODUCT_ENAME As  String  = ""
        Dim _PRODUCT_TYPE_ID As  System.Nullable(Of Long) 
        Dim _CUSTOMER_ID As  String  = ""
        Dim _SUPPLIER_ID As  String  = ""
        Dim _BRAND_NAME As  String  = ""
        Dim _PRODUCT_GROUP_ID As  System.Nullable(Of Long) 
        Dim _ACTIVE_STATUS As Char = "Y"
        Dim _CREATED_BY As String = ""
        Dim _CREATED_DATE As DateTime = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)

        'Generate Field Property 
        <Column(Storage:="_SEQUENCE_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property SEQUENCE_ID() As Long
            Get
                Return _SEQUENCE_ID
            End Get
            Set(ByVal value As Long)
               _SEQUENCE_ID = value
            End Set
        End Property 
        <Column(Storage:="_PRODUCT_ID", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property PRODUCT_ID() As String
            Get
                Return _PRODUCT_ID
            End Get
            Set(ByVal value As String)
               _PRODUCT_ID = value
            End Set
        End Property 
        <Column(Storage:="_PRODUCT_TNAME", DbType:="VarChar(300)")>  _
        Public Property PRODUCT_TNAME() As  String 
            Get
                Return _PRODUCT_TNAME
            End Get
            Set(ByVal value As  String )
               _PRODUCT_TNAME = value
            End Set
        End Property 
        <Column(Storage:="_PRODUCT_ENAME", DbType:="VarChar(300)")>  _
        Public Property PRODUCT_ENAME() As  String 
            Get
                Return _PRODUCT_ENAME
            End Get
            Set(ByVal value As  String )
               _PRODUCT_ENAME = value
            End Set
        End Property 
        <Column(Storage:="_PRODUCT_TYPE_ID", DbType:="BigInt")>  _
        Public Property PRODUCT_TYPE_ID() As  System.Nullable(Of Long) 
            Get
                Return _PRODUCT_TYPE_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _PRODUCT_TYPE_ID = value
            End Set
        End Property 
        <Column(Storage:="_CUSTOMER_ID", DbType:="VarChar(100)")>  _
        Public Property CUSTOMER_ID() As  String 
            Get
                Return _CUSTOMER_ID
            End Get
            Set(ByVal value As  String )
               _CUSTOMER_ID = value
            End Set
        End Property 
        <Column(Storage:="_SUPPLIER_ID", DbType:="VarChar(100)")>  _
        Public Property SUPPLIER_ID() As  String 
            Get
                Return _SUPPLIER_ID
            End Get
            Set(ByVal value As  String )
               _SUPPLIER_ID = value
            End Set
        End Property 
        <Column(Storage:="_BRAND_NAME", DbType:="VarChar(300)")>  _
        Public Property BRAND_NAME() As  String 
            Get
                Return _BRAND_NAME
            End Get
            Set(ByVal value As  String )
               _BRAND_NAME = value
            End Set
        End Property 
        <Column(Storage:="_PRODUCT_GROUP_ID", DbType:="BigInt")>  _
        Public Property PRODUCT_GROUP_ID() As  System.Nullable(Of Long) 
            Get
                Return _PRODUCT_GROUP_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _PRODUCT_GROUP_ID = value
            End Set
        End Property 
        <Column(Storage:="_ACTIVE_STATUS", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ACTIVE_STATUS() As Char
            Get
                Return _ACTIVE_STATUS
            End Get
            Set(ByVal value As Char)
               _ACTIVE_STATUS = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_BY() As String
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As String)
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_DATE() As DateTime
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As DateTime)
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _SEQUENCE_ID = 0
            _PRODUCT_ID = ""
            _PRODUCT_TNAME = ""
            _PRODUCT_ENAME = ""
            _PRODUCT_TYPE_ID = Nothing
            _CUSTOMER_ID = ""
            _SUPPLIER_ID = ""
            _BRAND_NAME = ""
            _PRODUCT_GROUP_ID = Nothing
            _ACTIVE_STATUS = "Y"
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into MS_Product table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to MS_Product table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _Sequence_ID > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("SEQUENCE_ID = @_SEQUENCE_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to MS_Product table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from MS_Product table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cSEQUENCE_ID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_SEQUENCE_ID", cSEQUENCE_ID)
                Return doDelete("SEQUENCE_ID = @_SEQUENCE_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of MS_Product by specified SEQUENCE_ID key is retrieved successfully.
        '/// <param name=cSEQUENCE_ID>The SEQUENCE_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cSEQUENCE_ID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_SEQUENCE_ID", cSEQUENCE_ID)
            Return doChkData("SEQUENCE_ID = @_SEQUENCE_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of MS_Product by specified SEQUENCE_ID key is retrieved successfully.
        '/// <param name=cSEQUENCE_ID>The SEQUENCE_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cSEQUENCE_ID As Long, trans As SQLTransaction) As MsProductLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_SEQUENCE_ID", cSEQUENCE_ID)
            Return doGetData("SEQUENCE_ID = @_SEQUENCE_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of MS_Product by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into MS_Product table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _SEQUENCE_ID = dt.Rows(0)("SEQUENCE_ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                        ret.NewID = _SEQUENCE_ID
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to MS_Product table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                            ret.NewID = _SEQUENCE_ID
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from MS_Product table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(13) As SqlParameter
            cmbParam(0) = New SqlParameter("@_SEQUENCE_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _SEQUENCE_ID

            cmbParam(1) = New SqlParameter("@_PRODUCT_ID", SqlDbType.VarChar)
            cmbParam(1).Value = _PRODUCT_ID.Trim

            cmbParam(2) = New SqlParameter("@_PRODUCT_TNAME", SqlDbType.VarChar)
            If _PRODUCT_TNAME.Trim <> "" Then 
                cmbParam(2).Value = _PRODUCT_TNAME.Trim
            Else
                cmbParam(2).Value = DBNull.value
            End If

            cmbParam(3) = New SqlParameter("@_PRODUCT_ENAME", SqlDbType.VarChar)
            If _PRODUCT_ENAME.Trim <> "" Then 
                cmbParam(3).Value = _PRODUCT_ENAME.Trim
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_PRODUCT_TYPE_ID", SqlDbType.BigInt)
            If _PRODUCT_TYPE_ID IsNot Nothing Then 
                cmbParam(4).Value = _PRODUCT_TYPE_ID.Value
            Else
                cmbParam(4).Value = DBNull.value
            End IF

            cmbParam(5) = New SqlParameter("@_CUSTOMER_ID", SqlDbType.VarChar)
            If _CUSTOMER_ID.Trim <> "" Then 
                cmbParam(5).Value = _CUSTOMER_ID.Trim
            Else
                cmbParam(5).Value = DBNull.value
            End If

            cmbParam(6) = New SqlParameter("@_SUPPLIER_ID", SqlDbType.VarChar)
            If _SUPPLIER_ID.Trim <> "" Then 
                cmbParam(6).Value = _SUPPLIER_ID.Trim
            Else
                cmbParam(6).Value = DBNull.value
            End If

            cmbParam(7) = New SqlParameter("@_BRAND_NAME", SqlDbType.VarChar)
            If _BRAND_NAME.Trim <> "" Then 
                cmbParam(7).Value = _BRAND_NAME.Trim
            Else
                cmbParam(7).Value = DBNull.value
            End If

            cmbParam(8) = New SqlParameter("@_PRODUCT_GROUP_ID", SqlDbType.BigInt)
            If _PRODUCT_GROUP_ID IsNot Nothing Then 
                cmbParam(8).Value = _PRODUCT_GROUP_ID.Value
            Else
                cmbParam(8).Value = DBNull.value
            End IF

            cmbParam(9) = New SqlParameter("@_ACTIVE_STATUS", SqlDbType.Char)
            cmbParam(9).Value = _ACTIVE_STATUS

            cmbParam(10) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            cmbParam(10).Value = _CREATED_BY.Trim

            cmbParam(11) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            cmbParam(11).Value = _CREATED_DATE

            cmbParam(12) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(12).Value = _UPDATED_BY.Trim
            Else
                cmbParam(12).Value = DBNull.value
            End If

            cmbParam(13) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(13).Value = _UPDATED_DATE.Value
            Else
                cmbParam(13).Value = DBNull.value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of MS_Product by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Sequence_ID")) = False Then _Sequence_ID = Convert.ToInt64(Rdr("Sequence_ID"))
                        If Convert.IsDBNull(Rdr("Product_ID")) = False Then _Product_ID = Rdr("Product_ID").ToString()
                        If Convert.IsDBNull(Rdr("Product_TName")) = False Then _Product_TName = Rdr("Product_TName").ToString()
                        If Convert.IsDBNull(Rdr("Product_EName")) = False Then _Product_EName = Rdr("Product_EName").ToString()
                        If Convert.IsDBNull(Rdr("Product_Type_ID")) = False Then _Product_Type_ID = Convert.ToInt64(Rdr("Product_Type_ID"))
                        If Convert.IsDBNull(Rdr("Customer_ID")) = False Then _Customer_ID = Rdr("Customer_ID").ToString()
                        If Convert.IsDBNull(Rdr("Supplier_ID")) = False Then _Supplier_ID = Rdr("Supplier_ID").ToString()
                        If Convert.IsDBNull(Rdr("Brand_Name")) = False Then _Brand_Name = Rdr("Brand_Name").ToString()
                        If Convert.IsDBNull(Rdr("Product_Group_ID")) = False Then _Product_Group_ID = Convert.ToInt64(Rdr("Product_Group_ID"))
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _Active_Status = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of MS_Product by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As MsProductLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("Sequence_ID")) = False Then _Sequence_ID = Convert.ToInt64(Rdr("Sequence_ID"))
                        If Convert.IsDBNull(Rdr("Product_ID")) = False Then _Product_ID = Rdr("Product_ID").ToString()
                        If Convert.IsDBNull(Rdr("Product_TName")) = False Then _Product_TName = Rdr("Product_TName").ToString()
                        If Convert.IsDBNull(Rdr("Product_EName")) = False Then _Product_EName = Rdr("Product_EName").ToString()
                        If Convert.IsDBNull(Rdr("Product_Type_ID")) = False Then _Product_Type_ID = Convert.ToInt64(Rdr("Product_Type_ID"))
                        If Convert.IsDBNull(Rdr("Customer_ID")) = False Then _Customer_ID = Rdr("Customer_ID").ToString()
                        If Convert.IsDBNull(Rdr("Supplier_ID")) = False Then _Supplier_ID = Rdr("Supplier_ID").ToString()
                        If Convert.IsDBNull(Rdr("Brand_Name")) = False Then _Brand_Name = Rdr("Brand_Name").ToString()
                        If Convert.IsDBNull(Rdr("Product_Group_ID")) = False Then _Product_Group_ID = Convert.ToInt64(Rdr("Product_Group_ID"))
                        If Convert.IsDBNull(Rdr("Active_Status")) = False Then _Active_Status = Rdr("Active_Status").ToString()
                        If Convert.IsDBNull(Rdr("Created_By")) = False Then _Created_By = Rdr("Created_By").ToString()
                        If Convert.IsDBNull(Rdr("Created_Date")) = False Then _Created_Date = Convert.ToDateTime(Rdr("Created_Date"))
                        If Convert.IsDBNull(Rdr("Updated_By")) = False Then _Updated_By = Rdr("Updated_By").ToString()
                        If Convert.IsDBNull(Rdr("Updated_Date")) = False Then _Updated_Date = Convert.ToDateTime(Rdr("Updated_Date"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table MS_Product
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (PRODUCT_ID, PRODUCT_TNAME, PRODUCT_ENAME, PRODUCT_TYPE_ID, CUSTOMER_ID, SUPPLIER_ID, BRAND_NAME, PRODUCT_GROUP_ID, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE)"
                Sql += " OUTPUT INSERTED.SEQUENCE_ID, INSERTED.PRODUCT_ID, INSERTED.PRODUCT_TNAME, INSERTED.PRODUCT_ENAME, INSERTED.PRODUCT_TYPE_ID, INSERTED.CUSTOMER_ID, INSERTED.SUPPLIER_ID, INSERTED.BRAND_NAME, INSERTED.PRODUCT_GROUP_ID, INSERTED.ACTIVE_STATUS, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE"
                Sql += " VALUES("
                sql += "@_PRODUCT_ID" & ", "
                sql += "@_PRODUCT_TNAME" & ", "
                sql += "@_PRODUCT_ENAME" & ", "
                sql += "@_PRODUCT_TYPE_ID" & ", "
                sql += "@_CUSTOMER_ID" & ", "
                sql += "@_SUPPLIER_ID" & ", "
                sql += "@_BRAND_NAME" & ", "
                sql += "@_PRODUCT_GROUP_ID" & ", "
                sql += "@_ACTIVE_STATUS" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE" & ", "
                sql += "@_UPDATED_BY" & ", "
                sql += "@_UPDATED_DATE"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table MS_Product
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "PRODUCT_ID = " & "@_PRODUCT_ID" & ", "
                Sql += "PRODUCT_TNAME = " & "@_PRODUCT_TNAME" & ", "
                Sql += "PRODUCT_ENAME = " & "@_PRODUCT_ENAME" & ", "
                Sql += "PRODUCT_TYPE_ID = " & "@_PRODUCT_TYPE_ID" & ", "
                Sql += "CUSTOMER_ID = " & "@_CUSTOMER_ID" & ", "
                Sql += "SUPPLIER_ID = " & "@_SUPPLIER_ID" & ", "
                Sql += "BRAND_NAME = " & "@_BRAND_NAME" & ", "
                Sql += "PRODUCT_GROUP_ID = " & "@_PRODUCT_GROUP_ID" & ", "
                Sql += "ACTIVE_STATUS = " & "@_ACTIVE_STATUS" & ", "
                Sql += "CREATED_BY = " & "@_CREATED_BY" & ", "
                Sql += "CREATED_DATE = " & "@_CREATED_DATE" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table MS_Product
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table MS_Product
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT SEQUENCE_ID, PRODUCT_ID, PRODUCT_TNAME, PRODUCT_ENAME, PRODUCT_TYPE_ID, CUSTOMER_ID, SUPPLIER_ID, BRAND_NAME, PRODUCT_GROUP_ID, ACTIVE_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
