﻿Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System.Globalization

Public Class MasterDataBL

#Region "Master"

#Region "Receive Type"
    Public Shared Function GetList_ReceiveType(name As String, status As String) As DataTable
        Dim lnq As New MsReceiveTypeLinqDB

        Dim parm(1) As SqlParameter
        Dim wh As String = ""

        wh += "1=1"

        If name <> "" Then
            wh += " AND Recieve_Type  Like '%' + @Receive_Type + '%' "
            parm(0) = SqlDB.SetText("@Receive_Type", name)
        End If

        If status <> "" Then
            wh += " AND Active_Status = '" & status & "'"
        End If

        Dim dt As DataTable = lnq.GetDataList(wh, "Recieve_Type", Nothing, parm)
        lnq = Nothing
        Return dt
    End Function
    Public Shared Sub GetListDDL_ReceiveType(ddl As DropDownList)
        Dim sql As String = "select Receive_Type_ID, Recieve_Type "
        sql += " from MS_Receive_Type"
        sql += " where Active_Status='Y'"
        sql += " order by Recieve_Type"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("Receive_Type_ID") = 0
        dr("Recieve_Type") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "Receive_Type_ID"
        ddl.DataTextField = "Recieve_Type"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub

    Public Shared Function GetReceiveTypeData(ReceiveTypeID As Long) As MsReceiveTypeLinqDB
        Dim ret As New MsReceiveTypeLinqDB
        ret.GetDataByPK(ReceiveTypeID, Nothing)
        Return ret
    End Function
    Public Shared Function SaveReceiveType(UserName As String, ReceiveTypeID As Long, ReceiveTypeName As String, ReceiveTypeDetial As String, ActiveStatus As Boolean) As MsReceiveTypeLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsReceiveTypeLinqDB
        lnq.GetDataByPK(ReceiveTypeID, trans.Trans)
        lnq.RECIEVE_TYPE = ReceiveTypeName
        lnq.RECIEVE_TYPE_DETAIL = ReceiveTypeDetial
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.RECEIVE_TYPE_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeleteReceiveType(ReceiveTypeID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsReceiveTypeLinqDB
        ret = lnq.DeleteByPK(ReceiveTypeID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateReceiveTypestatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsReceiveTypeLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.RECEIVE_TYPE_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "Document_Job_Type"
    Public Shared Function GetList_JobType(name As String, status As String) As DataTable
        Dim lnq As New MsDocumentJobTypeLinqDB

        Dim parm(1) As SqlParameter
        Dim wh As String = ""

        wh += "1=1"

        If name <> "" Then
            wh += " AND Job_Type Like '%' + @JobType_name + '%'"
            parm(0) = SqlDB.SetText("@JobType_name", name)
        End If

        If status <> "" Then
            wh += " AND Active_Status = '" & status & "'"
        End If

        Dim dt As DataTable = lnq.GetDataList(wh, "Job_Type", Nothing, parm)
        lnq = Nothing
        Return dt
    End Function
    Public Shared Sub GetListDDL_JobType(ddl As DropDownList)
        Dim sql As String = "select Document_Job_Type_ID, Job_Type "
        sql += " from MS_Document_Job_Type"
        sql += " where Active_Status='Y'"
        sql += " order by Job_Type"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("Document_Job_Type_ID") = 0
        dr("Job_Type") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "Document_Job_Type_ID"
        ddl.DataTextField = "Job_Type"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub
    Public Shared Function CheckDuplicateJobType(JobTypeID As Long, JobTypeName As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsDocumentJobTypeLinqDB
        Dim re As Boolean = lnq.ChkDuplicateByJobType(JobTypeName, JobTypeID, Nothing)
        If re = True Then
            ret.IsSuccess = False
            ret.ErrorMessage = "Duplicate JobType " & JobTypeName
        Else
            ret.IsSuccess = True
        End If

        Return ret
    End Function
    Public Shared Function GetJobTypeData(JobTypeID As Long) As MsDocumentJobTypeLinqDB
        Dim ret As New MsDocumentJobTypeLinqDB
        ret.GetDataByPK(JobTypeID, Nothing)
        Return ret
    End Function
    Public Shared Function SaveJobType(UserName As String, JobTypeID As Long, JobTypeName As String, ActiveStatus As Boolean) As MsDocumentJobTypeLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsDocumentJobTypeLinqDB
        lnq.GetDataByPK(JobTypeID, trans.Trans)
        lnq.JOB_TYPE = JobTypeName
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.DOCUMENT_JOB_TYPE_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeleteJobType(JobTypeID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsDocumentJobTypeLinqDB
        ret = lnq.DeleteByPK(JobTypeID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateJobTypestatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsDocumentJobTypeLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.DOCUMENT_JOB_TYPE_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "Document_Type"
    Public Shared Function GetList_DocumentType(name As String, status As String, Type As String) As DataTable

        Dim sql As String = "SELECT  Document_Type_ID,Document_Type,D.Job_Type_ID,j.Job_Type ,D.Active_Status"
        sql += " FROM MS_Document_Type D"
        sql += " inner join MS_Document_Job_Type J on D.Job_Type_ID  = J.Document_Job_Type_ID Where 1=1 "

        Dim parm(1) As SqlParameter

        If name <> "" Then
            sql += " AND Document_Type Like '%' + @DocumentType_name + '%'"
            parm(0) = SqlDB.SetText("@DocumentType_name", name)
        End If

        If Type = "0" Then Type = ""
        If Type <> "" Then
            sql += " AND D.Job_Type_ID = '" & Type & "'"
        End If

        If status <> "" Then
            sql += " AND D.Active_Status = '" & status & "'"
        End If

        sql += " order by Document_Type"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql, parm)
        Return dt
    End Function
    Public Shared Sub GetListDDL_DocumentType(ddl As DropDownList)
        Dim sql As String = "select Document_Type_ID, Document_Type "
        sql += " from MS_Document_Type"
        sql += " where Active_Status='Y'"
        sql += " order by Document_Type"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("Document_Type_ID") = 0
        dr("Document_Type") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "Document_Type_ID"
        ddl.DataTextField = "Document_Type"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub

    Public Shared Function GetDocumentTypeData(DocumentTypeID As Long) As MsDocumentTypeLinqDB
        Dim ret As New MsDocumentTypeLinqDB
        ret.GetDataByPK(DocumentTypeID, Nothing)
        Return ret
    End Function
    Public Shared Function SaveDocumentType(UserName As String, DocumentTypeID As Long, DocumentTypeName As String, JobType As Long, ActiveStatus As Boolean) As MsDocumentTypeLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsDocumentTypeLinqDB
        lnq.GetDataByPK(DocumentTypeID, trans.Trans)
        lnq.DOCUMENT_TYPE = DocumentTypeName
        lnq.JOB_TYPE_ID = JobType
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.DOCUMENT_TYPE_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeleteDocumentType(DocumentTypeID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsDocumentTypeLinqDB
        ret = lnq.DeleteByPK(DocumentTypeID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateDocumentTypestatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsDocumentTypeLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.DOCUMENT_TYPE_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "PickOrder"
    Public Shared Function GetList_PickOrder(name As String, status As String) As DataTable
        Dim lnq As New MsPickOrderLinqDB

        Dim parm(1) As SqlParameter
        Dim wh As String = ""

        wh += "1=1"

        If name <> "" Then
            wh += " AND (Pick_ID Like '%' + @PickOrder_name + '%' OR Pick_Format Like '%' + @PickOrder_name + '%') "
            parm(0) = SqlDB.SetText("@PickOrder_name", name)
        End If

        If status <> "" Then
            wh += " AND Active_Status = '" & status & "'"
        End If

        Dim dt As DataTable = lnq.GetDataList(wh, "Pick_ID", Nothing, parm)
        lnq = Nothing
        Return dt
    End Function
    Public Shared Sub GetListDDL_PickOrder(ddl As DropDownList)
        Dim sql As String = "select Pick_ID, Pick_Format "
        sql += " from MS_Pick_Order"
        sql += " where Active_Status='Y'"
        sql += " order by Pick_Format"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("Pick_ID") = 0
        dr("Pick_Format") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "Pick_ID"
        ddl.DataTextField = "Pick_Format"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub
    Public Shared Function CheckDuplicatePickOrder(PickOrderID As Long, PickOrderName As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsPickOrderLinqDB
        Dim re As Boolean = lnq.ChkDuplicateByPickOrder(PickOrderName, PickOrderID, Nothing)
        If re = True Then
            ret.IsSuccess = False
            ret.ErrorMessage = "Duplicate PickOrder " & PickOrderName
        Else
            ret.IsSuccess = True
        End If

        Return ret
    End Function
    Public Shared Function GetPickOrderData(PickOrderID As Long) As MsPickOrderLinqDB
        Dim ret As New MsPickOrderLinqDB
        ret.GetDataByPK(PickOrderID, Nothing)
        Return ret
    End Function
    Public Shared Function SavePickOrder(UserName As String, SequenceID As Long, PickOrderID As String, PickOrderName As String, ActiveStatus As Boolean) As MsPickOrderLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsPickOrderLinqDB
        lnq.GetDataByPK(SequenceID, trans.Trans)
        lnq.PICK_ID = PickOrderID
        lnq.PICK_FORMAT = PickOrderName
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.SEQUENCE_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeletePickOrder(PickOrderID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsPickOrderLinqDB
        ret = lnq.DeleteByPK(PickOrderID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdatePickOrderstatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsPickOrderLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.SEQUENCE_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "QCStatus"
    Public Shared Function GetList_QCStatus(name As String, status As String) As DataTable
        Dim lnq As New MsQcStatusLinqDB

        Dim parm(1) As SqlParameter
        Dim wh As String = ""

        wh += "1=1"

        If name <> "" Then
            wh += " AND (QC_Status Like '%' + @Name + '%' OR QC_Stage Like '%' + @Name + '%') "
            parm(0) = SqlDB.SetText("@Name", name)
        End If

        If status <> "" Then
            wh += " AND Active_Status = '" & status & "'"
        End If

        Dim dt As DataTable = lnq.GetDataList(wh, "QC_Status", Nothing, parm)
        lnq = Nothing
        Return dt
    End Function
    Public Shared Sub GetListDDL_QCStatus(ddl As DropDownList)
        Dim sql As String = "select QC_Status_ID, QC_Status "
        sql += " from MS_QC_Status"
        sql += " where Active_Status='Y'"
        sql += " order by QC_Status"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("QC_Status_ID") = 0
        dr("QC_Status") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "QC_Status_ID"
        ddl.DataTextField = "QC_Status"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub

    Public Shared Function GetQCStatusData(QCStatusID As Long) As MsQcStatusLinqDB
        Dim ret As New MsQcStatusLinqDB
        ret.GetDataByPK(QCStatusID, Nothing)
        Return ret
    End Function
    Public Shared Function SaveQCStatus(UserName As String, QCStatusID As Long, QCStatus As String, QCStage As String, ActiveStatus As Boolean) As MsQcStatusLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsQcStatusLinqDB
        lnq.GetDataByPK(QCStatusID, trans.Trans)
        lnq.QC_STATUS = QCStatus
        lnq.QC_STAGE = QCStage
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.QC_STATUS_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeleteQCStatus(QCStatusID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsQcStatusLinqDB
        ret = lnq.DeleteByPK(QCStatusID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateQCStatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsQcStatusLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.QC_STATUS_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "Product "
    Public Shared Function GetList_Product(Product_ID As String, Product_type As String, Product_Group As String, Product_Name As String, Status As String) As DataTable

        Dim sql As String = "SELECT  Sequence_ID,Product_ID,Product_TName,isnull(Product_EName,'') Product_EName,P.Product_Type_ID, T.Product_Type  ,Customer_ID ,Supplier_ID"
        sql += " ,isnull(Brand_Name,'') Brand_Name,P.Product_Group_ID,G.Product_Group ,P.Active_Status"
        sql += " FROM MS_Product P"
        sql += " inner join MS_Product_Type T ON P.Product_Type_ID = t.Product_Type_ID"
        sql += " inner join MS_Product_Group G ON p.Product_Group_ID = G.Product_Group_ID Where 1=1"

        Dim parm(2) As SqlParameter

        If Product_ID <> "" Then
            sql += " AND Product_ID Like '%' + @Product_ID + '%'"
            parm(0) = SqlDB.SetText("@Product_ID", Product_ID)
        End If

        If Product_type = "0" Then Product_type = ""
        If Product_type <> "" Then
            sql += " AND P.Product_Type_ID = '" & Product_type & "' "
        End If

        If Product_Group = "0" Then Product_Group = ""
        If Product_Group <> "" Then
            sql += " AND P.Product_Group_ID = '" & Product_Group & "'"
        End If

        If Product_Name <> "" Then
            sql += " AND (Product_TName  Like '%' + @Product_Name + '%' OR Product_EName Like '%' + @Product_Name)"
            parm(1) = SqlDB.SetText("@Product_Name", Product_Name)

        End If

        If Status <> "" Then
            sql += "AND P.Active_Status = '" & Status & "'"
        End If

        sql += " order by Product_TName,Product_EName"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql, parm)
        Return dt
    End Function

    Public Shared Sub GetListDDL_Product(ddl As DropDownList, ProductType As String)
        Dim sql As String = "select Product_ID, Product_TName  "
        sql += " from MS_Product"
        sql += " where Active_Status='Y'"

        If ProductType <> "" Then
            sql += " AND Product_Type_ID = '" & ProductType & "'"
        End If

        sql += " order by Product_TName"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("Product_ID") = 0
        dr("Product_TName") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "Product_ID"
        ddl.DataTextField = "Product_TName"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub

    Public Shared Function GetProductData(Sequence_ID As Long) As MsProductLinqDB
        Dim ret As New MsProductLinqDB
        ret.GetDataByPK(Sequence_ID, Nothing)
        Return ret
    End Function

    Public Shared Function SaveProduct(UserName As String, Sequence_ID As Long, Product_ID As String, Tname As String, Ename As String, BrandName As String, typeID As Long, GroupID As Long, CustomerID As Long, SupplierID As Long, ActiveStatus As Boolean) As MsProductLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsProductLinqDB
        lnq.GetDataByPK(Sequence_ID, trans.Trans)
        lnq.PRODUCT_ID = Product_ID
        lnq.PRODUCT_TNAME = Tname
        lnq.PRODUCT_ENAME = Ename
        lnq.BRAND_NAME = BrandName
        lnq.PRODUCT_TYPE_ID = typeID
        lnq.PRODUCT_GROUP_ID = GroupID
        lnq.CUSTOMER_ID = CustomerID
        lnq.SUPPLIER_ID = SupplierID
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.SEQUENCE_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeleteProduct(Sequence_ID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsProductLinqDB
        ret = lnq.DeleteByPK(Sequence_ID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateProductstatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsProductLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.SEQUENCE_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "Product Type"
    Public Shared Function GetList_ProductType(name As String, status As String) As DataTable
        Dim lnq As New MsProductTypeLinqDB

        Dim parm(1) As SqlParameter
        Dim wh As String = ""

        wh += "1=1"

        If name <> "" Then
            wh += " AND Product_Type  Like '%' + @Product_Type + '%' "
            parm(0) = SqlDB.SetText("@Product_Type", name)
        End If

        If status <> "" Then
            wh += " AND Active_Status = '" & status & "'"
        End If

        Dim dt As DataTable = lnq.GetDataList(wh, "Product_Type", Nothing, parm)
        lnq = Nothing
        Return dt
    End Function
    Public Shared Sub GetListDDL_ProductType(ddl As DropDownList)
        Dim sql As String = "select Product_Type_ID, Product_Type "
        sql += " from MS_Product_Type"
        sql += " where Active_Status='Y'"
        sql += " order by Product_Type"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("Product_Type_ID") = 0
        dr("Product_Type") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "Product_Type_ID"
        ddl.DataTextField = "Product_Type"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub
    Public Shared Function CheckDuplicateProductType(ProductTypeID As Long, ProductTypeName As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsProductTypeLinqDB
        Dim re As Boolean = lnq.ChkDuplicateByPRODUCT_TYPE(ProductTypeName, ProductTypeID, Nothing)
        If re = True Then
            ret.IsSuccess = False
            ret.ErrorMessage = "Duplicate Product Type " & ProductTypeName
        Else
            ret.IsSuccess = True
        End If

        Return ret
    End Function
    Public Shared Function GetProductTypeData(ProductTypeID As Long) As MsProductTypeLinqDB
        Dim ret As New MsProductTypeLinqDB
        ret.GetDataByPK(ProductTypeID, Nothing)
        Return ret
    End Function
    Public Shared Function SaveProductType(UserName As String, ProductTypeID As Long, ProductTypeName As String, ActiveStatus As Boolean) As MsProductTypeLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsProductTypeLinqDB
        lnq.GetDataByPK(ProductTypeID, trans.Trans)
        lnq.PRODUCT_TYPE = ProductTypeName
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.PRODUCT_TYPE_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeleteProductType(ProductTypeID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsProductTypeLinqDB
        ret = lnq.DeleteByPK(ProductTypeID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateProductTypestatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsProductTypeLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.PRODUCT_TYPE_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "Product Group"
    Public Shared Function GetList_ProductGroup(name As String, status As String) As DataTable
        Dim lnq As New MsProductGroupLinqDB

        Dim parm(1) As SqlParameter
        Dim wh As String = ""

        wh += "1=1"

        If name <> "" Then
            wh += " AND Product_Group  Like '%' + @Product_Group + '%' "
            parm(0) = SqlDB.SetText("@Product_Group", name)
        End If

        If status <> "" Then
            wh += " AND Active_Status = '" & status & "'"
        End If

        Dim dt As DataTable = lnq.GetDataList(wh, "", Nothing, parm)
        lnq = Nothing
        Return dt
    End Function
    Public Shared Sub GetListDDL_ProductGroup(ddl As DropDownList)
        Dim sql As String = "select Product_Group_ID, Product_Group "
        sql += " from MS_Product_Group"
        sql += " where Active_Status='Y'"
        sql += " order by Product_Group"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("Product_Group_ID") = 0
        dr("Product_Group") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "Product_Group_ID"
        ddl.DataTextField = "Product_Group"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub
    Public Shared Function CheckDuplicateProductGroup(ProductGroupID As Long, ProductGroupName As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsProductGroupLinqDB
        Dim re As Boolean = lnq.ChkDuplicateByPRODUCT_GROUP(ProductGroupName, ProductGroupID, Nothing)
        If re = True Then
            ret.IsSuccess = False
            ret.ErrorMessage = "Duplicate Product Group " & ProductGroupName
        Else
            ret.IsSuccess = True
        End If

        Return ret
    End Function
    Public Shared Function GetProductGroupData(ProductGroupID As Long) As MsProductGroupLinqDB
        Dim ret As New MsProductGroupLinqDB
        ret.GetDataByPK(ProductGroupID, Nothing)
        Return ret
    End Function
    Public Shared Function SaveProductGroup(UserName As String, ProductGroupID As Long, ProductGroupName As String, ActiveStatus As Boolean) As MsProductGroupLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsProductGroupLinqDB
        lnq.GetDataByPK(ProductGroupID, trans.Trans)
        lnq.PRODUCT_GROUP = ProductGroupName
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.PRODUCT_GROUP_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeleteProductGroup(ProductGroupID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsProductGroupLinqDB
        ret = lnq.DeleteByPK(ProductGroupID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateProductGroupstatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsProductGroupLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.PRODUCT_GROUP_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "Product Status"
    'Public Shared Function GetList_ProductGroup(name As String, status As String) As DataTable
    '    Dim lnq As New MsProductGroupLinqDB

    '    Dim parm(1) As SqlParameter
    '    Dim wh As String = ""

    '    wh += "1=1"

    '    If name <> "" Then
    '        wh += " AND Product_Group_Type  Like '%' + @Product_Group_Type + '%' "
    '        parm(0) = SqlDB.SetText("@Product_Group_Type", name)
    '    End If

    '    If status <> "" Then
    '        wh += " AND Active_Status = '" & status & "'"
    '    End If

    '    Dim dt As DataTable = lnq.GetDataList(wh, "Product_Group_Type", Nothing, parm)
    '    lnq = Nothing
    '    Return dt
    'End Function
    Public Shared Sub GetListDDL_ProductStatus(ddl As DropDownList)
        Dim sql As String = "select Product_Status_ID, Product_Status "
        sql += " from MS_Product_Status"
        sql += " where Active_Status='Y'"
        sql += " order by Product_Status"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("Product_Status_ID") = 0
        dr("Product_Status") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "Product_Status_ID"
        ddl.DataTextField = "Product_Status"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub
    'Public Shared Function CheckDuplicateProductGroup(ProductGroupID As Long, ProductGroupName As String) As ExecuteDataInfo
    '    Dim ret As New ExecuteDataInfo
    '    Dim lnq As New MsProductGroupLinqDB
    '    Dim re As Boolean = lnq.ChkDuplicateByPRODUCT_GROUP_TYPE(ProductGroupName, ProductGroupID, Nothing)
    '    If re = True Then
    '        ret.IsSuccess = False
    '        ret.ErrorMessage = "Duplicate Product Group " & ProductGroupName
    '    Else
    '        ret.IsSuccess = True
    '    End If

    '    Return ret
    'End Function
    'Public Shared Function GetProductGroupData(ProductGroupID As Long) As MsProductGroupLinqDB
    '    Dim ret As New MsProductGroupLinqDB
    '    ret.GetDataByPK(ProductGroupID, Nothing)
    '    Return ret
    'End Function
    'Public Shared Function SaveProductGroup(UserName As String, ProductGroupID As Long, ProductGroupName As String, ActiveStatus As Boolean) As MsProductGroupLinqDB
    '    Dim trans As New TransactionDB
    '    Dim lnq As New MsProductGroupLinqDB
    '    lnq.GetDataByPK(ProductGroupID, trans.Trans)
    '    lnq.PRODUCT_GROUP_TYPE = ProductGroupName
    '    lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

    '    Dim ret As New ExecuteDataInfo
    '    If lnq.PRODUCT_GROUP_ID > 0 Then
    '        ret = lnq.UpdateData(UserName, trans.Trans)
    '    Else
    '        ret = lnq.InsertData(UserName, trans.Trans)
    '    End If
    '    If ret.IsSuccess = True Then
    '        trans.CommitTransaction()
    '    Else
    '        trans.RollbackTransaction()
    '    End If

    '    Return lnq
    'End Function
    'Public Shared Function DeleteProductGroup(ProductGroupID As Long) As ExecuteDataInfo
    '    Dim trans As New TransactionDB

    '    Dim ret As New ExecuteDataInfo
    '    Dim lnq As New MsProductGroupLinqDB
    '    ret = lnq.DeleteByPK(ProductGroupID, trans.Trans)
    '    If ret.IsSuccess = True Then
    '        trans.CommitTransaction()
    '    Else
    '        trans.RollbackTransaction()
    '    End If
    '    lnq = Nothing

    '    Return ret
    'End Function

    'Public Shared Function UpdateProductGroupstatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
    '    Dim ret As New ExecuteDataInfo
    '    Dim trans As New TransactionDB
    '    Dim lnq As New MsProductGroupLinqDB
    '    lnq.GetDataByPK(ID, trans.Trans)
    '    If lnq.PRODUCT_GROUP_ID > 0 Then
    '        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
    '        ret = lnq.UpdateData(LoginUsername, trans.Trans)
    '    End If

    '    If ret.IsSuccess = True Then
    '        trans.CommitTransaction()
    '    Else
    '        trans.RollbackTransaction()
    '    End If
    '    lnq = Nothing

    '    Return ret
    'End Function
#End Region

#Region "Unit"
    Public Shared Function GetList_Unit(name As String, status As String) As DataTable
        Dim lnq As New MsUnitLinqDB

        Dim parm(1) As SqlParameter
        Dim wh As String = ""

        wh += "1=1"

        If name <> "" Then
            wh += " AND Unit  Like '%' + @Unit_name + '%' "
            parm(0) = SqlDB.SetText("@Unit_name", name)
        End If

        If status <> "" Then
            wh += " AND Active_Status = '" & status & "'"
        End If

        Dim dt As DataTable = lnq.GetDataList(wh, "Unit", Nothing, parm)
        lnq = Nothing
        Return dt
    End Function
    Public Shared Sub GetListDDL_Unit(ddl As DropDownList)
        Dim sql As String = "select Unit_id, Unit "
        sql += " from MS_Unit"
        sql += " where Active_Status='Y'"
        sql += " order by Unit"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("Unit_id") = 0
        dr("Unit") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "Unit_id"
        ddl.DataTextField = "Unit"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub
    Public Shared Function CheckDuplicateUnit(UnitID As Long, UnitName As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsUnitLinqDB
        Dim re As Boolean = lnq.ChkDuplicateByUnit_NAME(UnitName, UnitID, Nothing)
        If re = True Then
            ret.IsSuccess = False
            ret.ErrorMessage = "Duplicate Unit " & UnitName
        Else
            ret.IsSuccess = True
        End If

        Return ret
    End Function
    Public Shared Function GetUnitData(UnitID As Long) As MsUnitLinqDB
        Dim ret As New MsUnitLinqDB
        ret.GetDataByPK(UnitID, Nothing)
        Return ret
    End Function
    Public Shared Function SaveUnit(UserName As String, UnitID As Long, UnitName As String, ActiveStatus As Boolean) As MsUnitLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsUnitLinqDB
        lnq.GetDataByPK(UnitID, trans.Trans)
        lnq.UNIT = UnitName
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.UNIT_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeleteUnit(UnitID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsUnitLinqDB
        ret = lnq.DeleteByPK(UnitID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateUnitstatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsUnitLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.UNIT_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "SKU"
    Public Shared Function GetList_SKU(SKU_ID As String, SKUGroup_ID As String, ProductName As String, status As String) As DataTable
        Dim sql As String = "SELECT s.Sequence_ID,SKU_ID ,s.SKU_Group_ID,sg.SKU_Group_Name,s.Product_ID"
        sql += " ,(p.Product_TName + '/' +p.Product_EName) Project_Name ,s.Active_Status"
        sql += "   FROM MS_SKU S"
        sql += " left join MS_SKU_Group SG On  s.SKU_Group_ID = sg.SKU_Group_ID"
        sql += " left join MS_Product P On s.Product_ID = P.Product_ID where 1=1"

        Dim parm(2) As SqlParameter

        If SKU_ID <> "" Then
            sql += " AND SKU_ID Like '%' + @SKU_ID + '%'"
            parm(0) = SqlDB.SetText("@SKU_ID", SKU_ID)
        End If

        If SKUGroup_ID = "0" Then SKUGroup_ID = ""
        If SKUGroup_ID <> "" Then
            sql += " AND s.SKU_Group_ID = '" & SKUGroup_ID & "'"
        End If

        If ProductName <> "" Then
            sql += " AND (p.Product_TName Like '%' + @ProductName + '%' OR p.Product_EName Like '%' + @ProductName + '%')"
            parm(1) = SqlDB.SetText("@ProductName", ProductName)
        End If

        If status <> "" Then
            sql += " AND s.Active_Status = '" & status & "'"
        End If

        sql += " order by s.SKU_Group_ID,SKU_ID"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql, parm)
        Return dt
    End Function
    Public Shared Sub GetListDDL_SKU(ddl As DropDownList, GroupSKU As String)
        Dim sql As String = "select SKU_ID, SKU_ID +'(' + isnull(SKU_Description,'-') + ')' SKU"
        sql += " from MS_SKU"
        sql += " where Active_Status='Y'"

        If GroupSKU <> "" Then
            sql += "AND SKU_Group_ID = '" & GroupSKU & "'"
        End If

        sql += " order by SKU"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("SKU_ID") = 0
        dr("SKU") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "SKU_ID"
        ddl.DataTextField = "SKU"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub

    Public Shared Function GetSKUData(SKUID As Long) As MsSkuLinqDB
        Dim ret As New MsSkuLinqDB
        ret.GetDataByPK(SKUID, Nothing)
        Return ret
    End Function

    Public Shared Function SaveSKU(UserName As String, SequenceID As Long, ID As String, Description As String, GroupSKU As String, size As String, color As String, model As String, Brand As String, Unit As String, WeightPerUnit As String, ProductType As String, Product As String, VolumePerUnit As String, width As String, length As String, height As String, ProductionDate As String, ExpireDate As String, AgeDay As String, AgeMonth As String, AgeYear As String, Picture As String, BuyPrice As String, SellPrice As String, OtherPrice As String, AmountMax As String, AmountMin As String, WeightMax As String, WeightMin As String, VolumeMax As String, VolumeMin As String, PalletType As String, MAxPallet As String, PickOrder As String, Amount As String, ActiveStatus As Boolean) As MsSkuLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsSkuLinqDB
        lnq.GetDataByPK(SequenceID, trans.Trans)
        lnq.SKU_ID = ID
        lnq.SKU_DESCRIPTION = Description
        lnq.SKU_GROUP_ID = GroupSKU
        lnq.SIZE = size
        lnq.COLOR = color
        lnq.MODEL = model
        lnq.BRAND = Brand
        lnq.UNIT_ID = Unit
        If WeightPerUnit = "" Then WeightPerUnit = 0
        lnq.WEIGHT_PER_UNIT = WeightPerUnit
        lnq.PRODUCT_TYPE_ID = ProductType
        lnq.PRODUCT_ID = Product
        lnq.VOLUME_PER_UNIT = VolumePerUnit
        lnq.VOLUME_WIDTH = width
        lnq.VOLUME_LENGTH = length
        lnq.VOLUME_HIGHT = height
        lnq.AGE_PRODUCT_DAY = AgeDay
        lnq.AGE_PRODUCT_MONTH = AgeMonth
        lnq.AGE_PRODUCT_YEAR = AgeYear
        lnq.PRODUCTION_DATE = StringToDate(ProductionDate, "dd/MM/yyyy")
        lnq.EXPIRE_DATE = StringToDate(ExpireDate, "dd/MM/yyyy")
        lnq.PRODUCT_PICTURE = Picture
        lnq.BUY_PRICE = BuyPrice
        lnq.SELL_PRICE = SellPrice
        If OtherPrice = "" Then OtherPrice = 0
        lnq.OTHER_PRICE = OtherPrice
        lnq.MAXIMUM_AMOUNT = AmountMax
        lnq.MINIMUM_AMOUNT = AmountMin
        lnq.MAXIMUM_WEIGHT = WeightMax
        lnq.MINIMUM_WEIGHT = WeightMin
        lnq.MAXIMUM_VOLUME = VolumeMax
        lnq.MINIMUM_VOLUME = VolumeMin
        lnq.PALLET_TYPE_ID = PalletType
        lnq.MAXIMUM_PER_PALLET = MAxPallet
        lnq.PICK_ID = PickOrder
        lnq.AMOUNT_PER_UNIT = Amount
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.SEQUENCE_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeleteSKU(SequenceID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsSkuLinqDB
        ret = lnq.DeleteByPK(SequenceID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateSKUstatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsSkuLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.SEQUENCE_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "SKU Group"
    Public Shared Function GetList_SKUGroup(name As String, status As String) As DataTable
        Dim lnq As New MsSkuGroupLinqDB

        Dim parm(1) As SqlParameter
        Dim wh As String = ""

        wh += "1=1"

        If name <> "" Then
            wh += " AND SKU_Group_Name  Like '%' + @SKU_Group_Name + '%' "
            parm(0) = SqlDB.SetText("@SKU_Group_Name", name)
        End If

        If status <> "" Then
            wh += " AND Active_Status = '" & status & "'"
        End If

        Dim dt As DataTable = lnq.GetDataList(wh, "SKU_Group_Name", Nothing, parm)
        lnq = Nothing
        Return dt
    End Function
    Public Shared Sub GetListDDL_SKUGroup(ddl As DropDownList)
        Dim sql As String = "select SKU_Group_ID, SKU_Group_Name "
        sql += " from MS_SKU_Group"
        sql += " where Active_Status='Y'"
        sql += " order by SKU_Group_Name"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("SKU_Group_ID") = 0
        dr("SKU_Group_Name") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "SKU_Group_ID"
        ddl.DataTextField = "SKU_Group_Name"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub

    Public Shared Function CheckDuplicateSKUGroup(SKUGroupID As Long, SKUGroupName As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsSkuGroupLinqDB
        Dim re As Boolean = lnq.ChkDuplicateBySKU_GROUP_NAME(SKUGroupName, SKUGroupID, Nothing)
        If re = True Then
            ret.IsSuccess = False
            ret.ErrorMessage = "Duplicate SKU Group " & SKUGroupName
        Else
            ret.IsSuccess = True
        End If

        Return ret
    End Function

    Public Shared Function GetSKUGroupData(SKUGroupID As Long) As MsSkuGroupLinqDB
        Dim ret As New MsSkuGroupLinqDB
        ret.GetDataByPK(SKUGroupID, Nothing)
        Return ret
    End Function

    Public Shared Function SaveSKUGroup(UserName As String, SequenceID As Long, SKUGroupID As String, SKUGroupName As String, ActiveStatus As Boolean) As MsSkuGroupLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsSkuGroupLinqDB
        lnq.GetDataByPK(SequenceID, trans.Trans)
        lnq.SKU_GROUP_ID = SKUGroupID
        lnq.SKU_GROUP_NAME = SKUGroupName
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.SEQUENCE_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeleteSKUGroup(SequenceID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsSkuGroupLinqDB
        ret = lnq.DeleteByPK(SequenceID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateSKUGroupstatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsSkuGroupLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.SEQUENCE_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "SKU Ingredient"
    Public Shared Function GetList_Ingredient(name As String, status As String) As DataTable
        Dim lnq As New MsSkuIngredientLinqDB

        Dim parm(1) As SqlParameter
        Dim wh As String = ""

        wh += "1=1"

        If name <> "" Then
            wh += " AND (SKU_Ingredient_Name  Like '%' + @SKU_Ingredient_Name + '%' Or SKU_Ingredient_ID  Like '%' + @SKU_Ingredient_Name + '%')"
            parm(0) = SqlDB.SetText("@SKU_Ingredient_Name", name)
        End If

        If status <> "" Then
            wh += " AND Active_Status = '" & status & "'"
        End If

        Dim dt As DataTable = lnq.GetDataList(wh, "SKU_Ingredient_Name", Nothing, parm)
        lnq = Nothing
        Return dt
    End Function
    Public Shared Sub GetListDDL_Ingredient(ddl As DropDownList)
        Dim sql As String = "select SKU_Ingredient_ID, SKU_Ingredient_Name "
        sql += " from MS_SKU_Ingredient"
        sql += " where Active_Status='Y'"
        sql += " order by SKU_Ingredient_Name"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("SKU_Ingredient_ID") = 0
        dr("SKU_Ingredient_Name") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "SKU_Ingredient_ID"
        ddl.DataTextField = "SKU_Ingredient_Name"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub

    Public Shared Function GetIngredientData(IngredientID As Long) As MsSkuIngredientLinqDB
        Dim ret As New MsSkuIngredientLinqDB
        ret.GetDataByPK(IngredientID, Nothing)
        Return ret
    End Function

    Public Shared Function SaveIngredient(UserName As String, SequenceID As Long, IngredientID As String, IngredientName As String, ActiveStatus As Boolean) As MsSkuIngredientLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsSkuIngredientLinqDB
        lnq.GetDataByPK(SequenceID, trans.Trans)
        lnq.SKU_INGREDIENT_ID = IngredientID
        lnq.SKU_INGREDIENT_NAME = IngredientName
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.SEQUENCE_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeleteIngredient(SequenceID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsSkuIngredientLinqDB
        ret = lnq.DeleteByPK(SequenceID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateIngredientstatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsSkuIngredientLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.SEQUENCE_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "SKU Ingredient By"
    Public Shared Function GetList_IngredientBy(IngredientID As String) As DataTable
        Dim sql As String = "SELECT  SI.Sequence_ID,SKU_Ingredient_ID ,SI.SKU_Group_ID,SG.SKU_Group_Name ,Si.SKU_ID,Amount,Quantity,Volume ,SI.Active_Status"
        sql += "  FROM MS_SKU_Ingredient_By SI"
        sql += " left join MS_SKU_Group SG On SI.SKU_Group_ID = SG.SKU_Group_ID"

        If IngredientID <> "" Then
            sql += " Where SI.SKU_Ingredient_ID = '" & IngredientID & "'"
        End If

        sql += " order by SI.SKU_Group_ID"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Return dt
    End Function

    Public Shared Function SaveIngredientBy(UserName As String, IngredientID As String, dtIngredientBy As DataTable) As MsSkuIngredientByLinqDB
        Dim trans As New TransactionDB
        Dim lnqIngredient As New MsSkuIngredientByLinqDB
        Dim ret As New ExecuteDataInfo

        Dim p_ab(1) As SqlParameter
        p_ab(0) = SqlDB.SetText("@IngredientID", IngredientID)
        SqlDB.ExecuteNonQuery("DELETE FROM MS_SKU_Ingredient_By WHERE SKU_Ingredient_ID=@IngredientID", trans.Trans, p_ab)
        ret.IsSuccess = True

        If Not dtIngredientBy Is Nothing Then

            For i As Integer = 0 To dtIngredientBy.Rows.Count - 1
                Dim Sequence_ID As String = dtIngredientBy.Rows(i)("Sequence_ID").ToString()
                Dim SKU_Group_ID As String = dtIngredientBy.Rows(i)("SKU_Group_ID").ToString()
                Dim SKU_ID As String = dtIngredientBy.Rows(i)("SKU_ID").ToString()
                Dim Amount As String = dtIngredientBy.Rows(i)("Amount").ToString()
                Dim Quantity As String = dtIngredientBy.Rows(i)("Quantity").ToString()
                Dim Volume As String = dtIngredientBy.Rows(i)("Volume").ToString()
                Dim Active_Status As String = dtIngredientBy.Rows(i)("Active_Status").ToString()


                If Sequence_ID <> "" AndAlso IngredientID <> "" Then

                    With lnqIngredient
                        .GetDataByPK(Sequence_ID, trans.Trans)
                        .SKU_INGREDIENT_ID = IngredientID
                        .SKU_GROUP_ID = SKU_Group_ID
                        .SKU_ID = SKU_ID
                        .AMOUNT = Amount
                        .QUANTITY = Quantity
                        .VOLUME = Volume
                        .ACTIVE_STATUS = Active_Status

                        ret.IsSuccess = False
                        ret = lnqIngredient.InsertData(UserName, trans.Trans)
                        If ret.IsSuccess = False Then
                            Exit For
                        End If
                    End With
                End If
            Next

            If ret.IsSuccess = True Then
                trans.CommitTransaction()
            Else
                trans.RollbackTransaction()
            End If
        End If

        Return lnqIngredient

    End Function
#End Region

#Region "Warehouse"
    Public Shared Function GetList_Warehouse(name As String, status As String) As DataTable
        Dim sql As String = "SELECT Warehouse_ID,Warehouse_Code,Warehouse_Name,isnull(Warehouse_Size,'-') Warehouse_Size,isnull(Warehouse_Building,'-') Warehouse_Building"
        sql += ",isnull(Warehouse_Floor,'-') Warehouse_Floor,isnull(Warehouse_Room,'-') Warehouse_Room ,isnull(Warehouse_Zone,'-') Warehouse_Zone,isnull(Warehouse_Lock,'-') Warehouse_Lock"
        sql += ",isnull(Warehouse_Row,'-') Warehouse_Row,isnull(Warehouse_Shelf,'-') Warehouse_Shelf,isnull(Warehouse_Deep,'-') Warehouse_Deep"
        sql += ",W.Warehouse_Type_ID,Wt.Warehouse_Type ,W.Active_Status"
        sql += "  FROM MS_Warehouse W "
        sql += "  inner join MS_Warehouse_Type WT ON w.Warehouse_Type_ID = WT.Warehouse_Type_ID "

        'Dim parm(5) As SqlParameter
        Dim wh As String = ""
        'If name <> "" Then
        '    wh += " AND (u.First_Name  Like '%' + @_STAFF_NAME + '%' or u.last_name Like '%' + @_STAFF_NAME + '%') "
        '    parm(0) = SqlDB.SetText("@_STAFF_NAME", name)
        'End If
        'If sex <> "" Then
        '    wh += " AND u.gender = @_SEX"

        '    parm(1) = SqlDB.SetText("@_SEX", sex)
        'End If

        'If department <> 0 Then
        '    wh += " AND u.department_id = @_DEPARTMENT_ID"
        '    parm(2) = SqlDB.SetBigInt("@_DEPARTMENT_ID", department)
        'End If

        'If position <> 0 Then
        '    wh += " AND u.position_id = @_POSITION_ID"
        '    parm(3) = SqlDB.SetBigInt("@_POSITION_ID", position)
        'End If

        'If status <> "" Then
        '    wh += " AND u.active_status = '" & status & "'"
        '    parm(4) = SqlDB.SetText("@_STATUS", status)
        'End If

        'If wh.Trim <> "" Then
        '    sql += " where 1=1 " & wh
        'End If
        sql += " order by Warehouse_Name"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Return dt
    End Function

    Public Shared Function CheckDuplicateWarehouse(WarehouseTypeID As String, WarehouseName As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim sql As String = " select Sequence_ID ,Warehouse_ID,Warehouse_Type_ID,Warehouse_Name  from MS_Warehouse "

        If WarehouseTypeID <> "" Then
            sql += "WHERE Warehouse_Type_ID  =  '" & WarehouseTypeID & "'"
        End If
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        If dt.Rows.Count > 0 Then
            For i As Integer = 0 To dt.Rows.Count - 1
                Dim Warehouse_Name As String = dt.Rows(i)("Warehouse_Name").ToString()
                Dim Warehouse_ID As String = dt.Rows(i)("Warehouse_ID").ToString()

                If WarehouseName = Warehouse_Name Then
                    ret.IsSuccess = False
                    ret.ErrorMessage = "Duplicate Warehouse name " & WarehouseName
                    Exit For
                Else
                    ret.IsSuccess = True
                End If
            Next
        Else
            ret.IsSuccess = True
        End If
        Return ret
    End Function
    Public Shared Function GetWarehouseData(WarehouseID As Long) As MsWarehouseLinqDB
        Dim ret As New MsWarehouseLinqDB
        ret.GetDataByPK(WarehouseID, Nothing)
        Return ret
    End Function
    Public Shared Function SaveWarehouse(UserName As String, ID As Long, WarehouseID As String, name As String, WarehouseTypeID As Long, size As String, build As String, floor As String, room As String, zone As String, lock As String, row As String, shelf As String, deep As String, ActiveStatus As Boolean) As MsWarehouseLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsWarehouseLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        lnq.WAREHOUSE_ID = WarehouseID
        lnq.WAREHOUSE_NAME = name
        lnq.WAREHOUSE_TYPE_ID = WarehouseTypeID
        lnq.WAREHOUSE_SIZE = size
        lnq.WAREHOUSE_BUILDING = build
        lnq.WAREHOUSE_FLOOR = floor
        lnq.WAREHOUSE_ROOM = room
        lnq.WAREHOUSE_ZONE = zone
        lnq.WAREHOUSE_LOCK = lock
        lnq.WAREHOUSE_ROW = row
        lnq.WAREHOUSE_SHELF = shelf
        lnq.WAREHOUSE_DEEP = deep
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.WAREHOUSE_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function

    Public Shared Function DeleteWarehouse(WarehouseID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsWarehouseLinqDB
        ret = lnq.DeleteByPK(WarehouseID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateWarehousestatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsWarehouseLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.WAREHOUSE_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "Warehouse_Address"
    Public Shared Function GetList_Warehouse_Address(Warehouse_ID As String) As DataTable
        Dim sql As String = " Select Sequence_ID ,Warehouse_ID,Warehouse_House_No,Warehouse_Building,Warehouse_moo,Warehouse_soi,Warehouse_road,A.Warehouse_Tumbon"
        sql += " ,A.Warehouse_District,A.Warehouse_Province,A.Warehouse_Country,Warehouse_Telephone,Warehouse_Mobile"
        sql += " ,'บ้านเลขที่ ' + isnull(Warehouse_House_No ,'- ')  + ' อาคารที่ตั้ง ' + isnull(Warehouse_Building,'- ') "
        sql += " + ' หมู่ ' +isnull(Warehouse_moo,'- ') + ' ซอย ' +isnull(Warehouse_soi,'-') + ' ถนน ' + isnull(Warehouse_road,'- ')"
        sql += " + ' ตำบล/แขวง ' + isnull(AT.Tumbon_Name  ,'- ') + ' อำเภอ/เขต ' + isnull(AD.District_Name  ,'- ')"
        sql += " + ' จังหวัด ' +  isnull(AP.Province_Name ,'- ') + ' ประเทศ ' + isnull(Ac.Country_Name ,'- ') "
        sql += " + ' รหัสไปรษณีย์ ' +isnull((select CAST(Warehouse_Postcode as varchar(max))),'') Address ,Warehouse_Postcode,A.Active_Status"
        sql += "  from MS_Warehouse_Address    A "
        sql += " left join MS_Address_Tumbon  AT on a.Warehouse_Tumbon = AT.Tumbon_ID   "
        sql += " left join MS_Address_District  AD on a.Warehouse_District = Ad.District_ID "
        sql += " left join MS_Address_Province  AP on a.Warehouse_Province = AP.Province_ID "
        sql += " left join MS_Address_Country AC on a.Warehouse_Country = Ac.Country_ID "

        If Warehouse_ID <> "" Then
            sql += "WHERE Warehouse_ID = '" & Warehouse_ID & "'"
        End If

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Return dt
    End Function

    Public Shared Function GetWarehouse_AddressData(Warehouse_AddressID As Long) As MsWarehouseAddressLinqDB
        Dim ret As New MsWarehouseAddressLinqDB
        ret.GetDataByPK(Warehouse_AddressID, Nothing)
        Return ret
    End Function
    Public Shared Function SaveWarehouse_AddressData(UserName As String, WarehouseID As String, DtAddress As DataTable) As MsWarehouseAddressLinqDB
        Dim trans As New TransactionDB
        Dim lnqAddress As New MsWarehouseAddressLinqDB
        Dim ret As New ExecuteDataInfo

        Dim p_ab(1) As SqlParameter
        p_ab(0) = SqlDB.SetText("@WarehouseID", WarehouseID)
        SqlDB.ExecuteNonQuery("DELETE FROM MS_Warehouse_Address WHERE Warehouse_ID=@WarehouseID", trans.Trans, p_ab)
        ret.IsSuccess = True

        If Not DtAddress Is Nothing Then

            For i As Integer = 0 To DtAddress.Rows.Count - 1
                Dim Sequence_ID As String = DtAddress.Rows(i)("Sequence_ID").ToString()
                Dim Warehouse_ID As String = DtAddress.Rows(i)("Warehouse_ID").ToString()
                Dim House_No As String = DtAddress.Rows(i)("Warehouse_House_No").ToString()
                Dim Building_Name As String = DtAddress.Rows(i)("Warehouse_Building").ToString()
                Dim moo As String = DtAddress.Rows(i)("Warehouse_moo").ToString()
                Dim soi_name As String = DtAddress.Rows(i)("Warehouse_soi").ToString()
                Dim road_name As String = DtAddress.Rows(i)("Warehouse_road").ToString()
                Dim Tumbon_ID As String = DtAddress.Rows(i)("Warehouse_Tumbon").ToString()
                Dim District_ID As String = DtAddress.Rows(i)("Warehouse_District").ToString()
                Dim Province_ID As String = DtAddress.Rows(i)("Warehouse_Province").ToString()
                Dim Country_ID As String = DtAddress.Rows(i)("Warehouse_Country").ToString()
                Dim Postcode As String = DtAddress.Rows(i)("Warehouse_Postcode").ToString()
                Dim Tel As String = DtAddress.Rows(i)("Warehouse_Telephone").ToString()
                Dim Mobile As String = DtAddress.Rows(i)("Warehouse_Mobile").ToString()
                Dim Active_Status As String = DtAddress.Rows(i)("Active_Status").ToString()

                If Sequence_ID <> "" AndAlso WarehouseID <> "" Then

                    With lnqAddress
                        .GetDataByPK(Sequence_ID, trans.Trans)
                        .WAREHOUSE_ID = WarehouseID
                        .WAREHOUSE_HOUSE_NO = House_No
                        .WAREHOUSE_BUILDING = Building_Name
                        .WAREHOUSE_MOO = moo
                        .WAREHOUSE_SOI = soi_name
                        .WAREHOUSE_ROAD = road_name
                        .WAREHOUSE_TUMBON = Tumbon_ID
                        .WAREHOUSE_DISTRICT = District_ID
                        .WAREHOUSE_PROVINCE = Province_ID
                        .WAREHOUSE_COUNTRY = Country_ID
                        .WAREHOUSE_POSTCODE = Postcode
                        .WAREHOUSE_TELEPHONE = Tel
                        .WAREHOUSE_MOBILE = Mobile
                        .ACTIVE_STATUS = Active_Status

                        ret.IsSuccess = False
                        ret = lnqAddress.InsertData(UserName, trans.Trans)
                        If ret.IsSuccess = False Then
                            Exit For
                        End If
                    End With
                End If
            Next

            If ret.IsSuccess = True Then
                trans.CommitTransaction()
            Else
                trans.RollbackTransaction()
            End If
        End If

        Return lnqAddress

    End Function

    Public Shared Function DeleteWarehouse_Address(Warehouse_AddressID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsWarehouseAddressLinqDB
        ret = lnq.DeleteByPK(Warehouse_AddressID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "Warehouse Type"
    Public Shared Function GetList_WarehouseType(name As String, status As String) As DataTable
        Dim lnq As New MsWarehouseTypeLinqDB

        Dim parm(1) As SqlParameter
        Dim wh As String = ""

        wh += "1=1"

        If name <> "" Then
            wh += " AND Warehouse_Type  Like '%' + @Warehouse_Type + '%' "
            parm(0) = SqlDB.SetText("@Warehouse_Type", name)
        End If

        If status <> "" Then
            wh += " AND Active_Status = '" & status & "'"
        End If

        Dim dt As DataTable = lnq.GetDataList(wh, "Warehouse_Type", Nothing, parm)
        lnq = Nothing
        Return dt
    End Function
    Public Shared Sub GetListDDL_WarehouseType(ddl As DropDownList)
        Dim sql As String = "select Warehouse_Type_ID, Warehouse_Type "
        sql += " from MS_Warehouse_Type"
        sql += " where Active_Status='Y'"
        sql += " order by Warehouse_Type"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("Warehouse_Type_ID") = 0
        dr("Warehouse_Type") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "Warehouse_Type_ID"
        ddl.DataTextField = "Warehouse_Type"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub
    Public Shared Function CheckDuplicateWarehouseType(WarehouseTypeID As Long, WarehouseTypeName As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsWarehouseTypeLinqDB
        Dim re As Boolean = lnq.ChkDuplicateByWAREHOUSE_TYPE(WarehouseTypeName, WarehouseTypeID, Nothing)
        If re = True Then
            ret.IsSuccess = False
            ret.ErrorMessage = "Duplicate Warehouse Type " & WarehouseTypeName
        Else
            ret.IsSuccess = True
        End If

        Return ret
    End Function
    Public Shared Function GetWarehouseTypeData(WarehouseTypeID As Long) As MsWarehouseTypeLinqDB
        Dim ret As New MsWarehouseTypeLinqDB
        ret.GetDataByPK(WarehouseTypeID, Nothing)
        Return ret
    End Function
    Public Shared Function SaveWarehouseType(UserName As String, WarehouseTypeID As Long, WarehouseTypeName As String, ActiveStatus As Boolean) As MsWarehouseTypeLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsWarehouseTypeLinqDB
        lnq.GetDataByPK(WarehouseTypeID, trans.Trans)
        lnq.WAREHOUSE_TYPE = WarehouseTypeName
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.WAREHOUSE_TYPE_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeleteWarehouseType(WarehouseTypeID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsWarehouseTypeLinqDB
        ret = lnq.DeleteByPK(WarehouseTypeID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateWarehouseTypestatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsWarehouseTypeLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.WAREHOUSE_TYPE_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "Pallet"
    Public Shared Function GetList_Pallet(pallet_Type As String, Pallet_ID As String, Pallet_Status As String, Status As String) As DataTable
        Dim sql As String = "SELECT P.Sequence_ID,Pallet_No,P.Pallet_Type_ID,PT.Pallet_Type ,isnull(Pallet_Description,'') Pallet_Description,isnull(Pallet_Status,'') Pallet_Status,P.Active_Status"
        sql += "   FROM MS_Pallet P "
        sql += " inner join MS_Pallet_Type PT ON P.Pallet_Type_ID = PT.Pallet_Type_ID where 1=1"

        Dim parm(1) As SqlParameter
        If pallet_Type = "0" Then pallet_Type = ""
        If pallet_Type <> "" Then
            sql += " AND P.Pallet_Type_ID = '" & pallet_Type & "'"
        End If

        If Pallet_ID <> "" Then
            sql += " AND Pallet_No LIKE '%' + @Pallet_ID + '%' "
            parm(0) = SqlDB.SetText("@Pallet_ID", Pallet_ID)
        End If

        If Pallet_Status <> "" Then
            sql += " AND Pallet_Status  LIKE '%' + @Pallet_Status + '%' "
            parm(1) = SqlDB.SetText("@Pallet_Status", Pallet_Status)
        End If

        If Status <> "" Then
            sql += " AND P.Active_Status = '" & Status & "' "
        End If

        sql += " order by Pallet_No"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql, parm)
        Return dt
    End Function
    Public Shared Sub GetListDDL_Pallet(ddl As DropDownList)
        Dim sql As String = "Select Pallet_Type_ID, Pallet_Type "
        sql += " from MS_Pallet_Type"
        sql += " where Active_Status='Y'"
        sql += " order by Pallet_Type"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("Pallet_Type_ID") = 0
        dr("Pallet_Type") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "Pallet_Type_ID"
        ddl.DataTextField = "Pallet_Type"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub

    Public Shared Function CheckDuplicatePallet(PalletID As Long, PalletName As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsPalletLinqDB
        Dim re As Boolean = lnq.ChkDuplicateByPALLETNO(PalletName, PalletID, Nothing)
        If re = True Then
            ret.IsSuccess = False
            ret.ErrorMessage = "Duplicate Pallet Type " & PalletName
        Else
            ret.IsSuccess = True
        End If

        Return ret
    End Function

    Public Shared Function GetPalletData(PalletID As Long) As MsPalletLinqDB
        Dim ret As New MsPalletLinqDB
        ret.GetDataByPK(PalletID, Nothing)
        Return ret
    End Function
    Public Shared Function SavePallet(UserName As String, SeqID As Long, PalletINo As String, PalletTypeID As String, PalletDescription As String, PalletStatus As String, ActiveStatus As Boolean) As MsPalletLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsPalletLinqDB
        lnq.GetDataByPK(SeqID, trans.Trans)
        lnq.PALLET_NO = PalletINo
        lnq.PALLET_TYPE_ID = PalletTypeID
        lnq.PALLET_DESCRIPTION = PalletDescription
        lnq.PALLET_STATUS = PalletStatus
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As ExecuteDataInfo
        If lnq.SEQUENCE_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeletePallet(PalletID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsPalletLinqDB
        ret = lnq.DeleteByPK(PalletID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdatePalletstatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsPalletLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.SEQUENCE_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "Pallet Type"
    Public Shared Function GetList_PalletType(name As String, status As String) As DataTable
        Dim lnq As New MsPalletTypeLinqDB

        Dim parm(1) As SqlParameter
        Dim wh As String = ""

        wh += "1=1"

        If name <> "" Then
            wh += " AND Pallet_Type  Like '%' + @Pallet_Type + '%' "
            parm(0) = SqlDB.SetText("@Pallet_Type", name)
        End If

        If status <> "" Then
            wh += " AND Active_Status = '" & status & "'"
        End If

        Dim dt As DataTable = lnq.GetDataList(wh, "Pallet_Type", Nothing, parm)
        lnq = Nothing
        Return dt
    End Function
    Public Shared Sub GetListDDL_PalletType(ddl As DropDownList)
        Dim sql As String = "select Pallet_Type_ID, Pallet_Type "
        sql += " from MS_Pallet_Type"
        sql += " where Active_Status='Y'"
        sql += " order by Pallet_Type"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("Pallet_Type_ID") = 0
        dr("Pallet_Type") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "Pallet_Type_ID"
        ddl.DataTextField = "Pallet_Type"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub

    Public Shared Function CheckDuplicatePalletType(PalletTypeID As Long, PalletTypeName As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsPalletTypeLinqDB
        Dim re As Boolean = lnq.ChkDuplicateByPALLET_TYPE(PalletTypeName, PalletTypeID, Nothing)
        If re = True Then
            ret.IsSuccess = False
            ret.ErrorMessage = "Duplicate Pallet Type " & PalletTypeName
        Else
            ret.IsSuccess = True
        End If

        Return ret
    End Function

    Public Shared Function GetPalletTypeData(PalletTypeID As Long) As MsPalletTypeLinqDB
        Dim ret As New MsPalletTypeLinqDB
        ret.GetDataByPK(PalletTypeID, Nothing)
        Return ret
    End Function
    Public Shared Function SavePalletType(UserName As String, SeqID As Long, PalletTypeID As String, PalletTypeName As String, ActiveStatus As Boolean) As MsPalletTypeLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsPalletTypeLinqDB
        lnq.GetDataByPK(SeqID, trans.Trans)
        lnq.PALLET_TYPE = PalletTypeName
        lnq.PALLET_TYPE_ID = PalletTypeID
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As ExecuteDataInfo
        If lnq.SEQUENCE_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeletePalletType(PalletTypeID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsPalletTypeLinqDB
        ret = lnq.DeleteByPK(PalletTypeID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdatePalletTypestatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsPalletTypeLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.SEQUENCE_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "User Data"
    Public Shared Function GetList_User(name As String, sex As String, department As String, position As String, status As String) As DataTable
        Dim sql As String = "select u.user_id, u.user_login, u.department_id, d.department_name , u.position_id, p.position_name, "
        sql += " u.prefix_name_id, isnull(pn.prefix_name,'') + isnull(u.First_Name,'') + ' ' + isnull(u.last_name,'') full_name, "
        sql += " case u.gender when 'M' then 'ชาย' when 'F' then 'หญิง' end gender,"
        sql += " u.last_login_time, u.active_status"
        sql += " from MS_USER u "
        sql += " inner join MS_DEPARTMENT d on d.department_id=u.department_id "
        sql += " inner join MS_POSITION p on p.position_id=u.position_id "
        sql += " inner join MS_PREFIX_NAME pn on pn.prefix_name_id=u.prefix_name_id "

        Dim parm(5) As SqlParameter
        Dim wh As String = ""
        If name <> "" Then
            wh += " AND (u.First_Name  Like '%' + @_STAFF_NAME + '%' or u.last_name Like '%' + @_STAFF_NAME + '%') "
            parm(0) = SqlDB.SetText("@_STAFF_NAME", name)
        End If
        If sex <> "" Then
            wh += " AND u.gender = @_SEX"

            parm(1) = SqlDB.SetText("@_SEX", sex)
        End If

        If department <> 0 Then
            wh += " AND u.department_id = @_DEPARTMENT_ID"
            parm(2) = SqlDB.SetBigInt("@_DEPARTMENT_ID", department)
        End If

        If position <> 0 Then
            wh += " AND u.position_id = @_POSITION_ID"
            parm(3) = SqlDB.SetBigInt("@_POSITION_ID", position)
        End If

        If status <> "" Then
            wh += " AND u.active_status = '" & status & "'"
            parm(4) = SqlDB.SetText("@_STATUS", status)
        End If

        If wh.Trim <> "" Then
            sql += " where 1=1 " & wh
        End If
        sql += " order by u.First_Name, u.last_name"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql, parm)
        Return dt
    End Function

    Public Shared Function CheckDuplicateUser(UserLogin As String, UserID As Long) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim re As Boolean = False
        Dim lnq As New MsUserLinqDB
        re = lnq.ChkDuplicateByUSER_LOGIN(UserLogin, UserID, Nothing)
        If re = True Then
            ret.IsSuccess = False
            ret.ErrorMessage = "Duplication User Login " & UserLogin
        Else
            ret.IsSuccess = True
        End If
        Return ret
    End Function
    Public Shared Function CheckPasswordPolicy(pws As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        ret.IsSuccess = True
        Return ret
    End Function
    Public Shared Function GetUserData(UserID As Long) As MsUserLinqDB
        Dim ret As New MsUserLinqDB
        ret.GetDataByPK(UserID, Nothing)
        Return ret
    End Function
    Public Shared Function SaveUserData(UserName As String, UserID As Long, UserLogin As String, PssWd As String, DepartmentID As Long, PrefixNameID As String, FirstName As String, LastName As String, Gender As String, PositionID As Long, ActiveStatus As Boolean, UserRole As DataTable) As MsUserLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsUserLinqDB
        lnq.GetDataByPK(UserID, trans.Trans)
        If UserID = 0 Then
            lnq.USER_LOGIN = UserLogin
            lnq.USER_PASSWORD = PssWd
        End If
        lnq.DEPARTMENT_ID = DepartmentID
        lnq.PREFIX_NAME_ID = PrefixNameID
        lnq.FIRST_NAME = FirstName
        lnq.LAST_NAME = LastName
        lnq.GENDER = Gender
        lnq.POSITION_ID = PositionID
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.USER_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then

            'Save User Role
            Dim sql As String = "delete from MS_User_Role where user_id=@_USER_ID"
            Dim p(1) As SqlParameter
            p(0) = SqlDB.SetBigInt("@_USER_ID", lnq.USER_ID)

            ret = SqlDB.ExecuteNonQuery(sql, trans.Trans, p)
            If ret.IsSuccess = True Then
                'If UserRole.Rows.Count > 0 Then
                UserRole.DefaultView.RowFilter = "grant_role=true"
                If UserRole.DefaultView.Count > 0 Then
                    For Each dr As DataRowView In UserRole.DefaultView
                        Dim rLnq As New MsUserRoleLinqDB
                        rLnq.USER_ID = lnq.USER_ID
                        rLnq.ROLE_ID = dr("role_id")

                        ret = rLnq.InsertData(UserName, trans.Trans)
                        If ret.IsSuccess = False Then
                            Exit For
                        End If
                    Next
                End If
                UserRole.DefaultView.RowFilter = ""

                If ret.IsSuccess = True Then
                    trans.CommitTransaction()
                Else
                    trans.RollbackTransaction()
                    lnq.USER_ID = 0
                End If
            Else
                trans.RollbackTransaction()
                lnq.USER_ID = 0
            End If
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeleteUser(UserID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsUserLinqDB
        ret = lnq.DeleteByPK(UserID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
    Public Shared Function ChangePassword(UserID As Long, OldPssWd As String, NewPsswd As String) As ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsUserLinqDB
        lnq.GetDataByPK(UserID, trans.Trans)
        If lnq.USER_ID > 0 Then
            If SqlDB.EnCripPwd(OldPssWd) = lnq.USER_PASSWORD Then
                ret = CheckPasswordPolicy(NewPsswd)
                If ret.IsSuccess = True Then

                    lnq.USER_PASSWORD = SqlDB.EnCripPwd(NewPsswd)

                    ret = lnq.UpdateData(lnq.USER_LOGIN, trans.Trans)
                    If ret.IsSuccess = True Then
                        trans.CommitTransaction()
                    Else
                        trans.RollbackTransaction()
                    End If
                Else
                    trans.RollbackTransaction()
                End If
            Else
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = "Old password is incorrect"
            End If
        Else
            trans.RollbackTransaction()
            ret.IsSuccess = False
            ret.ErrorMessage = lnq.ErrorMessage
        End If

        lnq = Nothing

        Return ret
    End Function
    Public Shared Function ResetPassword(AdminUserName As String, UserID As Long, NewPsswd As String) As ExecuteDataInfo
        'UserID = คนที่จะถูก Reset Password
        Dim trans As New TransactionDB
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsUserLinqDB
        lnq.GetDataByPK(UserID, trans.Trans)
        If lnq.USER_ID > 0 Then
            lnq.USER_PASSWORD = SqlDB.EnCripPwd(NewPsswd)

            ret = lnq.UpdateData(AdminUserName, trans.Trans)
            If ret.IsSuccess = True Then
                trans.CommitTransaction()
            Else
                trans.RollbackTransaction()
            End If
        Else
            trans.RollbackTransaction()
            ret.IsSuccess = False
            ret.ErrorMessage = lnq.ErrorMessage
        End If
        lnq = Nothing

        Return ret
    End Function
    Public Shared Function UpdateUserStatus(LoginUsername As String, UserID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsUserLinqDB
        lnq.GetDataByPK(UserID, trans.Trans)
        If lnq.USER_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
    Public Shared Function GetList_UserRole(UserID As Long) As DataTable
        Dim sql As String = "select r.role_id, r.role_name, "
        sql += "   (select user_role_id from MS_User_Role where Role_ID=r.Role_ID and [user_id]=@_USER_ID) grant_role"
        sql += " From MS_Role r "
        sql += " where r.Active_Status='Y' "
        sql += " order by r.Role_Name"

        Dim p(1) As SqlParameter
        p(0) = SqlDB.SetBigInt("@_USER_ID", UserID)

        Dim dt As DataTable = SqlDB.ExecuteTable(sql, p)
        Return dt

    End Function
#End Region

#Region "Staff Data"
    Public Shared Function GetList_Staff(name As String, department As String, position As String, status As String) As DataTable
        Dim sql As String = "select s.Sequence_ID ,s.Staff_ID, s.department_id, d.department_name , s.position_id, p.position_name, s.prefix_name_id "
        sql += ", isnull(pn.prefix_name,'') + isnull(s.Fisrt_Name,'') + ' ' + isnull(s.last_name,'') full_name, s.active_status"
        sql += " from MS_Staff  s"
        sql += " left join MS_DEPARTMENT d on d.department_id=s.department_id "
        sql += " left join MS_POSITION p on p.position_id=s.position_id "
        sql += "left join MS_PREFIX_NAME pn on pn.prefix_name_id=s.prefix_name_id "

        'Dim parm(5) As SqlParameter
        'Dim wh As String = ""
        'If name <> "" Then
        '    wh += " AND (u.First_Name  Like '%' + @_STAFF_NAME + '%' or u.last_name Like '%' + @_STAFF_NAME + '%') "
        '    parm(0) = SqlDB.SetText("@_STAFF_NAME", name)
        'End If

        'If department <> 0 Then
        '    wh += " AND u.department_id = @_DEPARTMENT_ID"
        '    parm(2) = SqlDB.SetBigInt("@_DEPARTMENT_ID", department)
        'End If

        'If position <> 0 Then
        '    wh += " AND u.position_id = @_POSITION_ID"
        '    parm(3) = SqlDB.SetBigInt("@_POSITION_ID", position)
        'End If

        'If status <> "" Then
        '    wh += " AND u.active_status = '" & status & "'"
        '    parm(4) = SqlDB.SetText("@_STATUS", status)
        'End If

        'If wh.Trim <> "" Then
        '    sql += " where 1=1 " & wh
        'End If
        sql += " order by s.Fisrt_Name, s.last_name"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Return dt
    End Function

    Public Shared Sub GetListDDL_Staff(ddl As DropDownList)
        Dim sql As String = "Select Staff_ID,(Fisrt_Name +' '+ Last_Name) StaffName "
        sql += " from MS_Staff"
        sql += " where Active_Status='Y'"
        sql += " order by StaffName"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("Staff_ID") = 0
        dr("StaffName") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "Staff_ID"
        ddl.DataTextField = "StaffName"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub

    'Public Shared Function CheckDuplicateStaff(UserLogin As String, StaffID As Long) As ExecuteDataInfo
    '    Dim ret As New ExecuteDataInfo
    '    Dim re As Boolean = False
    '    Dim lnq As New MsStaffLinqDB
    '    re = lnq.ChkDuplicateByUSER_LOGIN(UserLogin, StaffID, Nothing)
    '    If re = True Then
    '        ret.IsSuccess = False
    '        ret.ErrorMessage = "Duplication Staff Login " & UserLogin
    '    Else
    '        ret.IsSuccess = True
    '    End If
    '    Return ret
    'End Function

    Public Shared Function GetUserStaff(StaffID As Long) As MsStaffLinqDB
        Dim ret As New MsStaffLinqDB
        ret.GetDataByPK(StaffID, Nothing)
        Return ret
    End Function
    Public Shared Function SaveUserStaff(UserName As String, SequenceID As Long, StaffID As String, DepartmentID As Long, PrefixNameID As String, FirstName As String, LastName As String, PositionID As Long, ActiveStatus As Boolean) As MsStaffLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsStaffLinqDB
        lnq.GetDataByPK(SequenceID, trans.Trans)
        lnq.STAFF_ID = StaffID
        lnq.DEPARTMENT_ID = DepartmentID
        lnq.PREFIX_NAME_ID = PrefixNameID
        lnq.FISRT_NAME = FirstName
        lnq.LAST_NAME = LastName
        lnq.POSITION_ID = PositionID
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.SEQUENCE_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeleteStaff(StaffID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsStaffLinqDB
        ret = lnq.DeleteByPK(StaffID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateStaffStatus(LoginUsername As String, StaffID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsStaffLinqDB
        lnq.GetDataByPK(StaffID, trans.Trans)
        If lnq.SEQUENCE_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "Prefix Name Data"
    Public Shared Function GetList_PrefixName(name As String, status As String) As DataTable
        Dim lnq As New MsPrefixNameLinqDB

        Dim parm(1) As SqlParameter
        Dim wh As String = ""

        wh += "1=1"

        If name <> "" Then
            wh += " AND prefix_name  Like '%' + @prefix_name + '%' "
            parm(0) = SqlDB.SetText("@prefix_name", name)
        End If

        If status <> "" Then
            wh += " AND Active_Status = '" & status & "'"
        End If

        Dim dt As DataTable = lnq.GetDataList(wh, "prefix_name", Nothing, parm)
        lnq = Nothing
        Return dt
    End Function
    Public Shared Sub GetListDDL_PrefixName(ddl As DropDownList)
        Dim sql As String = "select prefix_name_id, prefix_name "
        sql += " from MS_PREFIX_NAME"
        sql += " where Active_Status='Y'"
        sql += " order by prefix_name"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("prefix_name_id") = 0
        dr("prefix_name") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "prefix_name_id"
        ddl.DataTextField = "prefix_name"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub
    Public Shared Function CheckDuplicatePrefixName(PrefixNameID As Long, PrefixName As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsPrefixNameLinqDB
        Dim re As Boolean = lnq.ChkDuplicateByPREFIX_NAME(PrefixName, PrefixNameID, Nothing)
        If re = True Then
            ret.IsSuccess = False
            ret.ErrorMessage = "Duplicate Prefix name " & PrefixName
        Else
            ret.IsSuccess = True
        End If

        Return ret
    End Function
    Public Shared Function GetPrefixNameData(PrefixNameID As Long) As MsPrefixNameLinqDB
        Dim ret As New MsPrefixNameLinqDB
        ret.GetDataByPK(PrefixNameID, Nothing)
        Return ret
    End Function
    Public Shared Function SavePrefixName(UserName As String, PrefixNameID As Long, PrefixName As String, ActiveStatus As Boolean) As MsPrefixNameLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsPrefixNameLinqDB
        lnq.GetDataByPK(PrefixNameID, trans.Trans)
        lnq.PREFIX_NAME = PrefixName
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.PREFIX_NAME_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeletePrefixName(PrefixNameID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsPrefixNameLinqDB
        ret = lnq.DeleteByPK(PrefixNameID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdatePreFixstatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsPrefixNameLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.PREFIX_NAME_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "Department"
    Public Shared Function GetList_Department(name As String, status As String) As DataTable
        Dim lnq As New MsDepartmentLinqDB

        Dim parm(1) As SqlParameter
        Dim wh As String = ""

        wh += "1=1"

        If name <> "" Then
            wh += " AND department_name  Like '%' + @department_name + '%' "
            parm(0) = SqlDB.SetText("@department_name", name)
        End If

        If status <> "" Then
            wh += " AND Active_Status = '" & status & "'"
        End If

        Dim dt As DataTable = lnq.GetDataList(wh, "department_name", Nothing, parm)
        lnq = Nothing
        Return dt
    End Function
    Public Shared Sub GetListDDL_Department(ddl As DropDownList)
        Dim sql As String = "select department_id, department_name "
        sql += " from MS_DEPARTMENT"
        sql += " where Active_Status='Y'"
        sql += " order by department_name"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("department_id") = 0
        dr("department_name") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "department_id"
        ddl.DataTextField = "department_name"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub
    Public Shared Function CheckDuplicateDepartment(DepartmentID As Long, DepartmentName As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsDepartmentLinqDB
        Dim re As Boolean = lnq.ChkDuplicateByDEPARTMENT_NAME(DepartmentName, DepartmentID, Nothing)
        If re = True Then
            ret.IsSuccess = False
            ret.ErrorMessage = "Duplicate Department " & DepartmentName
        Else
            ret.IsSuccess = True
        End If

        Return ret
    End Function
    Public Shared Function GetDepartmentData(DepartmentID As Long) As MsDepartmentLinqDB
        Dim ret As New MsDepartmentLinqDB
        ret.GetDataByPK(DepartmentID, Nothing)
        Return ret
    End Function
    Public Shared Function SaveDepartment(UserName As String, DepartmentID As Long, DepartmentName As String, ActiveStatus As Boolean) As MsDepartmentLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsDepartmentLinqDB
        lnq.GetDataByPK(DepartmentID, trans.Trans)
        lnq.DEPARTMENT_NAME = DepartmentName
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.DEPARTMENT_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeleteDepartment(DepartmentID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsDepartmentLinqDB
        ret = lnq.DeleteByPK(DepartmentID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateDepartmentstatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsDepartmentLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.DEPARTMENT_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "Position"
    Public Shared Function GetList_Position(name As String, status As String) As DataTable
        Dim lnq As New MsPositionLinqDB

        Dim parm(1) As SqlParameter
        Dim wh As String = ""

        wh += "1=1"

        If name <> "" Then
            wh += " AND position_name  Like '%' + @position_name + '%' "
            parm(0) = SqlDB.SetText("@position_name", name)
        End If

        If status <> "" Then
            wh += " AND Active_Status = '" & status & "'"
        End If

        Dim dt As DataTable = lnq.GetDataList(wh, "position_name", Nothing, parm)
        lnq = Nothing
        Return dt
    End Function
    Public Shared Sub GetListDDL_Position(ddl As DropDownList)
        Dim sql As String = "select position_id, position_name "
        sql += " from MS_POSITION"
        sql += " where Active_Status='Y'"
        sql += " order by position_name"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("position_id") = 0
        dr("position_name") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "position_id"
        ddl.DataTextField = "position_name"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub
    Public Shared Function CheckDuplicatePosition(PositionID As Long, PositionName As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsPositionLinqDB
        Dim re As Boolean = lnq.ChkDuplicateByPOSITION_NAME(PositionName, PositionID, Nothing)
        If re = True Then
            ret.IsSuccess = False
            ret.ErrorMessage = "Duplicate Position " & PositionName
        Else
            ret.IsSuccess = True
        End If

        Return ret
    End Function
    Public Shared Function GetPositionData(PositionID As Long) As MsPositionLinqDB
        Dim ret As New MsPositionLinqDB
        ret.GetDataByPK(PositionID, Nothing)
        Return ret
    End Function
    Public Shared Function SavePosition(UserName As String, PositionID As Long, PositionName As String, ActiveStatus As Boolean) As MsPositionLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsPositionLinqDB
        lnq.GetDataByPK(PositionID, trans.Trans)
        lnq.POSITION_NAME = PositionName
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.POSITION_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeletePosition(PositionID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsPositionLinqDB
        ret = lnq.DeleteByPK(PositionID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdatePositionstatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsPositionLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.POSITION_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "Customer"
    Public Shared Function GetList_Customer(ID As String, CustomerType As String, Name As String, status As String) As DataTable

        Dim sql As String = "Select C.Customer_ID ,C.Customer_Code ,C.Prefix_ID ,isnull(C.Customer_TName,'') + ' / ' +isnull(C.Customer_EName,'')  full_name"
        sql += " ,C.Customer_Alias ,CT.Customer_Type "
        sql += " ,C.Customer_Tax_No ,C.Customer_Tax_No ,C.Customer_Mobile,C.Customer_Phone,C.Customer_Fax,C.Customer_Email,C.Active_Status"
        sql += " from MS_Customer C"
        sql += " inner join MS_Customer_Type CT On C.Customer_Type_ID = CT.Customer_Type_ID where 1=1"

        Dim parm(1) As SqlParameter

        If ID <> "" Then
            sql += " AND C.Customer_Code LIKE '%'+ @ID +'%'"
            parm(0) = SqlDB.SetText("@ID", ID)
        End If

        If CustomerType = "0" Then CustomerType = ""
        If CustomerType <> "" Then
            sql += " AND CT.Customer_Type_ID = '" & CustomerType & "'"
        End If

        If Name <> "" Then
            sql += " AND (C.Customer_TName LIKE '%'+ @NAME +'%' OR C.Customer_EName LIKE '%'+ @NAME +'%') "
            parm(1) = SqlDB.SetText("@NAME", Name)
        End If

        If status <> "" Then
            sql += " AND C.Active_Status = '" & status & "'"
        End If

        sql += " order by C.Customer_TName, C.Customer_EName"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql, parm)
        Return dt
    End Function

    Public Shared Function CheckDuplicateCustomerName(CustomerID As Long, CustomerName As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsCustomerLinqDB
        Dim re As Boolean = lnq.ChkDuplicateByCUSTOMER_TNAME(CustomerName, CustomerID, Nothing)
        If re = True Then
            ret.IsSuccess = False
            ret.ErrorMessage = "Duplicate Customer  " & CustomerName
        Else
            ret.IsSuccess = True
        End If

        Return ret
    End Function

    Public Shared Sub GetListDDL_Customet(ddl As DropDownList)
        Dim sql As String = "Select Customer_ID,(Customer_TName +'/'+ Customer_EName) CusName"
        sql += " from MS_Customer"
        sql += " where Active_Status='Y'"
        sql += " order by CusName"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("Customer_ID") = 0
        dr("CusName") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "Customer_ID"
        ddl.DataTextField = "CusName"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub

    Public Shared Function GetCustomerData(CustomerID As Long) As MsCustomerLinqDB
        Dim ret As New MsCustomerLinqDB
        ret.GetDataByPK(CustomerID, Nothing)
        Return ret
    End Function

    Public Shared Function DeleteCustomer(CustomerID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsCustomerLinqDB

        Dim p_ab(1) As SqlParameter
        p_ab(0) = SqlDB.SetBigInt("@CUSTOMER_ID", CustomerID)
        ret = SqlDB.ExecuteNonQuery("DELETE FROM MS_Customer_Contact WHERE Customer_ID=@CUSTOMER_ID", trans.Trans, p_ab)
        ret = SqlDB.ExecuteNonQuery("DELETE FROM MS_Customer_Address WHERE Customer_ID=@CUSTOMER_ID", trans.Trans, p_ab)

        ret = lnq.DeleteByPK(CustomerID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function SaveCustomerData(UserName As String, CustomerID As Long, CustomerCode As String, CusTName As String, CusEName As String, CusAlias As String, CusTaxNo As String, CustomerTypeID As Long, CusPhone As String, CusMobile As String, CusFax As String, CusEmail As String, ActiveStatus As Boolean) As MsCustomerLinqDB
        Dim trans As New TransactionDB
        Dim lnqCustomer As New MsCustomerLinqDB

        lnqCustomer.GetDataByPK(CustomerID, trans.Trans)
        lnqCustomer.CUSTOMER_CODE = CustomerCode
        lnqCustomer.CUSTOMER_TNAME = CusTName
        lnqCustomer.CUSTOMER_ENAME = CusEName
        lnqCustomer.CUSTOMER_ALIAS = CusAlias
        lnqCustomer.CUSTOMER_TAX_NO = CusTaxNo
        lnqCustomer.CUSTOMER_TYPE_ID = CustomerTypeID
        lnqCustomer.CUSTOMER_PHONE = CusPhone
        lnqCustomer.CUSTOMER_MOBILE = CusMobile
        lnqCustomer.CUSTOMER_FAX = CusFax
        lnqCustomer.CUSTOMER_EMAIL = CusEmail
        lnqCustomer.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnqCustomer.CUSTOMER_ID > 0 Then
            ret = lnqCustomer.UpdateData(UserName, trans.Trans)
        Else
            ret = lnqCustomer.InsertData(UserName, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnqCustomer
    End Function

    Public Shared Function UpdateCustomerstatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsCustomerLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.CUSTOMER_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "Customer_Contact"
    Public Shared Function GetList_Customer_Contact(wh As String, parm As SqlParameter(), Customer_ID As String) As DataTable
        Dim sql As String = "Select C.Customer_Contact_ID ,C.Customer_ID ,c.Prefix_ID,isnull(p.prefix_name ,'') prefix_name, C.First_Name , C.Last_Name "
        sql += " ,isnull(C.MobileNo,'') MobileNo, isnull(C.TelephoneNo,'') TelephoneNo,isnull(C.FaxNo,'') FaxNo,isnull(C.Email,'') Email ,C.Active_Status "
        sql += " from MS_Customer_Contact C"
        sql += " left join MS_PREFIX_NAME  P On c.Prefix_ID  = P.prefix_name_id "

        wh = "AND C.Customer_ID = '" & Customer_ID & "'"

        If wh.Trim <> "" Then
            sql += " where 1=1" & wh
        End If

        sql += " Order By C.First_Name,C.Last_Name"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql, parm)
        Return dt
    End Function

    Public Shared Function GetCustomer_ContactData(Customer_ContactID As Long) As MsCustomerContactLinqDB
        Dim ret As New MsCustomerContactLinqDB
        ret.GetDataByPK(Customer_ContactID, Nothing)
        Return ret
    End Function

    Public Shared Function SaveCustomer_ContactData(UserName As String, CustomerID As String, DtContact As DataTable) As MsCustomerContactLinqDB
        Dim trans As New TransactionDB
        Dim lnqContact As New MsCustomerContactLinqDB
        Dim ret As New ExecuteDataInfo

        Dim p_ab(1) As SqlParameter
        p_ab(0) = SqlDB.SetBigInt("@CUSTOMER_ID", CustomerID)
        SqlDB.ExecuteNonQuery("DELETE FROM MS_Customer_Contact WHERE Customer_ID=@CUSTOMER_ID", trans.Trans, p_ab)
        ret.IsSuccess = True

        If Not DtContact Is Nothing Then
            For i As Integer = 0 To DtContact.Rows.Count - 1
                Dim Customer_Contact_ID As String = DtContact.Rows(i)("Customer_Contact_ID").ToString()
                'Dim Customer_ID As String = DtContact.Rows(i)("Customer_ID").ToString()
                Dim Prefix_ID As String = DtContact.Rows(i)("Prefix_ID").ToString()
                Dim prefix_name As String = DtContact.Rows(i)("prefix_name").ToString()
                Dim First_Name As String = DtContact.Rows(i)("First_Name").ToString()
                Dim Last_Name As String = DtContact.Rows(i)("Last_Name").ToString()
                Dim MobileNo As String = DtContact.Rows(i)("MobileNo").ToString()
                Dim TelephoneNo As String = DtContact.Rows(i)("TelephoneNo").ToString()
                Dim FaxNo As String = DtContact.Rows(i)("FaxNo").ToString()
                Dim Email As String = DtContact.Rows(i)("Email").ToString()
                Dim Active_Status As String = DtContact.Rows(i)("Active_Status").ToString()

                If Customer_Contact_ID <> "" AndAlso CustomerID <> "" Then

                    With lnqContact
                        .GetDataByPK(Customer_Contact_ID, trans.Trans)
                        .CUSTOMER_ID = CustomerID
                        .PREFIX_ID = Prefix_ID
                        .FIRST_NAME = First_Name
                        .LAST_NAME = Last_Name
                        .MOBILENO = MobileNo
                        .TELEPHONENO = TelephoneNo
                        .FAXNO = FaxNo
                        .EMAIL = Email
                        .ACTIVE_STATUS = Active_Status

                        ret.IsSuccess = False
                        ret = lnqContact.InsertData(UserName, trans.Trans)
                        If ret.IsSuccess = False Then
                            Exit For
                        End If
                    End With
                End If
            Next

            If ret.IsSuccess = True Then
                trans.CommitTransaction()
            Else
                trans.RollbackTransaction()
            End If
        End If

        Return lnqContact

    End Function

    Public Shared Function DeleteCustomer_Contact(Customer_ContactID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsCustomerContactLinqDB
        ret = lnq.DeleteByPK(Customer_ContactID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateCustomerContactstatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsCustomerContactLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.CUSTOMER_CONTACT_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "Customer_Address"
    Public Shared Function GetList_Customer_Address(wh As String, parm As SqlParameter(), Customer_ID As String) As DataTable
        Dim sql As String = "Select Customer_Address_ID ,Customer_ID,House_No,Building_Name,moo,soi_name,road_name,A.Tumbon_ID,A.District_ID,A.Province_ID,A.Country_ID"
        sql += " ,'บ้านเลขที่ ' + isnull(House_No ,'- ')  + ' อาคารที่ตั้ง ' + isnull(Building_Name,'- ') "
        sql += " + ' หมู่ ' +isnull(moo,'- ') + ' ซอย ' +isnull(soi_name,'- ') + ' ถนน ' + isnull(road_name,'- ')"
        sql += " + ' ตำบล/แขวง ' + isnull(AT.Tumbon_Name  ,'- ') + ' อำเภอ/เขต ' + isnull(AD.District_Name  ,'- ')"
        sql += " + ' จังหวัด ' +  isnull(AP.Province_Name ,'- ') + ' ประเทศ ' + isnull(Ac.Country_Name ,'- ') "
        sql += " + ' รหัสไปรษณีย์ ' +isnull((select CAST(Postcode as varchar(max))),'') Address ,Postcode,A.Active_Status  "
        sql += " from MS_Customer_Address   A "
        sql += " left join MS_Address_Tumbon  AT on a.Tumbon_ID = AT.Tumbon_ID  "
        sql += " left join MS_Address_District  AD on a.District_ID = Ad.District_ID "
        sql += " left join MS_Address_Province  AP on a.Province_ID = AP.Province_ID  "
        sql += " left join MS_Address_Country AC on a.Country_ID = Ac.Country_ID "

        wh = " AND Customer_ID = '" & Customer_ID & "'"

        If wh.Trim <> "" Then
            sql += " where 1=1" & wh
        End If
        Dim dt As DataTable = SqlDB.ExecuteTable(sql, parm)
        Return dt
    End Function

    Public Shared Function GetCustomerr_AddressData(Customer_AddressID As Long) As MsCustomerAddressLinqDB
        Dim ret As New MsCustomerAddressLinqDB
        ret.GetDataByPK(Customer_AddressID, Nothing)
        Return ret
    End Function
    Public Shared Function SaveCustomer_AddressData(UserName As String, CustomerID As String, DtAddress As DataTable) As MsCustomerAddressLinqDB
        Dim trans As New TransactionDB
        Dim lnqAddress As New MsCustomerAddressLinqDB
        Dim ret As New ExecuteDataInfo

        Dim p_ab(1) As SqlParameter
        p_ab(0) = SqlDB.SetBigInt("@CUSTOMER_ID", CustomerID)
        SqlDB.ExecuteNonQuery("DELETE FROM MS_Customer_Address WHERE Customer_ID=@CUSTOMER_ID", trans.Trans, p_ab)
        ret.IsSuccess = True

        If Not DtAddress Is Nothing Then

            For i As Integer = 0 To DtAddress.Rows.Count - 1
                Dim Customer_Address_ID As String = DtAddress.Rows(i)("Customer_Address_ID").ToString()
                'Dim Customer_ID As String = DtAddress.Rows(i)("Customer_ID").ToString()
                Dim House_No As String = DtAddress.Rows(i)("House_No").ToString()
                Dim Building_Name As String = DtAddress.Rows(i)("Building_Name").ToString()
                Dim moo As String = DtAddress.Rows(i)("moo").ToString()
                Dim soi_name As String = DtAddress.Rows(i)("soi_name").ToString()
                Dim road_name As String = DtAddress.Rows(i)("road_name").ToString()
                Dim Tumbon_ID As String = DtAddress.Rows(i)("Tumbon_ID").ToString()
                Dim District_ID As String = DtAddress.Rows(i)("District_ID").ToString()
                Dim Province_ID As String = DtAddress.Rows(i)("Province_ID").ToString()
                Dim Country_ID As String = DtAddress.Rows(i)("Country_ID").ToString()
                Dim Postcode As String = DtAddress.Rows(i)("Postcode").ToString()
                Dim Active_Status As String = DtAddress.Rows(i)("Active_Status").ToString()


                If Customer_Address_ID <> "" AndAlso CustomerID <> "" Then

                    With lnqAddress
                        .GetDataByPK(Customer_Address_ID, trans.Trans)
                        .CUSTOMER_ID = CustomerID
                        .HOUSE_NO = House_No
                        .BUILDING_NAME = Building_Name
                        .MOO = moo
                        .SOI_NAME = soi_name
                        .ROAD_NAME = road_name
                        .TUMBON_ID = Tumbon_ID
                        .DISTRICT_ID = District_ID
                        .PROVINCE_ID = Province_ID
                        .COUNTRY_ID = Country_ID
                        .POSTCODE = Postcode
                        .ACTIVE_STATUS = Active_Status

                        ret.IsSuccess = False
                        ret = lnqAddress.InsertData(UserName, trans.Trans)
                        If ret.IsSuccess = False Then
                            Exit For
                        End If
                    End With
                End If
            Next

            If ret.IsSuccess = True Then
                trans.CommitTransaction()
            Else
                trans.RollbackTransaction()
            End If
        End If

        Return lnqAddress

    End Function

    Public Shared Function DeleteCustomer_Address(Customer_AddressID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsCustomerAddressLinqDB
        ret = lnq.DeleteByPK(Customer_AddressID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateCustomerAddressstatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsCustomerAddressLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.CUSTOMER_ADDRESS_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "Customer Type"
    Public Shared Function GetList_CustomerType(name As String, status As String) As DataTable
        Dim lnq As New MsCustomerTypeLinqDB

        Dim parm(1) As SqlParameter
        Dim wh As String = ""

        wh += "1=1"

        If name <> "" Then
            wh += " AND Customer_Type  Like '%' + @Customer_Type + '%' "
            parm(0) = SqlDB.SetText("@Customer_Type", name)
        End If

        If status <> "" Then
            wh += " AND Active_Status = '" & status & "'"
        End If

        Dim dt As DataTable = lnq.GetDataList(wh, "Customer_Type", Nothing, parm)
        lnq = Nothing
        Return dt
    End Function
    Public Shared Sub GetListDDL_CustomerType(ddl As DropDownList)
        Dim sql As String = "select Customer_Type_ID, Customer_Type "
        sql += " from MS_Customer_Type"
        sql += " where Active_Status='Y'"
        sql += " order by Customer_Type"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("Customer_Type_ID") = 0
        dr("Customer_Type") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "Customer_Type_ID"
        ddl.DataTextField = "Customer_Type"
        ddl.DataSource = dt
        ddl.DataBind()
    End Sub
    Public Shared Function CheckDuplicateCustomerType(CustomerTypeID As Long, CustomerTypeName As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsCustomerTypeLinqDB
        Dim re As Boolean = lnq.ChkDuplicateByCUSTOMER_TYPE(CustomerTypeName, CustomerTypeID, Nothing)
        If re = True Then
            ret.IsSuccess = False
            ret.ErrorMessage = "Duplicate Customer Type " & CustomerTypeName
        Else
            ret.IsSuccess = True
        End If

        Return ret
    End Function
    Public Shared Function GetCustomerTypeData(CustomerTypeID As Long) As MsCustomerTypeLinqDB
        Dim ret As New MsCustomerTypeLinqDB
        ret.GetDataByPK(CustomerTypeID, Nothing)
        Return ret
    End Function
    Public Shared Function SaveCustomerType(UserName As String, CustomerTypeID As Long, CustomerTypeName As String, ActiveStatus As Boolean) As MsCustomerTypeLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsCustomerTypeLinqDB
        lnq.GetDataByPK(CustomerTypeID, trans.Trans)
        lnq.CUSTOMER_TYPE = CustomerTypeName
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.CUSTOMER_TYPE_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeleteCustomerType(CustomerTypeID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsCustomerTypeLinqDB
        ret = lnq.DeleteByPK(CustomerTypeID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateCustomerTypestatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsCustomerTypeLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.CUSTOMER_TYPE_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "Supplier"
    Public Shared Function GetList_Supplier(ID As String, SupplireType As String, Name As String, status As String) As DataTable

        Dim sql As String = "Select C.Supplier_ID ,C.Supplier_Code ,C.Prefix_ID ,isnull(C.Supplier_TName,'') + ' / ' +isnull(C.Supplier_EName,'')  full_name"
        sql += " ,C.Supplier_Alias ,CT.Supplier_Type_Name "
        sql += " ,C.Supplier_Tax_No ,C.Supplier_Tax_No ,C.Supplier_Mobile,C.Supplier_Phone,C.Supplier_Fax,C.Supplier_Email,C.Active_Status"
        sql += " from MS_Supplier C"
        sql += " inner join MS_Supplier_Type CT On C.Supplier_Type_ID = CT.Supplier_Type_ID"

        Dim parm(1) As SqlParameter

        If ID <> "" Then
            sql += " AND C.Supplier_Code LIKE '%'+ @ID +'%'"
            parm(0) = SqlDB.SetText("@ID", ID)
        End If

        If SupplireType = "0" Then SupplireType = ""
        If SupplireType <> "" Then
            sql += " AND CT.Supplier_Type_ID = '" & SupplireType & "'"
        End If

        If Name <> "" Then
            sql += " AND (C.Supplier_TName LIKE '%'+ @NAME +'%' OR C.Supplier_EName LIKE '%'+ @NAME +'%') "
            parm(1) = SqlDB.SetText("@NAME", Name)
        End If

        If status <> "" Then
            sql += " AND C.Active_Status = '" & status & "'"
        End If

        sql += " order by C.Supplier_TName, C.Supplier_EName"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql, parm)
        Return dt
    End Function

    Public Shared Function CheckDuplicateSupplierName(SupplierID As Long, SupplierName As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsSupplierLinqDB
        Dim re As Boolean = lnq.ChkDuplicateBySUPPLIER_TNAME(SupplierName, SupplierID, Nothing)
        If re = True Then
            ret.IsSuccess = False
            ret.ErrorMessage = "Duplicate Supplier  " & SupplierName
        Else
            ret.IsSuccess = True
        End If

        Return ret
    End Function
    Public Shared Function GetSupplierData(SupplierID As Long) As MsSupplierLinqDB
        Dim ret As New MsSupplierLinqDB
        ret.GetDataByPK(SupplierID, Nothing)
        Return ret
    End Function

    Public Shared Function DeleteSupplier(SupplierID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsSupplierLinqDB

        Dim p_ab(1) As SqlParameter
        p_ab(0) = SqlDB.SetBigInt("@Supplier_ID", SupplierID)
        ret = SqlDB.ExecuteNonQuery("DELETE FROM MS_Supplier_Contact WHERE Supplier_ID=@Supplier_ID", trans.Trans, p_ab)
        ret = SqlDB.ExecuteNonQuery("DELETE FROM MS_Supplier_Address WHERE Supplier_ID=@Supplier_ID", trans.Trans, p_ab)

        ret = lnq.DeleteByPK(SupplierID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function SaveSupplierData(UserName As String, SupplierID As Long, SupplierCode As String, CusTName As String, CusEName As String, CusAlias As String, CusTaxNo As String, SupplierTypeID As Long, CusPhone As String, CusMobile As String, CusFax As String, CusEmail As String, ActiveStatus As Boolean) As MsSupplierLinqDB
        Dim trans As New TransactionDB
        Dim lnqSupplier As New MsSupplierLinqDB

        lnqSupplier.GetDataByPK(SupplierID, trans.Trans)
        lnqSupplier.SUPPLIER_CODE = SupplierCode
        lnqSupplier.SUPPLIER_TNAME = CusTName
        lnqSupplier.SUPPLIER_ENAME = CusEName
        lnqSupplier.SUPPLIER_ALIAS = CusAlias
        lnqSupplier.SUPPLIER_TAX_NO = CusTaxNo
        lnqSupplier.SUPPLIER_TYPE_ID = SupplierTypeID
        lnqSupplier.SUPPLIER_PHONE = CusPhone
        lnqSupplier.SUPPLIER_MOBILE = CusMobile
        lnqSupplier.SUPPLIER_FAX = CusFax
        lnqSupplier.SUPPLIER_EMAIL = CusEmail
        lnqSupplier.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnqSupplier.SUPPLIER_ID > 0 Then
            ret = lnqSupplier.UpdateData(UserName, trans.Trans)
        Else
            ret = lnqSupplier.InsertData(UserName, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnqSupplier
    End Function

    Public Shared Function UpdateSupplierstatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsSupplierLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.SUPPLIER_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "Supplier_Contact"
    Public Shared Function GetList_Supplier_Contact(wh As String, parm As SqlParameter(), Supplier_ID As String) As DataTable
        Dim sql As String = "Select C.Supplier_Contact_ID ,C.Supplier_ID ,c.Prefix_ID,isnull(p.prefix_name ,'') prefix_name, C.First_Name , C.Last_Name "
        sql += " ,isnull(C.MobileNo,'') MobileNo, isnull(C.TelephoneNo,'') TelephoneNo,isnull(C.FaxNo,'') FaxNo,isnull(C.Email,'') Email ,C.Active_Status "
        sql += " from MS_Supplier_Contact C"
        sql += " left join MS_PREFIX_NAME  P On c.Prefix_ID  = P.prefix_name_id "

        wh = "AND C.Supplier_ID = '" & Supplier_ID & "'"

        If wh.Trim <> "" Then
            sql += " where 1=1" & wh
        End If

        sql += " Order By C.First_Name,C.Last_Name"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql, parm)
        Return dt
    End Function

    Public Shared Function GetSupplier_ContactData(Supplier_ContactID As Long) As MsSupplierContactLinqDB
        Dim ret As New MsSupplierContactLinqDB
        ret.GetDataByPK(Supplier_ContactID, Nothing)
        Return ret
    End Function

    Public Shared Function SaveSupplier_ContactData(UserName As String, SupplierID As String, DtContact As DataTable) As MsSupplierContactLinqDB
        Dim trans As New TransactionDB
        Dim lnqContact As New MsSupplierContactLinqDB
        Dim ret As New ExecuteDataInfo

        Dim p_ab(1) As SqlParameter
        p_ab(0) = SqlDB.SetBigInt("@Supplier_ID", SupplierID)
        SqlDB.ExecuteNonQuery("DELETE FROM MS_Supplier_Contact WHERE Supplier_ID=@Supplier_ID", trans.Trans, p_ab)
        ret.IsSuccess = True

        If Not DtContact Is Nothing Then

            For i As Integer = 0 To DtContact.Rows.Count - 1
                Dim Supplier_Contact_ID As String = DtContact.Rows(i)("Supplier_Contact_ID").ToString()
                'Dim Supplier_ID As String = DtContact.Rows(i)("Supplier_ID").ToString()
                Dim Prefix_ID As String = DtContact.Rows(i)("Prefix_ID").ToString()
                Dim prefix_name As String = DtContact.Rows(i)("prefix_name").ToString()
                Dim First_Name As String = DtContact.Rows(i)("First_Name").ToString()
                Dim Last_Name As String = DtContact.Rows(i)("Last_Name").ToString()
                Dim MobileNo As String = DtContact.Rows(i)("MobileNo").ToString()
                Dim TelephoneNo As String = DtContact.Rows(i)("TelephoneNo").ToString()
                Dim FaxNo As String = DtContact.Rows(i)("FaxNo").ToString()
                Dim Email As String = DtContact.Rows(i)("Email").ToString()
                Dim Active_Status As String = DtContact.Rows(i)("Active_Status").ToString()

                If Supplier_Contact_ID <> "" AndAlso SupplierID <> "" Then

                    With lnqContact
                        .GetDataByPK(Supplier_Contact_ID, trans.Trans)
                        .SUPPLIER_ID = SupplierID
                        .PREFIX_ID = Prefix_ID
                        .FIRST_NAME = First_Name
                        .LAST_NAME = Last_Name
                        .MOBILENO = MobileNo
                        .TELEPHONENO = TelephoneNo
                        .FAXNO = FaxNo
                        .EMAIL = Email
                        .ACTIVE_STATUS = Active_Status

                        ret.IsSuccess = False
                        ret = lnqContact.InsertData(UserName, trans.Trans)
                        If ret.IsSuccess = False Then
                            Exit For
                        End If
                    End With
                End If
            Next

            If ret.IsSuccess = True Then
                trans.CommitTransaction()
            Else
                trans.RollbackTransaction()
            End If
        End If

        Return lnqContact

    End Function

    Public Shared Function DeleteSupplier_Contact(Supplier_ContactID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsSupplierContactLinqDB
        ret = lnq.DeleteByPK(Supplier_ContactID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateSupplierContactstatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsSupplierContactLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.SUPPLIER_CONTACT_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function



#End Region

#Region "Supplier_Address"
    Public Shared Function GetList_Supplier_Address(wh As String, parm As SqlParameter(), Supplier_ID As String) As DataTable
        Dim sql As String = "Select Supplier_Address_ID ,Supplier_ID,House_No,Building_Name,moo,soi_name,road_name,A.Tumbon_ID,A.District_ID,A.Province_ID,A.Country_ID"
        sql += " ,'บ้านเลขที่ ' + isnull(House_No ,'- ')  + ' อาคารที่ตั้ง ' + isnull(Building_Name,'- ') "
        sql += " + ' หมู่ ' +isnull(moo,'- ')  + ' ถนน ' + isnull(soi_name,'- ')"
        sql += " + ' ตำบล/แขวง ' + isnull(AT.Tumbon_Name  ,'- ') + ' อำเภอ/เขต ' + isnull(AD.District_Name  ,'- ')"
        sql += " + ' จังหวัด ' +  isnull(AP.Province_Name ,'- ') + ' ประเทศ ' + isnull(Ac.Country_Name ,'- ') "
        sql += " + ' รหัสไปรษณีย์ ' +isnull((select CAST(Postcode as varchar(max))),'') Address ,Postcode,A.Active_Status  "
        sql += " from MS_Supplier_Address   A "
        sql += " left join MS_Address_Tumbon  AT on a.Tumbon_ID = AT.Tumbon_ID  "
        sql += " left join MS_Address_District  AD on a.District_ID = Ad.District_ID "
        sql += " left join MS_Address_Province  AP on a.Province_ID = AP.Province_ID  "
        sql += " left join MS_Address_Country AC on a.Country_ID = Ac.Country_ID "

        wh = " AND Supplier_ID = '" & Supplier_ID & "'"

        If wh.Trim <> "" Then
            sql += " where 1=1" & wh
        End If
        Dim dt As DataTable = SqlDB.ExecuteTable(sql, parm)
        Return dt
    End Function

    Public Shared Function GetSupplierr_AddressData(Supplier_AddressID As Long) As MsSupplierAddressLinqDB
        Dim ret As New MsSupplierAddressLinqDB
        ret.GetDataByPK(Supplier_AddressID, Nothing)
        Return ret
    End Function
    Public Shared Function SaveSupplier_AddressData(UserName As String, SupplierID As String, DtAddress As DataTable) As MsSupplierAddressLinqDB
        Dim trans As New TransactionDB
        Dim lnqAddress As New MsSupplierAddressLinqDB
        Dim ret As New ExecuteDataInfo

        Dim p_ab(1) As SqlParameter
        p_ab(0) = SqlDB.SetBigInt("@Supplier_ID", SupplierID)
        SqlDB.ExecuteNonQuery("DELETE FROM MS_Supplier_Address WHERE Supplier_ID=@Supplier_ID", trans.Trans, p_ab)
        ret.IsSuccess = True

        If Not DtAddress Is Nothing Then

            For i As Integer = 0 To DtAddress.Rows.Count - 1
                Dim Supplier_Address_ID As String = DtAddress.Rows(i)("Supplier_Address_ID").ToString()
                'Dim Supplier_ID As String = DtAddress.Rows(i)("Supplier_ID").ToString()
                Dim House_No As String = DtAddress.Rows(i)("House_No").ToString()
                Dim Building_Name As String = DtAddress.Rows(i)("Building_Name").ToString()
                Dim moo As String = DtAddress.Rows(i)("moo").ToString()
                Dim soi_name As String = DtAddress.Rows(i)("soi_name").ToString()
                Dim road_name As String = DtAddress.Rows(i)("road_name").ToString()
                Dim Tumbon_ID As String = DtAddress.Rows(i)("Tumbon_ID").ToString()
                Dim District_ID As String = DtAddress.Rows(i)("District_ID").ToString()
                Dim Province_ID As String = DtAddress.Rows(i)("Province_ID").ToString()
                Dim Country_ID As String = DtAddress.Rows(i)("Country_ID").ToString()
                Dim Postcode As String = DtAddress.Rows(i)("Postcode").ToString()
                Dim Active_Status As String = DtAddress.Rows(i)("Active_Status").ToString()


                If Supplier_Address_ID <> "" AndAlso SupplierID <> "" Then

                    With lnqAddress
                        .GetDataByPK(Supplier_Address_ID, trans.Trans)
                        .SUPPLIER_ID = SupplierID
                        .HOUSE_NO = House_No
                        .BUILDING_NAME = Building_Name
                        .MOO = moo
                        .SOI_NAME = soi_name
                        .ROAD_NAME = road_name
                        .TUMBON_ID = Tumbon_ID
                        .DISTRICT_ID = District_ID
                        .PROVINCE_ID = Province_ID
                        .COUNTRY_ID = Country_ID
                        .POSTCODE = Postcode
                        .ACTIVE_STATUS = Active_Status

                        ret.IsSuccess = False
                        ret = lnqAddress.InsertData(UserName, trans.Trans)
                        If ret.IsSuccess = False Then
                            Exit For
                        End If
                    End With
                End If
            Next

            If ret.IsSuccess = True Then
                trans.CommitTransaction()
            Else
                trans.RollbackTransaction()
            End If
        End If

        Return lnqAddress

    End Function

    Public Shared Function DeleteSupplier_Address(Supplier_AddressID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsSupplierAddressLinqDB
        ret = lnq.DeleteByPK(Supplier_AddressID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateSupplierAddressstatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsSupplierAddressLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.SUPPLIER_ADDRESS_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "Supplier Type"
    Public Shared Function GetList_SupplierType(name As String, status As String) As DataTable
        Dim lnq As New MsSupplierTypeLinqDB

        Dim parm(1) As SqlParameter
        Dim wh As String = ""

        wh += "1=1"

        If name <> "" Then
            wh += " AND Supplier_Type_Name  Like '%' + @Supplier_Type_Name + '%' "
            parm(0) = SqlDB.SetText("@Supplier_Type_Name", name)
        End If

        If status <> "" Then
            wh += " AND Active_Status = '" & status & "'"
        End If

        Dim dt As DataTable = lnq.GetDataList(wh, "Supplier_Type_Name", Nothing, parm)
        lnq = Nothing
        Return dt
    End Function
    Public Shared Sub GetListDDL_SupplierType(ddl As DropDownList)
        Dim sql As String = "select Supplier_Type_ID, Supplier_Type_Name "
        sql += " from MS_Supplier_Type"
        sql += " where Active_Status='Y'"
        sql += " order by Supplier_Type_Name"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("Supplier_Type_ID") = 0
        dr("Supplier_Type_Name") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "Supplier_Type_ID"
        ddl.DataTextField = "Supplier_Type_Name"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub
    Public Shared Function CheckDuplicateSupplierType(SupplierTypeID As Long, SupplierTypeName As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsSupplierTypeLinqDB
        Dim re As Boolean = lnq.ChkDuplicateBySUPPLIER_TYPE_NAME(SupplierTypeName, SupplierTypeID, Nothing)
        If re = True Then
            ret.IsSuccess = False
            ret.ErrorMessage = "Duplicate Supplier Type " & SupplierTypeName
        Else
            ret.IsSuccess = True
        End If

        Return ret
    End Function
    Public Shared Function GetSupplierTypeData(SupplierTypeID As Long) As MsSupplierTypeLinqDB
        Dim ret As New MsSupplierTypeLinqDB
        ret.GetDataByPK(SupplierTypeID, Nothing)
        Return ret
    End Function
    Public Shared Function SaveSupplierType(UserName As String, SupplierTypeID As Long, SupplierTypeName As String, ActiveStatus As Boolean) As MsSupplierTypeLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsSupplierTypeLinqDB
        lnq.GetDataByPK(SupplierTypeID, trans.Trans)
        lnq.SUPPLIER_TYPE_NAME = SupplierTypeName
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.SUPPLIER_TYPE_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeleteSupplierType(SupplierTypeID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsSupplierTypeLinqDB
        ret = lnq.DeleteByPK(SupplierTypeID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateSupplierTypestatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsSupplierTypeLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.SUPPLIER_TYPE_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "Supplier_Backlist"
    Public Shared Function GetList_Supplier_Backlist(wh As String, parm As SqlParameter()) As DataTable

        Dim sql As String = "Select Supplier_Blacklist_ID,SupB.Supplier_ID, (Sup.Supplier_TName + '/' +sup.Supplier_EName) SupName"
        sql += " ,Blacklist_StartDate,Blacklist_EndDate,Blacklist_Reason,Notation,SupB.Active_Status "
        sql += " FROM MS_SUPPLIER_BLACKLIST SupB"
        sql += " left join MS_Supplier Sup On supb.Supplier_ID = Sup.Supplier_ID "

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Return dt
    End Function

    Public Shared Sub GetListDDL_Supplier(ddl As DropDownList)
        Dim sql As String = "Select Supplier_ID,(Supplier_TName +'/'+ Supplier_EName) SupName"
        sql += " from MS_Supplier"
        sql += " where Active_Status='Y'"
        sql += " order by SupName"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("Supplier_ID") = 0
        dr("SupName") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "Supplier_ID"
        ddl.DataTextField = "SupName"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub

    Public Shared Function GetSupplier_BacklistData(Supplier_BacklistID As Long) As MsSupplierBlacklistLinqDB
        Dim ret As New MsSupplierBlacklistLinqDB
        ret.GetDataByPK(Supplier_BacklistID, Nothing)
        Return ret
    End Function

    Public Shared Function DeleteSupplier_Backlist(Supplier_BacklistID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsSupplierBlacklistLinqDB

        ret = lnq.DeleteByPK(Supplier_BacklistID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function SaveSupplier_BacklistData(UserName As String, Supplier_BacklistID As Long, Supplier_ID As Long, StartDate As String, EndDate As String, Reason As String, ActiveStatus As Boolean) As MsSupplierBlacklistLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsSupplierBlacklistLinqDB

        lnq.GetDataByPK(Supplier_BacklistID, trans.Trans)
        lnq.SUPPLIER_ID = Supplier_ID
        lnq.BLACKLIST_STARTDATE = StringToDate(StartDate, "dd/MM/yyyy")
        lnq.BLACKLIST_ENDDATE = StringToDate(EndDate, "dd/MM/yyyy")
        lnq.BLACKLIST_REASON = Reason
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.SUPPLIER_BLACKLIST_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function

    Public Shared Function UpdateSupplierBackliststatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsSupplierBlacklistLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.SUPPLIER_BLACKLIST_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "Address -Tumbon,District,Province,Country"

    '---------------------------------------------------Tumbon----------------------------------------------------------------

    Public Shared Function GetList_Tumbon(DistrictName As String, Tumbonname As String, status As String) As DataTable
        Dim lnq As New MsAddressTumbonLinqDB

        Dim sql As String = "SELECT Tumbon_ID, D.District_ID, D.District_Name, Tumbon_Name ,T.Active_Status"
        sql += "  FROM MS_Address_Tumbon T"
        sql += "  inner join MS_Address_District D on D.District_ID = T.District_ID "

        Dim parm(2) As SqlParameter
        Dim wh As String = ""

        If DistrictName <> "" Then
            If DistrictName = 0 Then DistrictName = ""
            wh += " AND D.District_ID  Like '%' + @DistrictName + '%' "
            parm(0) = SqlDB.SetText("@DistrictName", DistrictName)
        End If

        If Tumbonname <> "" Then
            wh += " AND Tumbon_Name  Like '%' + @Tumbonname + '%' "
            parm(1) = SqlDB.SetText("@Tumbonname", Tumbonname)
        End If

        If status <> "" Then
            wh += " AND T.Active_Status = '" & status & "'"
        End If

        If wh.Trim <> "" Then
            sql += " where 1=1" & wh
        End If

        sql += " order by Tumbon_Name ,D.District_Name"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql, parm)
        Return dt
    End Function
    Public Shared Sub GetListDDL_Tumbon(ddl As DropDownList, ID As String)
        Dim sql As String = "select Tumbon_ID, Tumbon_Name "
        sql += " from MS_Address_Tumbon"
        sql += " where Active_Status='Y'"

        Dim parm(0) As SqlParameter
        If ID <> "" Then
            sql += " AND  District_ID = @ID"
            parm(0) = SqlDB.SetText("@ID", ID)
        End If

        sql += " order by Tumbon_Name"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql, parm)
        Dim dr As DataRow = dt.NewRow
        dr("Tumbon_ID") = 0
        dr("Tumbon_Name") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "Tumbon_ID"
        ddl.DataTextField = "Tumbon_Name"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub
    Public Shared Function CheckDuplicateTumbon(District_ID As String, Tumbon_Name As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim sql As String = " select Tumbon_ID ,District_ID,Tumbon_Name  from MS_Address_Tumbon "

        If District_ID <> "" Then
            sql += "WHERE District_ID  =  '" & District_ID & "'"
        End If
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        If dt.Rows.Count > 0 Then
            For i As Integer = 0 To dt.Rows.Count - 1
                Dim TumbonName As String = dt.Rows(i)("Tumbon_Name").ToString()
                If Tumbon_Name = TumbonName Then
                    ret.IsSuccess = False
                    ret.ErrorMessage = "Duplicate Tumbon name " & Tumbon_Name
                    Exit For
                Else
                    ret.IsSuccess = True
                End If
            Next
        Else
            ret.IsSuccess = True
        End If
        Return ret
    End Function
    Public Shared Function GetTumbonData(Tumbon_ID As Long) As MsAddressTumbonLinqDB
        Dim ret As New MsAddressTumbonLinqDB
        ret.GetDataByPK(Tumbon_ID, Nothing)
        Return ret
    End Function
    Public Shared Function SaveTumbon(UserName As String, Tumbon_ID As Long, District_ID As Long, Tumbon_Name As String, ActiveStatus As Boolean) As MsAddressTumbonLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsAddressTumbonLinqDB
        lnq.GetDataByPK(Tumbon_ID, trans.Trans)
        lnq.DISTRICT_ID = District_ID
        lnq.TUMBON_NAME = Tumbon_Name
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.TUMBON_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeleteTumbon(Tumbon_ID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsAddressTumbonLinqDB
        ret = lnq.DeleteByPK(Tumbon_ID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateTumbonstatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsAddressTumbonLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.TUMBON_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    '----------------------------------------------District------------------------------------------

    Public Shared Function GetList_District(Province_Name As String, District_name As String, status As String) As DataTable
        Dim lnq As New MsAddressDistrictLinqDB

        Dim sql As String = "SELECT District_ID, P.Province_ID, P.Province_Name, District_name,D.Active_Status"
        sql += " FROM MS_Address_District D "
        sql += "  inner join MS_Address_Province  P on D.Province_ID  = p.Province_ID  "

        Dim parm(2) As SqlParameter
        Dim wh As String = ""

        If Province_Name <> "" Then
            If Province_Name = 0 Then Province_Name = ""
            wh += " AND P.Province_ID  Like '%' + @Province_Name + '%' "
            parm(0) = SqlDB.SetText("@Province_Name", Province_Name)
        End If

        If District_name <> "" Then
            wh += " AND District_name  Like '%' + @District_name + '%' "
            parm(1) = SqlDB.SetText("@District_name", District_name)
        End If

        If status <> "" Then
            wh += " AND D.Active_Status = '" & status & "'"
        End If

        If wh.Trim <> "" Then
            sql += " where 1=1" & wh
        End If

        sql += " order by District_name ,P.Province_Name"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql, parm)
        Return dt

    End Function
    Public Shared Sub GetListDDL_District(ddl As DropDownList, ID As String)
        Dim sql As String = "select District_ID, District_Name "
        sql += " from MS_Address_District"
        sql += " where Active_Status='Y'"

        Dim parm(0) As SqlParameter
        If ID <> "" Then
            sql += " AND  Province_ID = @ID"
            parm(0) = SqlDB.SetText("@ID", ID)
        End If

        sql += " order by District_Name"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql, parm)
        Dim dr As DataRow = dt.NewRow
        dr("District_ID") = 0
        dr("District_Name") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "District_ID"
        ddl.DataTextField = "District_Name"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub
    Public Shared Function CheckDuplicateDistrict(Province_ID As String, District_Name As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo

        Dim sql As String = "select District_ID, Province_ID ,District_Name from MS_Address_District "

        If Province_ID <> "" Then
            sql += "WHERE Province_ID  =  '" & Province_ID & "'"
        End If
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        If dt.Rows.Count > 0 Then
            For i As Integer = 0 To dt.Rows.Count - 1
                Dim DistrictName As String = dt.Rows(i)("District_Name").ToString()
                If District_Name = DistrictName Then
                    ret.IsSuccess = False
                    ret.ErrorMessage = "Duplicate District name " & District_Name
                    Exit For
                Else
                    ret.IsSuccess = True
                End If
            Next
        Else
            ret.IsSuccess = True
        End If
        Return ret
    End Function
    Public Shared Function GetDistrictData(District_ID As Long) As MsAddressDistrictLinqDB
        Dim ret As New MsAddressDistrictLinqDB
        ret.GetDataByPK(District_ID, Nothing)
        Return ret
    End Function
    Public Shared Function SaveDistrict(UserName As String, District_ID As Long, Province_ID As Long, District_Name As String, ActiveStatus As Boolean) As MsAddressDistrictLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsAddressDistrictLinqDB
        lnq.GetDataByPK(District_ID, trans.Trans)
        lnq.PROVINCE_ID = Province_ID
        lnq.DISTRICT_NAME = District_Name
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.DISTRICT_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeleteDistrict(District_ID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsAddressDistrictLinqDB
        ret = lnq.DeleteByPK(District_ID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateDistrictstatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsAddressDistrictLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.DISTRICT_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
    '-----------------------------------------------Province-----------------------------------------

    Public Shared Function GetList_Province(Countryname As String, Provincename As String, status As String) As DataTable
        Dim lnq As New MsAddressProvinceLinqDB

        Dim sql As String = "SELECT Province_ID,p.Country_ID,c.Country_Name ,Province_Name,p.Active_Status"
        sql += " FROM MS_Address_Province P "
        sql += " inner join MS_Address_Country C on p.Country_ID =c.Country_ID "

        Dim parm(2) As SqlParameter
        Dim wh As String = ""

        If Countryname <> "" Then
            If Countryname = 0 Then Countryname = ""
            wh += " AND p.Country_ID  Like '%' + @Country_ID + '%' "
            parm(0) = SqlDB.SetText("@Country_ID", Countryname)
        End If

        If Provincename <> "" Then
            wh += " AND Province_Name  Like '%' + @Province_Name + '%' "
            parm(1) = SqlDB.SetText("@Province_Name", Provincename)
        End If

        If status <> "" Then
            wh += " AND P.Active_Status = '" & status & "'"
        End If

        If wh.Trim <> "" Then
            sql += " where 1=1" & wh
        End If

        sql += " order by Province_Name ,c.Country_Name"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql, parm)
        Return dt

    End Function
    Public Shared Sub GetListDDL_Province(ddl As DropDownList, ID As String)
        Dim sql As String = "select Province_ID, Province_Name "
        sql += " from MS_Address_Province"
        sql += " where Active_Status='Y'"

        Dim parm(0) As SqlParameter
        If ID <> "" Then
            sql += " AND Country_ID =  @ID"
            parm(0) = SqlDB.SetBigInt("@ID", ID)
        End If

        sql += " order by Province_Name"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql, parm)
        Dim dr As DataRow = dt.NewRow
        dr("Province_ID") = 0
        dr("Province_Name") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "Province_ID"
        ddl.DataTextField = "Province_Name"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub
    Public Shared Function CheckDuplicateProvince(Country_ID As String, Province_Name As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo

        Dim sql As String = "select Province_ID, Country_ID ,Province_Name from MS_Address_Province "

        If Country_ID <> "" Then
            sql += "WHERE Country_ID  =  '" & Country_ID & "'"
        End If
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        If dt.Rows.Count > 0 Then
            For i As Integer = 0 To dt.Rows.Count - 1
                Dim ProvinceName As String = dt.Rows(i)("Province_Name").ToString()
                If Province_Name = ProvinceName Then
                    ret.IsSuccess = False
                    ret.ErrorMessage = "Duplicate Province name " & Province_Name
                    Exit For
                Else
                    ret.IsSuccess = True
                End If
            Next
        Else
            ret.IsSuccess = True
        End If
        Return ret
    End Function
    Public Shared Function GetProvinceData(Province_ID As Long) As MsAddressProvinceLinqDB
        Dim ret As New MsAddressProvinceLinqDB
        ret.GetDataByPK(Province_ID, Nothing)
        Return ret
    End Function
    Public Shared Function SaveProvince(UserName As String, Province_ID As Long, Country_ID As Long, Province_Name As String, ActiveStatus As Boolean) As MsAddressProvinceLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsAddressProvinceLinqDB
        lnq.GetDataByPK(Province_ID, trans.Trans)
        lnq.COUNTRY_ID = Country_ID
        lnq.PROVINCE_NAME = Province_Name
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.PROVINCE_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeleteProvince(Province_ID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsAddressProvinceLinqDB
        ret = lnq.DeleteByPK(Province_ID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateProvincestatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsAddressProvinceLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.PROVINCE_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
    '--------------------------------------------Country--------------------------------------------

    Public Shared Function GetList_Country(name As String, status As String) As DataTable
        Dim lnq As New MsAddressCountryLinqDB

        Dim parm(1) As SqlParameter
        Dim wh As String = ""

        wh += "1=1"

        If name <> "" Then
            wh += " AND Country_Name  Like '%' + @Country_Name + '%' "
            parm(0) = SqlDB.SetText("@Country_Name", name)
        End If

        If status <> "" Then
            wh += " AND Active_Status = '" & status & "'"
        End If

        Dim dt As DataTable = lnq.GetDataList(wh, "Country_Name", Nothing, parm)
        lnq = Nothing
        Return dt
    End Function
    Public Shared Sub GetListDDL_Country(ddl As DropDownList)
        Dim sql As String = "select Country_ID, Country_Name "
        sql += " from MS_Address_Country"
        sql += " where Active_Status='Y'"

        sql += " order by Country_Name"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("Country_ID") = 0
        dr("Country_Name") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "Country_ID"
        ddl.DataTextField = "Country_Name"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub
    Public Shared Function CheckDuplicateCountry(Country_ID As Long, Country_Name As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsAddressCountryLinqDB
        Dim re As Boolean = lnq.ChkDuplicateByCOUNTRY_NAME(Country_Name, Country_ID, Nothing)
        If re = True Then
            ret.IsSuccess = False
            ret.ErrorMessage = "Duplicate Country name " & Country_Name
        Else
            ret.IsSuccess = True
        End If

        Return ret
    End Function
    Public Shared Function GetCountryData(Country_Name As Long) As MsAddressCountryLinqDB
        Dim ret As New MsAddressCountryLinqDB
        ret.GetDataByPK(Country_Name, Nothing)
        Return ret
    End Function
    Public Shared Function SaveCountry(UserName As String, Country_ID As Long, Country_Name As String, ActiveStatus As Boolean) As MsAddressCountryLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsAddressCountryLinqDB
        lnq.GetDataByPK(Country_ID, trans.Trans)
        lnq.COUNTRY_NAME = Country_Name
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.COUNTRY_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeleteCountry(Country_ID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsAddressCountryLinqDB
        ret = lnq.DeleteByPK(Country_ID, trans.Trans)
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateCountrystatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsAddressCountryLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.COUNTRY_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function
#End Region

#Region "Role Data"
    Public Shared Function GetList_Role(RoleName As String, ActiveStatus As String) As DataTable
        Dim sql As String = "select r.role_id, r.role_name, r.active_status, "
        sql += " (select count(rm.Menu_Role_ID) from MS_Menu_Role rm inner join MS_Menu m on m.menu_id=rm.menu_id where rm.role_id=r.role_id and m.menu_url<>'#') role_menu_qty, "
        sql += " (select count(User_Role_ID) from MS_User_Role where role_id=r.role_id) role_user_qty"
        sql += " from MS_ROLE r"

        Dim parm(2) As SqlParameter
        Dim wh As String = " where 1=1 "
        If RoleName.Trim <> "" Then
            wh += " and r.role_name like '%' + @_ROLE_NAME + '%'"
            parm(0) = SqlDB.SetText("@_ROLE_NAME", RoleName)
        End If
        If ActiveStatus.Trim <> "" Then
            wh += " and r.active_status=@_ACTIVE_STATUS"
            parm(1) = SqlDB.SetText("@_ACTIVE_STATUS", ActiveStatus)
        End If
        sql += wh
        sql += " order by r.role_name"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql, Nothing, parm)
        Return dt
    End Function
    Public Shared Function GetList_RoleUnselectUser(RoleID As Long) As DataTable
        Dim sql As String = "select u.user_id, u.user_login, u.first_name, u.last_name "
        sql += " from MS_User u "
        sql += " where u.user_id not in (select user_id from MS_User_Role where role_id=@_ROLE_ID)"
        sql += " order by u.first_name"
        Dim p(1) As SqlParameter
        p(0) = SqlDB.SetBigInt("@_ROLE_ID", RoleID)
        Dim dt As DataTable = SqlDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Shared Function GetList_RoleUser(RoleID As Long) As DataTable
        Dim sql As String = "select ur.user_id, u.user_login, u.first_name, u.last_name "
        sql += " from MS_User_Role ur"
        sql += " inner join MS_User u on u.user_id=ur.user_id "
        sql += " where ur.role_id=@_ROLE_ID"
        sql += " order by u.user_login"

        Dim p(1) As SqlParameter
        p(0) = SqlDB.SetBigInt("@_ROLE_ID", RoleID)
        Dim dt As DataTable = SqlDB.ExecuteTable(sql, p)
        Return dt
    End Function
    Public Shared Function CheckDuplicateRole(RoleID As Long, RoleName As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsRoleLinqDB
        Dim re As Boolean = lnq.ChkDuplicateByROLE_NAME(RoleName, RoleID, Nothing)
        If re = True Then
            ret.IsSuccess = False
            ret.ErrorMessage = "Duplicate role name " & RoleName
        Else
            ret.IsSuccess = True
        End If

        Return ret
    End Function
    Public Shared Function GetRoleData(RoleID As Long) As MsRoleLinqDB
        Dim ret As New MsRoleLinqDB
        ret.GetDataByPK(RoleID, Nothing)
        Return ret
    End Function
    Public Shared Function SaveRole(UserName As String, RoleID As Long, RoleName As String, ActiveStatus As Boolean, MenuList As DataTable, UserList As DataTable) As MsRoleLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New MsRoleLinqDB
        lnq.GetDataByPK(RoleID, trans.Trans)
        lnq.ROLE_NAME = RoleName
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.ROLE_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            Dim sql As String = "delete from MS_Menu_Role where role_id=@_ROLE_ID"
            Dim p(1) As SqlParameter
            p(0) = SqlDB.SetBigInt("@_ROLE_ID", lnq.ROLE_ID)

            ret = SqlDB.ExecuteNonQuery(sql, trans.Trans, p)
            If ret.IsSuccess = True Then
                sql = "delete from MS_User_Role where role_id=@_ROLE_ID"
                ReDim p(1)
                p(0) = SqlDB.SetBigInt("@_ROLE_ID", lnq.ROLE_ID)

                ret = SqlDB.ExecuteNonQuery(sql, trans.Trans, p)
                If ret.IsSuccess = True Then
                    'Insert Menu Role
                    MenuList.DefaultView.RowFilter = "grant_role=true"
                    If MenuList.DefaultView.Count > 0 Then
                        For Each dr As DataRowView In MenuList.DefaultView
                            'Loop ตามเมนูที่เลือก
                            'ตอนจะ Insert Menu ให้ Insert ข้อมูลตามลำดับชั้น

                            sql = " with menu as "
                            sql += " ( "
                            sql += "    Select  menu_id,menu_name,can_edit,can_delete, parent_menu_id, menu_order "
                            sql += "    from MS_Menu  "
                            sql += "    where menu_id=@_MENU_ID"
                            sql += "    union all "
                            sql += "    select m.menu_id,m.menu_name,m.can_edit,m.can_delete, m.parent_menu_id, m.menu_order "
                            sql += "    from MS_Menu m  "
                            sql += "    inner join menu on m.Menu_ID=menu.parent_menu_id "
                            sql += " ) "
                            sql += " select menu_id,menu_name,can_edit,can_delete, parent_menu_id, menu_order"
                            sql += " from menu"

                            ReDim p(1)
                            p(0) = SqlDB.SetBigInt("@_MENU_ID", dr("menu_id"))

                            Dim TmpDt As DataTable = SqlDB.ExecuteTable(sql, p)
                            If TmpDt.Rows.Count > 0 Then
                                For i As Integer = TmpDt.Rows.Count - 1 To 0 Step -1
                                    Dim mLnq As New MsMenuRoleLinqDB
                                    If mLnq.ChkDuplicateByMENU_ID_ROLE_ID(TmpDt.Rows(i)("menu_id"), lnq.ROLE_ID, mLnq.MENU_ROLE_ID, trans.Trans) = False Then
                                        mLnq.MENU_ID = TmpDt.Rows(i)("menu_id")
                                        mLnq.ROLE_ID = lnq.ROLE_ID

                                        ret = mLnq.InsertData(UserName, trans.Trans)
                                        If ret.IsSuccess = False Then
                                            Exit For
                                        End If
                                    End If
                                Next
                            End If
                            TmpDt.Dispose()

                            If ret.IsSuccess = False Then
                                Exit For
                            End If
                        Next
                    End If
                    MenuList.DefaultView.RowFilter = ""

                    'Insert User Role
                    If UserList.Rows.Count > 0 Then
                        For Each dr As DataRow In UserList.Rows
                            Dim uLnq As New MsUserRoleLinqDB
                            uLnq.ROLE_ID = lnq.ROLE_ID
                            uLnq.USER_ID = dr("user_id")

                            ret = uLnq.InsertData(UserName, trans.Trans)
                            If ret.IsSuccess = False Then
                                Exit For
                            End If
                        Next
                    End If

                    If ret.IsSuccess = True Then
                        trans.CommitTransaction()
                    Else
                        lnq.ROLE_ID = 0
                        trans.RollbackTransaction()
                    End If
                Else
                    lnq.ROLE_ID = 0
                    trans.RollbackTransaction()
                End If
            Else
                lnq.ROLE_ID = 0
                trans.RollbackTransaction()
            End If
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeleteRole(RoleID As Long) As ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim ret As New ExecuteDataInfo

        Dim sql As String = "delete from MS_Menu_Role where role_id=@_ROLE_ID"
        Dim p(1) As SqlParameter
        p(0) = SqlDB.SetBigInt("@_ROLE_ID", RoleID)

        ret = SqlDB.ExecuteNonQuery(sql, trans.Trans, p)
        If ret.IsSuccess = True Then
            sql = "delete from MS_User_Role where role_id=@_ROLE_ID"
            ReDim p(1)
            p(0) = SqlDB.SetBigInt("@_ROLE_ID", RoleID)

            ret = SqlDB.ExecuteNonQuery(sql, trans.Trans, p)
            If ret.IsSuccess = True Then
                Dim lnq As New MsRoleLinqDB
                ret = lnq.DeleteByPK(RoleID, trans.Trans)
                If ret.IsSuccess = True Then
                    trans.CommitTransaction()
                Else
                    trans.RollbackTransaction()
                End If
                lnq = Nothing
            Else
                trans.RollbackTransaction()
            End If
        Else
            trans.RollbackTransaction()
        End If

        Return ret
    End Function
    Public Shared Function UpdateRoleStatus(UserName As String, RoleID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New MsRoleLinqDB
        lnq.GetDataByPK(RoleID, trans.Trans)
        If lnq.ROLE_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(UserName, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    'Public Shared Function DeleteUserRole(RoleID As Long, UserID As Long) As ExecuteDataInfo
    '    Dim trans As New TransactionDB

    '    Dim sql As String = "delete from MS_User_Role where role_id=@_ROLE_ID and user_id=@_USER_ID"
    '    Dim p(2) As SqlParameter
    '    p(0) = SqlDB.SetBigInt("@_ROLE_ID", RoleID)
    '    p(1) = SqlDB.SetBigInt("@_USER_ID", UserID)

    '    Dim ret As ExecuteDataInfo = SqlDB.ExecuteNonQuery(sql, trans.Trans, p)
    '    Return ret
    'End Function

#End Region

#Region "Menu Data"
    Public Shared Function GetList_Menu(UserID As Long) As DataTable
        Dim sql As String = "select m.menu_id,m.menu_name, m.menu_url, m.menu_icon, m.parent_menu_id,m.menu_order"
        sql += " from MS_MENU m "
        sql += " inner join MS_Menu_Role mr on m.menu_id=mr.menu_id"
        sql += " inner join MS_Role r on r.role_id=mr.role_id"
        sql += " where 1=1 "
        sql += " and mr.role_id in (select role_id from MS_User_Role where user_id=@_USER_ID)"
        sql += " and m.active_status='Y' and r.active_status='Y' "
        sql += " order by m.menu_order"

        Dim p(1) As SqlParameter
        p(0) = SqlDB.SetBigInt("@_USER_ID", UserID)
        Dim dt As DataTable = SqlDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Shared Function GetList_RoleMenu(RoleID As Long) As DataTable
        Dim ret As New DataTable
        ret.Columns.Add("menu_id", GetType(Long))
        ret.Columns.Add("menu_full_name")
        ret.Columns.Add("grant_role")


        Dim sql As String = "select m.menu_id, "
        sql += "  (select count(mr.Menu_Role_ID) from MS_Menu_Role mr where mr.role_id=@_ROLE_ID and mr.menu_id=m.menu_id) grant_role"
        sql += " from MS_MENU m"
        sql += " where m.menu_url<>'#'"
        sql += " order by m.parent_menu_id,m.menu_order"

        Dim p(1) As SqlParameter
        p(0) = SqlDB.SetBigInt("@_ROLE_ID", RoleID)

        Dim dt As DataTable = SqlDB.ExecuteTable(sql, p)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                sql = " with menu as "
                sql += " ( "
                sql += "    Select  menu_id,menu_name,can_edit,can_delete, parent_menu_id, menu_order "
                sql += "    from MS_Menu  "
                sql += "    where menu_id=@_MENU_ID"
                sql += "    union all "
                sql += "    select m.menu_id,m.menu_name,m.can_edit,m.can_delete, m.parent_menu_id, m.menu_order "
                sql += "    from MS_Menu m  "
                sql += "    inner join menu on m.Menu_ID=menu.parent_menu_id "
                sql += " ) "
                sql += " select menu_id,menu_name,can_edit,can_delete, parent_menu_id, menu_order"
                sql += " from menu"

                ReDim p(1)
                p(0) = SqlDB.SetBigInt("@_MENU_ID", dr("menu_id"))

                Dim TmpDt As DataTable = SqlDB.ExecuteTable(sql, p)
                If TmpDt.Rows.Count > 0 Then
                    Dim MenuName As String = ""
                    For i As Integer = TmpDt.Rows.Count - 1 To 0 Step -1
                        If MenuName = "" Then
                            MenuName = TmpDt.Rows(i)("menu_name")
                        Else
                            MenuName += " > " & TmpDt.Rows(i)("menu_name")
                        End If
                    Next

                    Dim retDr As DataRow = ret.NewRow
                    retDr("menu_id") = dr("menu_id")
                    retDr("menu_full_name") = MenuName
                    retDr("grant_role") = dr("grant_role")
                    ret.Rows.Add(retDr)
                End If
            Next
        End If
        dt.Dispose()

        Return ret
    End Function


#End Region

#End Region

#Region "Activity"

#Region "Document Receive"

    'Document Receive
    Public Shared Function GetList_DocumentReceive(Document_ID As String, ReceiveType As String, PO As String, RDate As String, Rtime As String, Checker As String, Approver As String) As DataTable

        Dim sql As String = "SELECT Tbr.Sequence_ID,Document_ID,Document_Date,Document_Time,MsT.Recieve_Type,tbr.PO_Reference_Document "
        sql += " ,(s.Fisrt_Name +' '+ s.Last_Name) Checker_ID ,(Tbr.Created_By) Approver ,Tbr.Active_Status"
        sql += " FROM TB_RCStep1_Receive TbR"
        sql += " Left join MS_Receive_Type MsT On Tbr.Receive_Type_ID = MsT.Receive_Type_ID "
        sql += "  Left Join Ms_Staff s On s.Staff_ID = Tbr.Checker_ID where 1=1 "

        Dim parm(5) As SqlParameter

        If Document_ID <> "" Then
            sql += " And Document_ID Like '%' + @Document_ID + '%'"
            parm(0) = SqlDB.SetText("@Document_ID", Document_ID)
        End If

        If ReceiveType = "0" Then ReceiveType = ""
        If ReceiveType <> "" Then
            sql += " AND Tbr.Receive_Type_ID = '" & ReceiveType & "'"
        End If

        If PO = "0" Then PO = ""
        If PO <> "" Then
            sql += "AND tbr.PO_Reference_Document  = '" & PO & "'"
        End If

        If RDate <> "" Then
            Dim date1 As DateTime = DateTime.ParseExact(RDate, "dd/MM/yyyy", CultureInfo.InvariantCulture)
            Dim RecieveDate As String = date1.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

            sql += " AND Document_Date Like '%' + @Document_Date + '%'"
            parm(1) = SqlDB.SetText("@Document_Date", RecieveDate)
        End If

        If Rtime <> "" Then
            sql += " AND Document_Time Like '%' + @Document_Time + '%'"
            parm(2) = SqlDB.SetText("@Document_Time", Rtime)
        End If

        If Checker <> "" Then
            sql += " AND s.Staff_ID Like '%' + @Checker + '%'"
            parm(3) = SqlDB.SetText("@Checker", Checker)
        End If

        If Approver <> "" Then
            sql += " AND Tbr.Created_By Like '%' + @Approver + '%'"
            parm(4) = SqlDB.SetText("@Approver", Approver)
        End If

        sql += "Order By Document_ID"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql, parm)
        Return dt
    End Function

    Public Shared Sub GetListDDL_DocumentReceive(ddl As DropDownList)
        Dim sql As String = "select Sequence_ID, Document_ID "
        sql += " from TB_RCStep1_Receive"
        sql += " where Active_Status='Y'"
        sql += " order by Document_ID"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("Sequence_ID") = 0
        dr("Document_ID") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "Sequence_ID"
        ddl.DataTextField = "Document_ID"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub

    Public Shared Function GetDocumentReceiveData(DocumentReceiveID As Long) As TbRcstep1ReceiveLinqDB
        Dim ret As New TbRcstep1ReceiveLinqDB
        ret.GetDataByPK(DocumentReceiveID, Nothing)
        Return ret
    End Function
    Public Shared Function SaveDocumentReceive(UserName As String, SequenceID As Long, DocumentID As String, Document_Date As String, Document_Time As String, Receive_Type_ID As Long, PO_Reference_Document As String, ReferDate As String, DO_Reference_Document As String, DO_date As String, Checker As String, ReferDoc As String, Reason As String, TotalUnit As String, TotalWeihgt As String, TotalPrice As String, ActiveStatus As Boolean) As TbRcstep1ReceiveLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New TbRcstep1ReceiveLinqDB
        lnq.GetDataByPK(SequenceID, trans.Trans)
        lnq.DOCUMENT_ID = DocumentID
        lnq.DOCUMENT_DATE = StringToDate(Document_Date, "dd/MM/yyyy")
        lnq.DOCUMENT_TIME = Document_Time
        lnq.RECEIVE_TYPE_ID = Receive_Type_ID
        lnq.PO_REFERENCE_DOCUMENT = PO_Reference_Document
        lnq.PO_REFERENCE_DATE = StringToDate(ReferDate, "dd/MM/yyyy")
        lnq.DO_REFERENCE_DOCUMENT1 = DO_Reference_Document
        lnq.DO_REFERENCE_DATE = StringToDate(DO_date, "dd/MM/yyyy")
        lnq.CHECKER_ID = Checker
        lnq.REFERENCE_DOCUMENT1 = ReferDoc
        lnq.RECEIVE_REASON = Reason
        'lnq.TOTAL_UNIT = TotalUnit
        'lnq.GROSS_WEIGHT = TotalWeihgt
        'lnq.TOTAL_PRICE = TotalPrice
        lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")

        Dim ret As New ExecuteDataInfo
        If lnq.SEQUENCE_ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If

        Return lnq
    End Function
    Public Shared Function DeleteDocumentReceive(DocumentReceiveID As Long, Document_Id As String) As ExecuteDataInfo
        Dim trans As New TransactionDB

        Dim ret As New ExecuteDataInfo
        Dim lnq As New TbRcstep1ReceiveLinqDB
        ret = lnq.DeleteByPK(DocumentReceiveID, trans.Trans)

        Dim p_ab(2) As SqlParameter
        p_ab(0) = SqlDB.SetText("@Document_Id", Document_Id)
        SqlDB.ExecuteNonQuery("DELETE FROM TB_RCStep1_Receive_Detial WHERE Document_ID = @Document_Id", trans.Trans, p_ab)

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    Public Shared Function UpdateDocumentReceivestatus(LoginUsername As String, ID As Long, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim lnq As New TbRcstep1ReceiveLinqDB
        lnq.GetDataByPK(ID, trans.Trans)
        If lnq.SEQUENCE_ID > 0 Then
            lnq.ACTIVE_STATUS = IIf(ActiveStatus = True, "Y", "N")
            ret = lnq.UpdateData(LoginUsername, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    'Document Receive_detial

    Public Shared Function SaveDocumentReceive_detial(UserName As String, Document_Id As String, POID As String, DOID As String, DTSKU As DataTable) As TbRcstep1ReceiveDetialLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New TbRcstep1ReceiveDetialLinqDB
        Dim ret As New ExecuteDataInfo

        Dim p_ab(2) As SqlParameter
        p_ab(0) = SqlDB.SetText("@Document_Id", Document_Id)
        p_ab(1) = SqlDB.SetText("@POID", POID)
        SqlDB.ExecuteNonQuery("DELETE FROM TB_RCStep1_Receive_Detial WHERE Document_ID = @Document_Id AND PO_Reference_Document = @POID ", trans.Trans, p_ab)
        ret.IsSuccess = True

        If Not DTSKU Is Nothing Then

            For i As Integer = 0 To DTSKU.Rows.Count - 1
                Dim Sequence_ID As String = i + 1
                Dim SKU_ID As String = DTSKU.Rows(i)("SKU_ID").ToString()
                Dim AmountReceive As String = DTSKU.Rows(i)("AmountReceive").ToString()
                Dim AmountReceiveAccess As String = DTSKU.Rows(i)("AmountReceiveAccess").ToString()
                Dim AmountSent As String = DTSKU.Rows(i)("AmountSent").ToString()
                Dim AmountBalance As String = DTSKU.Rows(i)("AmountBalance").ToString()

                If Document_Id <> "" AndAlso POID <> "" Then

                    With lnq
                        .GetDataByPK(Sequence_ID, trans.Trans)
                        .SEQUENCE_ID = 0
                        .DOCUMENT_ID = Document_Id
                        .PO_REFERENCE_DOCUMENT = POID
                        .DO_REFERENCE_DOCUMENT1 = DOID
                        .SKU_ID = SKU_ID
                        .AMOUNTRECEIVE = AmountReceive
                        .AMOUNTRECEIVEACCESS = AmountReceiveAccess
                        .AMOUNTSENT = AmountSent
                        .AMOUNTBALANCE = AmountBalance
                        ret.IsSuccess = False
                        If lnq.SEQUENCE_ID > 0 Then
                            ret = lnq.UpdateData(UserName, trans.Trans)
                        Else
                            ret = lnq.InsertData(UserName, trans.Trans)
                        End If

                        If ret.IsSuccess = False Then
                            Exit For
                        End If
                    End With
                End If
            Next

            If ret.IsSuccess = True Then
                trans.CommitTransaction()
            Else
                trans.RollbackTransaction()
            End If
        End If

        Return lnq
    End Function

    ' Receive SKU
    Public Shared Function GetList_ReceiveSKU(ReferenceNO As String, Document_ID As String, Do_no As String) As DataTable

        Dim sql As String = "SELECT DISTINCT P.Sequence_ID,P.Receive_Type_ID,R.Recieve_Type ,P.Reference_No,P.SKU_ID "
        sql += " ,(Pro.Product_TName +'/'+ Pro.Product_EName) ProductName"
        sql += ",P.Lot_Batch,p.AmountReceive,isnull(TBRd.AmountReceiveAccess,'') AmountReceiveAccess ,(p.AmountReceive - isnull(TBRd.AmountReceiveAccess,'')) AmountBalance "
        sql += ",isnull(U.Unit,'') Unit,S.Expire_Date,P.Receive,Pros.Product_Status,P.Active_Status  "
        sql += " FROM TB_PO_Test P "
        sql += " Left join MS_Receive_Type R On R.Receive_Type_ID = P.Receive_Type_ID  "
        sql += " Left join Ms_SKU S ON S.SKU_ID= P.SKU_ID    "
        sql += "Left join MS_Product Pro On Pro.Product_ID = s.Product_ID    "
        sql += " left join MS_Product_Status  Pros On Pros.Product_Status_ID = P.Product_Status_ID   "
        sql += " Left Join MS_Unit U ON  U.Unit_ID = s.Unit_ID  "
        sql += " left join TB_RCStep1_Receive_Detial TBRd on P.Reference_No = TBRd.PO_Reference_Document AND S.SKU_ID = TBRd.SKU_ID "



        'ตรวจดูในตาราง TB_RCStep1_Receive_Detial มี PO_Reference_Document ที่ส่งมาอยู่ไหม
        Dim sqlReferno As String = "SELECT Distinct PO_Reference_Document From TB_RCStep1_Receive_Detial Where PO_Reference_Document = '" & ReferenceNO & "'"
        Dim dtPO As DataTable = SqlDB.ExecuteTable(sqlReferno)

        'If Do_no <> "" Then
        '    For i As Integer = 0 To dtPO.Rows.Count - 1
        '        Dim PO_Reference_Document As String = dtPO.Rows(i)("PO_Reference_Document").ToString()
        '        If ReferenceNO = PO_Reference_Document Then
        '            sql += " RIght join TB_Do_Test Do On Do.SKU_ID = P.SKU_ID "
        '        End If
        '    Next
        'End If

        If Document_ID = "" Then
            For i As Integer = 0 To dtPO.Rows.Count - 1
                Dim PO_Reference_Document As String = dtPO.Rows(i)("PO_Reference_Document").ToString()
                If ReferenceNO = PO_Reference_Document Then 'ถ้ามีให้แสดงอันล่าสุดออกมา 
                    sql += " Right Join View_Lastest_Receive VwLast on CONVERT(VARCHAR(10),VwLast.Mdate,21) + ' '+CONVERT(VARCHAR(10),VwLast.Mdate,108)  = CONVERT(VARCHAR(10),TBRd.Created_Date,21) + ' '+ CONVERT(VARCHAR(10),TBRd.Created_Date,108) AND TBRd.SKU_ID  =  VwLast.SKU_ID"
                End If
            Next

        End If
        sql += " WHERE 1=1"

        If ReferenceNO <> "" Then
            sql += " AND P.Reference_No = '" & ReferenceNO & "'"
        End If

        If Document_ID <> "" Then
            sql += " AND TBRd.Document_ID = '" & Document_ID & "'"
        End If

        'If Do_no <> "" Then
        '    For i As Integer = 0 To dtPO.Rows.Count - 1
        '        Dim PO_Reference_Document As String = dtPO.Rows(i)("PO_Reference_Document").ToString()
        '        If ReferenceNO = PO_Reference_Document Then
        '            sql += " AND Do.Do_No = '" & Do_no & "'"
        '        End If
        '    Next
        'End If

        sql += " order by P.SKU_ID"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Return dt
    End Function

    Public Shared Function UpdateReceiveSku(UserName As String, DTSKU As DataTable) As TbPoTestLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New TbPoTestLinqDB
        Dim ret As New ExecuteDataInfo

        If Not DTSKU Is Nothing Then

            For i As Integer = 0 To DTSKU.Rows.Count - 1
                Dim Sequence_ID As String = DTSKU.Rows(i)("Sequence_ID").ToString()
                Dim Receive_Type_ID As String = DTSKU.Rows(i)("Receive_Type_ID").ToString()
                Dim Reference_No As String = DTSKU.Rows(i)("Reference_No").ToString()
                Dim Receive As String = DTSKU.Rows(i)("Receive").ToString()

                If Sequence_ID <> "" AndAlso Reference_No <> "" Then

                    With lnq
                        .GetDataByPK(Sequence_ID, trans.Trans)
                        .SEQUENCE_ID = Sequence_ID
                        .RECEIVE = Receive

                        ret.IsSuccess = False
                        If lnq.SEQUENCE_ID > 0 Then
                            ret = lnq.UpdateData(UserName, trans.Trans)
                        Else
                            ret = lnq.InsertData(UserName, trans.Trans)
                        End If

                        If ret.IsSuccess = False Then
                            Exit For
                        End If
                    End With
                End If
            Next

            If ret.IsSuccess = True Then
                trans.CommitTransaction()
            Else
                trans.RollbackTransaction()
            End If
        End If

        Return lnq
    End Function

    'ของแถม
    Public Shared Function GetList_sku_free(DocumentId As String) As DataTable

        Dim sql As String = "SELECT Sequence_ID,Document_ID ,PO_Reference_Document,DO_Reference_Document1,SKU_ID,Amount"
        sql += "    FROM TB_RCStep1_Receive_Detial_Free"
        sql += " Where Document_ID = '" & DocumentId & "'"
        sql += "Order By PO_Reference_Document"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Return dt
    End Function

    Public Shared Function SaveDocumentReceive_detial_Free(UserName As String, Document_Id As String, POID As String, DOID As String, DTSKUFree As DataTable) As TbRcstep1ReceiveDetialFreeLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New TbRcstep1ReceiveDetialFreeLinqDB
        Dim ret As New ExecuteDataInfo

        Dim p_ab(2) As SqlParameter
        p_ab(0) = SqlDB.SetText("@Document_Id", Document_Id)
        p_ab(1) = SqlDB.SetText("@POID", POID)
        SqlDB.ExecuteNonQuery("DELETE FROM TB_RCStep1_Receive_Detial_Free WHERE Document_ID = @Document_Id AND PO_Reference_Document = @POID ", trans.Trans, p_ab)
        ret.IsSuccess = True

        If Not DTSKUFree Is Nothing Then

            For i As Integer = 0 To DTSKUFree.Rows.Count - 1
                Dim Sequence_ID As String = i + 1
                Dim SKU_ID As String = DTSKUFree.Rows(i)("SKU_ID").ToString()
                Dim Amount As String = DTSKUFree.Rows(i)("Amount").ToString()

                If Document_Id <> "" AndAlso POID <> "" Then

                    With lnq
                        .GetDataByPK(Sequence_ID, trans.Trans)
                        .SEQUENCE_ID = 0
                        .DOCUMENT_ID = Document_Id
                        .PO_REFERENCE_DOCUMENT = POID
                        .DO_REFERENCE_DOCUMENT1 = DOID
                        .SKU_ID = SKU_ID
                        .AMOUNT = Amount
                        ret.IsSuccess = False
                        If lnq.SEQUENCE_ID > 0 Then
                            ret = lnq.UpdateData(UserName, trans.Trans)
                        Else
                            ret = lnq.InsertData(UserName, trans.Trans)
                        End If

                        If ret.IsSuccess = False Then
                            Exit For
                        End If
                    End With
                End If
            Next

            If ret.IsSuccess = True Then
                trans.CommitTransaction()
            Else
                trans.RollbackTransaction()
            End If
        End If

        Return lnq
    End Function
#End Region

#Region "PO"

    Public Shared Function GetList_PO(ReceiveType As String, POID As String, PODATE As String) As DataTable
        Dim sql As String = "select DISTINCT R.Recieve_Type,Reference_No,Reference_date,PO.Active_Status"
        sql += "  From TB_PO_Test PO"
        sql += " LEFT JOIN MS_Receive_Type R On r.Receive_Type_ID = Po.Receive_Type_ID where 1=1 "

        Dim parm(2) As SqlParameter

        If ReceiveType = "0" Then ReceiveType = ""
        If ReceiveType <> "" Then
            sql += " AND r.Receive_Type_ID = '" & ReceiveType & "'"
        End If

        If POID <> "" Then
            sql += " AND Reference_No = @POID"
            parm(0) = SqlDB.SetText("@POID", POID)
        End If

        If PODATE <> "" Then
            Dim date1 As DateTime = DateTime.ParseExact(PODATE, "dd/MM/yyyy", CultureInfo.InvariantCulture)
            Dim PODATE1 As String = date1.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

            sql += " AND Reference_date = @PODATE"
            parm(1) = SqlDB.SetText("@PODATE", PODATE1)
        End If
        sql += " Order By R.Recieve_Type"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql, parm)
        Return dt
    End Function

    Public Shared Function GetList_PO_Detial(POID As String) As DataTable
        Dim sql As String = "SELECT Sequence_ID,R.Receive_Type_ID, R.Recieve_Type ,Reference_No,Reference_date ,SKU_ID"
        sql += "  ,Lot_Batch,AmountReceive ,Ps.Product_Status_ID,PS.Product_Status ,Po.Active_Status"
        sql += "  FROM TB_PO_Test PO "
        sql += "  LEFT JOIN MS_Product_Status PS On Po.Product_Status_ID = Ps.Product_Status_ID"
        sql += "  LEFT JOIN MS_Receive_Type R On r.Receive_Type_ID = Po.Receive_Type_ID "

        If POID <> "" Then
            sql += "  WHERE Reference_No = '" & POID & "' "
        End If
        sql += "Order By Reference_No,SKU_ID"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Return dt
    End Function

    Public Shared Sub GetListDDL_ReferenceNo(ddl As DropDownList, ReceiveTye As String) 'PO
        Dim sql As String = "select Distinct (Reference_No) ID, Reference_No "
        sql += " from TB_PO_Test"
        sql += " where Active_Status='Y'"

        If ReceiveTye <> "" Then
            sql += " AND Receive_Type_ID = '" & ReceiveTye & "'"
        End If
        sql += " order by Reference_No"

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("ID") = 0
        dr("Reference_No") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataValueField = "ID"
        ddl.DataTextField = "Reference_No"
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub

    Public Shared Function GetPOData(POID As Long) As TbPoTestLinqDB
        Dim ret As New TbPoTestLinqDB
        ret.GetDataByPK(POID, Nothing)
        Return ret
    End Function
    Public Shared Function SavePO(UserName As String, SequenceID As Long, ReceiveType As String, POID As String, PODate As String, DTPO As DataTable, ActiveStatus As Boolean) As TbPoTestLinqDB
        Dim trans As New TransactionDB
        Dim lnq As New TbPoTestLinqDB
        Dim ret As New ExecuteDataInfo

        Dim p_ab(1) As SqlParameter
        p_ab(0) = SqlDB.SetText("@POID", POID)
        SqlDB.ExecuteNonQuery("DELETE FROM TB_PO_Test WHERE Reference_No = @POID ", trans.Trans, p_ab)
        ret.IsSuccess = True

        If Not DTPO Is Nothing Then

            For i As Integer = 0 To DTPO.Rows.Count - 1
                Dim Sequence_ID As String = i + 1
                Dim SKU_ID As String = DTPO.Rows(i)("SKU_ID").ToString()
                Dim Lot_Batch As String = DTPO.Rows(i)("Lot_Batch").ToString()
                Dim AmountReceive As String = DTPO.Rows(i)("AmountReceive").ToString()
                Dim Product_Status_ID As String = DTPO.Rows(i)("Product_Status_ID").ToString()

                If POID <> "" Then

                    With lnq
                        .GetDataByPK(Sequence_ID, trans.Trans)
                        .SEQUENCE_ID = 0
                        .RECEIVE_TYPE_ID = ReceiveType
                        .REFERENCE_NO = POID
                        .REFERENCE_DATE = StringToDate(PODate, "dd/MM/yyyy")
                        .SKU_ID = SKU_ID
                        .LOT_BATCH = Lot_Batch
                        .AMOUNTRECEIVE = AmountReceive
                        .PRODUCT_STATUS_ID = Product_Status_ID
                        .RECEIVE = "N"

                        ret.IsSuccess = False
                        If lnq.SEQUENCE_ID > 0 Then
                            ret = lnq.UpdateData(UserName, trans.Trans)
                        Else
                            ret = lnq.InsertData(UserName, trans.Trans)
                        End If

                        If ret.IsSuccess = False Then
                            Exit For
                        End If
                    End With
                End If
            Next

            If ret.IsSuccess = True Then
                trans.CommitTransaction()
            Else
                trans.RollbackTransaction()
            End If
        End If

        Return lnq
    End Function
    Public Shared Function DeletePO(POID As String) As ExecuteDataInfo
        Dim trans As New TransactionDB
        Dim ret As New ExecuteDataInfo
        Dim lnq As New TbPoTestLinqDB

        Dim p_ab(1) As SqlParameter
        p_ab(0) = SqlDB.SetText("@POID", POID)
        SqlDB.ExecuteNonQuery("DELETE FROM TB_PO_Test WHERE Reference_No = @POID ", trans.Trans, p_ab)

        ret.IsSuccess = True

        Return ret
    End Function

    Public Shared Function UpdatePOstatus(LoginUsername As String, ID As String, ActiveStatus As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim trans As New TransactionDB

        If ActiveStatus = "true" Then
            SqlDB.ExecuteNonQuery("Update TB_PO_Test SET Active_Status = 'Y'  Where Reference_No = '" & ID & "' ", trans.Trans, Nothing)
            trans.CommitTransaction()
            ret.IsSuccess = True
        Else
            SqlDB.ExecuteNonQuery("Update TB_PO_Test SET Active_Status = 'N'  Where Reference_No = '" & ID & "' ", trans.Trans, Nothing)
            trans.CommitTransaction()
            ret.IsSuccess = True

        End If

        Return ret
    End Function

#End Region

#End Region

#Region "convert"

    Public Shared Function StringToDate(ByVal InputString As String, ByVal Format As String) As DateTime
        Dim Provider As CultureInfo = CultureInfo.GetCultureInfo("en-US")
        Return DateTime.ParseExact(InputString, Format, Provider)
    End Function

#End Region

End Class
