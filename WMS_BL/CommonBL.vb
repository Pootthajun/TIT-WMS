﻿Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Web.UI.WebControls

Public Class CommonBL
    Public Const UserLoginSessionName As String = "UserLoginSessionName"

#Region "Set validate Textbox Property"
    Public Shared Sub SetTextIntKeypress(txt As TextBox)
        txt.Attributes.Add("OnKeyPress", "ChkMinusInt(this,event);")
        txt.Attributes.Add("onKeyDown", "CheckKeyNumber(event);")
    End Sub
    Public Shared Sub SetTextDblKeypress(txt As TextBox)
        txt.Attributes.Add("OnKeyPress", "ChkMinusDbl(this,event);")
        txt.Attributes.Add("onKeyDown", "CheckKeyNumber(event);")
    End Sub
    Public Shared Sub SetTextAreaMaxLength(txt As TextBox, MaxLength As Int16)
        txt.Attributes.Add("onKeyDown", "checkTextAreaMaxLength(this,event,'" & MaxLength & "');")
    End Sub
#End Region

    Public Shared Function GetSystemConfig() As CfSystemConfigLinqDB
        Dim lnq As New CfSystemConfigLinqDB
        lnq.GetDataByPK(1, Nothing)
        Return lnq
    End Function

    Public Shared Function GetSystemConfig(trans As TransactionDB) As CfSystemConfigLinqDB
        Dim lnq As New CfSystemConfigLinqDB
        lnq.GetDataByPK(1, trans.Trans)
        Return lnq
    End Function




    Public Shared Function EncryptText(txt As String) As String
        Return SqlDB.EnCripPwd(txt)
    End Function
End Class
