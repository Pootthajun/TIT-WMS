﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="WMS.aspx.vb" Inherits="TIT_WMS.WMS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Index | WMS</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="site-menubar">
    <div class="site-menubar-body">
      <div>
        <div>
          <ul class="site-menu">
            <li class="site-menu-item has-sub">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-calendar-note" aria-hidden="true"></i>
                <span class="site-menu-title">Activity</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การรับสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">สร้างเอกสารใบรับสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">สถานะใบรับสินะค้า</span>
                        <span class="site-menu-arrow"></span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การเบิกสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">สร้างเอกสารใบเบิกสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">สถานะใบเบิกสินค้า</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การโอนสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/chartjs.html">
                        <span class="site-menu-title">สร้างเอกสารใบโอนสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/gauges.html">
                        <span class="site-menu-title">สถานะใบโอนสินค้า</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">ใบประกอบสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/chartjs.html">
                        <span class="site-menu-title">สร้างเอกสารใบประกอบสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/gauges.html">
                        <span class="site-menu-title">สถานะใบประกอบสินค้า</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
             <li class="site-menu-item has-sub">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-chart" aria-hidden="true"></i>
                <span class="site-menu-title">Status</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">Forms</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">General Elements</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">Editors</span>
                        <span class="site-menu-arrow"></span>
                      </a>
                      <ul class="site-menu-sub">
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-summernote.html">
                            <span class="site-menu-title">Summernote</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-markdown.html">
                            <span class="site-menu-title">Markdown</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-ace.html">
                            <span class="site-menu-title">Ace Editor</span>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/image-cropping.html">
                        <span class="site-menu-title">Image Cropping</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/file-uploads.html">
                        <span class="site-menu-title">File Uploads</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">Tables</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">Basic Tables</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">Bootstrap Tables</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/floatthead.html">
                        <span class="site-menu-title">floatThead</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">Chart</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/chartjs.html">
                        <span class="site-menu-title">Chart.js</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/gauges.html">
                        <span class="site-menu-title">Gauges</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/flot.html">
                        <span class="site-menu-title">Flot</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
             <li class="site-menu-item has-sub">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-home" aria-hidden="true"></i>
                <span class="site-menu-title">Warehouse</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">สรุปยอดคลังสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">General Elements</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">Editors</span>
                        <span class="site-menu-arrow"></span>
                      </a>
                      <ul class="site-menu-sub">
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-summernote.html">
                            <span class="site-menu-title">Summernote</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-markdown.html">
                            <span class="site-menu-title">Markdown</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-ace.html">
                            <span class="site-menu-title">Ace Editor</span>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/image-cropping.html">
                        <span class="site-menu-title">Image Cropping</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/file-uploads.html">
                        <span class="site-menu-title">File Uploads</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">ประวัติความเคลื่อนไหว</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">Basic Tables</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">Bootstrap Tables</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/floatthead.html">
                        <span class="site-menu-title">floatThead</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
             <li class="site-menu-item has-sub">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-check-circle-u" aria-hidden="true"></i>
                <span class="site-menu-title">Checking</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การตรวจนับสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">สร้างเอกสารใบตรวจนับ</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">ประวัติการตรวจนับสินค้า</span>
                      </a>  
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การตรวจ QC</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">สร้างเอกสาร QC</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">ประวัติการตรวจ QC</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
             <li class="site-menu-item has-sub active">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-settings" aria-hidden="true"></i>
                <span class="site-menu-title">Master</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub active ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">Master</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item active">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">เกี่ยวกับ SKU</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">สูตรประกอบสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/editor-summernote.html">
                        <span class="site-menu-title">ตำแหน่งที่จัดเก็บ</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-markdown.html">
                            <span class="site-menu-title">เกี่ยวกับ Pallet</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-ace.html">
                           <span class="site-menu-title">เกี่ยวกับ WH</span>
                         </a>
                     </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/image-cropping.html">
                        <span class="site-menu-title">เกี่ยวกับ Supplier</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/file-uploads.html">
                        <span class="site-menu-title">เกี่ยวกับ Customers</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">Role</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">Group</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">User</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <!-- Page -->
  <div class="page animsition">
    <div class="page-header">
      <h1 class="page-title">Statistics Widgets</h1>
      <ol class="breadcrumb">
        <li><a href="index.html">Home</a></li>
        <li><a href="javascript:void(0)">Widgets</a></li>
        <li class="active">Statistics</li>
      </ol>
      <div class="page-header-actions">
        <button type="button" class="btn btn-sm btn-primary btn-round">
          <span class="text hidden-xs">Settings</span>
          <i class="icon md-chevron-right" aria-hidden="true"></i>
        </button>
      </div>
    </div>
    <div class="page-content container-fluid">
      <div class="row">
          <div class="col-lg-2 col-md-6">
          <!-- Widget -->
          <div class="widget">
            <div class="widget-content padding-20 bg-blue-600 white">
              <div class="counter counter-lg counter-inverse">
                <div class="counter-label text-uppercase font-size-16">we have</div>
                  <span class="counter-icon margin-left-3s0"><i class="icon md-home" aria-hidden="true"></i></span>
                <div class="counter-label text-uppercase font-size-16">pictures</div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-6">
          <!-- Widget -->
          <div class="widget">
            <div class="widget-content padding-20 bg-blue-600 white">
              <div class="counter counter-lg counter-inverse">
                <div class="counter-label text-uppercase font-size-16">we have</div>
                  <span class="counter-icon margin-left-10"><i class="icon md-home" aria-hidden="true"></i></span>
                <div class="counter-label text-uppercase font-size-16">pictures</div>
              </div>
            </div>
          </div>
         </div>
         <div class="col-lg-2 col-md-6">
          <!-- Widget -->
          <div class="widget">
            <div class="widget-content padding-20 bg-blue-600 white">
              <div class="counter counter-lg counter-inverse">
                <div class="counter-label text-uppercase font-size-16">we have</div>
                  <span class="counter-icon margin-left-10"><i class="icon md-home" aria-hidden="true"></i></span>
                <div class="counter-label text-uppercase font-size-16">pictures</div>
              </div>
            </div>
          </div>
         </div>
          <div class="col-lg-2 col-md-6">
          <!-- Widget -->
          <div class="widget">
            <div class="widget-content padding-20 bg-blue-600 white">
              <div class="counter counter-lg counter-inverse">
                <div class="counter-label text-uppercase font-size-16">we have</div>
                  <span class="counter-icon margin-left-10"><i class="icon md-home" aria-hidden="true"></i></span>
                <div class="counter-label text-uppercase font-size-16">pictures</div>
              </div>
            </div>
          </div>
        </div>
          <div class="col-lg-2 col-md-6">
          <!-- Widget -->
          <div class="widget">
            <div class="widget-content padding-20 bg-blue-600 white">
              <div class="counter counter-lg counter-inverse">
                <div class="counter-label text-uppercase font-size-16">we have</div>
                  <span class="counter-icon margin-left-10"><i class="icon md-home" aria-hidden="true"></i></span>
               
                <div class="counter-label text-uppercase font-size-16">pictures</div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-6">
          <!-- Widget -->
          <div class="widget">
            <div class="widget-content padding-20 bg-blue-600 white">
              <div class="counter counter-lg counter-inverse">
                <div class="counter-label text-uppercase font-size-16">we have</div>
                  <span class="counter-icon margin-left-10"><i class="icon md-home" aria-hidden="true"></i></span>
                <div class="counter-label text-uppercase font-size-16">pictures</div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
          <div class="col-lg-2 col-md-6">
          <!-- Widget -->
          <div class="widget">
            <div class="widget-content padding-20 bg-blue-600 white">
              <div class="counter counter-lg counter-inverse">
                <div class="counter-label text-uppercase font-size-16">we have</div>
                <div class="counter-number-group">
                  <span class="counter-number">365</span>
                  <span class="counter-icon margin-left-10"><i class="icon md-image" aria-hidden="true"></i></span>
                </div>
                <div class="counter-label text-uppercase font-size-16">pictures</div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-6">
          <!-- Widget -->
          <div class="widget">
            <div class="widget-content padding-20 bg-blue-600 white">
              <div class="counter counter-lg counter-inverse">
                <div class="counter-label text-uppercase font-size-16">we have</div>
                <div class="counter-number-group">
                  <span class="counter-number">365</span>
                  <span class="counter-icon margin-left-10"><i class="icon md-image" aria-hidden="true"></i></span>
                </div>
                <div class="counter-label text-uppercase font-size-16">pictures</div>
              </div>
            </div>
          </div>
         </div>
         <div class="col-lg-2 col-md-6">
          <!-- Widget -->
          <div class="widget">
            <div class="widget-content padding-20 bg-blue-600 white">
              <div class="counter counter-lg counter-inverse">
                <div class="counter-label text-uppercase font-size-16">we have</div>
                <div class="counter-number-group">
                  <span class="counter-number">365</span>
                  <span class="counter-icon margin-left-10"><i class="icon md-image" aria-hidden="true"></i></span>
                </div>
                <div class="counter-label text-uppercase font-size-16">pictures</div>
              </div>
            </div>
          </div>
         </div>
          <div class="col-lg-2 col-md-6">
          <!-- Widget -->
          <div class="widget">
            <div class="widget-content padding-20 bg-blue-600 white">
              <div class="counter counter-lg counter-inverse">
                <div class="counter-label text-uppercase font-size-16">we have</div>
                <div class="counter-number-group">
                  <span class="counter-number">365</span>
                  <span class="counter-icon margin-left-10"><i class="icon md-image" aria-hidden="true"></i></span>
                </div>
                <div class="counter-label text-uppercase font-size-16">pictures</div>
              </div>
            </div>
          </div>
        </div>
          <div class="col-lg-2 col-md-6">
          <!-- Widget -->
          <div class="widget">
            <div class="widget-content padding-20 bg-blue-600 white">
              <div class="counter counter-lg counter-inverse">
                <div class="counter-label text-uppercase font-size-16">we have</div>
                <div class="counter-number-group">
                  <span class="counter-number">365</span>
                  <span class="counter-icon margin-left-10"><i class="icon md-image" aria-hidden="true"></i></span>
                </div>
                <div class="counter-label text-uppercase font-size-16">pictures</div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-6">
          <!-- Widget -->
          <div class="widget">
            <div class="widget-content padding-20 bg-blue-600 white">
              <div class="counter counter-lg counter-inverse">
                <div class="counter-label text-uppercase font-size-16">we have</div>
                <div class="counter-number-group">
                  <span class="counter-number">365</span>
                  <span class="counter-icon margin-left-10"><i class="icon md-image" aria-hidden="true"></i></span>
                </div>
                <div class="counter-label text-uppercase font-size-16">pictures</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Page -->
</asp:Content>
