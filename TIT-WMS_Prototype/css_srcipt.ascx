﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="css_srcipt.ascx.vb" Inherits="TIT_WMS.css_srcipt" %>


<!-- Plugins -->
  <link rel="stylesheet" href="global/vendor/select2/select2.css">
  <link rel="stylesheet" href="global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.css">
  <link rel="stylesheet" href="global/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css">
  <link rel="stylesheet" href="global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="global/vendor/icheck/icheck.css">
  <link rel="stylesheet" href="global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="global/vendor/asrange/asRange.css">
  <link rel="stylesheet" href="global/vendor/asspinner/asSpinner.css">
  <link rel="stylesheet" href="global/vendor/clockpicker/clockpicker.css">
  <link rel="stylesheet" href="global/vendor/ascolorpicker/asColorPicker.css">
  <link rel="stylesheet" href="global/vendor/bootstrap-touchspin/bootstrap-touchspin.css">
  <link rel="stylesheet" href="global/vendor/card/card.css">
  <link rel="stylesheet" href="global/vendor/jquery-labelauty/jquery-labelauty.css">
  <link rel="stylesheet" href="global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
  <link rel="stylesheet" href="global/vendor/bootstrap-maxlength/bootstrap-maxlength.css">
  <link rel="stylesheet" href="global/vendor/jt-timepicker/jquery-timepicker.css">
  <link rel="stylesheet" href="global/vendor/jquery-strength/jquery-strength.css">
  <link rel="stylesheet" href="global/vendor/multi-select/multi-select.css">
  <link rel="stylesheet" href="global/vendor/typeahead-js/typeahead.css">
  <link rel="stylesheet" href="assets/examples/css/forms/advanced.css">

<!-- Core  -->
  <script src="global/vendor/jquery/jquery.js"></script>
  <script src="global/vendor/bootstrap/bootstrap.js"></script>
  <script src="global/vendor/animsition/animsition.js"></script>
  <script src="global/vendor/asscroll/jquery-asScroll.js"></script>
  <script src="global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="global/vendor/asscrollable/jquery.asScrollable.all.js"></script>
  <script src="global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="global/vendor/switchery/switchery.min.js"></script>
  <script src="global/vendor/intro-js/intro.js"></script>
  <script src="global/vendor/screenfull/screenfull.js"></script>
  <script src="global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="global/vendor/select2/select2.min.js"></script>
  <script src="global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.min.js"></script>
  <script src="global/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
  <script src="global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="global/vendor/icheck/icheck.min.js"></script>
  <script src="global/vendor/switchery/switchery.min.js"></script>
  <script src="global/vendor/asrange/jquery-asRange.min.js"></script>
  <script src="global/vendor/asspinner/jquery-asSpinner.min.js"></script>
  <script src="global/vendor/clockpicker/bootstrap-clockpicker.min.js"></script>
  <script src="global/vendor/ascolor/jquery-asColor.min.js"></script>
  <script src="global/vendor/asgradient/jquery-asGradient.min.js"></script>
  <script src="global/vendor/ascolorpicker/jquery-asColorPicker.min.js"></script>
  <script src="global/vendor/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
  <script src="global/vendor/jquery-knob/jquery.knob.js"></script>
  <script src="global/vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
  <script src="global/vendor/card/jquery.card.js"></script>
  <script src="global/vendor/jquery-labelauty/jquery-labelauty.js"></script>
  <script src="global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
  <script src="global/vendor/jt-timepicker/jquery.timepicker.min.js"></script>
  <script src="global/vendor/datepair-js/datepair.min.js"></script>
  <script src="global/vendor/datepair-js/jquery.datepair.min.js"></script>
  <script src="global/vendor/jquery-strength/jquery-strength.min.js"></script>
  <script src="global/vendor/multi-select/jquery.multi-select.js"></script>
  <script src="global/vendor/typeahead-js/bloodhound.min.js"></script>
  <script src="global/vendor/typeahead-js/typeahead.jquery.min.js"></script>
  <script src="global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
  <!-- Scripts -->
  <script src="global/js/core.js"></script>
  <script src="assets/js/site.js"></script>
  <script src="assets/js/sections/menu.js"></script>
  <script src="assets/js/sections/menubar.js"></script>
  <script src="assets/js/sections/sidebar.js"></script>
  <script src="global/js/configs/config-colors.js"></script>
  <script src="assets/js/configs/config-tour.js"></script>
  <script src="global/js/components/asscrollable.js"></script>
  <script src="global/js/components/animsition.js"></script>
  <script src="global/js/components/slidepanel.js"></script>
  <script src="global/js/components/switchery.js"></script>
  <script src="global/js/components/tabs.js"></script>
  <script src="global/js/components/select2.js"></script>
  <script src="global/js/components/bootstrap-tokenfield.js"></script>
  <script src="global/js/components/bootstrap-tagsinput.js"></script>
  <script src="global/js/components/bootstrap-select.js"></script>
  <script src="global/js/components/icheck.js"></script>
  <script src="global/js/components/switchery.js"></script>
  <script src="global/js/components/asrange.js"></script>
  <script src="global/js/components/asspinner.js"></script>
  <script src="global/js/components/clockpicker.js"></script>
  <script src="global/js/components/ascolorpicker.js"></script>
  <script src="global/js/components/bootstrap-maxlength.js"></script>
  <script src="global/js/components/jquery-knob.js"></script>
  <script src="global/js/components/bootstrap-touchspin.js"></script>
  <script src="global/js/components/card.js"></script>
  <script src="global/js/components/jquery-labelauty.js"></script>
  <script src="global/js/components/bootstrap-datepicker.js"></script>
  <script src="global/js/components/jt-timepicker.js"></script>
  <script src="global/js/components/datepair-js.js"></script>
  <script src="global/js/components/jquery-strength.js"></script>
  <script src="global/js/components/multi-select.js"></script>
  <script src="global/js/components/jquery-placeholder.js"></script>
  <script src="assets/examples/js/forms/advanced.js"></script>
  <script src="global/js/components/material.js"></script>

