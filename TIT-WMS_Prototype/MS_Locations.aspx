﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="MS_Locations.aspx.vb" Inherits="TIT_WMS.MS_Locations" %>

<%@ Register Src="~/css_srcipt.ascx" TagPrefix="uc1" TagName="css_srcipt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <link rel="stylesheet" href="global/vendor/filament-tablesaw/tablesaw.css">
  
  <title>Master-Location | WMS</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="site-menubar">
    <div class="site-menubar-body">
      <div>
        <div>
          <ul class="site-menu">
            <li class="site-menu-item has-sub">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-calendar-note" aria-hidden="true"></i>
                <span class="site-menu-title">Activity</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การรับสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">สร้างเอกสารใบรับสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">สถานะใบรับสินะค้า</span>
                        <span class="site-menu-arrow"></span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การเบิกสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">สร้างเอกสารใบเบิกสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">สถานะใบเบิกสินค้า</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การโอนสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/chartjs.html">
                        <span class="site-menu-title">สร้างเอกสารใบโอนสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/gauges.html">
                        <span class="site-menu-title">สถานะใบโอนสินค้า</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">ใบประกอบสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/chartjs.html">
                        <span class="site-menu-title">สร้างเอกสารใบประกอบสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/gauges.html">
                        <span class="site-menu-title">สถานะใบประกอบสินค้า</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
             <li class="site-menu-item has-sub">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-chart" aria-hidden="true"></i>
                <span class="site-menu-title">Status</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">Forms</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">General Elements</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">Editors</span>
                        <span class="site-menu-arrow"></span>
                      </a>
                      <ul class="site-menu-sub">
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-summernote.html">
                            <span class="site-menu-title">Summernote</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-markdown.html">
                            <span class="site-menu-title">Markdown</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-ace.html">
                            <span class="site-menu-title">Ace Editor</span>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/image-cropping.html">
                        <span class="site-menu-title">Image Cropping</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/file-uploads.html">
                        <span class="site-menu-title">File Uploads</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">Tables</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">Basic Tables</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">Bootstrap Tables</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/floatthead.html">
                        <span class="site-menu-title">floatThead</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">Chart</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/chartjs.html">
                        <span class="site-menu-title">Chart.js</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/gauges.html">
                        <span class="site-menu-title">Gauges</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/flot.html">
                        <span class="site-menu-title">Flot</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
             <li class="site-menu-item has-sub">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-home" aria-hidden="true"></i>
                <span class="site-menu-title">Warehouse</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">สรุปยอดคลังสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">General Elements</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">Editors</span>
                        <span class="site-menu-arrow"></span>
                      </a>
                      <ul class="site-menu-sub">
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-summernote.html">
                            <span class="site-menu-title">Summernote</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-markdown.html">
                            <span class="site-menu-title">Markdown</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-ace.html">
                            <span class="site-menu-title">Ace Editor</span>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/image-cropping.html">
                        <span class="site-menu-title">Image Cropping</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/file-uploads.html">
                        <span class="site-menu-title">File Uploads</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">ประวัติความเคลื่อนไหว</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">Basic Tables</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">Bootstrap Tables</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/floatthead.html">
                        <span class="site-menu-title">floatThead</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
             <li class="site-menu-item has-sub">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-check-circle-u" aria-hidden="true"></i>
                <span class="site-menu-title">Checking</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การตรวจนับสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">สร้างเอกสารใบตรวจนับ</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">ประวัติการตรวจนับสินค้า</span>
                      </a>  
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การตรวจ QC</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">สร้างเอกสาร QC</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">ประวัติการตรวจ QC</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
             <li class="site-menu-item has-sub active">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-settings" aria-hidden="true"></i>
                <span class="site-menu-title">Master</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub">
                  <a href="MS_ProductGroup.aspx">
                    <span class="site-menu-title">Product (ข้อมูลสินค้า)</span>
                  </a>
                </li>
                <li class="site-menu-item has-sub active">
                  <a href="MS_Warehouse.aspx">
                    <span class="site-menu-title">Warehouse (คลังสินค้า)</span>
                  </a>
                </li>
                <li class="site-menu-item has-sub">
                  <a href="MS_Staff.aspx">
                    <span class="site-menu-title">Staff (ข้อมูลพนักงาน)</span>
                  </a>
                </li>
                <li class="site-menu-item has-sub">
                  <a href="MS_Supplier.aspx">
                    <span class="site-menu-title">Contact (ข้อมูลผู้ติดต่อ)</span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <div class="page animsition">
    <div class="page-aside">
      <div class="page-aside-switch">
        <i class="icon md-chevron-left" aria-hidden="true"></i>
        <i class="icon md-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="page-aside-inner">
        <section class="page-aside-section">
          <h5 class="page-aside-title">Master (ข้อมูลพื้นฐาน)</h5>
          <div class="list-group">
            <a class="list-group-item" href="MS_Warehouse.aspx"><i class="icon md-home" aria-hidden="true"></i>Warehouse</a>
            <a class="list-group-item active" href="MS_ProductType.aspx"><i class="icon md-pin" aria-hidden="true"></i>Location</a>
            <a class="list-group-item" href="MS_TypePallet.aspx"><i class="icon md-file-text" aria-hidden="true"></i>Type Pallet</a>
            <a class="list-group-item" href="MS_Pallet.aspx"><i class="icon md-file-text" aria-hidden="true"></i>Pallet</a>
          </div>
        </section>
   
      </div>
    </div>
    <div class="page-main">
      <div class="page-header">
        <h4 class="page-title">Location</h4>
      </div>
      <div class="page-content container-fluid">
      <div class="row">
        <div class="col-lg-12">
         <!-- Panel Kitchen Sink -->
          <div class="panel">
            <header class="panel-heading">
              <h5 class="panel-title">
               
              </h5>
            </header>
            <div class="panel-body">
              <div class="col-sm-12">
                <div class="nav-tabs-horizontal nav-tabs-inverse">
                  <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                    <li class="active" role="presentation">
                      <a data-toggle="tab" href="#Floor" aria-controls="exampleTabsLeftInverseOne" role="tab">Floor</a>
                    </li>
                    <li role="presentation">
                      <a data-toggle="tab" href="#Room" aria-controls="exampleTabsLeftInverseTwo" role="tab">Room</a>
                    </li>
                    <li role="presentation">
                      <a data-toggle="tab" href="#Zone" aria-controls="exampleTabsLeftInverseThree" role="tab">Zone</a>
                    </li>
                    <li role="presentation">
                      <a data-toggle="tab" href="#Lock" aria-controls="exampleTabsLeftInverseTwo" role="tab">Lock</a>
                    </li>
                    <li role="presentation">
                      <a data-toggle="tab" href="#Row" aria-controls="exampleTabsLeftInverseThree" role="tab">Row</a>
                    </li>
                    <li role="presentation">
                      <a data-toggle="tab" href="#Shelf" aria-controls="exampleTabsLeftInverseThree" role="tab">Shelf </a>
                    </li>
                    <li role="presentation">
                      <a data-toggle="tab" href="#Deep" aria-controls="exampleTabsLeftInverseThree" role="tab">Deep </a>
                    </li>
                  </ul>
                  <div class="tab-content padding-20">
                    <div class="tab-pane active" id="Floor" role="tabpanel">
                      <button type="button" class="btn btn-warning" data-target="#AddFloor" data-toggle="modal"><i class="icon md-plus-circle-o"></i>Add</button>
                        <div class="btn-group pull-right" role="group">
                          <button type="button" class="btn btn-info dropdown-toggle" id="PFloor"
                          data-toggle="dropdown" aria-expanded="false"><i class="icon md-print"></i>
                            Print
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="exampleGroupDrop1" role="menu">
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file text-danger"></i>PDF</a></li>
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file-text text-success"></i>Excel</a></li>
                          </ul>
                        </div>
                     <div class="clearfix visible-md-block"></div><br /><br />
                      <table class="tablesaw table-striped table-bordered table-hover">
                        <thead>
                          <tr class="bg-blue-grey-100">
                            <th>Warehouse</th>
                            <th class="text-center">ID Floor</th>
                            <th class="text-center">Quantity</th>
                            <th class="text-center row-3 row-Edit" >Edit</th>
                            <th class="text-center row-4 row-Delete">Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Warehouse 1</td>
                            <td class="text-center">F01</td>
                            <td class="text-center">2</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 2</td>
                            <td class="text-center">F02</td>
                            <td class="text-center">1</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 3</td>
                            <td class="text-center">F03</td>
                            <td class="text-center">2</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 4</td>
                            <td class="text-center">F04</td>
                            <td class="text-center">1</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 5</td>
                            <td class="text-center">F05</td>
                            <td class="text-center">1</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                        </tbody>
                      </table>
                         <!-------------------------- Modal AddFloor--------------------------------->
                        <div class="modal fade" id="AddFloor" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-center">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Add Floor</h4>
                              </div>
                              <div class="modal-body">
                              <form>
                                <div class="row">
                                   <div class="col-sm-12">
                                       <label class="col-sm-3 control-label">Warehouse :</label>
                                      <div class="form-group  col-sm-8">
                                         <select data-plugin="selectpicker" title='Choose'>
                                          <option>Warehouse 1</option>
                                          <option>Warehouse 2</option>
                                          <option>Warehouse 3</option>
                                          <option>Warehouse 4</option>
                                          <option>Warehouse 5</option>
                                      </select>
                                      </div>
                                    </div>
                                   <div class="col-sm-12">
                                      <label class="col-sm-3 control-label">ID Floor :</label>
                                      <div class="form-group  col-sm-4">
                                         <input class="form-control text-right" placeholder="F01">
                                      </div>
                                    </div>
                                   <div class="clearfix"></div>
                                    <div class="col-sm-12">
                                      <label class="col-sm-3 control-label">Quantity :</label>
                                      <div class="form-group  col-sm-4">
                                         <input class="form-control text-right" placeholder="0">
                                      </div>
                                    </div>
                                  <div class="clearfix"></div>
                                     <div class="col-sm-12">
                                     <!-- Example Size -->
                                      <label class="col-sm-3 control-label">Active Status :</label>
                                      <div class="form-group  col-sm-4">
                                          <div class="pull-left margin-right-20">
                                        <input type="checkbox" id="OnFloor" name="inputiCheckBasicCheckboxes" data-plugin="switchery" data-color="#009933" checked />
                                      </div>
                                      <label class="padding-top-3" for="inputBasicOn">On</label>
                                      </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                              </form>
                            </div>
                              <div class="modal-footer">
                                 <button type="button" class="btn btn-primary">Submit </button>
                                 <button type="reset" class="btn btn-warning">Reset</button>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="Room" role="tabpanel">
                        <button type="button" class="btn btn-warning" data-target="#AddRoom" data-toggle="modal"><i class="icon md-plus-circle-o"></i>Add</button>
                        <div class="btn-group pull-right" role="group">
                          <button type="button" class="btn btn-info dropdown-toggle" id="PRoom"
                          data-toggle="dropdown" aria-expanded="false"><i class="icon md-print"></i>
                            Print
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="exampleGroupDrop1" role="menu">
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file text-danger"></i>PDF</a></li>
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file-text text-success"></i>Excel</a></li>
                          </ul>
                        </div>
                     <div class="clearfix visible-md-block"></div><br /><br />
                      <table class="tablesaw table-striped table-bordered table-hover">
                        <thead>
                          <tr class="bg-blue-grey-100">
                            <th>Warehouse</th>
                            <th class="text-center">ID/Code Room</th>
                            <th class="text-center">Name</th>
                            <th class="text-center row-3 row-Edit" >Edit</th>
                            <th class="text-center row-4 row-Delete">Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Warehouse 1</td>
                            <td class="text-center">R01</td>
                            <td class="text-center">Room 1</td>
                            <td class="text-center"><a class="icon md-edit"></a></td>
                            <td class="text-center"><a class="icon md-delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 1</td>
                            <td class="text-center">R01</td>
                            <td class="text-center">Room 1</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 2</td>
                            <td class="text-center">R02</td>
                            <td class="text-center">Room 2</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 3</td>
                            <td class="text-center">R03</td>
                            <td class="text-center">Room 3</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 4</td>
                            <td class="text-center">R04</td>
                            <td class="text-center">Room 4</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 5</td>
                            <td class="text-center">R05</td>
                            <td class="text-center">Room 5</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 6</td>
                            <td class="text-center">R06</td>
                            <td class="text-center">Room 6</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                        </tbody>
                      </table>
                      <!-------------------------- Modal AddOther--------------------------------->
                        <div class="modal fade" id="AddRoom" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-center">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Add Room</h4>
                              </div>
                              <div class="modal-body">
                              <form>
                                <div class="row">
                                  <div class="col-sm-12">
                                   <label class="col-sm-3 control-label">Warehouse :</label>
                                  <div class="form-group  col-sm-8">
                                     <select data-plugin="selectpicker" title='Choose'>
                                      <option>Warehouse 1</option>
                                      <option>Warehouse 2</option>
                                      <option>Warehouse 3</option>
                                      <option>Warehouse 4</option>
                                  </select>
                                  </div>
                                </div>
                                  <div class="col-sm-12">
                                   <label class="col-sm-3 control-label">Location :</label>
                                  <div class="form-group  col-sm-4">
                                     <select data-plugin="selectpicker" title='Floor'>
                                      <option>Floor 1</option>
                                      <option>Floor 2</option>
                                      <option>Floor 3</option>
                                      <option>Floor 4</option>
                                  </select>
                                  </div>
                                </div>
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">ID/Code Room :</label>
                                   <div class="form-group  col-sm-4">
                                     <input class="form-control" placeholder="ID/Code">
                                     </div>
                                  </div>
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Name :</label>
                                   <div class="form-group  col-sm-8">
                                     <input class="form-control" placeholder="Name">
                                     </div>
                                  </div>
                                  
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Width :</label>
                                   <div class="form-group  col-sm-4">
                                     <input class="form-control text-right" placeholder="0">
                                     </div>
                                     <div class="form-group  col-sm-3">
                                     <select data-plugin="selectpicker" title='Unit'>
                                      <option>c3</option>
                                      <option>m</option>
                                      <option>cm 3</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Length :</label>
                                   <div class="form-group  col-sm-4">
                                     <input class="form-control text-right" placeholder="0">
                                     </div>
                                     <div class="form-group  col-sm-3">
                                     <select data-plugin="selectpicker" title='Unit'>
                                      <option>c3</option>
                                      <option>m</option>
                                      <option>cm 3</option>
                                      </select>
                                    </div>
                                  </div>
                                 <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Active Status :</label>
                                  <div class="form-group  col-sm-3">
                                      <div class="pull-left margin-right-20">
                                    <input type="checkbox" id="OnRoom" name="inputiCheckBasicCheckboxes" data-plugin="switchery" data-color="#009933" checked />
                                  </div>
                                  <label class="padding-top-3" for="inputBasicOn">On</label>
                                  </div>
                                </div>
                                </div>
                              </form>
                            </div>
                              <div class="modal-footer">
                                 <button type="button" class="btn btn-primary">Submit </button>
                                 <button type="reset" class="btn btn-warning">Reset</button>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="Zone" role="tabpanel">
                        <button type="button" class="btn btn-warning" data-target="#AddZone" data-toggle="modal"><i class="icon md-plus-circle-o"></i>Add</button>
                        <div class="btn-group pull-right" role="group">
                          <button type="button" class="btn btn-info dropdown-toggle" id="PZone"
                          data-toggle="dropdown" aria-expanded="false"><i class="icon md-print"></i>
                            Print
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="exampleGroupDrop1" role="menu">
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file text-danger"></i>PDF</a></li>
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file-text text-success"></i>Excel</a></li>
                          </ul>
                        </div>
                     <div class="clearfix visible-md-block"></div><br /><br />
                      <table class="tablesaw table-striped table-bordered table-hover">
                        <thead>
                          <tr class="bg-blue-grey-100">
                            <th>Warehouse</th>
                            <th class="text-center">ID/Code Zone</th>
                            <th class="text-center">Name</th>
                            <th class="text-center row-3 row-Edit" >Edit</th>
                            <th class="text-center row-4 row-Delete">Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Warehouse 1</td>
                            <td class="text-center">R01</td>
                            <td class="text-center">Zone 1</td>
                            <td class="text-center"><a class="icon md-edit"></a></td>
                            <td class="text-center"><a class="icon md-delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 2</td>
                            <td class="text-center">R02</td>
                            <td class="text-center">Zone 2</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 3</td>
                            <td class="text-center">R03</td>
                            <td class="text-center">Zone 3</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 4</td>
                            <td class="text-center">R04</td>
                            <td class="text-center">Zone 4</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 5</td>
                            <td class="text-center">Z05</td>
                            <td class="text-center">Zone 5</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 6</td>
                            <td class="text-center">Z06</td>
                            <td class="text-center">Zone 6</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                        </tbody>
                      </table>
                      <!-------------------------- Modal AddOther--------------------------------->
                        <div class="modal fade" id="AddZone" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-center">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Add Zone</h4>
                              </div>
                              <div class="modal-body">
                              <form>
                                <div class="row">
                                  <div class="col-sm-12">
                                   <label class="col-sm-3 control-label">Warehouse :</label>
                                  <div class="form-group  col-sm-8">
                                     <select data-plugin="selectpicker" title='Choose'>
                                      <option>Warehouse 1</option>
                                      <option>Warehouse 2</option>
                                      <option>Warehouse 3</option>
                                      <option>Warehouse 4</option>
                                  </select>
                                  </div>
                                </div>
                                  <div class="col-sm-12">
                                   <label class="col-sm-3 control-label">Location :</label>
                                  <div class="form-group  col-sm-4">
                                     <select data-plugin="selectpicker" title='Floor'>
                                      <option>Floor 1</option>
                                      <option>Floor 2</option>
                                      <option>Floor 3</option>
                                      <option>Floor 4</option>
                                  </select>
                                  </div><div class="form-group  col-sm-4">
                                     <select data-plugin="selectpicker" title='Room'>
                                      <option>Room 1</option>
                                      <option>Room 2</option>
                                      <option>Room 3</option>
                                      <option>Room 4</option>
                                  </select>
                                  </div>
                                </div>
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">ID/Code Zone :</label>
                                   <div class="form-group  col-sm-4">
                                     <input class="form-control" placeholder="ID/Code">
                                     </div>
                                  </div>
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Name :</label>
                                   <div class="form-group  col-sm-9">
                                     <input class="form-control" placeholder="Name">
                                     </div>
                                  </div>
                                  
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Width :</label>
                                   <div class="form-group  col-sm-4">
                                     <input class="form-control text-right" placeholder="0">
                                     </div>
                                     <div class="form-group  col-sm-3">
                                     <select data-plugin="selectpicker" title='Unit'>
                                      <option>c3</option>
                                      <option>m</option>
                                      <option>cm 3</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Length :</label>
                                   <div class="form-group  col-sm-4">
                                     <input class="form-control text-right" placeholder="0">
                                     </div>
                                     <div class="form-group  col-sm-3">
                                     <select data-plugin="selectpicker" title='Unit'>
                                      <option>c3</option>
                                      <option>m</option>
                                      <option>cm 3</option>
                                      </select>
                                    </div>
                                  </div>
                                 <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Active Status :</label>
                                  <div class="form-group  col-sm-3">
                                      <div class="pull-left margin-right-20">
                                    <input type="checkbox" id="OnZone" name="inputiCheckBasicCheckboxes" data-plugin="switchery" data-color="#009933" checked />
                                  </div>
                                  <label class="padding-top-3" for="inputBasicOn">On</label>
                                  </div>
                                </div>
                                </div>
                              </form>
                            </div>
                              <div class="modal-footer">
                                 <button type="button" class="btn btn-primary">Submit </button>
                                 <button type="reset" class="btn btn-warning">Reset</button>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="Lock" role="tabpanel">
                        <button type="button" class="btn btn-warning" data-target="#AddLock" data-toggle="modal"><i class="icon md-plus-circle-o"></i>Add</button>
                        <div class="btn-group pull-right" role="group">
                          <button type="button" class="btn btn-info dropdown-toggle" id="PLock"
                          data-toggle="dropdown" aria-expanded="false"><i class="icon md-print"></i>
                            Print
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="exampleGroupDrop1" role="menu">
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file text-danger"></i>PDF</a></li>
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file-text text-success"></i>Excel</a></li>
                          </ul>
                        </div>
                     <div class="clearfix visible-md-block"></div><br /><br />
                      <table class="tablesaw table-striped table-bordered table-hover">
                        <thead>
                          <tr class="bg-blue-grey-100">
                            <th>Warehouse</th>
                            <th class="text-center">ID/Code Lock</th>
                            <th class="text-center">Name</th>
                            <th class="text-center row-3 row-Edit" >Edit</th>
                            <th class="text-center row-4 row-Delete">Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Warehouse 1</td>
                            <td class="text-center">L01</td>
                            <td class="text-center">Lock 1</td>
                            <td class="text-center"><a class="icon md-edit"></a></td>
                            <td class="text-center"><a class="icon md-delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 2</td>
                            <td class="text-center">L02</td>
                            <td class="text-center">Lock 2</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 3</td>
                            <td class="text-center">L03</td>
                            <td class="text-center">Lock 3</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 4</td>
                            <td class="text-center">L04</td>
                            <td class="text-center">Lock 4</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 5</td>
                            <td class="text-center">L05</td>
                            <td class="text-center">Lock 5</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 6</td>
                            <td class="text-center">L06</td>
                            <td class="text-center">Lock 6</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                        </tbody>
                      </table>
                      <!-------------------------- Modal AddOther--------------------------------->
                        <div class="modal fade" id="AddLock" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-center">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Add Lock</h4>
                              </div>
                              <div class="modal-body">
                              <form>
                                <div class="row">
                                  <div class="col-sm-12">
                                   <label class="col-sm-3 control-label">Warehouse :</label>
                                  <div class="form-group  col-sm-8">
                                     <select data-plugin="selectpicker" title='Choose'>
                                      <option>Warehouse 1</option>
                                      <option>Warehouse 2</option>
                                      <option>Warehouse 3</option>
                                      <option>Warehouse 4</option>
                                  </select>
                                  </div>
                                </div>
                                <div class="col-sm-12">
                                   <label class="col-sm-3 control-label">Location :</label>
                                  <div class="form-group  col-sm-4">
                                     <select data-plugin="selectpicker" title='Floor'>
                                      <option>Floor 1</option>
                                      <option>Floor 2</option>
                                      <option>Floor 3</option>
                                      <option>Floor 4</option>
                                  </select>
                                  </div>
                                  <div class="form-group  col-sm-4">
                                     <select data-plugin="selectpicker" title='Room'>
                                      <option>Floor 1</option>
                                      <option>Floor 2</option>
                                      <option>Floor 3</option>
                                      <option>Floor 4</option>
                                  </select>
                                  </div>
                                </div>
                                
                                  <div class="col-sm-12">
                                   <label class="col-sm-3 control-label"></label>
                                  <div class="form-group  col-sm-4">
                                     <select data-plugin="selectpicker" title='Zone'>
                                      <option>Floor 1</option>
                                      <option>Floor 2</option>
                                      <option>Floor 3</option>
                                      <option>Floor 4</option>
                                  </select>
                                  </div>
                                </div>
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">ID/Code Lock :</label>
                                   <div class="form-group  col-sm-4">
                                     <input class="form-control" placeholder="ID/Code">
                                     </div>
                                  </div>
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Name :</label>
                                   <div class="form-group  col-sm-9">
                                     <input class="form-control" placeholder="Name">
                                     </div>
                                  </div>
                                  
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Width :</label>
                                   <div class="form-group  col-sm-4">
                                     <input class="form-control text-right" placeholder="0">
                                     </div>
                                     <div class="form-group  col-sm-3">
                                     <select data-plugin="selectpicker" title='Unit'>
                                      <option>c3</option>
                                      <option>m</option>
                                      <option>cm 3</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Length :</label>
                                   <div class="form-group  col-sm-4">
                                     <input class="form-control text-right" placeholder="0">
                                     </div>
                                     <div class="form-group  col-sm-3">
                                     <select data-plugin="selectpicker" title='Unit'>
                                      <option>c3</option>
                                      <option>m</option>
                                      <option>cm 3</option>
                                      </select>
                                    </div>
                                  </div>
                                 <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Active Status :</label>
                                  <div class="form-group  col-sm-3">
                                      <div class="pull-left margin-right-20">
                                    <input type="checkbox" id="OnLock" name="inputiCheckBasicCheckboxes" data-plugin="switchery" data-color="#009933" checked />
                                  </div>
                                  <label class="padding-top-3" for="inputBasicOn">On</label>
                                  </div>
                                </div>
                                </div>
                              </form>
                            </div>
                              <div class="modal-footer">
                                 <button type="button" class="btn btn-primary">Submit </button>
                                 <button type="reset" class="btn btn-warning">Reset</button>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="Row" role="tabpanel">
                        <button type="button" class="btn btn-warning" data-target="#AddRow" data-toggle="modal"><i class="icon md-plus-circle-o"></i>Add</button>
                        <div class="btn-group pull-right" role="group">
                          <button type="button" class="btn btn-info dropdown-toggle" id="PRow"
                          data-toggle="dropdown" aria-expanded="false"><i class="icon md-print"></i>
                            Print
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="exampleGroupDrop1" role="menu">
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file text-danger"></i>PDF</a></li>
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file-text text-success"></i>Excel</a></li>
                          </ul>
                        </div>
                     <div class="clearfix visible-md-block"></div><br /><br />
                      <table class="tablesaw table-striped table-bordered table-hover">
                        <thead>
                          <tr class="bg-blue-grey-100">
                            <th>Warehouse</th>
                            <th class="text-center">ID/Code Row</th>
                            <th class="text-center">Name</th>
                            <th class="text-center row-3 row-Edit" >Edit</th>
                            <th class="text-center row-4 row-Delete">Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Warehouse 1</td>
                            <td class="text-center">Row01</td>
                            <td class="text-center">Row 1</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 2</td>
                            <td class="text-center">Row02</td>
                            <td class="text-center">Row 2</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 3</td>
                            <td class="text-center">Row03</td>
                            <td class="text-center">Row 3</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 4</td>
                            <td class="text-center">Row04</td>
                            <td class="text-center">Row 4</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 5</td>
                            <td class="text-center">Row05</td>
                            <td class="text-center">Row 5</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 6</td>
                            <td class="text-center">Row06</td>
                            <td class="text-center">Row 6</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                        </tbody>
                      </table>
                      <!-------------------------- Modal AddOther--------------------------------->
                        <div class="modal fade" id="AddRow" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-center">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Add Row</h4>
                              </div>
                              <div class="modal-body">
                              <form>
                                <div class="row">
                                  <div class="col-sm-12">
                                   <label class="col-sm-3 control-label">Warehouse :</label>
                                  <div class="form-group  col-sm-8">
                                     <select data-plugin="selectpicker" title='Choose'>
                                      <option>Warehouse 1</option>
                                      <option>Warehouse 2</option>
                                      <option>Warehouse 3</option>
                                      <option>Warehouse 4</option>
                                  </select>
                                  </div>
                                </div>
                                <div class="col-sm-12">
                                 <label class="col-sm-3 control-label">Floor :</label>
                                  <div class="form-group  col-sm-4">
                                     <select data-plugin="selectpicker" title='Choose'>
                                      <option>Floor 1</option>
                                      <option>Floor 2</option>
                                      <option>Floor 3</option>
                                      <option>Floor 4</option>
                                  </select>
                                  </div>
                                  <div class="form-group  col-sm-4">
                                     <select data-plugin="selectpicker" title='Room'>
                                      <option>Room 1</option>
                                      <option>Room 2</option>
                                      <option>Room 3</option>
                                      <option>Room 4</option>
                                     </select>
                                  </div>
                                </div>
                                <div class="col-sm-12">
                                 <label class="col-sm-3 control-label"></label>
                                  <div class="form-group  col-sm-4">
                                     <select data-plugin="selectpicker" title='Zone'>
                                      <option>Zone 1</option>
                                      <option>Zone 2</option>
                                      <option>Zone 3</option>
                                      <option>Zone 4</option>
                                  </select>
                                  </div>
                                  <div class="form-group  col-sm-4">
                                     <select data-plugin="selectpicker" title='Lock'>
                                      <option>Lock 1</option>
                                      <option>Lock 2</option>
                                      <option>Lock 3</option>
                                      <option>Lock 4</option>
                                  </select>
                                  </div>
                                </div>
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">ID/Code Row :</label>
                                   <div class="form-group  col-sm-4">
                                     <input class="form-control" placeholder="ID/Code">
                                     </div>
                                  </div>
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Name :</label>
                                   <div class="form-group  col-sm-8">
                                     <input class="form-control" placeholder="Name">
                                     </div>
                                  </div>
                                  
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Width :</label>
                                   <div class="form-group  col-sm-4">
                                     <input class="form-control text-right" placeholder="0">
                                     </div>
                                     <div class="form-group  col-sm-3">
                                     <select data-plugin="selectpicker" title='Unit'>
                                      <option>c3</option>
                                      <option>m</option>
                                      <option>cm 3</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Length :</label>
                                   <div class="form-group  col-sm-4">
                                     <input class="form-control text-right" placeholder="0">
                                     </div>
                                     <div class="form-group  col-sm-3">
                                     <select data-plugin="selectpicker" title='Unit'>
                                      <option>c3</option>
                                      <option>m</option>
                                      <option>cm 3</option>
                                      </select>
                                    </div>
                                  </div>
                                 <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Active Status :</label>
                                  <div class="form-group  col-sm-3">
                                      <div class="pull-left margin-right-10">
                                    <input type="checkbox" id="OnRow" name="inputiCheckBasicCheckboxes" data-plugin="switchery" data-color="#009933" checked />
                                  </div>
                                  <label class="padding-top-3" for="inputBasicOn">On</label>
                                  </div>
                                </div>
                                </div>
                              </form>
                            </div>
                              <div class="modal-footer">
                                 <button type="button" class="btn btn-primary">Submit </button>
                                 <button type="reset" class="btn btn-warning">Reset</button>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="Shelf" role="tabpanel">
                      <button type="button" class="btn btn-warning" data-target="#AddShelf" data-toggle="modal"><i class="icon md-plus-circle-o"></i>Add</button>
                        <div class="btn-group pull-right" role="group">
                          <button type="button" class="btn btn-info dropdown-toggle" id="PShelf"
                          data-toggle="dropdown" aria-expanded="false"><i class="icon md-print"></i>
                            Print
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="exampleGroupDrop1" role="menu">
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file text-danger"></i>PDF</a></li>
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file-text text-success"></i>Excel</a></li>
                          </ul>
                        </div>
                     <div class="clearfix visible-md-block"></div><br /><br />
                      <table class="tablesaw table-striped table-bordered table-hover">
                        <thead>
                          <tr class="bg-blue-grey-100">
                            <th>Warehouse</th>
                            <th class="text-center">ID/Code Shelf</th>
                            <th class="text-center">Name</th>
                            <th class="text-center row-3 row-Edit" >Edit</th>
                            <th class="text-center row-4 row-Delete">Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Warehouse 1</td>
                            <td class="text-center">S01</td>
                            <td class="text-center">Shelf 1</td>
                            <td class="text-center"><a class="icon md-edit"></a></td>
                            <td class="text-center"><a class="icon md-delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 2</td>
                            <td class="text-center">S02</td>
                            <td class="text-center">Shelf 2</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 3</td>
                            <td class="text-center">S03</td>
                            <td class="text-center">Shelf 3</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 4</td>
                            <td class="text-center">S04</td>
                            <td class="text-center">Shelf 4</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 5</td>
                            <td class="text-center">S05</td>
                            <td class="text-center">Shelf 5</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 6</td>
                            <td class="text-center">S06</td>
                            <td class="text-center">Shelf 6</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                        </tbody>
                      </table>
                      <!-------------------------- Modal AddOther--------------------------------->
                        <div class="modal fade" id="AddShelf" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-center">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Add Shelf</h4>
                              </div>
                              <div class="modal-body">
                              <form>
                                <div class="row">
                                  <div class="col-sm-12">
                                   <label class="col-sm-3 control-label">Warehouse :</label>
                                  <div class="form-group  col-sm-8">
                                     <select data-plugin="selectpicker" title='Choose'>
                                      <option>Warehouse 1</option>
                                      <option>Warehouse 2</option>
                                      <option>Warehouse 3</option>
                                      <option>Warehouse 4</option>
                                  </select>
                                  </div>
                                </div>
                                  <div class="col-sm-12">
                                   <label class="col-sm-3 control-label">Location :</label>
                                  <div class="form-group  col-sm-4">
                                     <select data-plugin="selectpicker" title='Floor'>
                                      <option>Floor 1</option>
                                      <option>Floor 2</option>
                                      <option>Floor 3</option>
                                      <option>Floor 4</option>
                                  </select>
                                  </div>
                                  <div class="form-group  col-sm-4">
                                     <select data-plugin="selectpicker" title='Room'>
                                      <option>Room 1</option>
                                      <option>Room 2</option>
                                      <option>Room 3</option>
                                      <option>Room 4</option>
                                  </select>
                                  </div>
                                </div>
                                  <div class="col-sm-12">
                                   <label class="col-sm-3 control-label"></label>
                                  <div class="form-group  col-sm-4">
                                     <select data-plugin="selectpicker" title='Zone'>
                                      <option>Zone 1</option>
                                      <option>Zone 2</option>
                                      <option>Zone 3</option>
                                      <option>Zone 4</option>
                                  </select>
                                  </div>
                                  <div class="form-group  col-sm-4">
                                     <select data-plugin="selectpicker" title='Lock'>
                                      <option>Lock 1</option>
                                      <option>Lock 2</option>
                                      <option>Lock 3</option>
                                      <option>Lock 4</option>
                                  </select>
                                  </div>
                                </div>
                                 <div class="col-sm-12">
                                <label class="col-sm-3 control-label"></label>
                                  <div class="form-group  col-sm-4">
                                     <select data-plugin="selectpicker" title='Row'>
                                      <option>Row 1</option>
                                      <option>Row 2</option>
                                      <option>Row 3</option>
                                      <option>Row 4</option>
                                  </select>
                                  </div>
                                </div>
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">ID/Code Shelf :</label>
                                   <div class="form-group  col-sm-4">
                                     <input class="form-control" placeholder="ID/Code">
                                     </div>
                                  </div>
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Name :</label>
                                   <div class="form-group  col-sm-9">
                                     <input class="form-control" placeholder="Name">
                                     </div>
                                  </div>
                                  
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Width :</label>
                                   <div class="form-group  col-sm-4">
                                     <input class="form-control text-right" placeholder="0">
                                     </div>
                                     <div class="form-group  col-sm-3">
                                     <select data-plugin="selectpicker" title='Unit'>
                                      <option>c3</option>
                                      <option>m</option>
                                      <option>cm 3</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Length :</label>
                                   <div class="form-group  col-sm-4">
                                     <input class="form-control text-right" placeholder="0">
                                     </div>
                                     <div class="form-group  col-sm-3">
                                     <select data-plugin="selectpicker" title='Unit'>
                                      <option>c3</option>
                                      <option>m</option>
                                      <option>cm 3</option>
                                      </select>
                                    </div>
                                  </div>
                                 <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Active Status :</label>
                                  <div class="form-group  col-sm-3">
                                      <div class="pull-left margin-right-20">
                                    <input type="checkbox" id="OnSelf" name="inputiCheckBasicCheckboxes" data-plugin="switchery" data-color="#009933" checked />
                                  </div>
                                  <label class="padding-top-3" for="inputBasicOn">On</label>
                                  </div>
                                </div>
                                </div>
                              </form>
                            </div>
                              <div class="modal-footer">
                                 <button type="button" class="btn btn-primary">Submit </button>
                                 <button type="reset" class="btn btn-warning">Reset</button>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="Deep" role="tabpanel">
                      <button type="button" class="btn btn-warning" data-target="#AddDeep" data-toggle="modal"><i class="icon md-plus-circle-o"></i>Add</button>
                        <div class="btn-group pull-right" role="group">
                          <button type="button" class="btn btn-info dropdown-toggle" id="PDeep"
                          data-toggle="dropdown" aria-expanded="false"><i class="icon md-print"></i>
                            Print
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="exampleGroupDrop1" role="menu">
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file text-danger"></i>PDF</a></li>
                            <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon md-file-text text-success"></i>Excel</a></li>
                          </ul>
                        </div>
                     <div class="clearfix visible-md-block"></div><br /><br />
                      <table class="tablesaw table-striped table-bordered table-hover">
                        <thead>
                          <tr class="bg-blue-grey-100">
                            <th>Warehouse</th>
                            <th class="text-center">ID/Code Deep</th>
                            <th class="text-center">Name</th>
                            <th class="text-center row-3 row-Edit" >Edit</th>
                            <th class="text-center row-4 row-Delete">Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Warehouse 1</td>
                            <td class="text-center">D01</td>
                            <td class="text-center">Deep 1</td>
                            <td class="text-center"><a class="icon md-edit"></a></td>
                            <td class="text-center"><a class="icon md-delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 2</td>
                            <td class="text-center">D02</td>
                            <td class="text-center">Deep 2</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 3</td>
                            <td class="text-center">D03</td>
                            <td class="text-center">Deep 3</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 4</td>
                            <td class="text-center">D04</td>
                            <td class="text-center">Deep 4</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 5</td>
                            <td class="text-center">D05</td>
                            <td class="text-center">Deep 5</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                          <tr>
                            <td>Warehouse 6</td>
                            <td class="text-center">D06</td>
                            <td class="text-center">Deep 6</td>
                            <td class="text-center"><a class="icon md-edit"data-toggle="tooltip" data-original-title="Edit"></a></td>
                            <td class="text-center"><a class="icon md-delete" data-toggle="tooltip" data-original-title="Delete"></a></td>
                          </tr>
                        </tbody>
                      </table>
                      <!-------------------------- Modal AddOther--------------------------------->
                        <div class="modal fade" id="AddDeep" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-center">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Add Deep</h4>
                              </div>
                              <div class="modal-body">
                              <form>
                                <div class="row">
                                  <div class="col-sm-12">
                                   <label class="col-sm-3 control-label">Warehouse :</label>
                                  <div class="form-group  col-sm-8">
                                     <select data-plugin="selectpicker" title='Choose'>
                                      <option>Warehouse 1</option>
                                      <option>Warehouse 2</option>
                                      <option>Warehouse 3</option>
                                      <option>Warehouse 4</option>
                                  </select>
                                  </div>
                                </div>
                                  <div class="col-sm-12">
                                   <label class="col-sm-3 control-label">Location :</label>
                                  <div class="form-group  col-sm-4">
                                     <select data-plugin="selectpicker" title='Floor'>
                                      <option>Floor 1</option>
                                      <option>Floor 2</option>
                                      <option>Floor 3</option>
                                      <option>Floor 4</option>
                                  </select>
                                  </div>
                                  <div class="form-group  col-sm-4">
                                     <select data-plugin="selectpicker" title='Room'>
                                      <option>Room 1</option>
                                      <option>Room 2</option>
                                      <option>Room 3</option>
                                      <option>Room 4</option>
                                  </select>
                                  </div>
                                </div>
                                  <div class="col-sm-12">
                                   <label class="col-sm-3 control-label"></label>
                                  <div class="form-group  col-sm-4">
                                     <select data-plugin="selectpicker" title='Zone'>
                                      <option>Zone 1</option>
                                      <option>Zone 2</option>
                                      <option>Zone 3</option>
                                      <option>Zone 4</option>
                                  </select>
                                  </div>
                                  <div class="form-group  col-sm-4">
                                     <select data-plugin="selectpicker" title='Lock'>
                                      <option>Lock 1</option>
                                      <option>Lock 2</option>
                                      <option>Lock 3</option>
                                      <option>Lock 4</option>
                                  </select>
                                  </div>
                                </div>
                                 <div class="col-sm-12">
                                <label class="col-sm-3 control-label"></label>
                                  <div class="form-group  col-sm-4">
                                     <select data-plugin="selectpicker" title='Row'>
                                      <option>Row 1</option>
                                      <option>Row 2</option>
                                      <option>Row 3</option>
                                      <option>Row 4</option>
                                      </select>
                                  </div>
                                  <div class="form-group  col-sm-4">
                                     <select data-plugin="selectpicker" title='Shelf'>
                                      <option>Shelf 1</option>
                                      <option>Shelf 2</option>
                                      <option>Shelf 3</option>
                                      <option>Shelf 4</option>
                                  </select>
                                  </div>
                                </div>
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">ID/Code Deep :</label>
                                   <div class="form-group  col-sm-4">
                                     <input class="form-control" placeholder="ID/Code">
                                     </div>
                                  </div>
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Name :</label>
                                   <div class="form-group  col-sm-9">
                                     <input class="form-control" placeholder="Name">
                                     </div>
                                  </div>
                                  
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Width :</label>
                                   <div class="form-group  col-sm-4">
                                     <input class="form-control text-right" placeholder="0">
                                     </div>
                                     <div class="form-group  col-sm-3">
                                     <select data-plugin="selectpicker" title='Unit'>
                                      <option>c3</option>
                                      <option>m</option>
                                      <option>cm 3</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Length :</label>
                                   <div class="form-group  col-sm-4">
                                     <input class="form-control text-right" placeholder="0">
                                     </div>
                                     <div class="form-group  col-sm-3">
                                     <select data-plugin="selectpicker" title='Unit'>
                                      <option>c3</option>
                                      <option>m</option>
                                      <option>cm 3</option>
                                      </select>
                                    </div>
                                  </div>
                                 <div class="col-sm-12">
                                  <label class="col-sm-3 control-label">Active Status :</label>
                                  <div class="form-group  col-sm-3">
                                      <div class="pull-left margin-right-20">
                                    <input type="checkbox" id="OnSelf" name="inputiCheckBasicCheckboxes" data-plugin="switchery" data-color="#009933" checked />
                                  </div>
                                  <label class="padding-top-3" for="inputBasicOn">On</label>
                                  </div>
                                </div>
                                </div>
                              </form>
                            </div>
                              <div class="modal-footer">
                                 <button type="button" class="btn btn-primary">Submit </button>
                                 <button type="reset" class="btn btn-warning">Reset</button>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>

                  </div>
                 </div>
                </div> 
              </div>
           <!-- End Panel Kitchen Sink -->
           </div>
         </div> 
        </div>
      </div>
    </div>
  </div>
    <uc1:css_srcipt runat="server" ID="css_srcipt" />
</asp:Content>
