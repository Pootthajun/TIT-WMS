﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="MS_AddTag.aspx.vb" Inherits="TIT_WMS.MS_AddTag" %>

<%@ Register Src="~/css_srcipt.ascx" TagPrefix="uc1" TagName="css_srcipt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  
  <title>Master-Tag | WMS</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="site-menubar">
    <div class="site-menubar-body">
      <div>
        <div>
          <ul class="site-menu">
            <li class="site-menu-item has-sub">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-calendar-note" aria-hidden="true"></i>
                <span class="site-menu-title">Activity</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การรับสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">สร้างเอกสารใบรับสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">สถานะใบรับสินะค้า</span>
                        <span class="site-menu-arrow"></span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การเบิกสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">สร้างเอกสารใบเบิกสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">สถานะใบเบิกสินค้า</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การโอนสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/chartjs.html">
                        <span class="site-menu-title">สร้างเอกสารใบโอนสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/gauges.html">
                        <span class="site-menu-title">สถานะใบโอนสินค้า</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">ใบประกอบสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/chartjs.html">
                        <span class="site-menu-title">สร้างเอกสารใบประกอบสินค้า</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/gauges.html">
                        <span class="site-menu-title">สถานะใบประกอบสินค้า</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
             <li class="site-menu-item has-sub">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-chart" aria-hidden="true"></i>
                <span class="site-menu-title">Status</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">Forms</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">General Elements</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">Editors</span>
                        <span class="site-menu-arrow"></span>
                      </a>
                      <ul class="site-menu-sub">
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-summernote.html">
                            <span class="site-menu-title">Summernote</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-markdown.html">
                            <span class="site-menu-title">Markdown</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-ace.html">
                            <span class="site-menu-title">Ace Editor</span>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/image-cropping.html">
                        <span class="site-menu-title">Image Cropping</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/file-uploads.html">
                        <span class="site-menu-title">File Uploads</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">Tables</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">Basic Tables</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">Bootstrap Tables</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/floatthead.html">
                        <span class="site-menu-title">floatThead</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">Chart</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/chartjs.html">
                        <span class="site-menu-title">Chart.js</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/gauges.html">
                        <span class="site-menu-title">Gauges</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="charts/flot.html">
                        <span class="site-menu-title">Flot</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
             <li class="site-menu-item has-sub">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-home" aria-hidden="true"></i>
                <span class="site-menu-title">Warehouse</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">สรุปยอดคลังสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">General Elements</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">Editors</span>
                        <span class="site-menu-arrow"></span>
                      </a>
                      <ul class="site-menu-sub">
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-summernote.html">
                            <span class="site-menu-title">Summernote</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-markdown.html">
                            <span class="site-menu-title">Markdown</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="forms/editor-ace.html">
                            <span class="site-menu-title">Ace Editor</span>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/image-cropping.html">
                        <span class="site-menu-title">Image Cropping</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/file-uploads.html">
                        <span class="site-menu-title">File Uploads</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">ประวัติความเคลื่อนไหว</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">Basic Tables</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">Bootstrap Tables</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/floatthead.html">
                        <span class="site-menu-title">floatThead</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
             <li class="site-menu-item has-sub">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-check-circle-u" aria-hidden="true"></i>
                <span class="site-menu-title">Checking</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การตรวจนับสินค้า</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="forms/general.html">
                        <span class="site-menu-title">สร้างเอกสารใบตรวจนับ</span>
                      </a>
                    </li>
                    <li class="site-menu-item has-sub">
                      <a href="javascript:void(0)">
                        <span class="site-menu-title">ประวัติการตรวจนับสินค้า</span>
                      </a>  
                    </li>
                  </ul>
                </li>
                <li class="site-menu-item has-sub ">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">การตรวจ QC</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/basic.html">
                        <span class="site-menu-title">สร้างเอกสาร QC</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="tables/bootstrap.html">
                        <span class="site-menu-title">ประวัติการตรวจ QC</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
             <li class="site-menu-item has-sub active">
              <a href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon md-settings" aria-hidden="true"></i>
                <span class="site-menu-title">Master</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item has-sub active">
                  <a href="MS_ProductGroup.aspx">
                    <span class="site-menu-title">Product (ข้อมูลสินค้า)</span>
                  </a>
                </li>
                <li class="site-menu-item has-sub">
                  <a href="MS_Warehouse.aspx">
                    <span class="site-menu-title">Warehouse (คลังสินค้า)</span>
                  </a>
                </li>
                <li class="site-menu-item has-sub">
                  <a href="MS_Staff.aspx">
                    <span class="site-menu-title">Staff (ข้อมูลพนักงาน)</span>
                  </a>
                </li>
                <li class="site-menu-item has-sub">
                  <a href="MS_Supplier.aspx">
                    <span class="site-menu-title">Contact (ข้อมูลผู้ติดต่อ)</span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <uc1:css_srcipt runat="server" ID="css_srcipt" />
    <div class="page animsition">
    <div class="page-aside">
      <div class="page-aside-switch">
        <i class="icon md-chevron-left" aria-hidden="true"></i>
        <i class="icon md-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="page-aside-inner">
        <section class="page-aside-section">
          <h5 class="page-aside-title">Master (ข้อมูลพื้นฐาน)</h5>
          <div class="list-group">
<<<<<<< HEAD:NPO-WMS/NPO-WMS/MS_AddTag.aspx
            <a class="list-group-item " href="MS_ProductGroup.aspx"><i class="icon md-file-text" aria-hidden="true"></i>Product Group</a>
            <a class="list-group-item " href="MS_ProductType.aspx"><i class="icon md-file-text" aria-hidden="true"></i>Product Type</a>
            <a class="list-group-item" href="MS_Product.aspx"><i class="icon md-file-text" aria-hidden="true"></i>Product</a>
            <a class="list-group-item" href="MS_SKUGroup.aspx"><i class="icon md-file-text" aria-hidden="true"></i>SKU Group</a>
            <a class="list-group-item" href="MS_SKU.aspx"><i class="icon md-file-text" aria-hidden="true"></i>SKU</a>
            <a class="list-group-item active" href="MS_Tag.aspx"><i class="icon md-label" aria-hidden="true"></i>Tag</a>
            <a class="list-group-item" href="MS_UOM.aspx"><i class="icon md-file-text" aria-hidden="true"></i>UOM</a>
            

<<<<<<< HEAD
            <a class="list-group-item" href="frmMS_ProductGroup.aspx">  <i class="icon md-file-text" aria-hidden="true"></i>Product Group</a>
            <a class="list-group-item" href="frmMS_Product.aspx">       <i class="icon md-file-text" aria-hidden="true"></i>Product</a>
            <a class="list-group-item" href="frmMS_SKUGroup.aspx">      <i class="icon md-file-text" aria-hidden="true"></i>SKU Group</a>
            <a class="list-group-item" href="frmMS_SKU.aspx">           <i class="icon md-file-text" aria-hidden="true"></i>SKU</a>
            <a class="list-group-item" href="frmMS_Locations.aspx">     <i class="icon md-pin" aria-hidden="true"></i>Location</a>
            <a class="list-group-item" href="frmMS_Warehouse.aspx">     <i class="icon md-home" aria-hidden="true"></i>Warehouse</a>
            <a class="list-group-item" href="frmMS_Pallet.aspx">        <i class="icon md-file-text" aria-hidden="true"></i>Pallet</a>
            <a class="list-group-item" href="frmMS_Checker.aspx">       <i class="icon md-assignment-check" aria-hidden="true"></i>Checker</a>
            <a class="list-group-item active" href="frmMS_Tag.aspx">    <i class="icon md-label" aria-hidden="true"></i>Tag</a>
            <a class="list-group-item" href="frmMS_BOM.aspx">           <i class="icon md-file-text" aria-hidden="true"></i>BOM</a>
            <a class="list-group-item" href="frmMS_TypeDocument.aspx">  <i class="icon md-file-text" aria-hidden="true"></i>Type Document</a>

            <a class="list-group-item" href="frmMS_ProductGroup.aspx"><i class="icon md-file-text" aria-hidden="true"></i>Product Group</a>
            <a class="list-group-item " href="frmMS_ProductType.aspx"><i class="icon md-file-text" aria-hidden="true"></i>Product Type</a>
            <a class="list-group-item" href="frmMS_Product.aspx"><i class="icon md-file-text" aria-hidden="true"></i>Product</a>
            <a class="list-group-item" href="frmMS_SKUGroup.aspx"><i class="icon md-file-text" aria-hidden="true"></i>SKU Group</a>
            <a class="list-group-item" href="frmMS_SKU.aspx"><i class="icon md-file-text" aria-hidden="true"></i>SKU</a>
            <a class="list-group-item" href="frmMS_Locations.aspx"><i class="icon md-pin" aria-hidden="true"></i>Location</a>
            <a class="list-group-item" href="frmMS_Warehouse.aspx"><i class="icon md-home" aria-hidden="true"></i>Warehouse</a>
            <a class="list-group-item" href="frmMS_Pallet.aspx"><i class="icon md-file-text" aria-hidden="true"></i>Pallet</a>
            <a class="list-group-item" href="frmMS_Checker.aspx"><i class="icon md-assignment-check" aria-hidden="true"></i>Checker</a>
            <a class="list-group-item active" href="frmMS_Tag.aspx"><i class="icon md-label" aria-hidden="true"></i>Tag</a>
            <a class="list-group-item" href="frmMS_BOM.aspx"><i class="icon md-file-text" aria-hidden="true"></i>BOM</a>
            <a class="list-group-item" href="frmMS_TypeDocument.aspx"><i class="icon md-file-text" aria-hidden="true"></i>Type Document</a>
>>>>>>> cbe0f533894b6ba02987a83ad93f3c5b493999aa
>>>>>>> b75db024e8a533edd7cd174b147d05828f3bd3a9:NPO-WMS/NPO-WMS/frmMS_AddTag.aspx
          </div>
        </section>
   
      </div>
    </div>
    <div class="page-main">
      <div class="page-header">
        <h4 class="page-title">Tag.</h4>
      </div>
      <div class="page-content container-fluid">
      <div class="row">
        <div class="col-lg-12">
            <!-- Panel -->
            <div class="panel">
                <header class="panel-heading">
                    <h5 class="panel-title"></h5>
                </header>
                <div class="panel-body">

                    <!-- == -->
                    <!-- Create Date & status -->
                    <!-- == -->
                    <div class="clearfix"></div>
                    <div class="col-sm-12">                           
                        <!-- Create Date -->                        
                        <div class="form-group  col-sm-4 pull-right">
                            <!-- Date Picker -->
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="icon md-calendar" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control" data-plugin="datepicker" placeholder="Create Date">
                            </div>
                            <!-- End Date Picker-->
                        </div>
                    </div>

                    <!-- End Create Date & status  -->
                    
                    <!-- == -->
                    <!-- Description  -->
                    <!-- == -->

                    <div class="clearfix"></div>
                    <div class="col-sm-12">
                        <label class="col-sm-2 control-label">Description :</label>
                        <div class="form-group  col-sm-10">
                            <input class="form-control" placeholder="Description">
                        </div>
                    </div>

                    <!-- End Description  -->

                    <!-- == -->
                    <!-- Tag No. & Pallet No. -->
                    <!-- == -->

                    <div class="clearfix"></div>
                    <div class="col-sm-12">
                        <!-- เลขที่ Tag -->
                        <label class="col-sm-2 control-label">Tag No. :</label>
                        <div class="form-group  col-sm-4">
                            <input class="form-control" placeholder="Tag No.">
                        </div>

                        <!-- เลขที่ Pallet -->
                        <label class="col-sm-2 control-label">Pallet No. :</label>
                        <div class="form-group  col-sm-4">
                            <input class="form-control" placeholder="Pallet No.">
                        </div>
                    </div>
                    
                    <!-- End Tag No. & Pallet No. -->

                    <div class="col-sm-12" style="border-top: 6px solid #CCD0CF; padding-bottom: 2%"></div>

                    <!-- == -->
                    <!-- Total Unit & Volume-->
                    <!-- == -->

                    <div class="clearfix"></div>
                    <div class="col-sm-12">
                        <!-- Weight -->
                        <label class="col-sm-2 control-label">Total Unit :</label>
                        <div class="form-group  col-sm-4">
                            <input class="form-control" placeholder="Total Unit">
                        </div>

                        <!-- Gross Weight -->
                        <label class="col-sm-2 control-label">Volume :</label>
                        <div class="form-group  col-sm-4">
                            <input class="form-control" placeholder="Volumet">
                        </div>
                    </div>
                    
                    <!-- End Total Unit & Volume -->

                    <!-- == -->
                    <!-- Weight -->
                    <!-- == -->

                    <div class="clearfix"></div>
                    <div class="col-sm-12">
                        <!-- Weight -->
                        <label class="col-sm-2 control-label">Weight :</label>
                        <div class="form-group  col-sm-4">
                            <input class="form-control" placeholder="Weight">
                        </div>

                        <!-- Gross Weight -->
                        <label class="col-sm-2 control-label">Gross Weight :</label>
                        <div class="form-group  col-sm-4">
                            <input class="form-control" placeholder="Gross Weight">
                        </div>
                    </div>
                    
                    <!-- End Weight -->

                    <!-- == -->
                    <!-- Total Price -->
                    <!-- == -->

                    <div class="col-sm-12">                        
                        <label class="col-sm-2 control-label">Total Price :</label>
                        <div class="form-group  col-sm-4">
                            <input class="form-control" placeholder="Total Price ">
                        </div>
                    </div>                                                           
                    <!-- End Total Price -->

                    <div class="col-sm-12" style="border-top: 6px solid #CCD0CF; padding-bottom: 2%"></div>

                    <!-- == -->
                    <!-- Creater -->   
                    <!-- == -->      

                    <!-- รหัสผู้สร้างเอกสาร -->
                    <div class="clearfix"></div>
                    <div class="col-sm-12">
                        <!-- Create By -->
                        <label class="col-sm-2 control-label">Create By :</label>
                        <div class="form-group  col-sm-6">
                            <input class="form-control" placeholder="Create By">
                        </div>
                    </div>
                    <!-- == -->

                    <!-- รหัสแผนกผู้สร้างเอกสาร -->
                    <div class="clearfix"></div>
                    <div class="col-sm-12">
                        <!-- Create Department ID -->
                        <label class="col-sm-2 control-label">Department ID :</label>
                        <div class="form-group  col-sm-4">
                            <input class="form-control" placeholder="Creater Department ID">
                        </div>

                        <!-- == -->
                        <!-- Create Position ID -->
                        <label class="col-sm-2 control-label">Position ID :</label>
                        <div class="form-group  col-sm-4">
                            <input class="form-control" placeholder="Creater Position ID">
                        </div>
                    </div>

                     <!-- End Creater -->

                    <div class="col-sm-12" style="border-top: 6px solid #CCD0CF; padding-bottom: 2%"></div>
                    
                    <!-- == -->
                    <!-- Approve -->
                    <!-- == -->
                                           
                    <!-- Approve & Date -->
                    <div class="clearfix"></div>
                    <div class="col-sm-12">
                        <!-- รหัสผู้ Approve -->
                        <label class="col-sm-2 control-label">Approve By :</label>
                        <div class="form-group  col-sm-6">
                            <input class="form-control" placeholder="Approve By">
                        </div>

                        <!-- วันที่ Approve -->
                        <%--<label class="col-sm-2 control-label">Approve Date :</label>--%>
                        <div class="form-group  col-sm-4 pull-right">
                            <!-- Date Picker -->
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="icon md-calendar" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control" data-plugin="datepicker" placeholder="Approve Date">
                            </div>
                            <!-- End Date Picker-->
                        </div>
                    </div>                                                           
                    <!-- End Approve -->

                    <!-- == -->
                    <!-- Update -->
                    <!-- == -->
                                           
                    <!-- Update By & Update Time -->
                    <div class="clearfix"></div>
                    <div class="col-sm-12">
                        <!-- รหัสผู้ Update -->
                        <label class="col-sm-2 control-label">Update By :</label>
                        <div class="form-group  col-sm-6">
                            <input class="form-control" placeholder="Update By">
                        </div>

                        <!-- วันที่ Update -->
                        <%--<label class="col-sm-2 control-label">Update Date :</label>--%>
                        <div class="form-group  col-sm-4 pull-right">
                            <!-- Date Picker -->
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="icon md-calendar" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control" data-plugin="datepicker" placeholder="Update Time">
                            </div>
                            <!-- End Date Picker-->
                        </div>
                    </div>                                                           
                    <!-- End Update -->

                    <!-- == -->
                    <!-- Checker -->
                    <!-- == -->
                                           
                    <!-- Checker & Date -->
                    <div class="clearfix"></div>
                    <div class="col-sm-12">
                        <!-- รหัสผู้ Checker -->
                        <label class="col-sm-2 control-label">Checker ID :</label>
                        <div class="form-group  col-sm-4">
                            <input class="form-control" placeholder="Checker ID ">
                        </div>
                    </div>
                    <!-- End Checker -->

                    <!-- == -->
                    <!-- Checker Name -->
                    <!-- == -->
                    <div class="col-sm-12">                        
                        <label class="col-sm-2 control-label">Checker Name :</label>
                        <div class="form-group  col-sm-6">
                            <input class="form-control" placeholder="Checker Name">
                        </div>
                    </div>                                                           
                    <!-- End Checker Name -->

                    <!-- == -->
                    <!-- Active Status -->
                    <!-- == -->
                    <div class="col-sm-12">
                        <label class="col-sm-2 control-label">Active Status :</label>
                        <div class="form-group  col-sm-10">
                            <input type="checkbox" data-plugin="switchery" data-color="#2CBA44" checked data-toggle="tooltip" title="เปิด" />
                        </div>
                    </div>
                    <!-- End Active Status -->

                    <!-- == -->
                    <!-- Submit & Reset -->
                    <!-- == -->
                    <div class="form-group form-material">
                        <div class="col-sm-12 col-sm-offset-9">
                            <button type="button" class="btn btn-primary">Submit </button>
                            <button type="reset" class="btn btn-warning">Reset</button>
                        </div>
                    </div>
                    <!-- End Submit & Reset -->

                </div>    
                <!-- End Panel Body -->  
           </div>
           <!-- End Panel -->             
         </div> 
        </div>
      </div>
    </div>
</asp:Content>



